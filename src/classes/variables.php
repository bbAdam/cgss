<?php

// SETTING UP THIS WAY HELPS KEEP THINGS OUT OF THE GLOBAL SPACE WHILE HAVING GLOBAL ACCESS
class Variables {

    function __construct() {
        $this->variables = array(
            "siteName" => "Council of the Great City Schools",
            "siteDomain" => "https://adamcruse.schoolwires.net",
            "homepageUrl" => "/Page/1",
            "subpageUrl" => "/Page/17",
            "subpageNoNavUrl" => "/Page/309",
            "siteAlias" => "dev1",
            "siteAddress" => "1331 Pennsylvania Ave, N.W., Ste 1100N",
            "siteCity" => "Washington",
            "siteState" => "D.C.",
            "siteZip" => "20004",
            "sitePhone" => "202-393-2427",
            "siteFax" => "202-393-2400",
        );
    }

    public function Get($variable) {
        if(array_key_exists($variable, $this->variables)) {
            return $this->variables[$variable];
        } else {
            return null;
        }
    }

}

?>
