$(function() {
	CreativeTemplate.Init();
	
	$(window).on("load", function(){ CreativeTemplate.WindowLoad(); });
});

var CreativeTemplate = {
	// PROPERTIES
	"KeyCodes": { "tab": 9, "enter": 13, "esc": 27, "space": 32, "end": 35, "home": 36, "left": 37, "up": 38, "right": 39, "down": 40 },
	"IsMyViewPage": false, // UPDATES IN SetTemplateProps METHOD
	
	// METHODS
	"Init": function() {
		// FOR SCOPE
		var _this = this;
		
		this.SetTemplateProps();
		this.JsMediaQueries();
		this.Header();
		this.MyStart();
		this.Search();		
		this.ChannelBar();
		this.GlobalIcons();
		this.Slideshow();
		this.StreamingVideo();
		this.Body();
		this.UpcomingEvents();
		this.SocialMedia();
		this.Footer();
		this.RsMenu();
		this.AppAccordion();
		
		csGlobalJs.OpenInNewWindowWarning();

		$(window).on("resize", function(){ _this.WindowResize(); });
		$(window).on("scroll", function(){ _this.WindowScroll(); });
	},
	
	"SetTemplateProps": function() {
		// FOR SCOPE
		var _this = this;
		
		// MYVIEW PAGE CHECK
		if($("#pw-body").length) this.IsMyViewPage = true;
	},

	"WindowLoad": function() {
		// FOR SCOPE
		var _this = this;
		
		$("div.ui-widget.app.navigation li .bullet").each(function() {
			$(this).insertAfter($(this).siblings("a"));
		});
	},

	"WindowResize": function() {
		// FOR SCOPE
		var _this = this;
		
		this.JsMediaQueries();
	},
	
	"WindowScroll": function() {
		// FOR SCOPE
		var _this = this;
	},	
	
	"JsMediaQueries": function() {
		// FOR SCOPE
		var _this = this;
		
		switch(this.GetBreakPoint()) {
			case "Desktop": 
			   
			break;
			case "1024": 
			   
			break;
			case "768":
			case "640":
			case "480":
			case "320":
				
			break;
		}
	},

	"MyStart": function() {
		// FOR SCOPE
		var _this = this;
		
		// ADD USER OPTIONS
		var userOptionsItems = "";
		
		if($(".sw-signout").length) {
			if($(".sw-mystart-button.manage").length) {
				userOptionsItems += '<li><a href="#" onclick="' + $(".sw-mystart-button.manage a").attr("onclick") + 'return false;"><span>Site Manager</span></a></li>';
			}

			userOptionsItems += "<li>" + $("#sw-myaccount-list > li:first-child").html() + "</li>";
			
			if($(".sw-mystart-button.pw").length) {
				userOptionsItems += '<li><a href="' + window.location.protocol + '//' + window.location.host + '/myview"><span>MyView</span></a></li>';
			}
			
			userOptionsItems += "<li>" + $("#sw-myaccount-list > li:last-child").html() + "</li>";  
		} else {
			if($("#ui-btn-signin").length) {
				userOptionsItems += "<li>" + $("#ui-btn-signin").html() + "</li>";
			}
			
			if($("#ui-btn-register").length) {
				userOptionsItems += "<li>" + $("#ui-btn-register").html() + "</li>";
			}
		}

		$("#cs-user-options-list .cs-dropdown-list").append(userOptionsItems);
		
		$("#sw-mystart-mypasskey").prependTo($("#gb-mystart-right"));
		
		// REMOVE TAB INDEXES FROM SYSTEM <a> TAGS
		$("#cs-user-options-list a").removeAttr("tabindex");
	
		// ADD SCHOOLS
		$("#cs-schools-list .cs-dropdown-list").html($(".sw-mystart-dropdown.schoollist .sw-dropdown-list").html());
		
		// ADD TRANSLATE
		$(".cs-mystart-dropdown.translate .cs-dropdown").creativeTranslate({
			"type": 2,
			"advancedOptions": {
				"addMethod": "append"
			},
			"languages": [ // ["ENGLISH LANGUAGE NAME", "TRANSLATED LANGUAGE NAME", "LANGUAGE CODE"]
				["Afrikaans", "Afrikaans", "af"],
				["Albanian", "shqiptar", "sq"],
				["Amharic", "አማርኛ", "am"],
				["Arabic", "العربية", "ar"],
				["Armenian", "հայերեն", "hy"],
				["Azerbaijani", "Azərbaycan", "az"],
				["Basque", "Euskal", "eu"],
				["Belarusian", "Беларуская", "be"],
				["Bengali", "বাঙালি", "bn"],
				["Bosnian", "bosanski", "bs"],
				["Bulgarian", "български", "bg"],
				["Burmese", "မြန်မာ", "my"],
				["Catalan", "català", "ca"],
				["Cebuano", "Cebuano", "ceb"],
				["Chichewa", "Chichewa", "ny"],
				["Chinese Simplified", "简体中文", "zh-CN"],
				["Chinese Traditional", "中國傳統的", "zh-TW"],
				["Corsican", "Corsu", "co"],
				["Croatian", "hrvatski", "hr"],
				["Czech", "čeština", "cs"],
				["Danish", "dansk", "da"],
				["Dutch", "Nederlands", "nl"],
				["English", "English", "en"],
				["Esperanto", "esperanto", "eo"],
				["Estonian", "eesti", "et"],
				["Filipino", "Pilipino", "tl"],
				["Finnish", "suomalainen", "fi"],
				["French", "français", "fr"],
				["Galician", "galego", "gl"],
				["Georgian", "ქართული", "ka"],
				["German", "Deutsche", "de"],
				["Greek", "ελληνικά", "el"],
				["Gujarati", "ગુજરાતી", "gu"],
				["Haitian Creole", "kreyòl ayisyen", "ht"],
				["Hausa", "Hausa", "ha"],
				["Hawaiian", "ʻŌlelo Hawaiʻi", "haw"],
				["Hebrew", "עִברִית", "iw"],
				["Hindi", "हिंदी", "hi"],
				["Hmong", "Hmong", "hmn"],
				["Hungarian", "Magyar", "hu"],
				["Icelandic", "Íslenska", "is"],
				["Igbo", "Igbo", "ig"],
				["Indonesian", "bahasa Indonesia", "id"],
				["Irish", "Gaeilge", "ga"],
				["Italian", "italiano", "it"],
				["Japanese", "日本語", "ja"],
				["Javanese", "Jawa", "jw"],
				["Kannada", "ಕನ್ನಡ", "kn"],
				["Kazakh", "Қазақ", "kk"],
				["Khmer", "ភាសាខ្មែរ", "km"],
				["Korean", "한국어", "ko"],
				["Kurdish", "Kurdî", "ku"],
				["Kyrgyz", "Кыргызча", "ky"],
				["Lao", "ລາວ", "lo"],
				["Latin", "Latinae", "la"],
				["Latvian", "Latvijas", "lv"],
				["Lithuanian", "Lietuvos", "lt"],
				["Luxembourgish", "lëtzebuergesch", "lb"],
				["Macedonian", "Македонски", "mk"],
				["Malagasy", "Malagasy", "mg"],
				["Malay", "Malay", "ms"],
				["Malayalam", "മലയാളം", "ml"],
				["Maltese", "Malti", "mt"],
				["Maori", "Maori", "mi"],
				["Marathi", "मराठी", "mr"],
				["Mongolian", "Монгол", "mn"],
				["Myanmar", "မြန်မာ", "my"],
				["Nepali", "नेपाली", "ne"],
				["Norwegian", "norsk", "no"],
				["Nyanja", "madambwe", "ny"],
				["Pashto", "پښتو", "ps"],
				["Persian", "فارسی", "fa"],
				["Polish", "Polskie", "pl"],
				["Portuguese", "português", "pt"],
				["Punjabi", "ਪੰਜਾਬੀ ਦੇ", "pa"],
				["Romanian", "Română", "ro"],
				["Russian", "русский", "ru"],
				["Samoan", "Samoa", "sm"],
				["Scottish Gaelic", "Gàidhlig na h-Alba", "gd"],
				["Serbian", "Српски", "sr"],
				["Sesotho", "Sesotho", "st"],
				["Shona", "Shona", "sn"],
				["Sindhi", "سنڌي", "sd"],
				["Sinhala", "සිංහල", "si"],
				["Slovak", "slovenský", "sk"],
				["Slovenian", "slovenski", "sl"],
				["Somali", "Soomaali", "so"],
				["Spanish", "Español", "es"],
				["Sundanese", "Sunda", "su"],
				["Swahili", "Kiswahili", "sw"],
				["Swedish", "svenska", "sv"],
				["Tajik", "Тоҷикистон", "tg"],
				["Tamil", "தமிழ்", "ta"],
				["Telugu", "తెలుగు", "te"],
				["Thai", "ไทย", "th"],
				["Turkish", "Türk", "tr"],
				["Ukrainian", "український", "uk"],
				["Urdu", "اردو", "ur"],
				["Uzbek", "O'zbekiston", "uz"],
				["Vietnamese", "Tiếng Việt", "vi"],
				["Welsh", "Cymraeg", "cy"],
				["Western Frisian", "Western Frysk", "fy"],
				["Xhosa", "isiXhosa", "xh"],
				["Yiddish", "ייִדיש", "yi"],
				["Yoruba", "yorùbá", "yo"],
				["Zulu", "Zulu", "zu"]
			],
			"translateLoaded": function() {
				$(".cs-mystart-dropdown.translate .cs-dropdown #google_translate_element").wrap('<ul class="cs-dropdown-list cs-html-list-reset"></ul>');
			}
		});

		this.MyStartDropdownActions({
			"dropdownParent": ".cs-mystart-dropdown",
			"dropdownSelector": ".cs-selector",
			"dropdown": ".cs-dropdown",
			"dropdownList": ".cs-dropdown-list"
		});
	},
	
	"MyStartDropdownActions": function(params) {
		// FOR SCOPE
		var _this = this;

		var dropdownParent = params.dropdownParent;
		var dropdownSelector = params.dropdownSelector;
		var dropdown = params.dropdown;
		var dropdownList = params.dropdownList;
		
		$(dropdownParent + " " + dropdownList + " a").attr("tabindex", "-1");
		
		// MYSTART DROPDOWN SELECTOR CLICK EVENT
		$(document).on("click", dropdownSelector, function(e) {
			e.preventDefault();
			
			if($(this).parent().hasClass("open")){
				$("+ " + dropdownList + " a").attr("tabindex", "-1");
				$(this).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing"); 
			} else {
				$(this).attr("aria-expanded", "true").parent().addClass("open").find(dropdown).attr("aria-hidden","false").slideDown(300, "swing");
			}
		});
		
		// MYSTART DROPDOWN SELECTOR KEYDOWN EVENTS
		$(document).on("keydown", dropdownSelector, function(e) {
			// CAPTURE KEY CODE
			switch(e.keyCode) {
				// CONSUME LEFT AND UP ARROWS
				case _this.KeyCodes.enter:
				case _this.KeyCodes.space:
					e.preventDefault();

					// IF THE DROPDOWN IS OPEN, CLOSE IT
					if($(dropdownParent).hasClass("open")){
						$("+ " + dropdown + " " + dropdownList + " a").attr("tabindex", "-1");
						$(this).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing"); 
					} else {
						$(this).attr("aria-expanded", "true").parent().addClass("open").find(dropdown).attr("aria-hidden", "false").slideDown(300, "swing", function(){
							if($(dropdownList + " .goog-te-combo", this).length) {
								$(dropdownList + " .goog-te-combo", this).focus();
							} else {
								$(dropdownList + " li:first-child a", this).attr("tabindex", "0").focus();
							}
						});
					}
				break;

				// CONSUME TAB KEY
				case _this.KeyCodes.tab:
					e.preventDefault();
				
					$("+ " + dropdown + " " + dropdownList + " a").attr("tabindex", "-1");
					$(this).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
				
					if(e.shiftKey) {
						var index = $(":focusable").index(this);
						
						$(":focusable").eq(index-1).focus();
					} else {
						var index = $(":focusable").index(this);
						
						$(":focusable").eq(index+1).focus();
					}
				break;

				// CONSUME LEFT AND UP ARROWS
				case _this.KeyCodes.down:
				case _this.KeyCodes.right:
					e.preventDefault();
					
					$("+ " + dropdown + " " + dropdownList + " a").attr("tabindex", "-1");
					$("+ " + dropdown + " " + dropdownList + " li:first-child > a", this).attr("tabindex", "0").focus();
				break;
			}
		});
		
		// MYSTART DROPDOWN LINK KEYDOWN EVENTS
		$(document).on("keydown", dropdownList + " li a", function(e) {
			// CAPTURE KEY CODE
			switch(e.keyCode) {
				// CONSUME LEFT AND UP ARROWS
				case _this.KeyCodes.left:
				case _this.KeyCodes.up:
					e.preventDefault();

					// IS FIRST ITEM
					if($(this).parent().is(":first-child")) {
						// FOCUS DROPDOWN BUTTON
						$(this).closest(dropdownList).find("a").attr("tabindex", "-1");
						$(this).closest(dropdownParent).find(dropdownSelector).focus();
					} else {
						// FOCUS PREVIOUS ITEM
						$(this).closest(dropdownList).find("a").attr("tabindex", "-1");
						$(this).parent().prev("li").find("> a").attr("tabindex", "0").focus();
					}
				break;

				// CONSUME RIGHT AND DOWN ARROWS
				case _this.KeyCodes.right:
				case _this.KeyCodes.down:
					e.preventDefault();

					// IS LAST ITEM
					if($(this).parent().is(":last-child")) {
						// FOCUS FIRST ITEM
						$(this).closest(dropdownList).find("a").attr("tabindex", "-1");
						$(this).closest(dropdownList).find("li:first-child > a").attr("tabindex", "0").focus();
					} else {
						// FOCUS NEXT ITEM
						$(this).closest(dropdownList).find("a").attr("tabindex", "-1");
						$(this).parent().next("li").find("> a").attr("tabindex", "0").focus();
					}
				break;
				
				// CONSUME TAB KEY
				case _this.KeyCodes.tab:
					if(e.shiftKey) {
						e.preventDefault();
						
						// FOCUS DROPDOWN BUTTON
						$(this).closest(dropdownList).find("a").attr("tabindex", "-1");
						$(this).closest(dropdownParent).find(dropdownSelector).focus();
					}
				break;

				// CONSUME HOME KEY
				case _this.KeyCodes.home:
					e.preventDefault();

					// FOCUS FIRST ITEM
					$(this).closest(dropdownList).find("a").attr("tabindex", "-1");
					$(this).closest(dropdownList).find("li:first-child > a").attr("tabindex", "0").focus();
				break;

				// CONSUME END KEY
				case _this.KeyCodes.end:
					e.preventDefault();

					// FOCUS LAST ITEM
					$(this).closest(dropdownList).find("a").attr("tabindex", "-1");
					$(this).closest(dropdownList).find("li:last-child > a").attr("tabindex", "0").focus();
				break;
				
				// CONSUME ESC KEY
				case _this.KeyCodes.esc:
					e.preventDefault();
				
					// CLOSE MENU
					$(this).closest(dropdownList).find("a").attr("tabindex", "-1");
					$(this).parents(dropdownParent).find(dropdownSelector).focus().attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing"); 
				break;
			}
		});
		
		$(dropdownParent).mouseleave(function() {
			$(dropdownList + " a", this).attr("tabindex", "-1");
			$(dropdownSelector, this).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing"); 
		}).focusout(function() {
			var thisDropdown = this;
			
			setTimeout(function () {
				if(!$(thisDropdown).find(":focus").length) {
					$(dropdownSelector, thisDropdown).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
				}
			}, 500);
		});
	},

	"Header": function() {
		// FOR SCOPE
		var _this = this;
		
		// ADD LOGO
		this.LoadImage("#gb-logo", '<SWCtrl controlname="Custom" props="Name:logoSrc" />', '//drt.schoolwires.net//cms/lib9/SWCS000001/Centricity/template/83/defaults/phoenix.svg', <SWCtrl controlname="Custom" props="Name:showLogo" />, true, "[$SiteName$] logo");

		// ADD SITENAME
		var siteNameOne = $.trim("<SWCtrl controlname="Custom" props="Name:siteNameOne" />");
		var siteNameTwo = $.trim("<SWCtrl controlname="Custom" props="Name:siteNameTwo" />");
		if((siteNameOne == "") && (siteNameTwo == "")) {
			var splitLen = 2;
			var siteName = "[$SiteName$]";
			siteName = siteName.split(" ");
			var siteNameLength = siteName.length;
			if(siteNameLength > 2){
				siteNameEnd = $.trim(siteName.splice(-splitLen, siteName.length).toString().replace(/,/g, " "));
			} else {
				siteNameEnd = $.trim(siteName.splice(-1, siteName.length).toString().replace(/,/g, " "));
			}
			siteNameBegin = $.trim(siteName.toString().replace(/,/g, " "));
			$("#gb-sitename").prepend("<h1><span class='gb-sitename one'>" + siteNameBegin + "</span><span class='gb-sitename two'>" + siteNameEnd + "</span></h1>");
		} else if((siteNameOne != "") && (siteNameTwo == "")) {
			$("#gb-sitename").prepend("<h1><span class='gb-sitename one'>" + siteNameOne + "</span></h1>");
		} else if((siteNameOne == "") && (siteNameTwo != "")) {
			$("#gb-sitename").prepend("<h1><span class='gb-sitename two'>" + siteNameTwo + "</span></h1>");
		} else if((siteNameOne != "") && (siteNameTwo != "")) {
			$("#gb-sitename").prepend("<h1><span class='gb-sitename one'>" + siteNameOne + "</span><span class='gb-sitename two'>" + siteNameTwo + "</span></h1>");
		}
	},

	"ChannelBar": function() {
		// FOR SCOPE
		var _this = this;
		
		$(".hidden-sections").remove();
	
		$(".sw-channel-item").unbind('hover');
		$(".sw-channel-item").hover(function(){
			$(".sw-channel-item ul").stop(true, true);
			var subList = $(this).children('ul');
			if ($.trim(subList.html()) !== "") {
				subList.slideDown(300, "swing");
			}
			$(this).addClass("hover");
		}, function(){
			$(".sw-channel-dropdown").slideUp(300, "swing");
			$(this).removeClass("hover");
		});
		
		$(".sw-channel-item").each(function(){
			if($(".sw-channel-dropdown li", this).length == 0) {
				$(this).addClass("no-dropdown");
			}
		});
		
		$(".sw-channel-item").on('mouseenter mouseleave', function (e) {
			if ($('.sw-channel-dropdown', this).length) {
				var elm = $('.sw-channel-dropdown', this);
				var off = elm.offset();
				var l = off.left;
				var w = elm.width();
				var docH = $("header").height();
				var docW = $("header").width();

				var isEntirelyVisible = (l + w <= docW);

				if (!isEntirelyVisible) {
					$(this).addClass('edge');
				} else {
					$(this).removeClass('edge');
				}
			}
		});
	},

	"GlobalIcons": function() {
		// FOR SCOPE
		var _this = this;

		$("#gb-global-icons").creativeIcons({
			"iconNum"       : "<SWCtrl controlname="Custom" props="Name:numOfIcons" />",
			"defaultIconSrc": "",
			"icons"         : [
				{
					"image": "<SWCtrl controlname="Custom" props="Name:Icon1Image" />",
					"showText": <SWCtrl controlname="Custom" props="Name:Icon1ShowText" />,
					"text": "<SWCtrl controlname="Custom" props="Name:Icon1Text" />",
					"url": "<SWCtrl controlname="Custom" props="Name:Icon1Link" />",
					"target": "<SWCtrl controlname="Custom" props="Name:Icon1Target" />"
				},
				{
					"image": "<SWCtrl controlname="Custom" props="Name:Icon2Image" />",
					"showText": <SWCtrl controlname="Custom" props="Name:Icon2ShowText" />,
					"text": "<SWCtrl controlname="Custom" props="Name:Icon2Text" />",
					"url": "<SWCtrl controlname="Custom" props="Name:Icon2Link" />",
					"target": "<SWCtrl controlname="Custom" props="Name:Icon2Target" />"
				},
				{
					"image": "<SWCtrl controlname="Custom" props="Name:Icon3Image" />",
					"showText": <SWCtrl controlname="Custom" props="Name:Icon3ShowText" />,
					"text": "<SWCtrl controlname="Custom" props="Name:Icon3Text" />",
					"url": "<SWCtrl controlname="Custom" props="Name:Icon3Link" />",
					"target": "<SWCtrl controlname="Custom" props="Name:Icon3Target" />"
				},
				{
					"image": "<SWCtrl controlname="Custom" props="Name:Icon4Image" />",
					"showText": <SWCtrl controlname="Custom" props="Name:Icon4ShowText" />,
					"text": "<SWCtrl controlname="Custom" props="Name:Icon4Text" />",
					"url": "<SWCtrl controlname="Custom" props="Name:Icon4Link" />",
					"target": "<SWCtrl controlname="Custom" props="Name:Icon4Target" />"
				},
				{
					"image": "<SWCtrl controlname="Custom" props="Name:Icon5Image" />",
					"showText": <SWCtrl controlname="Custom" props="Name:Icon5ShowText" />,
					"text": "<SWCtrl controlname="Custom" props="Name:Icon5Text" />",
					"url": "<SWCtrl controlname="Custom" props="Name:Icon5Link" />",
					"target": "<SWCtrl controlname="Custom" props="Name:Icon5Target" />"
				},
				{
					"image": "<SWCtrl controlname="Custom" props="Name:Icon6Image" />",
					"showText": <SWCtrl controlname="Custom" props="Name:Icon6ShowText" />,
					"text": "<SWCtrl controlname="Custom" props="Name:Icon6Text" />",
					"url": "<SWCtrl controlname="Custom" props="Name:Icon6Link" />",
					"target": "<SWCtrl controlname="Custom" props="Name:Icon6Target" />"
				},
				{
					"image": "<SWCtrl controlname="Custom" props="Name:Icon7Image" />",
					"showText": <SWCtrl controlname="Custom" props="Name:Icon7ShowText" />,
					"text": "<SWCtrl controlname="Custom" props="Name:Icon7Text" />",
					"url": "<SWCtrl controlname="Custom" props="Name:Icon7Link" />",
					"target": "<SWCtrl controlname="Custom" props="Name:Icon7Target" />"
				},
				{
					"image": "<SWCtrl controlname="Custom" props="Name:Icon8Image" />",
					"showText": <SWCtrl controlname="Custom" props="Name:Icon8ShowText" />,
					"text": "<SWCtrl controlname="Custom" props="Name:Icon8Text" />",
					"url": "<SWCtrl controlname="Custom" props="Name:Icon8Link" />",
					"target": "<SWCtrl controlname="Custom" props="Name:Icon8Target" />"
				},
				{
					"image": "<SWCtrl controlname="Custom" props="Name:Icon9Image" />",
					"showText": <SWCtrl controlname="Custom" props="Name:Icon9ShowText" />,
					"text": "<SWCtrl controlname="Custom" props="Name:Icon9Text" />",
					"url": "<SWCtrl controlname="Custom" props="Name:Icon9Link" />",
					"target": "<SWCtrl controlname="Custom" props="Name:Icon9Target" />"
				},
				{
					"image": "<SWCtrl controlname="Custom" props="Name:Icon10Image" />",
					"showText": <SWCtrl controlname="Custom" props="Name:Icon10ShowText" />,
					"text": "<SWCtrl controlname="Custom" props="Name:Icon10Text" />",
					"url": "<SWCtrl controlname="Custom" props="Name:Icon10Link" />",
					"target": "<SWCtrl controlname="Custom" props="Name:Icon10Target" />"
				}
			],
			"siteID"        : "[$SiteID$]",
			"siteAlias"     : "[$SiteAlias$]",
			"calendarLink"  : "[$SiteCalendarLink$]",
			"contactEmail"  : "[$SiteContactEmail$]",
			"allLoaded"     : function(){ }
		});		
	},
	
	"Body": function() {
		// FOR SCOPE
		var _this = this; 
		
		//VARIABLES
		var url = window.location.href;
		
		if(document.documentMode || /Edge/.test(navigator.userAgent)) {
			$("html").addClass("edge");
		}
		
		if($(".sw-special-mode-bar").length || $('#templateconfiguration-iframe-preview', window.parent.document).length) {
			$("#gb-page").addClass("preview-changes");
		}
		
		if(url.indexOf("stay=1") != -1) {
			$("#gb-page").addClass("preview-changes");
		}
			
		// AUTO FOCUS SIGN IN FIELD
		$("#swsignin-txt-username").focus();

		// CENTER SIGNED OUT MESSAGE AND SIGNIN BUTTON
		if($("div.ui-spn > div > p > span").text() == "You have been signed out."){
			$("div.ui-spn > div").css({"text-align" : "center", "padding" : "50px 0px 50px 0px"});
			//DOC add signed out breadcrumb
			$(".ui-breadcrumbs").append("<li class='ui-breadcrumb-last'>Signed Out</li>");
		}

		// ADJUST SIGN IN PAGE
		if($("#swlogin").length){
			$("#sw-content-layout-wrapper .ui-widget.app").addClass("signin");
			$(".ui-breadcrumbs").append("<li class='ui-breadcrumb-last'>Sign In</li>");
		}

		// ADJUST REGISTER PAGE
		if($("#swageprompt-txt-birthmonth").length && $("#swageprompt-txt-birthday").length && $("#swageprompt-txt-birthyear").length) {
			$("#sw-content-layout-wrapper .ui-widget.app").addClass("register");
			$(".ui-breadcrumbs").append("<li class='ui-breadcrumb-last'>Register</li>");
		}

		// ADJUST SEARCH RESULTS PAGE
		if($("#sw-content-layout-wrapper.ui-spn #swsearch-pnl-main").length) {
			$("#sw-content-layout-wrapper").children().wrapAll('<div class="ui-widget app search-results"><div class="ui-widget-detail"></div></div>');
			$(".ui-breadcrumbs").append("<li class='ui-breadcrumb-last'>Search Results</li>");
		}

		// ADJUST MORE/VIEW ALL
		$(".more-link").each(function(){
			$(this).parent("li").addClass("cs-more-link-parent");
		});

		// APPLY RESPONSIVE DIMENSIONS TO CONTENT IMAGES
		$("div.ui-widget.app .ui-widget-detail img")
			.not($("div.ui-widget.app.cs-rs-multimedia-rotator .ui-widget-detail img"))
			.not($("div.ui-widget.app.gallery.json .ui-widget-detail img"))
			.not($("div.ui-widget.app.headlines .ui-widget-detail img"))
			.each(function() {
				if ($(this).attr('width') !== undefined && $(this).attr('height') !== undefined) { // IMAGE HAS INLINE DIMENSIONS
					$(this).css({"display": "inline-block", "width": "100%", "max-width": $(this).attr("width") + "px", "height": "auto", "max-height": $(this).attr("height") + "px"});
				}
		});

		// ADJUST FIRST BREADCRUMB
		$("li.ui-breadcrumb-first > a > span").text("Home");

		// USE CHANNEL NAME FOR PAGELIST NAV HEADER IF ONE IS NOT PRESENT
		if(!$("div.sp-column.one .ui-widget-header h1").length) {
			$("div.sp-column.one .ui-widget-header").append("<h1>[$ChannelName$]</h1>");
		}
		
		$(".sp-column.two .ui-widget.app, #spn-content .ui-widget.app").first().addClass("first-app");
		
		/*$(document).on("click keypress", ".gb-icon-menu-btn", function(e) { 
			if(_this.AllyClick(e)) {

			}
		});*/
	},
	
	"UpcomingEvents": function() {
		// FOR SCOPE
		var _this = this;   

		if (!$(".upcomingevents .joel-month").length) {
			$(".upcomingevents").modEvents({
				columns     : "yes",
				monthLong   : "no",
				dayWeek     : "no"
			});
			eventsByDay(".upcomingevents .ui-articles");
		}

		function eventsByDay(container) {
			var countLi = 0;
			$(".ui-article", container).each(function(){
				if (!$(this).find("h1.ui-article-title.sw-calendar-block-date").size()){
					var moveArticle = $(this).html();
					$(this).parent().prev().children().children().next().append(moveArticle);
					$(this).parent().remove();
				};

			});
			$(".sw-calendar-block-time").each(function() {
				$(this).html($(this).text().replace(/ AM/g, "am"));
				$(this).html($(this).text().replace(/ PM/g, "pm"));
			});
		}
	},

	"SocialMedia": function() {
		// FOR SCOPE
		var _this = this;
		
		var socialIcons = [
			{
				"show": <SWCtrl controlname="Custom" props="Name:showFacebook" />,
				"label": "Facebook",
				"class": "facebook",
				"url": "<SWCtrl controlname="Custom" props="Name:FacebookUrl" />",
				"target": "<SWCtrl controlname="Custom" props="Name:FacebookTarget" />"
			},
			{
				"show": <SWCtrl controlname="Custom" props="Name:showTwitter" />,
				"label": "Twitter",
				"class": "twitter",
				"url": "<SWCtrl controlname="Custom" props="Name:TwitterUrl" />",
				"target": "<SWCtrl controlname="Custom" props="Name:TwitterTarget" />"
			},
			{
				"show": <SWCtrl controlname="Custom" props="Name:showYouTube" />,
				"label": "YouTube",
				"class": "youtube",
				"url": "<SWCtrl controlname="Custom" props="Name:YouTubeUrl" />",
				"target": "<SWCtrl controlname="Custom" props="Name:YouTubeTarget" />"
			},
			{
				"show": <SWCtrl controlname="Custom" props="Name:showInstagram" />,
				"label": "Instagram",
				"class": "instagram",
				"url": "<SWCtrl controlname="Custom" props="Name:InstagramUrl" />",
				"target": "<SWCtrl controlname="Custom" props="Name:InstagramTarget" />"
			},
			{
				"show": <SWCtrl controlname="Custom" props="Name:showLinkedIn" />,
				"label": "LinkedIn",
				"class": "linkedin",
				"url": "<SWCtrl controlname="Custom" props="Name:LinkedInUrl" />",
				"target": "<SWCtrl controlname="Custom" props="Name:LinkedInTarget" />"
			},
			{
				"show": <SWCtrl controlname="Custom" props="Name:showPinterest" />,
				"label": "Pinterest",
				"class": "pinterest",
				"url": "<SWCtrl controlname="Custom" props="Name:PinterestUrl" />",
				"target": "<SWCtrl controlname="Custom" props="Name:PinterestTarget" />"
			},
			{
				"show": <SWCtrl controlname="Custom" props="Name:showFlickr" />,
				"label": "Flickr",
				"class": "flickr",
				"url": "<SWCtrl controlname="Custom" props="Name:FlickrUrl" />",
				"target": "<SWCtrl controlname="Custom" props="Name:FlickrTarget" />"
			},
			{
				"show": <SWCtrl controlname="Custom" props="Name:showVimeo" />,
				"label": "Vimeo",
				"class": "vimeo",
				"url": "<SWCtrl controlname="Custom" props="Name:VimeoUrl" />",
				"target": "<SWCtrl controlname="Custom" props="Name:VimeoTarget" />"
			},
			{
				"show": <SWCtrl controlname="Custom" props="Name:showPeachjar" />,
				"label": "Peachjar",
				"class": "peachjar",
				"url": "<SWCtrl controlname="Custom" props="Name:PeachjarUrl" />",
				"target": "<SWCtrl controlname="Custom" props="Name:PeachjarTarget" />"
			}
		];

		var icons = '';
		$.each(socialIcons, function(index, icon) {
			if(icon.show) {
				icons += '<li><a class="gb-social-media-icon cs-dev-icon cs-dev-icon-' + icon.class + '" href="' + icon.url + '" target="' + icon.target + '" aria-label="' + icon.label + '"><span class="path1"></span><span class="path2"></span></span></a></li>';
			}
		});

		if(icons.length) {
			$("#gb-social-media-icons").html('<ul>' + icons + '</ul>');
		}
	},

	"Footer": function() {
		// FOR SCOPE
		var _this = this;
		
		// MOVE Bb FOOTER STUFF
		$(".gb-legal-logo").html($("#sw-footer-logo").html());
		var legalLinks = '';
		legalLinks += '<li>' + $.trim($("#sw-footer-links li:eq(0)").html().replace("|", "")) + '</li>';
		legalLinks += '<li>' + $.trim($("#sw-footer-links li:eq(2)").html().replace("|", "")) + '</li>';
		legalLinks += '<li>' + $.trim($("#sw-footer-links li:eq(1)").html().replace("|", "")) + '</li>';
		$(".gb-legal-footer.links").append('<ul>' + legalLinks + '</ul>');
		$(".gb-legal-footer.copyright").append($("#sw-footer-copyright").html());
	},

	"Slideshow": function() {
		// FOR SCOPE
		var _this = this;
		
		if($("#sw-content-container10 .ui-widget.app.multimedia-gallery").length) {
			var mmg = eval("multimediaGallery" + $("#sw-content-container10 .ui-widget.app.multimedia-gallery:first").attr("data-pmi"));
			mmg.props.defaultGallery = false;

			$("#sw-content-container10 .ui-widget.app.multimedia-gallery:first").csMultimediaGallery({
				"efficientLoad" : true,
				"imageWidth" : 1500,
				"imageHeight" : 910,
				"mobileDescriptionContainer": [960, 768, 640, 480, 320], // [960, 768, 640, 480, 320]
				"galleryOverlay" : false,
				"linkedElement" : [], // ["image", "title", "overlay"]
				"playPauseControl" : true,
				"backNextControls" : true,
				"bullets" : false,
				"thumbnails" : false,
				"thumbnailViewerNum": [4, 4, 3, 3, 2], // NUMERICAL - [960 view, 768 view, 640 view, 480 view, 320 view]
				"autoRotate" : true,
				"hoverPause" : true,
				"transitionType" : "fade", // fade, slide, custom
				"transitionSpeed" : <SWCtrl controlname="Custom" props="Name:transitionSpeed" />,
				"transitionDelay" : <SWCtrl controlname="Custom" props="Name:transitionDelay" />,
				"fullScreenRotator" : true,
				"fullScreenBreakpoints" : [960], // NUMERICAL - [960, 768, 640, 480, 320]
				"allLoaded" : function(props) {} // props.element, props.mmgRecords
			});
		}
	},
	
	"StreamingVideo": function() {
		// FOR SCOPE
		var _this = this;
		
		$("#hp-streaming-background-video-outer").csStreamingBackgroundVideo({
			"videoSource" : "YouTube", // OPTIONS ARE: YouTube, Vimeo, MyVRSpot
			"videoID": "QkefIAVRQpI", // DEFAULT IS BLANK TO AUTOFILL THE PROPER ID FOR THE VENDOR
			"myVRSpotVideoWidth": "853.5",
            "myVRSpotVideoHeight": "480",
			"fullScreenBreakpoints" : [1024], // OPTIONS ARE: ["Desktop", 1024, 768, 640, 480, 320]
			"fullScreenCSSPosition": "fixed", // OPITONS ARE: static, relative, fixed, absolute, sticky
			"showAudioButtons" : true,
			"showWatchVideoButton": true,
			"useBackgroundPhoto" : true,
            "backgroundPhoto" : "//placehold.it/768x432?text=Photo+Placeholder",
            "backgroundPhotoBreakpoints" : [480, 320], // OPTIONS ARE: ["Desktop", 1024, 768, 640, 480, 320]
			"backgroundPhotoCSSPosition": "static", // OPITONS ARE: static, relative, fixed, absolute, sticky
            "useCustomResize": false,
		});
	
		/*if(<SWCtrl controlname="Custom" props="Name:streamingVideoSwitch" />) {
			//Variables
			var mobilePhoto = '<SWCtrl controlname="Custom" props="Name:streamingVideoMobileImage" />';
			$("html").addClass("cs-streaming-video");
			
			if($("#sw-content-container10 .ui-widget.app.multimedia-gallery").length) {
				$("#sw-content-container10 > div:not(#cs-fullscreen-video-outer), #sw-content-container10 > .mmg-description-outer").hide().attr("aria-hidden", true);
			}
			
			if($.trim(mobilePhoto) == "" || mobilePhoto.indexOf("default-man.jpg") > -1) {
				mobilePhoto = "/cms/lib/SW00000019/Centricity/Template/2313/subsite-mobile-photo.jpg";
			}
			
			var videoVendor = '<SWCtrl controlname="Custom" props="Name:streamingVideoVendor" />';
			videoVendor = videoVendor.toLowerCase();
			
			$("#sw-content-container10.region.ui-hp").fullScreenRotator({
				"videoSource" : videoVendor, // OPTIONS ARE: youtube, vimeo
				"videoID": '<SWCtrl controlname="Custom" props="Name:streamingVideoID" />', // YouTube and Vimeo ids are set as default in the script
				"fullScreenBreakpoints" : [960], // OPTIONS ARE [960, 768, 640, 480, 320]
				"showControls" : true,
				"mobileBackgroundPhoto" : mobilePhoto,
				"mobileBackgroundPhotoBreakpoint" : 768, // OPTIONS ARE 768, 640, 480, 320
				"onReady" : function(props) {                        
					// ADD POSITIONING STYLES
					var dynStyleSheet = document.createElement('style');
					if(dynStyleSheet) {
						dynStyleSheet.setAttribute('type', 'text/css');
						var head = document.getElementsByTagName('head')[0];

						if(head) {
							head.insertBefore(dynStyleSheet, head.childNodes[0]);
						}

						var dynStyles = 'iframe#cs-fullscreen-video {' +
											'top: calc(((100vw * .4) - (100vw * ' + (props.height / props.width) + ')) / 2);' +	
										'}';

						var rules = document.createTextNode(dynStyles);

						if(dynStyleSheet.styleSheet){ // IE
							dynStyleSheet.styleSheet.cssText = dynStyles;
						} else {
							dynStyleSheet.appendChild(rules);
						}
					}
					
					var sbvStructure = "";
					
					sbvStructure += '<div class="mmg-description">';

					
					if('<SWCtrl controlname="Custom" props="Name:streamingVideoTitle" />' != "") {
						sbvStructure += '<h2 class="mmg-description-title"><span><SWCtrl controlname="Custom" props="Name:streamingVideoTitle" /></span></h2>';
					}
					
					if('<SWCtrl controlname="Custom" props="Name:streamingVideoLinkText" />' != "" && '<SWCtrl controlname="Custom" props="Name:streamingVideoLink" />' != "") {
						sbvStructure += '<div class="mmg-description-links"><a class="mmg-description-link" href="<SWCtrl controlname="Custom" props="Name:streamingVideoLink" />"><span><SWCtrl controlname="Custom" props="Name:streamingVideoLinkText" /></span></a></div>';
					}
					
					sbvStructure += '</div>';
											
					$("#cs-fullscreen-video-outer").append(sbvStructure);
					
					$("#cs-fullscreen-video-outer .cs-fullscreen-video-buttons").addClass("hide768").appendTo($("#cs-fullscreen-video-outer .mmg-description-inner"));
				   
					// ADD ARIA LABELS TO BUTTONS
					$("#hp-slideshow .cs-fullscreen-video-button.mute-button").attr("aria-label", "Mute Background Video");
					$("#hp-slideshow .cs-fullscreen-video-button.unmute-button").attr("aria-label", "Unmute Background Video");
					$("#hp-slideshow .cs-fullscreen-video-button.play-button").attr("aria-label", "Play Background Video");
					$("#hp-slideshow .cs-fullscreen-video-button.pause-button").attr("aria-label", "Pause Background Video");
					
					if($(".w-1024").length && $(".cs-fullscreen-video-buttons.video-playing").length) {
						$(".cs-fullscreen-video-button.pause-button").click();
						
						setTimeout(function(){ $(".cs-fullscreen-video-button.play-button").click(); }, 100);
					}
					
					// BUTTON ACTIONS
					$("#hp-slideshow").on("click keydown", ".cs-fullscreen-video-button.play-button", function(e) {
						if(_this.AllyClick(e)) {
							setTimeout(function() {
								$("#hp-slideshow .cs-fullscreen-video-button.pause-button").focus();
							}, 100);
						}
					});
					$("#hp-slideshow").on("click keydown", ".cs-fullscreen-video-button.pause-button", function(e) {
						if(_this.AllyClick(e)) {
							setTimeout(function() {
								$("#hp-slideshow .cs-fullscreen-video-button.play-button").focus();
							}, 100);
						}
					});
					$("#hp-slideshow").on("click keydown", ".cs-fullscreen-video-button.mute-button", function(e) {
						if(_this.AllyClick(e)) {
							setTimeout(function() {
								$("#hp-slideshow .cs-fullscreen-video-button.unmute-button").focus();
							}, 100);
						}
					});
					$("#hp-slideshow").on("click keydown", ".cs-fullscreen-video-button.unmute-button", function(e) {
						if(_this.AllyClick(e)) {
							setTimeout(function() {
								$("#hp-slideshow .cs-fullscreen-video-button.mute-button").focus();
							}, 100);
						}
					});
				},
				"onStateChange" : function(props) { }
			});
			
			// MOVE BUTTONS
			$("#hp-slideshow #cs-fullscreen-video-outer").append($("#hp-slideshow .cs-fullscreen-video-buttons"));
		}*/
	},        

	"RsMenu": function() {
		// FOR SCOPE
		var _this = this;
		
					
		// ADDITIONAL ITEMS OBJECT
		var additionalItems = {};

	    /*var linkBank = [
			{
				"text": "",
				"url": ""
			}
		];
		
		var links = [];
		$.each(linkBank, function(index, link) {
			if(link.text != "") {
				links.push({ "text": link.text, "url": link.url });
			}
		});

		if(links.length) {
			additionalItems["Extra Links"] = links;
		}*/
		
		$.csRsMenu({
			"breakPoint" : 768, // SYSTEM BREAK POINTS - 768, 640, 480, 320
			"slideDirection" : "left-to-right", // OPTIONS - left-to-right, right-to-left
			"menuButtonParent" : "#gb-mystart",
			"menuBtnText" : "Menu",
			"colors": {
				"pageOverlay": "#000000", // DEFAULT #000000
				"menuBackground": "#FFFFFF", // DEFAULT #FFFFFF
				"menuText": "#333333", // DEFAULT #333333
				"menuTextAccent": "#333333", // DEFAULT #333333
				"dividerLines": "#E6E6E6", // DEFAULT #E6E6E6
				"buttonBackground": "#E6E6E6", // DEFAULT #E6E6E6
				"buttonText": "#333333" // DEFAULT #333333
			},
			"showSchools" : true,
			"schoolMenuText": "Schools",
			"showTranslate" : true,
			"translateMenuText": "Translate",
			"translateVersion": 2, // 1 = FRAMESET, 2 = BRANDED
			"translateId" : "",
			"translateLanguages": [ // ["ENGLISH LANGUAGE NAME", "TRANSLATED LANGUAGE NAME", "LANGUAGE CODE"]
				["Afrikaans", "Afrikaans", "af"],
				["Albanian", "shqiptar", "sq"],
				["Amharic", "አማርኛ", "am"],
				["Arabic", "العربية", "ar"],
				["Armenian", "հայերեն", "hy"],
				["Azerbaijani", "Azərbaycan", "az"],
				["Basque", "Euskal", "eu"],
				["Belarusian", "Беларуская", "be"],
				["Bengali", "বাঙালি", "bn"],
				["Bosnian", "bosanski", "bs"],
				["Bulgarian", "български", "bg"],
				["Burmese", "မြန်မာ", "my"],
				["Catalan", "català", "ca"],
				["Cebuano", "Cebuano", "ceb"],
				["Chichewa", "Chichewa", "ny"],
				["Chinese Simplified", "简体中文", "zh-CN"],
				["Chinese Traditional", "中國傳統的", "zh-TW"],
				["Corsican", "Corsu", "co"],
				["Croatian", "hrvatski", "hr"],
				["Czech", "čeština", "cs"],
				["Danish", "dansk", "da"],
				["Dutch", "Nederlands", "nl"],
				["Esperanto", "esperanto", "eo"],
				["Estonian", "eesti", "et"],
				["Filipino", "Pilipino", "tl"],
				["Finnish", "suomalainen", "fi"],
				["French", "français", "fr"],
				["Galician", "galego", "gl"],
				["Georgian", "ქართული", "ka"],
				["German", "Deutsche", "de"],
				["Greek", "ελληνικά", "el"],
				["Gujarati", "ગુજરાતી", "gu"],
				["Haitian Creole", "kreyòl ayisyen", "ht"],
				["Hausa", "Hausa", "ha"],
				["Hawaiian", "ʻŌlelo Hawaiʻi", "haw"],
				["Hebrew", "עִברִית", "iw"],
				["Hindi", "हिंदी", "hi"],
				["Hmong", "Hmong", "hmn"],
				["Hungarian", "Magyar", "hu"],
				["Icelandic", "Íslenska", "is"],
				["Igbo", "Igbo", "ig"],
				["Indonesian", "bahasa Indonesia", "id"],
				["Irish", "Gaeilge", "ga"],
				["Italian", "italiano", "it"],
				["Japanese", "日本語", "ja"],
				["Javanese", "Jawa", "jw"],
				["Kannada", "ಕನ್ನಡ", "kn"],
				["Kazakh", "Қазақ", "kk"],
				["Khmer", "ភាសាខ្មែរ", "km"],
				["Korean", "한국어", "ko"],
				["Kurdish", "Kurdî", "ku"],
				["Kyrgyz", "Кыргызча", "ky"],
				["Lao", "ລາວ", "lo"],
				["Latin", "Latinae", "la"],
				["Latvian", "Latvijas", "lv"],
				["Lithuanian", "Lietuvos", "lt"],
				["Luxembourgish", "lëtzebuergesch", "lb"],
				["Macedonian", "Македонски", "mk"],
				["Malagasy", "Malagasy", "mg"],
				["Malay", "Malay", "ms"],
				["Malayalam", "മലയാളം", "ml"],
				["Maltese", "Malti", "mt"],
				["Maori", "Maori", "mi"],
				["Marathi", "मराठी", "mr"],
				["Mongolian", "Монгол", "mn"],
				["Myanmar", "မြန်မာ", "my"],
				["Nepali", "नेपाली", "ne"],
				["Norwegian", "norsk", "no"],
				["Nyanja", "madambwe", "ny"],
				["Pashto", "پښتو", "ps"],
				["Persian", "فارسی", "fa"],
				["Polish", "Polskie", "pl"],
				["Portuguese", "português", "pt"],
				["Punjabi", "ਪੰਜਾਬੀ ਦੇ", "pa"],
				["Romanian", "Română", "ro"],
				["Russian", "русский", "ru"],
				["Samoan", "Samoa", "sm"],
				["Scottish Gaelic", "Gàidhlig na h-Alba", "gd"],
				["Serbian", "Српски", "sr"],
				["Sesotho", "Sesotho", "st"],
				["Shona", "Shona", "sn"],
				["Sindhi", "سنڌي", "sd"],
				["Sinhala", "සිංහල", "si"],
				["Slovak", "slovenský", "sk"],
				["Slovenian", "slovenski", "sl"],
				["Somali", "Soomaali", "so"],
				["Spanish", "Español", "es"],
				["Sundanese", "Sunda", "su"],
				["Swahili", "Kiswahili", "sw"],
				["Swedish", "svenska", "sv"],
				["Tajik", "Тоҷикистон", "tg"],
				["Tamil", "தமிழ்", "ta"],
				["Telugu", "తెలుగు", "te"],
				["Thai", "ไทย", "th"],
				["Turkish", "Türk", "tr"],
				["Ukrainian", "український", "uk"],
				["Urdu", "اردو", "ur"],
				["Uzbek", "O'zbekiston", "uz"],
				["Vietnamese", "Tiếng Việt", "vi"],
				["Welsh", "Cymraeg", "cy"],
				["Western Frisian", "Western Frysk", "fy"],
				["Xhosa", "isiXhosa", "xh"],
				["Yiddish", "ייִדיש", "yi"],
				["Yoruba", "yorùbá", "yo"],
				["Zulu", "Zulu", "zu"]
			],
			"showAccount": true,
			"accountMenuText": "User Options",
			"usePageListNavigation": true,
			"extraMenuOptions": {}, 
			"siteID": "[$siteID$]",
			"allLoaded": function() {}
		});
	},

	"AppAccordion": function() {
		// FOR SCOPE
		var _this = this;
		
		$("#hp-content").csAppAccordion({
			"accordionBreakpoints" : [640, 480, 320]
		});

		$(".sp-column.one").csAppAccordion({
			"accordionBreakpoints" : [480, 320]
		});
	},

	"Search": function() {
		// FOR SCOPE
		var _this = this;
		
		//Variables
		var placeholder = "Search...";
		
		$("#gb-search-input").focus(function() {
			if($(this).val() == placeholder) {
				$(this).val("");
			}
		}).blur(function() {
			if($(this).val() == "") {
				$(this).val(placeholder);
			}
		});
		
		// FORM SUMBIT
		$("#gb-search-form").submit(function(e) {
			e.preventDefault();
			if($("#gb-search-input").val() != "" && $("#gb-search-input").val() != placeholder) {
				window.location.href = "/site/Default.aspx?PageType=6&SiteID=[$SiteID$]&SearchString=" + $("#gb-search-input").val();
			}
		});
	},
	
	/* General Methods */
	"AllyClick": function(event) {
		// FOR SCOPE
		var _this = this;
		
		if(event.type == "click") {
			event.preventDefault();
			return true;
		} else if(event.type == "keypress" || event.type == "keydown" && (event.keyCode == this.KeyCodes.space || event.keyCode == this.KeyCodes.enter)) {
			event.preventDefault();
			return true;
		} else {
			return false;
		}
	},
	
	"CutHex": function(hex) {
		// FOR SCOPE
		var _this = this;
		
		return (hex.charAt(0)=="#") ? hex.substring(1,7):hex;
	},
	
	"GetBreakPoint": function() {
		// FOR SCOPE
		var _this = this;
		
		return window.getComputedStyle(document.querySelector("body"), ":before").getPropertyValue("content").replace(/"|'/g, "");/*"*/
	},
	
	"HexToRgb": function(hex) {
		// FOR SCOPE
		var _this = this;
		
		var r = parseInt((this.CutHex(hex)).substring(0,2),16);
		var g = parseInt((this.CutHex(hex)).substring(2,4),16);
		var b = parseInt((this.CutHex(hex)).substring(4,6),16);
		return r+', '+g+', '+b;
	},
	
	"LoadImage": function(element, image, defaultImage, toggle, link, altText) {
		// FOR SCOPE
		var _this = this;
		
		//Variables
		image = $.trim(image);
		
		if(toggle) {
			var srcCheck = image.indexOf("Faces/default-man.jpg") > -1;
							
			if((image != "") && (!srcCheck)) {
				if(link) {
					$(element).append('<a href="/[$SITEALIAS$]"><img src="' + image + '" alt="' + altText + '" /></a>');
				} else {
					$(element).append('<img src="' + image + '" alt="' + altText + '" />');
				}
			} else {
				if(link) {
					$(element).append('<a href="/[$SITEALIAS$]"><img src="'+defaultImage+'" alt="' + altText + '" /></a>');
				} else {
					$(element).append('<img src="'+defaultImage+'" alt="' + altText + '" />');
				}
			}
		}
		
	}
};