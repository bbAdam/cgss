$(window).on("load", function() { CreativeTemplate.WindowLoad(); });

$(function() {
        CreativeTemplate.Init();
    });

    window.CreativeTemplate = {
        // PROPERTIES
        "KeyCodes": { "tab": 9, "enter": 13, "esc": 27, "space": 32, "end": 35, "home": 36, "left": 37, "up": 38, "right": 39, "down": 40 },
        "IsMyViewPage": false, // UPDATES IN SetTemplateProps METHOD
        "ShowDistrictHome": true,
        "ShowSchoolList": false, // UPDATES IN SetTemplateProps METHOD
        "ShowTranslate": true,

        // METHODS
        "Init": function() {
            // FOR SCOPE
            var template = this;

            csGlobalJs.OpenInNewWindowWarning();
            this.SetTemplateProps();
            this.MyStart();
            this.JsMediaQueries();
            this.SchoolList();
            this.Translate();
            this.Header();
            this.Search();
            this.RsMenu();
            this.ChannelBar();
            this.StickyChannelBar();
            if($("#gb-page.hp").length){
                this.Slideshow();
                this.CheckSlideshow();
                this.Homepage();
                this.Shortcuts();
            }
            this.Body();
            this.ModEvents();
            this.GlobalIcons();
            this.SocialIcons();
            this.AppAccordion();
            this.Footer();


            $(window).resize(function() { template.WindowResize(); });
            $(window).scroll(function() { template.WindowScroll(); });
        },

        "SetTemplateProps": function() {
            // MYVIEW PAGE CHECK
            if($("#pw-body").length) this.IsMyViewPage = true;

            // SCHOOL LIST CHECK
            if($(".sw-mystart-dropdown.schoollist").length) this.ShowSchoolList = true;
        },

        "WindowLoad": function() {
        },

        "WindowResize": function() {
            this.JsMediaQueries();
            this.CheckSlideshow();
            this.Shortcuts();
        },

        "WindowScroll": function() {
            this.CheckSlideshow();
        },

        "JsMediaQueries": function() {
            switch(this.GetBreakPoint()) {
                case "desktop":

                break;
                case "768":

                break;
                case "640":

                break;
                case "480":

                break;
                case "320":

                break;
            }
        },

        "MyStart": function() {
            // FOR SCOPE
            var template = this;

            // BUILD USER OPTIONS DROPDOWN
            var userOptionsItems = "";



            // SIGNIN BUTTON
            if($(".sw-mystart-button.signin").length) {
                userOptionsItems += "<li>" + $(".sw-mystart-button.signin").html() + "</li>";
            }

            // REGISTER BUTTON
            if($(".sw-mystart-button.register").length) {
                userOptionsItems += "<li>" + $(".sw-mystart-button.register").html() + "</li>";
            }



            // ADD USER OPTIONS DROPDOWN TO THE DOM
            $(".cs-mystart-dropdown.user-options .cs-dropdown-list").html(userOptionsItems);

            // BIND DROPDOWN EVENTS
            this.DropdownActions({
                "dropdownParent": ".cs-mystart-dropdown.user-options",
                "dropdownSelector": ".cs-dropdown-selector",
                "dropdown": ".cs-dropdown",
                "dropdownList": ".cs-dropdown-list"
            });
        },

        "SchoolList": function() {
            // ADD SCHOOL LIST
            if(this.ShowSchoolList) {
                var schoolDropdown =    '<div class="cs-mystart-dropdown schools">' +
                                            '<div class="cs-dropdown-selector" tabindex="0" aria-label="Schools" role="button" aria-expanded="false" aria-haspopup="true">Schools</div>' +
                                            '<div class="cs-dropdown" aria-hidden="true" style="display:none;">' +
                                                '<ul class="cs-dropdown-list">' + $(".sw-mystart-dropdown.schoollist .sw-dropdown-list").html() + '</ul>' +
                                            '</div>' +
                                        '</div>';

                // ADD SCHOOL LIST TO THE DOM
                $(".cs-mystart-button.home").after(schoolDropdown);

                // BIND DROPDOWN EVENTS
                this.DropdownActions({
                    "dropdownParent": ".cs-mystart-dropdown.schools",
                    "dropdownSelector": ".cs-dropdown-selector",
                    "dropdown": ".cs-dropdown",
                    "dropdownList": ".cs-dropdown-list"
                });
            }
        },

        "Translate": function() {
            // ADD TRANSLATE
            if(this.ShowTranslate) {
                $(".cs-mystart-dropdown.translate .cs-dropdown").creativeTranslate({
                    "type": 2, // 1 = FRAMESET, 2 = BRANDED, 3 = API
                    "languages": [ // ["ENGLISH LANGUAGE NAME", "TRANSLATED LANGUAGE NAME", "LANGUAGE CODE"]
                        ["Afrikaans", "Afrikaans", "af"],
                        ["Albanian", "shqiptar", "sq"],
                        ["Amharic", "አማርኛ", "am"],
                        ["Arabic", "العربية", "ar"],
                        ["Armenian", "հայերեն", "hy"],
                        ["Azerbaijani", "Azərbaycan", "az"],
                        ["Basque", "Euskal", "eu"],
                        ["Belarusian", "Беларуская", "be"],
                        ["Bengali", "বাঙালি", "bn"],
                        ["Bosnian", "bosanski", "bs"],
                        ["Bulgarian", "български", "bg"],
                        ["Burmese", "မြန်မာ", "my"],
                        ["Catalan", "català", "ca"],
                        ["Cebuano", "Cebuano", "ceb"],
                        ["Chichewa", "Chichewa", "ny"],
                        ["Chinese Simplified", "简体中文", "zh-CN"],
                        ["Chinese Traditional", "中國傳統的", "zh-TW"],
                        ["Corsican", "Corsu", "co"],
                        ["Croatian", "hrvatski", "hr"],
                        ["Czech", "čeština", "cs"],
                        ["Danish", "dansk", "da"],
                        ["Dutch", "Nederlands", "nl"],
                        ["Esperanto", "esperanto", "eo"],
                        ["Estonian", "eesti", "et"],
                        ["Filipino", "Pilipino", "tl"],
                        ["Finnish", "suomalainen", "fi"],
                        ["French", "français", "fr"],
                        ["Galician", "galego", "gl"],
                        ["Georgian", "ქართული", "ka"],
                        ["German", "Deutsche", "de"],
                        ["Greek", "ελληνικά", "el"],
                        ["Gujarati", "ગુજરાતી", "gu"],
                        ["Haitian Creole", "kreyòl ayisyen", "ht"],
                        ["Hausa", "Hausa", "ha"],
                        ["Hawaiian", "ʻŌlelo Hawaiʻi", "haw"],
                        ["Hebrew", "עִברִית", "iw"],
                        ["Hindi", "हिंदी", "hi"],
                        ["Hmong", "Hmong", "hmn"],
                        ["Hungarian", "Magyar", "hu"],
                        ["Icelandic", "Íslenska", "is"],
                        ["Igbo", "Igbo", "ig"],
                        ["Indonesian", "bahasa Indonesia", "id"],
                        ["Irish", "Gaeilge", "ga"],
                        ["Italian", "italiano", "it"],
                        ["Japanese", "日本語", "ja"],
                        ["Javanese", "Jawa", "jw"],
                        ["Kannada", "ಕನ್ನಡ", "kn"],
                        ["Kazakh", "Қазақ", "kk"],
                        ["Khmer", "ភាសាខ្មែរ", "km"],
                        ["Korean", "한국어", "ko"],
                        ["Kurdish", "Kurdî", "ku"],
                        ["Kyrgyz", "Кыргызча", "ky"],
                        ["Lao", "ລາວ", "lo"],
                        ["Latin", "Latinae", "la"],
                        ["Latvian", "Latvijas", "lv"],
                        ["Lithuanian", "Lietuvos", "lt"],
                        ["Luxembourgish", "lëtzebuergesch", "lb"],
                        ["Macedonian", "Македонски", "mk"],
                        ["Malagasy", "Malagasy", "mg"],
                        ["Malay", "Malay", "ms"],
                        ["Malayalam", "മലയാളം", "ml"],
                        ["Maltese", "Malti", "mt"],
                        ["Maori", "Maori", "mi"],
                        ["Marathi", "मराठी", "mr"],
                        ["Mongolian", "Монгол", "mn"],
                        ["Myanmar", "မြန်မာ", "my"],
                        ["Nepali", "नेपाली", "ne"],
                        ["Norwegian", "norsk", "no"],
                        ["Nyanja", "madambwe", "ny"],
                        ["Pashto", "پښتو", "ps"],
                        ["Persian", "فارسی", "fa"],
                        ["Polish", "Polskie", "pl"],
                        ["Portuguese", "português", "pt"],
                        ["Punjabi", "ਪੰਜਾਬੀ ਦੇ", "pa"],
                        ["Romanian", "Română", "ro"],
                        ["Russian", "русский", "ru"],
                        ["Samoan", "Samoa", "sm"],
                        ["Scottish Gaelic", "Gàidhlig na h-Alba", "gd"],
                        ["Serbian", "Српски", "sr"],
                        ["Sesotho", "Sesotho", "st"],
                        ["Shona", "Shona", "sn"],
                        ["Sindhi", "سنڌي", "sd"],
                        ["Sinhala", "සිංහල", "si"],
                        ["Slovak", "slovenský", "sk"],
                        ["Slovenian", "slovenski", "sl"],
                        ["Somali", "Soomaali", "so"],
                        ["Spanish", "Español", "es"],
                        ["Sundanese", "Sunda", "su"],
                        ["Swahili", "Kiswahili", "sw"],
                        ["Swedish", "svenska", "sv"],
                        ["Tajik", "Тоҷикистон", "tg"],
                        ["Tamil", "தமிழ்", "ta"],
                        ["Telugu", "తెలుగు", "te"],
                        ["Thai", "ไทย", "th"],
                        ["Turkish", "Türk", "tr"],
                        ["Ukrainian", "український", "uk"],
                        ["Urdu", "اردو", "ur"],
                        ["Uzbek", "O'zbekiston", "uz"],
                        ["Vietnamese", "Tiếng Việt", "vi"],
                        ["Welsh", "Cymraeg", "cy"],
                        ["Western Frisian", "Western Frysk", "fy"],
                        ["Xhosa", "isiXhosa", "xh"],
                        ["Yiddish", "ייִדיש", "yi"],
                        ["Yoruba", "yorùbá", "yo"],
                        ["Zulu", "Zulu", "zu"]
                    ],
                    "advancedOptions": {
                        "addMethod": "append", // PREPEND OR APPEND THE TRANSLATE ELEMENT
                        "dropdownHandleText": "Translate", // ONLY FOR FRAMESET AND API VERSIONS AND NOT USING A CUSTOM ELEMENT
                        "customElement": { // ONLY FOR FRAMESET AND API VERSIONS
                            "useCustomElement": false,
                            "translateItemsList": true, // true = THE TRANSLATE ITEMS WILL BE AN UNORDERED LIST, false = THE TRANSLATE ITEMS WILL JUST BE A COLLECTION OF <a> TAGS
                            "customElementMarkup": "" // CUSTOM HTML MARKUP THAT MAKES THE CUSTOM TRANSLATE ELEMENT/STRUCTURE - USE [$CreativeTranslateListItems$] ACTIVE BLOCK IN THE MARKUP WHERE THE TRANSLATE ITEMS SHOULD BE ADDED
                        },
                        "apiKey": "", // ONLY FOR API VERSION
                        "brandedLayout": 1, // 1 = VERTICAL (TEXT UNDER SELECT), 2 = HORIZONTAL (TEXT BESIDE SELECT), 3 = NON-RESPONSIVE MEGA DROPDOWN
                        "removeBrandedDefaultStyling": false // 1 = VERTICAL (TEXT UNDER SELECT), 2 = HORIZONTAL (TEXT BESIDE SELECT), 3 = NON-RESPONSIVE MEGA DROPDOWN
                    },
                    "translateLoaded": function() {}
                });

                // BIND DROPDOWN EVENTS
                this.DropdownActions({
                    "dropdownParent": ".cs-mystart-dropdown.translate",
                    "dropdownSelector": ".cs-dropdown-selector",
                    "dropdown": ".cs-dropdown",
                    "dropdownList": ".cs-dropdown-list"
                });
            }
        },

        "DropdownActions": function(params) {
            // FOR SCOPE
            var template = this;

            var dropdownParent = params.dropdownParent;
            var dropdownSelector = params.dropdownSelector;
            var dropdown = params.dropdown;
            var dropdownList = params.dropdownList;

            $(dropdownParent + " " + dropdownList + " a").attr("tabindex", "-1");

            // MYSTART DROPDOWN SELECTOR CLICK EVENT
            $(dropdownParent).on("click", dropdownSelector, function(e) {
                e.preventDefault();

                if($(this).parent().hasClass("open")){
                    $("+ " + dropdownList + " a").attr("tabindex", "-1");
                    $(this).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
                } else {
                    $(this).attr("aria-expanded", "true").parent().addClass("open").find(dropdown).attr("aria-hidden","false").slideDown(300, "swing");
                }
            });

            // MYSTART DROPDOWN SELECTOR KEYDOWN EVENTS
            $(dropdownParent).on("keydown", dropdownSelector, function(e) {
                // CAPTURE KEY CODE
                switch(e.keyCode) {
                    // CONSUME LEFT AND UP ARROWS
                    case template.KeyCodes.enter:
                    case template.KeyCodes.space:
                        e.preventDefault();

                        // IF THE DROPDOWN IS OPEN, CLOSE IT
                        if($(dropdownParent).hasClass("open")){
                            $("+ " + dropdown + " " + dropdownList + " a").attr("tabindex", "-1");
                            $(this).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
                        } else {
                            $(this).attr("aria-expanded", "true").parent().addClass("open").find(dropdown).attr("aria-hidden", "false").slideDown(300, "swing", function(){
                                $(dropdownList + " li:first-child a", this).attr("tabindex", "0").focus();
                            });
                        }
                    break;

                    // CONSUME TAB KEY
                    case template.KeyCodes.tab:
                        if($("+ " + dropdown + " " + dropdownList + " a").length) {
                            $("+ " + dropdown + " " + dropdownList + " a").attr("tabindex", "-1");
                            $(this).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
                        }
                    break;

                    // CONSUME LEFT AND UP ARROWS
                    case template.KeyCodes.down:
                    case template.KeyCodes.right:
                        e.preventDefault();

                        $("+ " + dropdown + " " + dropdownList + " a").attr("tabindex", "-1");
                        $("+ " + dropdown + " " + dropdownList + " li:first-child > a", this).attr("tabindex", "0").focus();
                    break;
                }
            });

            // MYSTART DROPDOWN LINK KEYDOWN EVENTS
            $(dropdownParent).on("keydown", dropdownList + " li a", function(e) {
                // CAPTURE KEY CODE
                switch(e.keyCode) {
                    // CONSUME LEFT AND UP ARROWS
                    case template.KeyCodes.left:
                    case template.KeyCodes.up:
                        e.preventDefault();

                        // IS FIRST ITEM
                        if($(this).parent().is(":first-child")) {
                            // FOCUS DROPDOWN BUTTON
                            $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                            $(this).closest(dropdownParent).find(dropdownSelector).focus();
                        } else {
                            // FOCUS PREVIOUS ITEM
                            $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                            $(this).parent().prev("li").find("> a").attr("tabindex", "0").focus();
                        }
                    break;

                    // CONSUME RIGHT AND DOWN ARROWS
                    case template.KeyCodes.right:
                    case template.KeyCodes.down:
                        e.preventDefault();

                        // IS LAST ITEM
                        if($(this).parent().is(":last-child")) {
                            // FOCUS FIRST ITEM
                            $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                            $(this).closest(dropdownList).find("li:first-child > a").attr("tabindex", "0").focus();
                        } else {
                            // FOCUS NEXT ITEM
                            $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                            $(this).parent().next("li").find("> a").attr("tabindex", "0").focus();
                        }
                    break;

                    // CONSUME TAB KEY
                    case template.KeyCodes.tab:
                        if(e.shiftKey) {
                            e.preventDefault();

                            // FOCUS DROPDOWN BUTTON
                            $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                            $(this).closest(dropdownParent).find(dropdownSelector).focus();
                        }
                    break;

                    // CONSUME HOME KEY
                    case template.KeyCodes.home:
                        e.preventDefault();

                        // FOCUS FIRST ITEM
                        $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                        $(this).closest(dropdownList).find("li:first-child > a").attr("tabindex", "0").focus();
                    break;

                    // CONSUME END KEY
                    case template.KeyCodes.end:
                        e.preventDefault();

                        // FOCUS LAST ITEM
                        $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                        $(this).closest(dropdownList).find("li:last-child > a").attr("tabindex", "0").focus();
                    break;

                    // CONSUME ESC KEY
                    case template.KeyCodes.esc:
                        e.preventDefault();

                        // FOCUS DROPDOWN BUTTON AND CLOSE DROPDOWN
                        $(this).closest(dropdownParent).find(dropdownSelector).focus();
                        $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                        $(dropdownSelector).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
                    break;
                }
            });

            $(dropdownParent).mouseleave(function() {
                $(dropdownList + " a", this).attr("tabindex", "-1");
                $(dropdownSelector, this).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
            }).focusout(function() {
                var thisDropdown = this;

                setTimeout(function () {
                    if(!$(thisDropdown).find(":focus").length) {
                        $(dropdownSelector, thisDropdown).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
                    }
                }, 500);
            });
        },

        "Header": function() {
            // ADD LOGO
            var logoSrc = jQuery.trim("<SWCtrl controlname="Custom" props="Name:schoolLogo" />");
            var srcSplit = logoSrc.split("/");
            var srcSplitLen = srcSplit.length;
            if((logoSrc != "") && (srcSplit[srcSplitLen - 1] != "default-man.jpg")) {
                $("#gb-logo").append("<a href='/[$SITEALIAS$]'><img src='" + logoSrc + "' alt='[$SiteName$] Logo' /></a>");
            } else {
                $("#gb-logo").append("<a href='/[$SITEALIAS$]'><img width='120' height='120' src='/cms/lib/SWCS000001/Centricity/Template/103/defaults/renaissance-default-logo.svg' alt='[$SiteName$] Logo' /></a>");
            }

        },

        "ChannelBar": function() {
            $(".sw-channel-item").off('hover');
            $(".sw-channel-item").hover(function(){

                //GET TOP POSITION OF CHANNEL FOR MEGA DROPDOWN POSITIONING
                var childPos = $(this).offset();
                var parentPos = $(this).parent().offset();
                var childOffset = {
                    top: childPos.top - parentPos.top
                }
                $(this).find(".sw-channel-dropdown").css("top", childOffset.top + 61 +"px");
        
                $(".sw-channel-item ul").stop(true, true);
                var subList = $(this).children('ul');
                if ($.trim(subList.html()) !== "") {
                    subList.slideDown(300, "swing");
                }
                $(this).addClass("hover");
            }, function(){
                $(".sw-channel-dropdown").slideUp(300, "swing");
                $(this).removeClass("hover");
            });
        },

        "StickyChannelBar": function() {
            var headerHeight = $("#gb-header-bottom").outerHeight();
            var navHeight = $("#gb-header-top").outerHeight();

                $(document).scroll(function() {
                    distanceFromTop = $(this).scrollTop();

                    if(distanceFromTop >= headerHeight){
                        $("#gb-page").addClass("sticky-header");
                    } else {
                        $("#gb-page").removeClass("sticky-header");
                    }

                    if($(".sp").length && $(".ui-widget.app.calendar").length) {
                        if($(".wcm-controls").hasClass("wcm-stuck")) {
                             $(".wcm-controls").css("margin-top", navHeight);
                        }
                    }

                    $("#sw-maincontent").css({
                        "display": "block",
                        "position": "relative",
                        "top": "-" + navHeight + "px"
                    });
                });
        },

        "Body": function() {
            // FOR SCOPE
            var template = this;

            // AUTO FOCUS SIGN IN FIELD
            $("#swsignin-txt-username").focus();

            // APPLY RESPONSIVE DIMENSIONS TO CONTENT IMAGES
            $(".ui-widget.app .ui-widget-detail img")
                .not($(".ui-widget.app.multimedia-gallery .ui-widget-detail img"))
                .each(function() {
                    if ($(this).attr('width') !== undefined && $(this).attr('height') !== undefined) { // IMAGE HAS INLINE DIMENSIONS
                        $(this).css({"display": "inline-block", "width": "100%", "max-width": $(this).attr("width") + "px", "height": "auto", "max-height": $(this).attr("height") + "px"});
                    }
            });

            // ADJUST FIRST BREADCRUMB
            $("li.ui-breadcrumb-first > a > span").text("Home").show();

            // CHECK PAGELIST HEADER
            if($.trim($(".ui-widget.app.pagenavigation .ui-widget-header").text()) == "") {
                $(".ui-widget.app.pagenavigation .ui-widget-header").html("<h1>[$ChannelName$]</h1>");
            }

            //SHORTCUTS HOVERS
            $(".siteshortcuts a").hover(function() {
                $(this).closest("li").addClass("hover");
            }, function() {
                $(this).closest("li").removeClass("hover");
            });

            $(".siteshortcuts a").focus(function() {
                $(this).closest("li").addClass("hover");
            });

            $(".siteshortcuts a").focusout(function() {
                $(this).closest("li").removeClass("hover");
            });

            //REMOVE UNUSED ELEMENTS
            $("[data-content=''], [data-toggle='false']").remove();


        },

        "Homepage": function() {
            // FOR SCOPE
            var template = this;

            //HF HOVERS
            $(".headlines .ui-article-title a").hover(function() {
                $(this).closest("li").addClass("hover");
            }, function() {
                $(this).closest("li").removeClass("hover");
            });

            $(".headlines .ui-article-title a").focus(function() {
                $(this).closest("li").addClass("hover");
            });

            $(".headlines .ui-article-title a").focusout(function() {
                $(this).closest("li").removeClass("hover");
            });



        },

        "Shortcuts": function() {
            var template = this;

            // EDIT THESE TWO VARS
            var columnNums = [3, 1, 1, 1, 1]; // [960, 768, 640, 480, 320]
            var selector = "[data-region='b'] div.ui-widget.app.siteshortcuts, [data-region='c'] div.ui-widget.app.siteshortcuts";

            // RETURN BREAKPOINT INDEX
            var bp = function() {
                switch(template.GetBreakPoint()) {
                    case "desktop": return 0; break;
                    case "768": return 1; break;
                    case "640": return 2; break;
                    case "480": return 3; break;
                    case "320": return 4; break;
                }
            }

            // SET COLUMN NUM AND OTHER VARS
            var columnNum = columnNums[bp()];
            var endRange;

            $(selector).each(function() {
                // RETURN THE LI'S TO THE ORIGINAL UL
                $(".ui-widget-detail > ul.site-shortcuts", this).append($(".site-shortcuts-column > li", this));

                // REMOVE COLUMN CONTAINER FOR REBUILD
                $(".site-shortcuts-columns", this).remove();

                // GET SHORTCUT NUM
                var shortcutNum = $(".ui-widget-detail > ul.site-shortcuts > li", this).length;

                // ADD COLUMN CONTAINER
                $(".ui-widget-detail", this).prepend('<div class="site-shortcuts-columns"></div>');

                // LOOP TO BUILD COLUMNS
                for(var i = 0; i < columnNum; i++) {
                    // KEEP FROM ADDING EMPTY UL'S TO THE DOM
                    if(i < shortcutNum) {
                        // IF shortcutNum / columnNum REMAINDER IS BETWEEN .0 AND .5 AND THIS IS THE FIRST LOOP ITERATION
                        // WE'LL ADD 1 TO THE END RANGE SO THAT THE EXTRA LINK GOES INTO THE FIRST COLUMN

                        if((shortcutNum / columnNum) % 1 > 0.0 && (shortcutNum / columnNum) % 1 < 0.5 && i == 0) {
                           endRange = Math.round(shortcutNum / columnNum) + 1;
                       } else if((shortcutNum / columnNum) % 1 == 0.5 && i >= (columnNum / i)) {
                           endRange = Math.round(shortcutNum / columnNum) - 1;
                       } else {
                           endRange = Math.round(shortcutNum / columnNum)
                       }

                        // ADD THE COLUMN UL
                        $(".site-shortcuts-columns", this).append('<ul class="site-shortcuts-column column-' + (i + 1) + '"></ul>');

                        // MOVE THE RANGE OF LI'S TO THE COLUMN UL
                        $(".site-shortcuts-column.column-" + (i + 1), this).append($(".ui-widget-detail > ul.site-shortcuts > li:nth-child(n+1):nth-child(-n+" + endRange +")", this));
                    }
                }

                // HIDE THE ORIGINAL UL
                $(".ui-widget-detail > ul.site-shortcuts", this).hide();
            });
        },

        "Footer": function() {
            // FOR SCOPE
            var template = this;

            $(document).csBbFooter({
                'footerContainer'   : 'footer',
                'useDisclaimer'     : false,
                'disclaimerText'    : ''
            });


            //BACK TO TOP
            $(".to-top").click(function(e){
                e.preventDefault();
                        $("html, body").animate({ scrollTop: 0 }, "slow", function() {
                            $("li.sw-channel-item:first-child > a").focus();
                        });
            });

        },

        "Slideshow": function() {
            // FOR SCOPE
            var template = this;

            if("<SWCtrl controlname="Custom" props="Name:slideshowFeature" />" == "Multimedia Gallery;Streaming Video" || "<SWCtrl controlname="Custom" props="Name:slideshowFeature" />" == "Multimedia Gallery") {
                if($("#sw-content-container10 .ui-widget.app.multimedia-gallery").length) {
                    this.MMGPlugin();
                }
            } else {
                this.StreamingVideo();
            }
        },

        "MMGPlugin": function() {
            // FOR SCOPE
            var template = this;

            var mmg = eval("multimediaGallery" + $("#sw-content-container10 .ui-widget.app.multimedia-gallery:first").attr("data-pmi"));
            mmg.props.defaultGallery = false;

            $("#sw-content-container10 .ui-widget.app.multimedia-gallery:first").csMultimediaGallery({
                "efficientLoad" : true,
                "imageWidth" : 1500,
                "imageHeight" : 478,
                "mobileDescriptionContainer": [960, 768, 640, 480, 320], // [960, 768, 640, 480, 320]
                "galleryOverlay" : false,
                "linkedElement" : [],  // ["image", "title", "overlay"]
                "playPauseControl" : true,
                "backNextControls" : true,
                "bullets" : false,
                "thumbnails" : false,
                "thumbnailViewerNum": [3, 4, 3, 3, 2], // NUMERICAL - [960 view, 768 view, 640 view, 480 view, 320 view]
                "autoRotate" : true,
                "hoverPause" : true,
                "transitionType" : "fade",
                "transitionSpeed" : <SWCtrl controlname="Custom" props="Name:transitionSpeed" />,
                "transitionDelay" : <SWCtrl controlname="Custom" props="Name:transitionDelay" />,
                "transitionSpeed" : 3,
                "transitionDelay" : 8,
                "fullScreenRotator" : false,
                "fullScreenBreakpoints" : [960], // NUMERICAL - [960, 768, 640, 480, 320]
                "onImageLoad" : function(props) {}, // props.element, props.recordIndex, props.mmgRecords
                "allImagesLoaded" : function(props) {}, // props.element, props.mmgRecords
                "onTransitionStart" : function(props) {

                },
                "onTransitionEnd" : function(props) {}, // props.element, props.currentRecordIndex, props.currentGalleryIndex, props.mmgRecords
                "allLoaded" : function(props) {

                    //WRAP CONTROLS
                    $(".mmg-control.play-pause", props.element).insertAfter(".mmg-control.back", props.element);
                    $(".mmg-control", props.element).wrapAll("<div class='mmg-control-wrapper'></div>");

                }, // props.element, props.mmgRecords
                "onWindowResize": function(props) {

                } // props.element, props.mmgRecords
            });
        },

        "CheckSlideshow": function() {
            // FOR SCOPE
            var template = this;

            if($(".hp").length && this.GetBreakPoint() != "desktop" && "<SWCtrl controlname="Custom" props="Name:slideshowFeature" />" != "Streaming Video") {
                if ($(window).scrollTop() <= $("#hp-slideshow").offset().top + $("#hp-slideshow").height()) {
                    if(this.SlideshowDescFixed) {
                        $("#hp-slideshow").removeAttr("style");

                        this.SlideshowDescFixed = false;
                    }
                } else {
                    if(!this.SlideshowDescFixed) {
                        $("#hp-slideshow").css({
                            "height": $("#hp-slideshow").height(),
                            "overflow": "hidden"
                        });

                        this.SlideshowDescFixed = true;
                    }
                }
            }
        },

        "StreamingVideo": function() {
            // FOR SCOPE
            var template = this;

            var videoVendor = "<SWCtrl controlname="Custom" props="Name:streamingVideoVendor" />";
            var mobilePhoto = "<SWCtrl controlname="Custom" props="Name:mobileBackgroundPhoto" />";

            if($.trim(mobilePhoto) == "" || mobilePhoto.indexOf("default-man.jpg") > -1) {
                mobilePhoto = "/cms/lib/SWCS000001/Centricity/template/103/defaults/streaming-video-default.jpg";
            }

            $("#hp-slideshow").csStreamingVideo({
                // USER CONFIGURATIONS
                "videoSource" : videoVendor, // OPTIONS ARE: YouTube, Vimeo, MyVRSpot
                "videoID": '<SWCtrl controlname="Custom" props="Name:streamingVideoID" />', // DEFAULT IS BLANK TO AUTOFILL THE PROPER ID FOR THE VENDOR
                "myVRSpotVideoWidth": "853.5",
                "myVRSpotVideoHeight": "480",
                "fullWidthBreakpoints" : [], // OPTIONS ARE: ["Desktop", 1024, 768, 640, 480, 320]
                "fullWidthCSSPosition": "", // OPITONS ARE: static, relative, fixed, absolute, sticky
                "fullScreenBreakpoints" : [], // OPTIONS ARE: ["Desktop", 1024, 768, 640, 480, 320]
                "fullScreenCSSPosition": "", // OPITONS ARE: static, relative, fixed, absolute, sticky
                "showVideoTitle" : <SWCtrl controlname="Custom" props="Name:showVideoTitle" />,
                "videoTitleText" : '<SWCtrl controlname="Custom" props="Name:videoTitle" />',
                "showVideoDescription" : <SWCtrl controlname="Custom" props="Name:showVideoCaption" />,
                "videoDescriptionText" : '<SWCtrl controlname="Custom" props="Name:videoCaption" />',
                "showVideoLinks" : <SWCtrl controlname="Custom" props="Name:showVideoLink" />,
                "videoLinks" : [{ "text": '<SWCtrl controlname="Custom" props="Name:videoLinkText" />', "link": '<SWCtrl controlname="Custom" props="Name:videoLinkUrl" />', "target": '<SWCtrl controlname="Custom" props="Name:videoLinkTarget" />'  }], //{ "text": "Video Text", "link": "Video Link", "target": "Video Target"  }
                "showAudioButtons" : true,
                "showWatchVideoButton": true,
                "useBackgroundPhoto" : true,
                "backgroundPhoto" : mobilePhoto,
                "backgroundPhotoBreakpoints" : [1024], // OPTIONS ARE: ["Desktop", 1024, 768, 640, 480, 320]
                "backgroundPhotoCSSPosition": "", // OPITONS ARE: static, relative, fixed, absolute, sticky
                "useCustomResize": false,
                "extendResize": function(props) { }, //element, videoHeight, videoWidth
                "onReady" : function(props) { }, // IF NOEMBED REQUEST RETURNS VIDEO DATA, PROPS WILL BE THE RETURNED VIDEO DATA OBJECT. OTHERWISE, PROPS WILL BE props.width AND props.height FOR VIDEO WIDTH AND HEIGHT
                "allLoaded" : function(props) { }, // props.element
            });
        },

        "ModEvents": function() {
            // FOR SCOPE
            var template = this;

            $(".ui-widget.app.upcomingevents").modEvents({
                columns     : "yes",
                monthLong   : "no",
                dayWeek     : "yes"
            });

            eventsByDay(".upcomingevents .ui-articles");

            function eventsByDay(container) {
                $(".ui-article", container).each(function(){
                    if (!$(this).find("h1.ui-article-title.sw-calendar-block-date").size()){
                        var moveArticle = $(this).html();
                        $(this).parent().prev().children().children().next().append(moveArticle);
                        $(this).parent().remove();
                    };
                });

                $(".ui-article", container).each(function(){
                    var newDateTime = $('.upcoming-column.left h1', this).html().toLowerCase().split("</span>");
                    var newDate = '';
                    var dateIndex = ((newDateTime.length > 2) ? 2 : 1); //if the dayWeek is set to yes we need to account for that in our array indexing

                    //if we have the day of the week make sure to include it.
                    if( dateIndex > 1 ){
                        newDate += newDateTime[0] + "</span>";
                    }

                    //add in the month
                    newDate += newDateTime[dateIndex - 1] + '</span>'; //the month is always the in the left array position to the date

                    //wrap the date in a new tag
                    newDate += '<span class="jeremy-date">' + newDateTime[dateIndex] + '</span>';

                    //append the date and month back into their columns
                    $('.upcoming-column.left h1', this).html(newDate);


                    //add an ALL DAY label if no time was given
                    $('.upcoming-column.right .ui-article-description', this).each(function(){
                        if( $('.sw-calendar-block-time', this).length < 1 ){ //if it doesnt exist add it
                            $(this).prepend('<span class="sw-calendar-block-time">ALL DAY</span>');
                        }
                        $(".sw-calendar-block-time", this).appendTo($(this));
                    });

                    //WRAP DATE AND MONTH IN A CONTAINER
                    $(".jeremy-date, .joel-month", this).wrapAll("<span class='adam-hug'></span>");
                })
            }

            //ADD NO EVENTS TEXT
            $(".upcomingevents").each(function(){
                if(!$(this).find(".ui-article").length){
                    $("<li><p>There are no upcoming events to display.</p></l1>").appendTo($(this).find(".ui-articles"));
                }
            });

        },

        "GlobalIcons": function() {
            $("#gb-icons").creativeIcons({
                "iconNum"       : "<SWCtrl controlname="Custom" props="Name:numOfIcons" />",
                "defaultIconSrc": "",
                "icons"         : [
                    {
                        "image": "<SWCtrl controlname="Custom" props="Name:Icon1Image" />",
                        "showText": true,
                        "text": "<SWCtrl controlname="Custom" props="Name:Icon1Text" />",
                        "url": "<SWCtrl controlname="Custom" props="Name:Icon1Link" />",
                        "target": "<SWCtrl controlname="Custom" props="Name:Icon1Target" />"
                    },
                    {
                        "image": "<SWCtrl controlname="Custom" props="Name:Icon2Image" />",
                        "showText": true,
                        "text": "<SWCtrl controlname="Custom" props="Name:Icon2Text" />",
                        "url": "<SWCtrl controlname="Custom" props="Name:Icon2Link" />",
                        "target": "<SWCtrl controlname="Custom" props="Name:Icon2Target" />"
                    },
                    {
                        "image": "<SWCtrl controlname="Custom" props="Name:Icon3Image" />",
                        "showText": true,
                        "text": "<SWCtrl controlname="Custom" props="Name:Icon3Text" />",
                        "url": "<SWCtrl controlname="Custom" props="Name:Icon3Link" />",
                        "target": "<SWCtrl controlname="Custom" props="Name:Icon3Target" />"
                    },
                    {
                        "image": "<SWCtrl controlname="Custom" props="Name:Icon4Image" />",
                        "showText": true,
                        "text": "<SWCtrl controlname="Custom" props="Name:Icon4Text" />",
                        "url": "<SWCtrl controlname="Custom" props="Name:Icon4Link" />",
                        "target": "<SWCtrl controlname="Custom" props="Name:Icon4Target" />"
                    },
                    {
                        "image": "<SWCtrl controlname="Custom" props="Name:Icon5Image" />",
                        "showText": true,
                        "text": "<SWCtrl controlname="Custom" props="Name:Icon5Text" />",
                        "url": "<SWCtrl controlname="Custom" props="Name:Icon5Link" />",
                        "target": "<SWCtrl controlname="Custom" props="Name:Icon5Target" />"
                    },
                    {
                        "image": "<SWCtrl controlname="Custom" props="Name:Icon6Image" />",
                        "showText": true,
                        "text": "<SWCtrl controlname="Custom" props="Name:Icon6Text" />",
                        "url": "<SWCtrl controlname="Custom" props="Name:Icon6Link" />",
                        "target": "<SWCtrl controlname="Custom" props="Name:Icon6Target" />"
                    },
                    {
                        "image": "<SWCtrl controlname="Custom" props="Name:Icon7Image" />",
                        "showText": true,
                        "text": "<SWCtrl controlname="Custom" props="Name:Icon7Text" />",
                        "url": "<SWCtrl controlname="Custom" props="Name:Icon7Link" />",
                        "target": "<SWCtrl controlname="Custom" props="Name:Icon7Target" />"
                    },
                    {
                        "image": "<SWCtrl controlname="Custom" props="Name:Icon8Image" />",
                        "showText": true,
                        "text": "<SWCtrl controlname="Custom" props="Name:Icon8Text" />",
                        "url": "<SWCtrl controlname="Custom" props="Name:Icon8Link" />",
                        "target": "<SWCtrl controlname="Custom" props="Name:Icon8Target" />"
                    },
                    {
                        "image": "<SWCtrl controlname="Custom" props="Name:Icon9Image" />",
                        "showText": true,
                        "text": "<SWCtrl controlname="Custom" props="Name:Icon9Text" />",
                        "url": "<SWCtrl controlname="Custom" props="Name:Icon9Link" />",
                        "target": "<SWCtrl controlname="Custom" props="Name:Icon9Target" />"
                    },
                    {
                        "image": "<SWCtrl controlname="Custom" props="Name:Icon10Image" />",
                        "showText": true,
                        "text": "<SWCtrl controlname="Custom" props="Name:Icon10Text" />",
                        "url": "<SWCtrl controlname="Custom" props="Name:Icon10Link" />",
                        "target": "<SWCtrl controlname="Custom" props="Name:Icon10Target" />"
                    }
                ],
                "siteID"        : "[$SiteID$]",
                "siteAlias"     : "[$SiteAlias$]",
                "calendarLink"  : "[$SiteCalendarLink$]",
                "contactEmail"  : "[$SiteContactEmail$]",
                "allLoaded"     : function(){ }
            });

        },

        "SocialIcons": function() {
            var socialIcons = [
                {
                    "show": <SWCtrl controlname="Custom" props="Name:showFacebook" />,
                    "label": "Facebook",
                    "class": "facebook",
                    "url": "<SWCtrl controlname="Custom" props="Name:FacebookUrl" />",
                    "target": "<SWCtrl controlname="Custom" props="Name:FacebookTarget" />"
                },
                {
                    "show": <SWCtrl controlname="Custom" props="Name:showTwitter" />,
                    "label": "Twitter",
                    "class": "twitter",
                    "url": "<SWCtrl controlname="Custom" props="Name:TwitterUrl" />",
                    "target": "<SWCtrl controlname="Custom" props="Name:TwitterTarget" />"
                },
                {
                    "show": <SWCtrl controlname="Custom" props="Name:showYouTube" />,
                    "label": "YouTube",
                    "class": "youtube",
                    "url": "<SWCtrl controlname="Custom" props="Name:YouTubeUrl" />",
                    "target": "<SWCtrl controlname="Custom" props="Name:YouTubeTarget" />"
                },
                {
                    "show": <SWCtrl controlname="Custom" props="Name:showInstagram" />,
                    "label": "Instagram",
                    "class": "instagram",
                    "url": "<SWCtrl controlname="Custom" props="Name:InstagramUrl" />",
                    "target": "<SWCtrl controlname="Custom" props="Name:InstagramTarget" />"
                },
                {
                    "show": <SWCtrl controlname="Custom" props="Name:showLinkedIn" />,
                    "label": "LinkedIn",
                    "class": "linkedin",
                    "url": "<SWCtrl controlname="Custom" props="Name:LinkedInUrl" />",
                    "target": "<SWCtrl controlname="Custom" props="Name:LinkedInTarget" />"
                },
                {
                    "show": <SWCtrl controlname="Custom" props="Name:showPinterest" />,
                    "label": "Pinterest",
                    "class": "pinterest",
                    "url": "<SWCtrl controlname="Custom" props="Name:PinterestUrl" />",
                    "target": "<SWCtrl controlname="Custom" props="Name:PinterestTarget" />"
                },
                {
                    "show": <SWCtrl controlname="Custom" props="Name:showFlickr" />,
                    "label": "Flickr",
                    "class": "flickr",
                    "url": "<SWCtrl controlname="Custom" props="Name:FlickrUrl" />",
                    "target": "<SWCtrl controlname="Custom" props="Name:FlickrTarget" />"
                },
                {
                    "show": <SWCtrl controlname="Custom" props="Name:showPeachjar" />,
                    "label": "Peachjar",
                    "class": "peachjar",
                    "url": "<SWCtrl controlname="Custom" props="Name:peachjarUrl" />",
                    "target": "<SWCtrl controlname="Custom" props="Name:peachjarTarget" />"
                },
                {
                    "show": <SWCtrl controlname="Custom" props="Name:showVimeo" />,
                    "label": "Vimeo",
                    "class": "vimeo",
                    "url": "<SWCtrl controlname="Custom" props="Name:VimeoUrl" />",
                    "target": "<SWCtrl controlname="Custom" props="Name:VimeoTarget" />"
                }
            ];

            var icons = '';
            $.each(socialIcons, function(index, icon) {
                if(icon.show) {
                    icons += '<a class="gb-social-media-icon ' + icon.class + ' district" href="' + icon.url + '" target="' + icon.target + '" aria-label="' + icon.label + '"></a>';
                }
            });

            if(icons.length) {
                $("#social-icons").prepend(icons);
            }
        },

        "RsMenu": function() {
            // FOR SCOPE
            var template = this;

            $.csRsMenu({
              "breakPoint" : 999999, // SYSTEM BREAK POINTS - 768, 640, 480, 320
              "slideDirection" : "left-to-right", // OPTIONS - left-to-right, right-to-left
              "menuButtonParent" : "#gb-header-top nav",
              "menuBtnText" : "MENU",
              "colors": {
                  "pageOverlay": "<SWCtrl controlname="Custom" props="Name:rsMenuPageOverlayBackgroundColor" />", // DEFAULT #000000
                  "menuBackground": "<SWCtrl controlname="Custom" props="Name:rsMenuBackgroundColor" />", // DEFAULT #FFFFFF
                  "menuText": "<SWCtrl controlname="Custom" props="Name:rsMenuTextColor" />", // DEFAULT #333333
                  "menuTextAccent": "<SWCtrl controlname="Custom" props="Name:rsMenuTextAccentColor" />", // DEFAULT #333333
                  "dividerLines": "<SWCtrl controlname="Custom" props="Name:rsMenuDividerLinesColor" />", // DEFAULT #E6E6E6
                  "buttonBackground": "<SWCtrl controlname="Custom" props="Name:rsMenuButtonBackgroundColor" />", // DEFAULT #E6E6E6
                  "buttonText": "<SWCtrl controlname="Custom" props="Name:rsMenuButtonTextColor" />" // DEFAULT #333333
              },
              "showDistrictHome": template.ShowDistrictHome,
              "districtHomeText": "District",
              "showSchools" : template.ShowSchoolList,
              "schoolMenuText": "Schools",
              "showTranslate" : true,
              "translateMenuText": "Translate",
              "translateVersion": 2, // 1 = FRAMESET, 2 = BRANDED
              "translateId" : "",
              "translateLanguages": [ // ["ENGLISH LANGUAGE NAME", "TRANSLATED LANGUAGE NAME", "LANGUAGE CODE"]
                  ["Afrikaans", "Afrikaans", "af"],
                  ["Albanian", "shqiptar", "sq"],
                  ["Amharic", "አማርኛ", "am"],
                  ["Arabic", "العربية", "ar"],
                  ["Armenian", "հայերեն", "hy"],
                  ["Azerbaijani", "Azərbaycan", "az"],
                  ["Basque", "Euskal", "eu"],
                  ["Belarusian", "Беларуская", "be"],
                  ["Bengali", "বাঙালি", "bn"],
                  ["Bosnian", "bosanski", "bs"],
                  ["Bulgarian", "български", "bg"],
                  ["Burmese", "မြန်မာ", "my"],
                  ["Catalan", "català", "ca"],
                  ["Cebuano", "Cebuano", "ceb"],
                  ["Chichewa", "Chichewa", "ny"],
                  ["Chinese Simplified", "简体中文", "zh-CN"],
                  ["Chinese Traditional", "中國傳統的", "zh-TW"],
                  ["Corsican", "Corsu", "co"],
                  ["Croatian", "hrvatski", "hr"],
                  ["Czech", "čeština", "cs"],
                  ["Danish", "dansk", "da"],
                  ["Dutch", "Nederlands", "nl"],
                  ["Esperanto", "esperanto", "eo"],
                  ["Estonian", "eesti", "et"],
                  ["Filipino", "Pilipino", "tl"],
                  ["Finnish", "suomalainen", "fi"],
                  ["French", "français", "fr"],
                  ["Galician", "galego", "gl"],
                  ["Georgian", "ქართული", "ka"],
                  ["German", "Deutsche", "de"],
                  ["Greek", "ελληνικά", "el"],
                  ["Gujarati", "ગુજરાતી", "gu"],
                  ["Haitian Creole", "kreyòl ayisyen", "ht"],
                  ["Hausa", "Hausa", "ha"],
                  ["Hawaiian", "ʻŌlelo Hawaiʻi", "haw"],
                  ["Hebrew", "עִברִית", "iw"],
                  ["Hindi", "हिंदी", "hi"],
                  ["Hmong", "Hmong", "hmn"],
                  ["Hungarian", "Magyar", "hu"],
                  ["Icelandic", "Íslenska", "is"],
                  ["Igbo", "Igbo", "ig"],
                  ["Indonesian", "bahasa Indonesia", "id"],
                  ["Irish", "Gaeilge", "ga"],
                  ["Italian", "italiano", "it"],
                  ["Japanese", "日本語", "ja"],
                  ["Javanese", "Jawa", "jw"],
                  ["Kannada", "ಕನ್ನಡ", "kn"],
                  ["Kazakh", "Қазақ", "kk"],
                  ["Khmer", "ភាសាខ្មែរ", "km"],
                  ["Korean", "한국어", "ko"],
                  ["Kurdish", "Kurdî", "ku"],
                  ["Kyrgyz", "Кыргызча", "ky"],
                  ["Lao", "ລາວ", "lo"],
                  ["Latin", "Latinae", "la"],
                  ["Latvian", "Latvijas", "lv"],
                  ["Lithuanian", "Lietuvos", "lt"],
                  ["Luxembourgish", "lëtzebuergesch", "lb"],
                  ["Macedonian", "Македонски", "mk"],
                  ["Malagasy", "Malagasy", "mg"],
                  ["Malay", "Malay", "ms"],
                  ["Malayalam", "മലയാളം", "ml"],
                  ["Maltese", "Malti", "mt"],
                  ["Maori", "Maori", "mi"],
                  ["Marathi", "मराठी", "mr"],
                  ["Mongolian", "Монгол", "mn"],
                  ["Myanmar", "မြန်မာ", "my"],
                  ["Nepali", "नेपाली", "ne"],
                  ["Norwegian", "norsk", "no"],
                  ["Nyanja", "madambwe", "ny"],
                  ["Pashto", "پښتو", "ps"],
                  ["Persian", "فارسی", "fa"],
                  ["Polish", "Polskie", "pl"],
                  ["Portuguese", "português", "pt"],
                  ["Punjabi", "ਪੰਜਾਬੀ ਦੇ", "pa"],
                  ["Romanian", "Română", "ro"],
                  ["Russian", "русский", "ru"],
                  ["Samoan", "Samoa", "sm"],
                  ["Scottish Gaelic", "Gàidhlig na h-Alba", "gd"],
                  ["Serbian", "Српски", "sr"],
                  ["Sesotho", "Sesotho", "st"],
                  ["Shona", "Shona", "sn"],
                  ["Sindhi", "سنڌي", "sd"],
                  ["Sinhala", "සිංහල", "si"],
                  ["Slovak", "slovenský", "sk"],
                  ["Slovenian", "slovenski", "sl"],
                  ["Somali", "Soomaali", "so"],
                  ["Spanish", "Español", "es"],
                  ["Sundanese", "Sunda", "su"],
                  ["Swahili", "Kiswahili", "sw"],
                  ["Swedish", "svenska", "sv"],
                  ["Tajik", "Тоҷикистон", "tg"],
                  ["Tamil", "தமிழ்", "ta"],
                  ["Telugu", "తెలుగు", "te"],
                  ["Thai", "ไทย", "th"],
                  ["Turkish", "Türk", "tr"],
                  ["Ukrainian", "український", "uk"],
                  ["Urdu", "اردو", "ur"],
                  ["Uzbek", "O'zbekiston", "uz"],
                  ["Vietnamese", "Tiếng Việt", "vi"],
                  ["Welsh", "Cymraeg", "cy"],
                  ["Western Frisian", "Western Frysk", "fy"],
                  ["Xhosa", "isiXhosa", "xh"],
                  ["Yiddish", "ייִדיש", "yi"],
                  ["Yoruba", "yorùbá", "yo"],
                  ["Zulu", "Zulu", "zu"]
              ],
              "showAccount": true,
              "accountMenuText": "User Options",
              "usePageListNavigation": false,
              "extraMenuOptions": {},
              "siteID": "[$siteID$]",
              "allLoaded": function(){}
          });
        },

        "AppAccordion": function() {
            $(".sp-column.one").csAppAccordion({
                "accordionBreakpoints" : [768, 640, 480, 320]
            });
        },

        "Search": function() {
            // FOR SCOPE
            var template = this;

            //OPENING SEARCH
            $(".cs-mystart-button.search .cs-button-selector").on('click keydown', function(event){

                if(_this.AllyClick(event) === true){
                    //DONT LET THE PAGE JUMP ON KEYPRESS
                    event.preventDefault();
                    if(!$(this).parent().hasClass("open")){
                        openSearch();
                    } else {
                        closeSearch();
                    }
                }
            })

            $(".cs-mystart-button.search .cs-button-selector, #gb-search-input, #gb-search-button").on("focusout", function(){
                setTimeout(function () {
                    if(!$(".cs-mystart-button.search").find(":focus").length) {
                        closeSearch();
                    }
                }, 500);
            })

            function closeSearch(){
                $(".cs-mystart-button.search .cs-button-selector").parent().removeClass("open");
                $(".cs-mystart-button.search .cs-button-selector").removeClass("open").attr("aria-expanded","false");
                $("#gb-search").removeClass("open").attr("aria-hidden","true");
                $("#gb-search-input, #gb-search-button").attr("tabindex","-1");
            }

            function openSearch(){
                $(".cs-mystart-button.search .cs-button-selector").addClass("open").attr("aria-expanded","true");
                $(".cs-mystart-button.search .cs-button-selector").parent().addClass("open");
                $("#gb-search").addClass("open").attr("aria-hidden","false");
                $("#gb-search-input").focus().attr("tabindex","0");
                $("#gb-search-button").attr("tabindex","0");
            }

            //SEARCH FORM SUBMISSION
            $("#gb-search-form").submit(function(e){
                e.preventDefault();

                if($.trim($("#gb-search-input").val()) != "I'm looking for..." && $.trim($("#gb-search-input").val()) != "") {
                    window.location.href = "/site/Default.aspx?PageType=6&SiteID=[$SiteID$]&SearchString=" + $("#gb-search-input").val();
                }
            });

            //SEARCH INPUT TEXT
            $("#gb-search-input").focus(function() {
                if($(this).val() == "I'm looking for...") {
                    $(this).val("");
                }
            });

            $("#gb-search-input").blur(function() {
                if($(this).val() == "") {
                    $(this).val("I'm looking for...");
                }
            });

            //SEARCH KEYBOARD NAV
            $("#gb-search-input").on("keydown", function(e){
                // CAPTURE KEY CODE
                switch(e.keyCode) {
                    case template.KeyCodes.tab:
                        if(!e.shiftKey){
                            e.preventDefault();
                            $("#gb-search-control").focus();
                        }
                    break;
                }
            });

        },

        "AllyClick": function(event) {
            if(event.type == "click") {
                return true;
            } else if(event.type == "keydown") {
                if(event.keyCode == this.KeyCodes.space || event.keyCode == this.KeyCodes.enter) {
                    return true;
                }
            } else {
                return false;
            }
        },

        "GetBreakPoint": function() {
            return window.getComputedStyle(document.querySelector("body"), ":before").getPropertyValue("content").replace(/"|'/g, "");/*"*/
        }
    };
