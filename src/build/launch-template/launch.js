// https://www.npmjs.com/package/sync-request
const request = require('sync-request');

// https://www.npmjs.com/package/inquirer
const inquirer = require('inquirer');

// GET THE localHostPath VALUE FROM package.json
// THE REGEX REMOVES ANY "/" CHARACTERS FROM THE BEGINNING AND END OF THE STRING SO THAT WE CAN WORK WITH A PREDICTABLE VALUE
const pJson = require('../../../package.json');
let localHostPath = pJson.localHostPath.replace(/(^\/?)(.*?)(\/?$)/gi, '$2');
if(localHostPath == "") {
    localHostPath = "/";
} else {
    localHostPath = "/" + localHostPath + "/";
}

// COMMAND LINE PROMPTS
inquirer.prompt([
    {
        type: "list",
        name: "skeleton",
        message: "Skeleton to Use",
        choices: [
            {
                name: "Adam Cruse",
                value: "adam-cruse"
            },
            {
                name: "Brenton Kelly",
                value: "brenton-kelly"
            },
            {
                name: "DRT MyWay Premium",
                value: "my-way-premium"
            },
            {
                name: "Jacinda Grannas",
                value: "jacinda-grannas"
            },
            {
                name: "Jeremy Katlic",
                value: "jeremy-katlic"
            },
            {
                name: "Matt Bassett",
                value: "matt-bassett"
            },
            {
                name: "Paul Nauman",
                value: "paul-nauman"
            }
        ]
    },
    {
        type: "input",
        name: "site-name",
        message: "Site Name",
        default: "Adam Cruse School District"
    },
    {
        type: "input",
        name: "domain",
        message: "SW Sandbox Domain",
        default: "https://adamcruse.schoolwires.net"
    },
    {
        type: "input",
        name: "homepage-url",
        message: "Homepage",
        default: "/Page/1"
    },
    {
        type: "input",
        name: "subpage-url",
        message: "Subpage",
        default: "/Page/17"
    },
    {
        type: "input",
        name: "subpage-nonav-url",
        message: "Subpage No Nav",
        default: "/Page/2"
    },
    {
        type: "input",
        name: "site-alias",
        message: "Site Alias",
        default: "dev1"
    },
    {
        type: "input",
        name: "site-address",
        message: "Site Address",
        default: "330 Innovation Blvd."
    },
    {
        type: "input",
        name: "site-city",
        message: "Site City",
        default: "State College"
    },
    {
        type: "input",
        name: "site-state",
        message: "Site State",
        default: "PA"
    },
    {
        type: "input",
        name: "site-zip",
        message: "Site Zip",
        default: "16801"
    },
    {
        type: "input",
        name: "site-phone",
        message: "Site Phone",
        default: "814.272.7300"
    },
    {
        type: "input",
        name: "site-fax",
        message: "Site Fax",
        default: "012.345.6789"
    }
]).then(answers => {
    // NODE SYNCHRONOUS AJAX REQUEST
    let res = request('POST', 'http://localhost:8888' + localHostPath + 'src/build/launch-template/launch.php', {
        headers: {
            'content-type': 'application/x-www-form-urlencoded'
        },
        body: 'skeleton=' + answers["skeleton"] + '&siteName=' + answers["site-name"] + '&siteDomain=' + answers["domain"] + '&homepageUrl=' + answers["homepage-url"] + '&subpageUrl=' + answers["subpage-url"] + '&subpageNoNavUrl=' + answers["subpage-nonav-url"] + '&siteAlias=' + answers["site-alias"] + '&siteAddress=' + answers["site-address"] + '&siteCity=' + answers["site-city"] + '&siteState=' + answers["site-state"] + '&siteZip=' + answers["site-zip"] + '&sitePhone=' + answers["site-phone"] + '&siteFax=' + answers["site-fax"]
    });
    var response = JSON.parse(res.getBody('utf8'));

    // ADD COLORED TEXT TO TERMINAL - FIRST \x1b[31m TO SET COLOR TO RED AND THEN \x1b[0m TO RESET BACK TO DEFAULT TERMINAL COLOR.
    // https://stackoverflow.com/questions/9781218/how-to-change-node-jss-console-font-color
    if(response.error) {
        console.log("\x1b[31m", response.msg, "\x1b[0m");
    } else {
        console.log("\x1b[32m", response.msg, "\x1b[0m");
    }
}).catch(error => {
    if(error.isTtyError) {
        // Prompt couldn't be rendered in the current environment
    } else {
        // Something else when wrong
    }
});
