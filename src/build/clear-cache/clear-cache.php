<?php

// REMOVE ALL CACHE FILES AND DIRECTORY
$cacheFiles = array(
    "../../cache/hp.php",
    "../../cache/sp.php",
    "../../cache/spn.php",
    "../../cache/mv.php"
);

foreach($cacheFiles as $file) {
    if(file_exists($file)) {
        unlink($file);
    }
}

rmdir("../../cache/");

echo json_encode(array("msg" => "Cache files have been removed successfully.", "error" => false));

?>