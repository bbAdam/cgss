$(window).on("load", function() { CreativeTemplate.WindowLoad(); });

$(function() {
        CreativeTemplate.Init();
    });

    window.CreativeTemplate = {
        // PROPERTIES
        "KeyCodes": { "tab": 9, "enter": 13, "esc": 27, "space": 32, "end": 35, "home": 36, "left": 37, "up": 38, "right": 39, "down": 40 },
        "IsMyViewPage": false, // UPDATES IN SetTemplateProps METHOD
        "ShowDistrictHome": true,
        "ShowSchoolList": false, // UPDATES IN SetTemplateProps METHOD
        "ShowTranslate": true,

        // METHODS
        "Init": function() {
            // FOR SCOPE
            var template = this;

            csGlobalJs.OpenInNewWindowWarning();
            this.SetTemplateProps();
            this.MyStart();
            this.JsMediaQueries();
            this.SchoolList();
            this.Translate();
            this.Header();
            this.Search();
            this.RsMenu();
            this.ChannelBar();
            this.StickyChannelBar();
            this.ModEvents();
            if($("#gb-page.hp").length){
                this.Slideshow();
                this.CheckSlideshow();
                this.Homepage();
                this.Shortcuts();
            }
            this.Body();
            this.GlobalIcons();
            this.SocialIcons();
            this.AppAccordion();
            this.Footer();


            $(window).resize(function() { template.WindowResize(); });
            $(window).scroll(function() { template.WindowScroll(); });
        },

        "SetTemplateProps": function() {
            // MYVIEW PAGE CHECK
            if($("#pw-body").length) this.IsMyViewPage = true;

            // SCHOOL LIST CHECK
            if($(".sw-mystart-dropdown.schoollist").length) this.ShowSchoolList = true;
        },

        "WindowLoad": function() {
            this.MegaChannelMenu();

            if($("#gb-page.sp").length){
                this.Subpage();
            }
        },

        "WindowResize": function() {
            this.JsMediaQueries();
            this.CheckSlideshow();
            this.Shortcuts();
            this.StickyChannelBar();
            this.EntranceAnimations();
        },

        "WindowScroll": function() {
            this.CheckSlideshow();
            this.EntranceAnimations();
        },

        "JsMediaQueries": function() {
            switch(this.GetBreakPoint()) {
                case "desktop":
                    if($(".header-column.two .translate").length){
                        $(".cs-mystart-dropdown.translate").appendTo("#gb-header");
                    }
                break;
                case "768":
                    if(!$(".header-column.two .translate").length){
                        $(".cs-mystart-dropdown.translate").appendTo(".header-column.two");
                    }
                break;
                case "640":
                    if(!$(".header-column.two .translate").length){
                        $(".cs-mystart-dropdown.translate").appendTo(".header-column.two");
                    }
                break;
                case "480":
                    if(!$(".header-column.two .translate").length){
                        $(".cs-mystart-dropdown.translate").appendTo(".header-column.two");
                    }
                break;
                case "320":
                    if(!$(".header-column.two .translate").length){
                        $(".cs-mystart-dropdown.translate").appendTo(".header-column.two");
                    }
                break;
            }
        },

        "MyStart": function() {
            // FOR SCOPE
            var template = this;

            // BUILD USER OPTIONS DROPDOWN
            var userOptionsItems = "";

            [$IF LOGGED IN$]
                if ($(".sw-mystart-button.manage").length) {
                  $(".sw-mystart-button.manage a").attr("onclick", $(".sw-mystart-button.manage a").attr("onclick") + "return false;");
                  userOptionsItems += '<li id="user-options-manage"><a href="#" onclick="' + $(".sw-mystart-button.manage a").attr("onclick") + '"><span>Site Manager</span></a></li>';
                } // MY ACCOUNT BUTTONS


                $("#sw-myaccount-list > li:first-child a span").text("My Account");
                $("#sw-myaccount-list > li").each(function () {
                  userOptionsItems += "<li>" + $(this).html() + "</li>";
                }); // MY PASSKEYS BUTTON

                if ($("#sw-mystart-mypasskey").length) {
                  $("#sw-mystart-mypasskey").removeClass("sw-mystart-button").addClass("cs-mystart-dropdown passkeys");
                  $("#ui-btn-mypasskey").addClass("cs-dropdown-selector");
                  $("#sw-mystart-mypasskey").insertBefore(".cs-mystart-dropdown.user-options");
                }
            [$ELSE IF LOGGED$]

                // SIGNIN BUTTON
                if($(".sw-mystart-button.signin").length) {
                    userOptionsItems += "<li>" + $(".sw-mystart-button.signin").html() + "</li>";
                }

                // REGISTER BUTTON
                if($(".sw-mystart-button.register").length) {
                    userOptionsItems += "<li>" + $(".sw-mystart-button.register").html() + "</li>";
                }
            [$END IF LOGGED IN$]



            // ADD USER OPTIONS DROPDOWN TO THE DOM
            $(".cs-mystart-dropdown.user-options .cs-dropdown-list").html(userOptionsItems);

            // BIND DROPDOWN EVENTS
            this.DropdownActions({
                "dropdownParent": ".cs-mystart-dropdown.user-options",
                "dropdownSelector": ".cs-dropdown-selector",
                "dropdown": ".cs-dropdown",
                "dropdownList": ".cs-dropdown-list"
            });
        },

        "SchoolList": function() {
            // ADD SCHOOL LIST
            if(this.ShowSchoolList) {
                var schoolDropdown =    '<div class="cs-mystart-dropdown schools">' +
                                            '<div class="cs-dropdown-selector" tabindex="0" aria-label="Schools" role="button" aria-expanded="false" aria-haspopup="true">Schools</div>' +
                                            '<div class="cs-dropdown" aria-hidden="true" style="display:none;">' +
                                                '<ul class="cs-dropdown-list">' + $(".sw-mystart-dropdown.schoollist .sw-dropdown-list").html() + '</ul>' +
                                            '</div>' +
                                        '</div>';

                // ADD SCHOOL LIST TO THE DOM
                $(".cs-mystart-button.home").after(schoolDropdown);

                // BIND DROPDOWN EVENTS
                this.DropdownActions({
                    "dropdownParent": ".cs-mystart-dropdown.schools",
                    "dropdownSelector": ".cs-dropdown-selector",
                    "dropdown": ".cs-dropdown",
                    "dropdownList": ".cs-dropdown-list"
                });
            }
        },

        "Translate": function() {
            // ADD TRANSLATE
            if(this.ShowTranslate) {
                $(".cs-mystart-dropdown.translate .cs-dropdown").creativeTranslate({
                    "type": 2, // 1 = FRAMESET, 2 = BRANDED, 3 = API
                    "languages": [ // ["ENGLISH LANGUAGE NAME", "TRANSLATED LANGUAGE NAME", "LANGUAGE CODE"]
                        ["Afrikaans", "Afrikaans", "af"],
                        ["Albanian", "shqiptar", "sq"],
                        ["Amharic", "አማርኛ", "am"],
                        ["Arabic", "العربية", "ar"],
                        ["Armenian", "հայերեն", "hy"],
                        ["Azerbaijani", "Azərbaycan", "az"],
                        ["Basque", "Euskal", "eu"],
                        ["Belarusian", "Беларуская", "be"],
                        ["Bengali", "বাঙালি", "bn"],
                        ["Bosnian", "bosanski", "bs"],
                        ["Bulgarian", "български", "bg"],
                        ["Burmese", "မြန်မာ", "my"],
                        ["Catalan", "català", "ca"],
                        ["Cebuano", "Cebuano", "ceb"],
                        ["Chichewa", "Chichewa", "ny"],
                        ["Chinese Simplified", "简体中文", "zh-CN"],
                        ["Chinese Traditional", "中國傳統的", "zh-TW"],
                        ["Corsican", "Corsu", "co"],
                        ["Croatian", "hrvatski", "hr"],
                        ["Czech", "čeština", "cs"],
                        ["Danish", "dansk", "da"],
                        ["Dutch", "Nederlands", "nl"],
                        ["Esperanto", "esperanto", "eo"],
                        ["Estonian", "eesti", "et"],
                        ["Filipino", "Pilipino", "tl"],
                        ["Finnish", "suomalainen", "fi"],
                        ["French", "français", "fr"],
                        ["Galician", "galego", "gl"],
                        ["Georgian", "ქართული", "ka"],
                        ["German", "Deutsche", "de"],
                        ["Greek", "ελληνικά", "el"],
                        ["Gujarati", "ગુજરાતી", "gu"],
                        ["Haitian Creole", "kreyòl ayisyen", "ht"],
                        ["Hausa", "Hausa", "ha"],
                        ["Hawaiian", "ʻŌlelo Hawaiʻi", "haw"],
                        ["Hebrew", "עִברִית", "iw"],
                        ["Hindi", "हिंदी", "hi"],
                        ["Hmong", "Hmong", "hmn"],
                        ["Hungarian", "Magyar", "hu"],
                        ["Icelandic", "Íslenska", "is"],
                        ["Igbo", "Igbo", "ig"],
                        ["Indonesian", "bahasa Indonesia", "id"],
                        ["Irish", "Gaeilge", "ga"],
                        ["Italian", "italiano", "it"],
                        ["Japanese", "日本語", "ja"],
                        ["Javanese", "Jawa", "jw"],
                        ["Kannada", "ಕನ್ನಡ", "kn"],
                        ["Kazakh", "Қазақ", "kk"],
                        ["Khmer", "ភាសាខ្មែរ", "km"],
                        ["Korean", "한국어", "ko"],
                        ["Kurdish", "Kurdî", "ku"],
                        ["Kyrgyz", "Кыргызча", "ky"],
                        ["Lao", "ລາວ", "lo"],
                        ["Latin", "Latinae", "la"],
                        ["Latvian", "Latvijas", "lv"],
                        ["Lithuanian", "Lietuvos", "lt"],
                        ["Luxembourgish", "lëtzebuergesch", "lb"],
                        ["Macedonian", "Македонски", "mk"],
                        ["Malagasy", "Malagasy", "mg"],
                        ["Malay", "Malay", "ms"],
                        ["Malayalam", "മലയാളം", "ml"],
                        ["Maltese", "Malti", "mt"],
                        ["Maori", "Maori", "mi"],
                        ["Marathi", "मराठी", "mr"],
                        ["Mongolian", "Монгол", "mn"],
                        ["Myanmar", "မြန်မာ", "my"],
                        ["Nepali", "नेपाली", "ne"],
                        ["Norwegian", "norsk", "no"],
                        ["Nyanja", "madambwe", "ny"],
                        ["Pashto", "پښتو", "ps"],
                        ["Persian", "فارسی", "fa"],
                        ["Polish", "Polskie", "pl"],
                        ["Portuguese", "português", "pt"],
                        ["Punjabi", "ਪੰਜਾਬੀ ਦੇ", "pa"],
                        ["Romanian", "Română", "ro"],
                        ["Russian", "русский", "ru"],
                        ["Samoan", "Samoa", "sm"],
                        ["Scottish Gaelic", "Gàidhlig na h-Alba", "gd"],
                        ["Serbian", "Српски", "sr"],
                        ["Sesotho", "Sesotho", "st"],
                        ["Shona", "Shona", "sn"],
                        ["Sindhi", "سنڌي", "sd"],
                        ["Sinhala", "සිංහල", "si"],
                        ["Slovak", "slovenský", "sk"],
                        ["Slovenian", "slovenski", "sl"],
                        ["Somali", "Soomaali", "so"],
                        ["Spanish", "Español", "es"],
                        ["Sundanese", "Sunda", "su"],
                        ["Swahili", "Kiswahili", "sw"],
                        ["Swedish", "svenska", "sv"],
                        ["Tajik", "Тоҷикистон", "tg"],
                        ["Tamil", "தமிழ்", "ta"],
                        ["Telugu", "తెలుగు", "te"],
                        ["Thai", "ไทย", "th"],
                        ["Turkish", "Türk", "tr"],
                        ["Ukrainian", "український", "uk"],
                        ["Urdu", "اردو", "ur"],
                        ["Uzbek", "O'zbekiston", "uz"],
                        ["Vietnamese", "Tiếng Việt", "vi"],
                        ["Welsh", "Cymraeg", "cy"],
                        ["Western Frisian", "Western Frysk", "fy"],
                        ["Xhosa", "isiXhosa", "xh"],
                        ["Yiddish", "ייִדיש", "yi"],
                        ["Yoruba", "yorùbá", "yo"],
                        ["Zulu", "Zulu", "zu"]
                    ],
                    "advancedOptions": {
                        "addMethod": "append", // PREPEND OR APPEND THE TRANSLATE ELEMENT
                        "dropdownHandleText": "Translate", // ONLY FOR FRAMESET AND API VERSIONS AND NOT USING A CUSTOM ELEMENT
                        "customElement": { // ONLY FOR FRAMESET AND API VERSIONS
                            "useCustomElement": false,
                            "translateItemsList": true, // true = THE TRANSLATE ITEMS WILL BE AN UNORDERED LIST, false = THE TRANSLATE ITEMS WILL JUST BE A COLLECTION OF <a> TAGS
                            "customElementMarkup": "" // CUSTOM HTML MARKUP THAT MAKES THE CUSTOM TRANSLATE ELEMENT/STRUCTURE - USE [$CreativeTranslateListItems$] ACTIVE BLOCK IN THE MARKUP WHERE THE TRANSLATE ITEMS SHOULD BE ADDED
                        },
                        "apiKey": "", // ONLY FOR API VERSION
                        "brandedLayout": 1, // 1 = VERTICAL (TEXT UNDER SELECT), 2 = HORIZONTAL (TEXT BESIDE SELECT), 3 = NON-RESPONSIVE MEGA DROPDOWN
                        "removeBrandedDefaultStyling": false // 1 = VERTICAL (TEXT UNDER SELECT), 2 = HORIZONTAL (TEXT BESIDE SELECT), 3 = NON-RESPONSIVE MEGA DROPDOWN
                    },
                    "translateLoaded": function() {}
                });

                // BIND DROPDOWN EVENTS
                this.DropdownActions({
                    "dropdownParent": ".cs-mystart-dropdown.translate",
                    "dropdownSelector": ".cs-dropdown-selector",
                    "dropdown": ".cs-dropdown",
                    "dropdownList": ".cs-dropdown-list"
                });
            }
        },

        "DropdownActions": function(params) {
            // FOR SCOPE
            var template = this;

            var dropdownParent = params.dropdownParent;
            var dropdownSelector = params.dropdownSelector;
            var dropdown = params.dropdown;
            var dropdownList = params.dropdownList;

            $(dropdownParent + " " + dropdownList + " a").attr("tabindex", "-1");

            // MYSTART DROPDOWN SELECTOR CLICK EVENT
            $(dropdownParent).on("click", dropdownSelector, function(e) {
                e.preventDefault();

                if($(this).parent().hasClass("open")){
                    $("+ " + dropdownList + " a").attr("tabindex", "-1");
                    $(this).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
                } else {
                    $(this).attr("aria-expanded", "true").parent().addClass("open").find(dropdown).attr("aria-hidden","false").slideDown(300, "swing");
                }
            });

            // MYSTART DROPDOWN SELECTOR KEYDOWN EVENTS
            $(dropdownParent).on("keydown", dropdownSelector, function(e) {
                // CAPTURE KEY CODE
                switch(e.keyCode) {
                    // CONSUME LEFT AND UP ARROWS
                    case template.KeyCodes.enter:
                    case template.KeyCodes.space:
                        e.preventDefault();

                        // IF THE DROPDOWN IS OPEN, CLOSE IT
                        if($(dropdownParent).hasClass("open")){
                            $("+ " + dropdown + " " + dropdownList + " a").attr("tabindex", "-1");
                            $(this).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
                        } else {
                            $(this).attr("aria-expanded", "true").parent().addClass("open").find(dropdown).attr("aria-hidden", "false").slideDown(300, "swing", function(){
                                $(dropdownList + " li:first-child a", this).attr("tabindex", "0").focus();
                            });
                        }
                    break;

                    // CONSUME TAB KEY
                    case template.KeyCodes.tab:
                        if($("+ " + dropdown + " " + dropdownList + " a").length) {
                            $("+ " + dropdown + " " + dropdownList + " a").attr("tabindex", "-1");
                            $(this).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
                        }
                    break;

                    // CONSUME LEFT AND UP ARROWS
                    case template.KeyCodes.down:
                    case template.KeyCodes.right:
                        e.preventDefault();

                        $("+ " + dropdown + " " + dropdownList + " a").attr("tabindex", "-1");
                        $("+ " + dropdown + " " + dropdownList + " li:first-child > a", this).attr("tabindex", "0").focus();
                    break;
                }
            });

            // MYSTART DROPDOWN LINK KEYDOWN EVENTS
            $(dropdownParent).on("keydown", dropdownList + " li a", function(e) {
                // CAPTURE KEY CODE
                switch(e.keyCode) {
                    // CONSUME LEFT AND UP ARROWS
                    case template.KeyCodes.left:
                    case template.KeyCodes.up:
                        e.preventDefault();

                        // IS FIRST ITEM
                        if($(this).parent().is(":first-child")) {
                            // FOCUS DROPDOWN BUTTON
                            $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                            $(this).closest(dropdownParent).find(dropdownSelector).focus();
                        } else {
                            // FOCUS PREVIOUS ITEM
                            $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                            $(this).parent().prev("li").find("> a").attr("tabindex", "0").focus();
                        }
                    break;

                    // CONSUME RIGHT AND DOWN ARROWS
                    case template.KeyCodes.right:
                    case template.KeyCodes.down:
                        e.preventDefault();

                        // IS LAST ITEM
                        if($(this).parent().is(":last-child")) {
                            // FOCUS FIRST ITEM
                            $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                            $(this).closest(dropdownList).find("li:first-child > a").attr("tabindex", "0").focus();
                        } else {
                            // FOCUS NEXT ITEM
                            $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                            $(this).parent().next("li").find("> a").attr("tabindex", "0").focus();
                        }
                    break;

                    // CONSUME TAB KEY
                    case template.KeyCodes.tab:
                        if(e.shiftKey) {
                            e.preventDefault();

                            // FOCUS DROPDOWN BUTTON
                            $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                            $(this).closest(dropdownParent).find(dropdownSelector).focus();
                        }
                    break;

                    // CONSUME HOME KEY
                    case template.KeyCodes.home:
                        e.preventDefault();

                        // FOCUS FIRST ITEM
                        $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                        $(this).closest(dropdownList).find("li:first-child > a").attr("tabindex", "0").focus();
                    break;

                    // CONSUME END KEY
                    case template.KeyCodes.end:
                        e.preventDefault();

                        // FOCUS LAST ITEM
                        $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                        $(this).closest(dropdownList).find("li:last-child > a").attr("tabindex", "0").focus();
                    break;

                    // CONSUME ESC KEY
                    case template.KeyCodes.esc:
                        e.preventDefault();

                        // FOCUS DROPDOWN BUTTON AND CLOSE DROPDOWN
                        $(this).closest(dropdownParent).find(dropdownSelector).focus();
                        $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                        $(dropdownSelector).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
                    break;
                }
            });

            $(dropdownParent).mouseleave(function() {
                $(dropdownList + " a", this).attr("tabindex", "-1");
                $(dropdownSelector, this).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
            }).focusout(function() {
                var thisDropdown = this;

                setTimeout(function () {
                    if(!$(thisDropdown).find(":focus").length) {
                        $(dropdownSelector, thisDropdown).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
                    }
                }, 500);
            });
        },

        "Header": function() {
            // ADD LOGO
            var logoSrc = jQuery.trim('<SWCtrl controlname="Custom" props="Name:schoolLogo" />');
            var srcSplit = logoSrc.split("/");
            var srcSplitLen = srcSplit.length;
            if((logoSrc != "") && (srcSplit[srcSplitLen - 1] != "default-man.jpg")) {
                $("#gb-logo").append("<a href='/[$SITEALIAS$]'><img src='" + logoSrc + "' alt='[$SiteName$] Logo' /></a>");
            } else {
                $("#gb-logo").append("<a href='/[$SITEALIAS$]'><img width='79' height='56' src='/src/template/assets/defaults/logo.svg' alt='[$SiteName$] Logo' /></a>");
            }
        },

        "ChannelBar": function() {
            $(".sw-channel-item").off('hover');

            //FIND CHANNELS THAT HAVE ACTUAL DROPDOWNS
            $(".sw-channel-item").each(function(){
                if($(this).find(".sw-channel-dropdown").length){
                    $(this).addClass("has-dropdown");
                }
            })

            $(".sw-channel-item").hover(function(){

                if($(this).hasClass("csmc")){
                    // //GET TOP POSITION OF CHANNEL FOR MEGA DROPDOWN POSITIONING
                    var childPos = $(this).offset();
                    var parentPos = $(this).parent().offset();
                    var childOffset = {
                        top: childPos.top - parentPos.top
                    }
                    $(this).find(".sw-channel-dropdown").css("top", childOffset.top + 55 +"px");
                }

                $("ul", this).stop(true, true);
                var subList = $(this).children('.sw-channel-dropdown');
                if ($.trim(subList.html()) !== "") {
                    subList.slideDown(300, "swing");
                }
                $(this).addClass("hover");
            }, function(){
                $(".sw-channel-dropdown").slideUp(300, "swing");
                $(this).removeClass("hover");
            });
        },

        "StickyChannelBar": function() {

            var template = this;

            //ALWAYS SET THE HEADER HEIGHT BACK TO AUTO TO BE AS TALL AS IT NEEDS TO BE TO FIT THE CONTENT
            $("#gb-header").css("height", "auto");

            //FIND OUT HOW TALL THE HEADER IS AND CREATE A VARIABLE
            var headerHeight = $("#gb-header").outerHeight();

            //STICKY HEADER HEIGHT SHOULD ALWAYS BE 22px TALLER THAN THE CHANNEL BAR
            var stickyHeaderHeight = $(".header-column.two").outerHeight() + 22;

            //SINCE THE HEADER IS POSITIONED FIXED, WE NEED TO ACCOUNT FOR ITS HEIGHT AND PAD THE TOP OF THE PAGE DOWN SO NO PAGE CONTENT IS HIDDEN
            $("#gb-page").css("padding-top", headerHeight);

            //SETUP A VARIABLE FOR OUR LAST SCROLLED POSITION
            var lastScrollTop = 0;

            //FIND OUT HOW TALL THE SITENAME IS, AND HOW FAR IT IS FROM THE TOP OF THE PAGE, SO WE KNOW HOW FAR TO MOVE IT UNTIL ITS OUT OF THE VIEWPORT
            var siteNameDistanceToScroll = $("#gb-sitename").position().top + $("#gb-sitename").outerHeight();

                $(document).scroll(function() { //WHEN THE PAGE IS SCROLLED...

                    //SETUP A VARIABLE FOR THE DISTANCE THE PAGE HAS BEEN SCROLLED
                    var scrolled = $(window).scrollTop();

                    //MOVING THE SITENAME
                    //CHECK TO SEE IF WE ARE SCROLLING UP OR DOWN THE PAGE
                    if(scrolled < lastScrollTop && scrolled <= siteNameDistanceToScroll){

                        //WE ARE SCROLLING UP THE PAGE (scrolled < lastScrollTop) AND
                        //THE SITENAME IS STILL VISIBLE (scrolled <= siteNameDistanceToScroll)
                        //SO... MOVE THE SITENAME DOWN
                        $("#gb-sitename").css("top", scrolled * -1);
                    } else {
                        //WE ARE SCROLLING DOWN THE PAGE, SO CHECK TO SEE IF #gb-sitename IS IN THE VIEWPORT, IF ITS NOT, WE DONT WANT TO KEEP MOVING IT
                        if(template.CheckElementPosition($("#gb-sitename"))){

                            //IT IS IN THE VIEWPORT, SO MOVE IT UP
                            $("#gb-sitename").css("top", scrolled * -1);
                        }
                    }

                    //MOVING THE TRANSLATE BUTTON
                    //CHECK TO SEE IF WE ARE SCROLLING UP OR DOWN THE PAGE
                    if(scrolled < lastScrollTop && scrolled <= 158){
                        //WE ARE SCROLLING UP THE PAGE (scrolled < lastScrollTop) AND
                        //THE TRANSLATE IS STILL VISIBLE (scrolled <= 158) (158 = translate width + distance from right side of page)
                        //SO... MOVE THE TRANSLATE LEFT
                        $(".cs-mystart-dropdown.translate").css("right", 24 - scrolled);
                    } else { // SCROLLING DOWN THE PAGE

                        //WE ARE SCROLLING DOWN THE PAGE, SO CHECK TO SEE IF .cs-mystart-dropdown.translate IS IN THE VIEWPORT, IF ITS NOT, WE DONT WANT TO KEEP MOVING IT
                        if(template.CheckElementPosition($(".cs-mystart-dropdown.translate"))){
                            //IT IS IN THE VIEWPORT, SO MOVE IT RIGHT
                            $(".cs-mystart-dropdown.translate").css("right", 24 - scrolled);
                        }
                    }

                    //SET LASTSCROLLTOP TO BE CURRENT SCROLL POSITION
                    lastScrollTop = scrolled;

                    if(headerHeight - scrolled > stickyHeaderHeight){
                        //WE HAVE SCROLLED UP TO THE DISTANCE WHERE THE REGULAR HEADER NEEDS TO KICK IN...OR WE HAVENT SCROLLED DOWN TO THE STICKY HEADER TAKEOVER POINT YET
                        $("#gb-header").css("height", headerHeight - scrolled);
                        $("#gb-page").removeClass("sticky-header");
                    } else {
                        //WE HAVE SCROLLED TO (OR PAST) THE DISTANCE WHERE THE STICKY HEADER NEEDS TO KICK IN...
                        $("#gb-page").addClass("sticky-header");
                        $("#gb-header").css("height", stickyHeaderHeight);
                    }

                    if($(".sp").length && $(".ui-widget.app.calendar").length) {
                        if($(".wcm-controls").hasClass("wcm-stuck")) {
                             $(".wcm-controls").css("margin-top", headerHeight);
                        }
                    }

                    $("#sw-maincontent").css({
                        "display": "block",
                        "position": "relative",
                        "top": "-" + headerHeight + "px"
                    });
                });
        },

        "MegaChannelMenu": function(){
            $("#sw-channel-list-container").megaChannelMenu({
                "numberOfChannels"					: 10, // integer
                "maxSectionColumnsPerChannel"		: 3, // integer, MAX 4 COLUMNS (this does NOT include the extra, non-section column)
                "extraColumn"						: true, // boolean
                "extraColumnTitle"					: true, //bolean
                "extraColumnDescription"			: true, //bolean
                "extraColumnImage"					: true, //bolean
                "extraColumnLink"					: false, //bolean
                "extraColumnPosition"				: "ABOVE", // string, "ABOVE", "BELOW", "LEFT", "RIGHT"
                "sectionHeadings"					: false, // boolean
                "sectionHeadingLinks"				: false, // boolean
                "maxNumberSectionHeadingsPerColumn"	: 0, // integer
                "megaMenuElements"					: [
                    { //CHANNEL 1
                        "channelName"			: '<SWCtrl controlname="Custom" props="Name:csmChannel1Name" />',
                        "channelTitle"			: '<SWCtrl controlname="Custom" props="Name:csmChannel1Title" />',
                        "channelDescription"	: '<SWCtrl controlname="Custom" props="Name:csmChannel1Description" />',
                        "channelImage"			: '<SWCtrl controlname="Custom" props="Name:csmChannel1Image" />',
                        "channelImageAlt"		: '<SWCtrl controlname="Custom" props="Name:csmChannel1ImageAlt" />',
                    },
                    { //CHANNEL 2
                        "channelName"			: '<SWCtrl controlname="Custom" props="Name:csmChannel2Name" />',
                        "channelTitle"			: '<SWCtrl controlname="Custom" props="Name:csmChannel2Title" />',
                        "channelDescription"	: '<SWCtrl controlname="Custom" props="Name:csmChannel2Description" />',
                        "channelImage"			: '<SWCtrl controlname="Custom" props="Name:csmChannel2Image" />',
                        "channelImageAlt"		: '<SWCtrl controlname="Custom" props="Name:csmChannel2ImageAlt" />',
                    },
                    { //CHANNEL 3
                        "channelName"			: '<SWCtrl controlname="Custom" props="Name:csmChannel3Name" />',
                        "channelTitle"			: '<SWCtrl controlname="Custom" props="Name:csmChannel3Title" />',
                        "channelDescription"	: '<SWCtrl controlname="Custom" props="Name:csmChannel3Description" />',
                        "channelImage"			: '<SWCtrl controlname="Custom" props="Name:csmChannel3Image" />',
                        "channelImageAlt"		: '<SWCtrl controlname="Custom" props="Name:csmChannel3ImageAlt" />',
                    },
                    { //CHANNEL 4
                        "channelName"			: '<SWCtrl controlname="Custom" props="Name:csmChannel4Name" />',
                        "channelTitle"			: '<SWCtrl controlname="Custom" props="Name:csmChannel4Title" />',
                        "channelDescription"	: '<SWCtrl controlname="Custom" props="Name:csmChannel4Description" />',
                        "channelImage"			: '<SWCtrl controlname="Custom" props="Name:csmChannel4Image" />',
                        "channelImageAlt"		: '<SWCtrl controlname="Custom" props="Name:csmChannel4ImageAlt" />',
                    },
                    { //CHANNEL 5
                        "channelName"			: '<SWCtrl controlname="Custom" props="Name:csmChannel5Name" />',
                        "channelTitle"			: '<SWCtrl controlname="Custom" props="Name:csmChannel5Title" />',
                        "channelDescription"	: '<SWCtrl controlname="Custom" props="Name:csmChannel5Description" />',
                        "channelImage"			: '<SWCtrl controlname="Custom" props="Name:csmChannel5Image" />',
                        "channelImageAlt"		: '<SWCtrl controlname="Custom" props="Name:csmChannel5ImageAlt" />',
                    },
                    { //CHANNEL 6
                        "channelName"			: '<SWCtrl controlname="Custom" props="Name:csmChannel6Name" />',
                        "channelTitle"			: '<SWCtrl controlname="Custom" props="Name:csmChannel6Title" />',
                        "channelDescription"	: '<SWCtrl controlname="Custom" props="Name:csmChannel6Description" />',
                        "channelImage"			: '<SWCtrl controlname="Custom" props="Name:csmChannel6Image" />',
                        "channelImageAlt"		: '<SWCtrl controlname="Custom" props="Name:csmChannel6ImageAlt" />',
                    },
                    { //CHANNEL 7
                        "channelName"			: '<SWCtrl controlname="Custom" props="Name:csmChannel7Name" />',
                        "channelTitle"			: '<SWCtrl controlname="Custom" props="Name:csmChannel7Title" />',
                        "channelDescription"	: '<SWCtrl controlname="Custom" props="Name:csmChannel7Description" />',
                        "channelImage"			: '<SWCtrl controlname="Custom" props="Name:csmChannel7Image" />',
                        "channelImageAlt"		: '<SWCtrl controlname="Custom" props="Name:csmChannel7ImageAlt" />',
                    },
                    { //CHANNEL 8
                        "channelName"			: '<SWCtrl controlname="Custom" props="Name:csmChannel8Name" />',
                        "channelTitle"			: '<SWCtrl controlname="Custom" props="Name:csmChannel8Title" />',
                        "channelDescription"	: '<SWCtrl controlname="Custom" props="Name:csmChannel8Description" />',
                        "channelImage"			: '<SWCtrl controlname="Custom" props="Name:csmChannel8Image" />',
                        "channelImageAlt"		: '<SWCtrl controlname="Custom" props="Name:csmChannel8ImageAlt" />',
                    },
                    { //CHANNEL 9
                        "channelName"			: '<SWCtrl controlname="Custom" props="Name:csmChannel9Name" />',
                        "channelTitle"			: '<SWCtrl controlname="Custom" props="Name:csmChannel9Title" />',
                        "channelDescription"	: '<SWCtrl controlname="Custom" props="Name:csmChannel9Description" />',
                        "channelImage"			: '<SWCtrl controlname="Custom" props="Name:csmChannel9Image" />',
                        "channelImageAlt"		: '<SWCtrl controlname="Custom" props="Name:csmChannel9ImageAlt" />',
                    },
                    { //CHANNEL 10
                        "channelName"			: '<SWCtrl controlname="Custom" props="Name:csmChannel10Name" />',
                        "channelTitle"			: '<SWCtrl controlname="Custom" props="Name:csmChannel10Title" />',
                        "channelDescription"	: '<SWCtrl controlname="Custom" props="Name:csmChannel10Description" />',
                        "channelImage"			: '<SWCtrl controlname="Custom" props="Name:csmChannel10Image" />',
                        "channelImageAlt"		: '<SWCtrl controlname="Custom" props="Name:csmChannel10ImageAlt" />',
                    }
                ],
                "allLoaded"						   : function allLoaded() {
                    //LOOP THROUGH EACH MEGA CHANNEL
                    $(".sw-channel-item.csmc").each(function(){
                        //IF THE NUMBER OF SECTIONS = 2, ADD A CLASS TO THE CHANNEL
                        if($(".csmc-section", this).length == 2){
                            $(this).addClass("two-sections");
                        }
                    })
                }
            })

        },

        "Subpage": function(){
            //FOR SCOPE
            var template = this;

            //SETUP PAGE HEADER
            //if($("#sw-content-layout-wrapper").css("background-image") != "none"){ //THERE IS A HEADER BANNER IMAGE
            if(true){
                //var imgURL = $("#sw-content-layout-wrapper").css("background-image");
                var imgURL = 'src/template/assets/defaults/sp-header-bg.jpg';
                $(".page-title").prepend("<img src='"+imgURL+"' alt='' />").addClass("has-bg-img");

                // ADD PAGE NAME
                if(!$("[id*='pagenavigation'].active").length){
                	if(!$(".app.headlines.detail").length){
                    	var pageName = $("title").text().split("/");
                        pageName = $.trim(pageName[1]);
                    	$('<div class="title-text"><h2>'+pageName+'</h2></div>').appendTo(".page-title");
                    }
                } else {
                    $('<div class="title-text"><h2>'+$("[id*='pagenavigation'].active > a span").text()+'</h2></div>').appendTo(".page-title");
                }
            }

            var useSocial = <SWCtrl controlname="Custom" props="Name:useSocialSharing" />;
            if( useSocial ){
            	//DEFAULT'S AS FOUND IN THE PLUGIN, AND EVERY PARAMETER AVAILABLE
                $('#gb-page').socialSharingButtons({
                    "placementSelector"   : ".social-sharing", //where you want to place the buttons
                    "placementMethod"     : "prepend",
                    "buttons"             : [ //social-media-icon-name
                        "facebook",
                        "twitter",
                        "linkedin",
                        "email",
                        "print"
                    ]
                });
            }
        },

        "Body": function() {
            // FOR SCOPE
            var template = this;

            // AUTO FOCUS SIGN IN FIELD
            $("#swsignin-txt-username").focus();

            // APPLY RESPONSIVE DIMENSIONS TO CONTENT IMAGES
            $(".ui-widget.app .ui-widget-detail img")
                .not($(".ui-widget.app.multimedia-gallery .ui-widget-detail img"))
                .each(function() {
                    if ($(this).attr('width') !== undefined && $(this).attr('height') !== undefined) { // IMAGE HAS INLINE DIMENSIONS
                        $(this).css({"display": "inline-block", "width": "100%", "max-width": $(this).attr("width") + "px", "height": "auto", "max-height": $(this).attr("height") + "px"});
                    }
            });

            // ADJUST FIRST BREADCRUMB
            $("li.ui-breadcrumb-first > a > span").text("Home").show();

            //RSS BUTTON TEXT
            $(".ui-btn-toolbar.rss span").text("RSS");

            // CHECK PAGELIST HEADER
            if($.trim($(".ui-widget.app.pagenavigation .ui-widget-header").text()) == "") {
                $(".ui-widget.app.pagenavigation .ui-widget-header").html("<h1>[$ChannelName$]</h1>");
            }

            //HOVERS
            $(".headlines .ui-article-title a, .upcomingevents a, .navigation a").hover(function() {
                $(this).closest("li").addClass("hover");
            }, function() {
                $(this).closest("li").removeClass("hover");
            });

            $(".headlines .ui-article-title a, .upcomingevents a, .navigation a").focus(function() {
                $(this).closest("li").addClass("hover");
            });

            $(".headlines .ui-article-title a, .upcomingevents a, .navigation a").focusout(function() {
                $(this).closest("li").removeClass("hover");
            });


            //HOVERS
            $(".fr-row.two a").hover(function() {
                $(this).parent().addClass("hover");
            }, function() {
                $(this).parent().removeClass("hover");
            });

            $(".fr-row.two a").focus(function() {
                $(this).parent().addClass("hover");
            });

            $(".fr-row.two a").focusout(function() {
                $(this).parent().removeClass("hover");
            });

            //REMOVE UNUSED ELEMENTS
            $("[data-content=''], [data-toggle='false']").remove();


        },

        "Homepage": function() {
            // FOR SCOPE
            var template = this;

            // ADD STATS SECTION BACKGROUND IMAGE
            var statSrc = jQuery.trim('<SWCtrl controlname="Custom" props="Name:statSectionBg" />');
            var srcSplit = statSrc.split("/");
            var srcSplitLen = srcSplit.length;
            if((statSrc != "") && (srcSplit[srcSplitLen - 1] != "default-man.jpg")) {
                $(".hp-row.six").css("background-image","url('" + statSrc + "')");
            } else {
                $(".hp-row.six").css("background-image","url('/src/template/assets/defaults/stats-bg.jpg')");
            }

            // ADD FEATURED REPORT IMAGE
            var frSrc = jQuery.trim('<SWCtrl controlname="Custom" props="Name:frImage" />');
            var srcSplit = frSrc.split("/");
            var srcSplitLen = srcSplit.length;
            if((frSrc != "") && (srcSplit[srcSplitLen - 1] != "default-man.jpg")) {
                $(".fr-image").append("<img src='" + frSrc + "' alt='Featured Report Cover' />");
            } else {
                $(".fr-image").append("<img src='/src/template/assets/defaults/featured-report.png' alt='Featured Report Cover' />");
            }

            //ARTICLE SLIDERS
            var numHeadlines = 3;
            var useTabbedSocial = <SWCtrl controlname="Custom" props="Name:useRegionCTabbedSocial" />;

            if(useTabbedSocial != true){
                numHeadlines = 4;
            }

            $(".hp-row.three .headlines").csArticleSlider({
                "viewerNum": [numHeadlines, 1, 1, 1, 1], // NUMERICAL - [960 view, 768 view, 640 view, 480 view, 320 view]
            	"transitionSpeed": .5, // SECONDS
            	"slideFullView": false,
                "extend": function(config, element, ArticleSlider) {}
            });

            $(".hp-row.five .headlines").csArticleSlider({
                "viewerNum": [3, 1, 1, 1, 1], // NUMERICAL - [960 view, 768 view, 640 view, 480 view, 320 view]
            	"transitionSpeed": .5, // SECONDS
            	"slideFullView": false,
                "extend": function(config, element, ArticleSlider) {}
            });

            $(".hp-row .upcomingevents").csArticleSlider({
                "viewerNum": [4, 2, 2, 1, 1], // NUMERICAL - [960 view, 768 view, 640 view, 480 view, 320 view]
            	"transitionSpeed": .5, // SECONDS
            	"slideFullView": false,
                "extend": function(config, element, ArticleSlider) {}
            });

            //MOVE MORE LINKS
            $(".app").each(function(){
                var thisApp = $(this);

                if($(thisApp).find(".cs-article-slider").length){ //IF THERE IS A SLIDER IN THE APP...

                    //CREATE A NEW CONTAINER AND PLACE IT IN THE APP HEADER
                    $(thisApp).find(".ui-widget-header").append("<div class='cs-header-text-wrap'></div>");

                    //GRAB THE H1 FROM THE APP AND PLACE IT AS THE LAST ITEM IN THE NEW CONTAINER
                    $(thisApp).find(".ui-widget-header h1").appendTo($(thisApp).find(".cs-header-text-wrap"));

                    if($(thisApp).find(".more-link, .view-calendar-link, .ui-btn-toolbar.rss").length){ //IF THERE IS A MORE LINK OR A VIEW CALENDAR LINK IN THE APP...

                        //LOOK FOR THE MORE LINK OR THE VIEW CALENDAR LINK WITHIN THE APP AND PLACE THEM AS THE LAST ITEM IN THE NEW CONTAINER
                        $(thisApp).find(".more-link, .view-calendar-link, .ui-btn-toolbar.rss").appendTo($(thisApp).find(".cs-header-text-wrap"));

                        //ADD A CLASS TO THE HEADER FOR STYLING
                        $(thisApp).find(".ui-widget-header").addClass("has-extra-link");
                    }

                    //CREATE A NEW CONTAINER AND PLACE IT IN THE APP HEADER
                    $(thisApp).find(".ui-widget-header").append("<div class='cs-slider-controls'></div>");

                    //GRAB THE SLIDER CONTROL BUTTONS AND PLACE THEM IN THE NEW CONTAINER
                    $(thisApp).find(".cs-article-slider-control").appendTo($(thisApp).find(".cs-slider-controls"));

                    //ADD A CLASS TO THE HEADER FOR STYLING
                    $(thisApp).find(".ui-widget-header").addClass("has-slider-controls");
                } else {
                    if($(thisApp).find(".more-link").length){ //IF THERE IS A MORE LINK OR A VIEW CALENDAR LINK IN THE APP...

                        //LOOK FOR THE MORE LINK OR THE VIEW CALENDAR LINK WITHIN THE APP AND PLACE THEM AS THE LAST ITEM IN THE NEW CONTAINER
                        $(thisApp).find(".more-link").prependTo($(thisApp).find(".ui-widget-footer"));

                        //ADD A CLASS TO THE HEADER FOR STYLING
                        $(thisApp).find(".ui-widget-footer").addClass("has-extra-link");
                    }
                }
            })

            //ADD EXTRA TEXT TO REGION B HEADLINES APP
            $(".hp-row.three .headlines .cs-header-text-wrap").append('<span class="cs-header-sub-text"><SWCtrl controlname="Custom" props="Name:regionBHeadlinesHeaderSubText" /></span>');

            //MOVE HEADLINES TEASER TEXT IN REGION D
            $(".hp-row.four .headlines li").each(function(){ //LOOP THROUGH EACH LIST ITEM

                //FIND THE UI-ARTICLE-DESCRIPTION AND MOVE IT INTO THE IMAGE CONTAINER
                $(this).find(".ui-article-description").appendTo($(this).find(".img"));
            })

            //TABBED SOCIAL MEDIA
            $(".hp-row.three .hp-column.two").socialTabs({
                "useAppNameInTab"   : true, // boolean
                "useImagesForTabs"  : false, // boolean
                "tabImages"         : [],
                "allLoaded"         : function() {
                    $(".hp-row.three[data-tabbed-social=true] .hp-column.two").css("opacity", "1");
                }
            });

        },

        "CheckElementPosition": function(element){

            var template = this;

            //CHECK IF ELEMENT IS IN VIEWPORT
            var elementTop = $(element).offset().top;
            var elementLeft = $(element).offset().left;
            var elementBottom = elementTop + $(element).outerHeight();
            var viewportTop = $(window).scrollTop();
            var viewportBottom = viewportTop + $(window).height();
            var viewportWidth = $(window).width();

            return elementBottom > viewportTop && elementTop < viewportBottom && elementLeft < viewportWidth;

        },

        "EntranceAnimations": function() {

            var template = this;

            $('.hp-row.six').each(function() {
                if (template.CheckElementPosition($(this))) {
                     if(!$(this).hasClass("nowucme")){
                        $(this).addClass("nowucme");

                        $(this).find("li").each(function () {
                        	if($(this).attr("data-icon-animate") == "true"){
                                var textAnimating = $(this).find(".anim-text");

                                textAnimating.prop('Counter',0).animate({
                                    Counter: textAnimating.text()
                                }, {
                                    duration: 3000,
                                    easing: 'swing',
                                    step: function (now) {
                                        textAnimating.text(Math.ceil(now));
                                    }
                                });
                            }
                        })
                    }
                }
            });
          },

        "Shortcuts": function() {
            var template = this;

            // EDIT THESE TWO VARS
            var columnNums = [4, 2, 2, 1, 1]; // [960, 768, 640, 480, 320]
            var selector = ".hp-row .siteshortcuts";

            // RETURN BREAKPOINT INDEX
            var bp = function() {
                switch(template.GetBreakPoint()) {
                    case "desktop": return 0; break;
                    case "768": return 1; break;
                    case "640": return 2; break;
                    case "480": return 3; break;
                    case "320": return 4; break;
                }
            }

            // SET COLUMN NUM AND OTHER VARS
            var columnNum = columnNums[bp()];
            var endRange;

            $(selector).each(function() {
                // CHECK FOR ROW 3

                if($(this).closest(".hp-row").hasClass("three") && $(this).closest(".hp-row").attr("data-tabbed-social") == "true"){

                    // SET COLUMN NUM AND OTHER VARS
                    columnNums = [3, 2, 2, 1, 1];
                    columnNum = columnNums[bp()];
                } else {
                    // SET COLUMN NUM AND OTHER VARS
                    columnNums = [4, 2, 2, 1, 1];
                    columnNum = columnNums[bp()];
                }

                // RETURN THE LI'S TO THE ORIGINAL UL
                $(".ui-widget-detail > ul.site-shortcuts", this).append($(".site-shortcuts-column > li", this));

                // REMOVE COLUMN CONTAINER FOR REBUILD
                $(".site-shortcuts-columns", this).remove();

                // GET SHORTCUT NUM
                var shortcutNum = $(".ui-widget-detail > ul.site-shortcuts > li", this).length;

                // ADD COLUMN CONTAINER
                $(".ui-widget-detail", this).prepend('<div class="site-shortcuts-columns"></div>');

                // LOOP TO BUILD COLUMNS
                for(var i = 0; i < columnNum; i++) {
                    // KEEP FROM ADDING EMPTY UL'S TO THE DOM
                    if(i < shortcutNum) {
                        // IF shortcutNum / columnNum REMAINDER IS BETWEEN .0 AND .5 AND THIS IS THE FIRST LOOP ITERATION
                        // WE'LL ADD 1 TO THE END RANGE SO THAT THE EXTRA LINK GOES INTO THE FIRST COLUMN

                        if((shortcutNum / columnNum) % 1 > 0.0 && (shortcutNum / columnNum) % 1 < 0.5 && i == 0) {
                           endRange = Math.round(shortcutNum / columnNum) + 1;
                       } else if((shortcutNum / columnNum) % 1 == 0.5 && i >= (columnNum / i)) {
                           endRange = Math.round(shortcutNum / columnNum) - 1;
                       } else {
                           endRange = Math.round(shortcutNum / columnNum)
                       }

                        // ADD THE COLUMN UL
                        $(".site-shortcuts-columns", this).append('<ul class="site-shortcuts-column column-' + (i + 1) + '"></ul>');

                        // MOVE THE RANGE OF LI'S TO THE COLUMN UL
                        $(".site-shortcuts-column.column-" + (i + 1), this).append($(".ui-widget-detail > ul.site-shortcuts > li:nth-child(n+1):nth-child(-n+" + endRange +")", this));
                    }
                }

                // HIDE THE ORIGINAL UL
                $(".ui-widget-detail > ul.site-shortcuts", this).hide();
            });
        },

        "Footer": function() {
            // FOR SCOPE
            var template = this;

            $(document).csBbFooter({
                'footerContainer'   : '.footer-column.one',
                'useDisclaimer'     : false,
                'disclaimerText'    : ''
            });


            //BACK TO TOP
            $(".to-top").click(function(e){
                e.preventDefault();
                        $("html, body").animate({ scrollTop: 0 }, "slow", function() {
                            $("li.sw-channel-item:first-child > a").focus();
                        });
            });

        },

        "Slideshow": function() {
            // FOR SCOPE
            var template = this;

            if($("#sw-content-container10 .ui-widget.app.multimedia-gallery").length) {
                this.MMGPlugin();
            }
        },

        "Test": function(){
                var template = this;

                console.log($(".mmg-description").outerHeight());
        },

        "MMGPlugin": function() {
            // FOR SCOPE
            var template = this;

            var mmg = eval("multimediaGallery" + $("#sw-content-container10 .ui-widget.app.multimedia-gallery:first").attr("data-pmi"));
            mmg.props.defaultGallery = false;

            $("#sw-content-container10 .ui-widget.app.multimedia-gallery:first").csMultimediaGallery({
                "efficientLoad" : true,
                "imageWidth" : 910,
                "imageHeight" : 550,
                "mobileDescriptionContainer": [960, 768, 640, 480, 320], // [960, 768, 640, 480, 320]
                "galleryOverlay" : false,
                "linkedElement" : [],  // ["image", "title", "overlay"]
                "playPauseControl" : true,
                "backNextControls" : true,
                "bullets" : false,
                "thumbnails" : false,
                "thumbnailViewerNum": [3, 4, 3, 3, 2], // NUMERICAL - [960 view, 768 view, 640 view, 480 view, 320 view]
                "autoRotate" : true,
                "hoverPause" : true,
                "transitionType" : "slide",
                "transitionSpeed" : <SWCtrl controlname="Custom" props="Name:transitionSpeed" />,
                "transitionDelay" : <SWCtrl controlname="Custom" props="Name:transitionDelay" />,
                "fullScreenRotator" : false,
                "fullScreenBreakpoints" : [960], // NUMERICAL - [960, 768, 640, 480, 320]
                "onImageLoad" : function(props) {}, // props.element, props.recordIndex, props.mmgRecords
                "allImagesLoaded" : function(props) {}, // props.element, props.mmgRecords
                "onTransitionStart" : function(props) {
                    $(".mmg-description-link", props.element).wrapInner("<span></span>");
                    $(".mmg-description-outer", props.element).removeClass("too-tall");

                    var currentSlideDescriptionHeight = $(".mmg-description-outer .mmg-description", props.element).outerHeight();
                    var slideshowHeight = $(".mmg-container", props.element).outerHeight();

                    if(currentSlideDescriptionHeight > slideshowHeight){
                        $(".mmg-description-outer", props.element).addClass("too-tall");
                    }

                },
                "onTransitionEnd" : function(props) {
                    // var currentSlideDescriptionHeight = $(".mmg-description-outer .mmg-description", props.element).outerHeight();
                    // var slideshowHeight = $(".mmg-container", props.element).outerHeight();
                    //
                    // if(currentSlideDescriptionHeight <= slideshowHeight){
                    //     $(".mmg-description-outer", props.element).removeClass("too-tall");
                    // }

                }, // props.element, props.currentRecordIndex, props.currentGalleryIndex, props.mmgRecords
                "allLoaded" : function(props) {



                    $(".mmg-control", props.element).wrapAll("<div class='cs-mmg-control-wrapper'></div>");
                    $(".mmg-description-link", props.element).wrapInner("<span></span>");

                    //SET HEIGHT OF CONTAINER
                    $(".mmg-container", props.element).height($(".mmg-slides", props.element).outerHeight());

                    var currentSlideDescriptionHeight = $(".mmg-description-outer .mmg-description", props.element).outerHeight();
                    var slideshowHeight = $(".mmg-container", props.element).outerHeight();

                    if(currentSlideDescriptionHeight > slideshowHeight){
                        $(".mmg-description-outer", props.element).addClass("too-tall");
                    }
                }, // props.element, props.mmgRecords
                "onWindowResize": function(props) {
                    $(".mmg-description-outer", props.element).removeClass("too-tall");

                    //SET HEIGHT OF CONTAINER
                    $(".mmg-container", props.element).height($(".mmg-slides", props.element).outerHeight());

                    var currentSlideDescriptionHeight = $(".mmg-description-outer .mmg-description", props.element).outerHeight();
                    var slideshowHeight = $(".mmg-container", props.element).outerHeight();

                    if(currentSlideDescriptionHeight > slideshowHeight){
                        $(".mmg-description-outer", props.element).addClass("too-tall");
                    }
                } // props.element, props.mmgRecords
            });
        },

        "CheckSlideshow": function() {
            // FOR SCOPE
            var template = this;

            if($(".hp").length && this.GetBreakPoint() != "desktop" && '<SWCtrl controlname="Custom" props="Name:slideshowFeature" />' != "Streaming Video") {
                if ($(window).scrollTop() <= $("#hp-slideshow").offset().top + $("#hp-slideshow").height()) {
                    if(this.SlideshowDescFixed) {
                        $("#hp-slideshow").removeAttr("style");

                        this.SlideshowDescFixed = false;
                    }
                } else {
                    if(!this.SlideshowDescFixed) {
                        $("#hp-slideshow").css({
                            "height": $("#hp-slideshow").height(),
                            "overflow": "hidden"
                        });

                        this.SlideshowDescFixed = true;
                    }
                }
            }
        },

        "ModEvents": function() {
            // FOR SCOPE
            var template = this;

            // $(".ui-widget.app.upcomingevents").modEvents({
            //     "columns"       : true, // boolean
            //     "monthLong"     : true, // boolean
            //     "dayWeek"       : true, // boolean
            //     "groupByDay"    : true, // boolean
            //     "addZero"       : true, // boolean
            //     "limitEvents"   : true, // boolean
            //     "numEvents"     : 2, // integer
            //     "allLoaded"     : function() {}
            //
            // });

            $(".ui-widget.app.upcomingevents").modEvents({
                columns     : "yes",
                monthLong   : "yes",
                dayWeek     : "yes"
            });

            eventsByDay(".upcomingevents .ui-articles");

            function eventsByDay(container) {
                $(".ui-article", container).each(function(){
                    if (!$(this).find("h1.ui-article-title.sw-calendar-block-date").size()){
                        var moveArticle = $(this).html();
                        $(this).parent().prev().children().children().next().append(moveArticle);
                        $(this).parent().remove();
                    };
                });

                $(".ui-article", container).each(function(){
                    var newDateTime = $('.upcoming-column.left h1', this).html().toLowerCase().split("</span>");
                    var newDate = '';
                    var dateIndex = ((newDateTime.length > 2) ? 2 : 1); //if the dayWeek is set to yes we need to account for that in our array indexing

                    //if we have the day of the week make sure to include it.
                    if( dateIndex > 1 ){
                        newDate += newDateTime[0] + "</span>";
                    }

                    //add in the month
                    newDate += newDateTime[dateIndex - 1] + '</span>'; //the month is always the in the left array position to the date

                    //wrap the date in a new tag
                    newDate += '<span class="jeremy-date">' + newDateTime[dateIndex] + '</span>';

                    //append the date and month back into their columns
                    $('.upcoming-column.left h1', this).html(newDate);


                    //add an ALL DAY label if no time was given
                    $('.upcoming-column.right .ui-article-description', this).each(function(){
                        if( $('.sw-calendar-block-time', this).length < 1 ){ //if it doesnt exist add it
                            $(this).prepend('<span class="sw-calendar-block-time">ALL DAY</span>');
                        }
                        $(".sw-calendar-block-time", this).appendTo($(this));
                    });

                    //WRAP DATE AND MONTH IN A CONTAINER
                    $(".jeremy-date, .joel-day", this).wrapAll("<span class='adam-hug'></span>");

                    var eventDay = $.trim($(this).find(".joel-day").html().toLowerCase());
                    var eventMonth = $.trim($(this).find(".joel-month").text().toLowerCase());
                    var eventNumberDay = $.trim($(this).find(".jeremy-date").text());
                    var ariaDate = "";

                    switch(eventNumberDay){
                        case "1":
                            ariaDate = "First";
                        break;
                        case "2":
                            ariaDate = "Second";
                        break;
                        case "3":
                            ariaDate = "Third";
                        break;
                        case "4":
                            ariaDate = "Fourth";
                        break;
                        case "5":
                            ariaDate = "Fifth";
                        break;
                        case "6":
                            ariaDate = "Sixth";
                        break;
                        case "7":
                            ariaDate = "Seventh";
                        break;
                        case "8":
                            ariaDate = "Eighth";
                        break;
                        case "9":
                            ariaDate = "Ninth";
                        break;
                        case "10":
                            ariaDate = "Tenth";
                        break;
                        case "11":
                            ariaDate = "Eleventh";
                        break;
                        case "12":
                            ariaDate = "Twelveth";
                        break;
                        case "13":
                            ariaDate = "Thirteenth";
                        break;
                        case "14":
                            ariaDate = "Fourteenth";
                        break;
                        case "15":
                            ariaDate = "Fifteenth";
                        break;
                        case "16":
                            ariaDate = "Sixteenth";
                        break;
                        case "17":
                            ariaDate = "Seventeenth";
                        break;
                        case "18":
                            ariaDate = "Eighteenth";
                        break;
                        case "19":
                            ariaDate = "Nineteenth";
                        break;
                        case "20":
                            ariaDate = "Twentieth";
                        break;
                        case "21":
                            ariaDate = "Twenty-first";
                        break;
                        case "22":
                            ariaDate = "Twenty-second";
                        break;
                        case "23":
                            ariaDate = "Twenty-third";
                        break;
                        case "24":
                            ariaDate = "Twenty-fourth";
                        break;
                        case "25":
                            ariaDate = "Twenty-fifth";
                        break;
                        case "26":
                            ariaDate = "Twenty-sixth";
                        break;
                        case "27":
                            ariaDate = "Twenty-seventh";
                        break;
                        case "28":
                            ariaDate = "Twenty-eighth";
                        break;
                        case "29":
                            ariaDate = "Twenty-ninth";
                        break;
                        case "30":
                            ariaDate = "Thirtieth";
                        break;
                        case "31":
                            ariaDate = "Thirty-first";
                        break;
                        default:
                            ariaDate = eventNumberDay;
                        break;
                    }

                    //ADD MORE EVENTS LINK
                    if($(this).find(".ui-article-description").length > 2){
                        var moreEventsLink = $(this).find(".ui-article-description:nth-child(1) a").attr("href");
                        moreEventsLink = moreEventsLink.split("/").slice(0, -2).join("/");
                        moreEventsLink = moreEventsLink+"/day";

                        $(this).find(".upcoming-column.right").append("<a href='"+moreEventsLink+"' target='blank' aria-label='View all events for "+eventDay+", "+eventMonth+" "+ariaDate+".' class='all-events-link'></>");
                    }
                })
            }

            //ADD NO EVENTS TEXT
            $(".upcomingevents").each(function(){
                if(!$(this).find(".ui-article").length){
                    $("<li><p>There are no upcoming events to display.</p></l1>").appendTo($(this).find(".ui-articles"));
                }
            });

            //IF ON SUBPAGE
            if($("#gb-page").hasClass("sp")){
                //LOOP THROUGH EACH EVENT DAY
                $(".upcomingevents .ui-article").each(function(){
                    //FIND THE DAY & PREPEND IT TO THE FIRST EVENT DESCRIPTION
                    $(this).find(".joel-day").prependTo($(this).find(".ui-article-description:first-child"));
                })
            }

        },

        "GlobalIcons": function() {
            $("#gb-icons .content-wrap").creativeIcons({
                "iconNum"       : '<SWCtrl controlname="Custom" props="Name:numOfIcons" />',
                "defaultIconSrc": "",
                "icons"         : [
                    {
                        "image": '<SWCtrl controlname="Custom" props="Name:Icon1Image" />',
                        "showText": true,
                        "text": '<SWCtrl controlname="Custom" props="Name:Icon1Text" />',
                        "url": '<SWCtrl controlname="Custom" props="Name:Icon1Link" />',
                        "target": '<SWCtrl controlname="Custom" props="Name:Icon1Target" />'
                    },
                    {
                        "image": '<SWCtrl controlname="Custom" props="Name:Icon2Image" />',
                        "showText": true,
                        "text": '<SWCtrl controlname="Custom" props="Name:Icon2Text" />',
                        "url": '<SWCtrl controlname="Custom" props="Name:Icon2Link" />',
                        "target": '<SWCtrl controlname="Custom" props="Name:Icon2Target" />'
                    },
                    {
                        "image": '<SWCtrl controlname="Custom" props="Name:Icon3Image" />',
                        "showText": true,
                        "text": '<SWCtrl controlname="Custom" props="Name:Icon3Text" />',
                        "url": '<SWCtrl controlname="Custom" props="Name:Icon3Link" />',
                        "target": '<SWCtrl controlname="Custom" props="Name:Icon3Target" />'
                    },
                    {
                        "image": '<SWCtrl controlname="Custom" props="Name:Icon4Image" />',
                        "showText": true,
                        "text": '<SWCtrl controlname="Custom" props="Name:Icon4Text" />',
                        "url": '<SWCtrl controlname="Custom" props="Name:Icon4Link" />',
                        "target": '<SWCtrl controlname="Custom" props="Name:Icon4Target" />'
                    },
                    {
                        "image": '<SWCtrl controlname="Custom" props="Name:Icon5Image" />',
                        "showText": true,
                        "text": '<SWCtrl controlname="Custom" props="Name:Icon5Text" />',
                        "url": '<SWCtrl controlname="Custom" props="Name:Icon5Link" />',
                        "target": '<SWCtrl controlname="Custom" props="Name:Icon5Target" />'
                    },
                    {
                        "image": '<SWCtrl controlname="Custom" props="Name:Icon6Image" />',
                        "showText": true,
                        "text": '<SWCtrl controlname="Custom" props="Name:Icon6Text" />',
                        "url": '<SWCtrl controlname="Custom" props="Name:Icon6Link" />',
                        "target": '<SWCtrl controlname="Custom" props="Name:Icon6Target" />'
                    }
                ],
                "siteID"        : "[$SiteID$]",
                "siteAlias"     : "[$SiteAlias$]",
                "calendarLink"  : "[$SiteCalendarLink$]",
                "contactEmail"  : "[$SiteContactEmail$]",
                "allLoaded"     : function(){}
            });

        },

        "SocialIcons": function() {
            var socialIcons = [
                {
                    "show": <SWCtrl controlname="Custom" props="Name:showFacebook" />,
                    "label": "Facebook",
                    "class": "facebook",
                    "url": '<SWCtrl controlname="Custom" props="Name:FacebookUrl" />',
                    "target": '<SWCtrl controlname="Custom" props="Name:FacebookTarget" />'
                },
                {
                    "show": <SWCtrl controlname="Custom" props="Name:showTwitter" />,
                    "label": "Twitter",
                    "class": "twitter",
                    "url": '<SWCtrl controlname="Custom" props="Name:TwitterUrl" />',
                    "target": '<SWCtrl controlname="Custom" props="Name:TwitterTarget" />'
                },
                {
                    "show": <SWCtrl controlname="Custom" props="Name:showVimeo" />,
                    "label": "Vimeo",
                    "class": "vimeo",
                    "url": '<SWCtrl controlname="Custom" props="Name:VimeoUrl" />',
                    "target": '<SWCtrl controlname="Custom" props="Name:VimeoTarget" />'
                },
                {
                    "show": <SWCtrl controlname="Custom" props="Name:showYouTube" />,
                    "label": "YouTube",
                    "class": "youtube",
                    "url": '<SWCtrl controlname="Custom" props="Name:YouTubeUrl" />',
                    "target": '<SWCtrl controlname="Custom" props="Name:YouTubeTarget" />'
                },
                {
                    "show": <SWCtrl controlname="Custom" props="Name:showInstagram" />,
                    "label": "Instagram",
                    "class": "instagram",
                    "url": '<SWCtrl controlname="Custom" props="Name:InstagramUrl" />',
                    "target": '<SWCtrl controlname="Custom" props="Name:InstagramTarget" />'
                }
            ];

            var icons = '';
            $.each(socialIcons, function(index, icon) {
                if(icon.show) {
                    icons += '<li><a class="gb-social-media-icon ' + icon.class + ' district" href="' + icon.url + '" target="' + icon.target + '" aria-label="' + icon.label + '"></a></li>';
                }
            });

            if(icons.length) {
                $(".social-icons").prepend(icons);
            }
        },

        "RsMenu": function() {
            // FOR SCOPE
            var template = this;

            $.csRsMenu({
              "breakPoint" : 768, // SYSTEM BREAK POINTS - 768, 640, 480, 320
              "slideDirection" : "left-to-right", // OPTIONS - left-to-right, right-to-left
              "menuButtonParent" : "#gb-channel-bar",
              "menuBtnText" : "MENU",
              "colors": {
                  "pageOverlay": "#000000", // DEFAULT #000000
                  "menuBackground": "#FFFFFF", // DEFAULT #FFFFFF
                  "menuText": "#333333", // DEFAULT #333333
                  "menuTextAccent": "#333333", // DEFAULT #333333
                  "dividerLines": "#E6E6E6", // DEFAULT #E6E6E6
                  "buttonBackground": "#E6E6E6", // DEFAULT #E6E6E6
                  "buttonText": "#333333" // DEFAULT #333333
              },
              "showDistrictHome": false,
              "districtHomeText": "District",
              "showSchools" : false,
              "schoolMenuText": "Schools",
              "showTranslate" : true,
              "translateMenuText": "Translate",
              "translateVersion": 2, // 1 = FRAMESET, 2 = BRANDED
              "translateId" : "",
              "translateLanguages": [ // ["ENGLISH LANGUAGE NAME", "TRANSLATED LANGUAGE NAME", "LANGUAGE CODE"]
                  ["Afrikaans", "Afrikaans", "af"],
                  ["Albanian", "shqiptar", "sq"],
                  ["Amharic", "አማርኛ", "am"],
                  ["Arabic", "العربية", "ar"],
                  ["Armenian", "հայերեն", "hy"],
                  ["Azerbaijani", "Azərbaycan", "az"],
                  ["Basque", "Euskal", "eu"],
                  ["Belarusian", "Беларуская", "be"],
                  ["Bengali", "বাঙালি", "bn"],
                  ["Bosnian", "bosanski", "bs"],
                  ["Bulgarian", "български", "bg"],
                  ["Burmese", "မြန်မာ", "my"],
                  ["Catalan", "català", "ca"],
                  ["Cebuano", "Cebuano", "ceb"],
                  ["Chichewa", "Chichewa", "ny"],
                  ["Chinese Simplified", "简体中文", "zh-CN"],
                  ["Chinese Traditional", "中國傳統的", "zh-TW"],
                  ["Corsican", "Corsu", "co"],
                  ["Croatian", "hrvatski", "hr"],
                  ["Czech", "čeština", "cs"],
                  ["Danish", "dansk", "da"],
                  ["Dutch", "Nederlands", "nl"],
                  ["Esperanto", "esperanto", "eo"],
                  ["Estonian", "eesti", "et"],
                  ["Filipino", "Pilipino", "tl"],
                  ["Finnish", "suomalainen", "fi"],
                  ["French", "français", "fr"],
                  ["Galician", "galego", "gl"],
                  ["Georgian", "ქართული", "ka"],
                  ["German", "Deutsche", "de"],
                  ["Greek", "ελληνικά", "el"],
                  ["Gujarati", "ગુજરાતી", "gu"],
                  ["Haitian Creole", "kreyòl ayisyen", "ht"],
                  ["Hausa", "Hausa", "ha"],
                  ["Hawaiian", "ʻŌlelo Hawaiʻi", "haw"],
                  ["Hebrew", "עִברִית", "iw"],
                  ["Hindi", "हिंदी", "hi"],
                  ["Hmong", "Hmong", "hmn"],
                  ["Hungarian", "Magyar", "hu"],
                  ["Icelandic", "Íslenska", "is"],
                  ["Igbo", "Igbo", "ig"],
                  ["Indonesian", "bahasa Indonesia", "id"],
                  ["Irish", "Gaeilge", "ga"],
                  ["Italian", "italiano", "it"],
                  ["Japanese", "日本語", "ja"],
                  ["Javanese", "Jawa", "jw"],
                  ["Kannada", "ಕನ್ನಡ", "kn"],
                  ["Kazakh", "Қазақ", "kk"],
                  ["Khmer", "ភាសាខ្មែរ", "km"],
                  ["Korean", "한국어", "ko"],
                  ["Kurdish", "Kurdî", "ku"],
                  ["Kyrgyz", "Кыргызча", "ky"],
                  ["Lao", "ລາວ", "lo"],
                  ["Latin", "Latinae", "la"],
                  ["Latvian", "Latvijas", "lv"],
                  ["Lithuanian", "Lietuvos", "lt"],
                  ["Luxembourgish", "lëtzebuergesch", "lb"],
                  ["Macedonian", "Македонски", "mk"],
                  ["Malagasy", "Malagasy", "mg"],
                  ["Malay", "Malay", "ms"],
                  ["Malayalam", "മലയാളം", "ml"],
                  ["Maltese", "Malti", "mt"],
                  ["Maori", "Maori", "mi"],
                  ["Marathi", "मराठी", "mr"],
                  ["Mongolian", "Монгол", "mn"],
                  ["Myanmar", "မြန်မာ", "my"],
                  ["Nepali", "नेपाली", "ne"],
                  ["Norwegian", "norsk", "no"],
                  ["Nyanja", "madambwe", "ny"],
                  ["Pashto", "پښتو", "ps"],
                  ["Persian", "فارسی", "fa"],
                  ["Polish", "Polskie", "pl"],
                  ["Portuguese", "português", "pt"],
                  ["Punjabi", "ਪੰਜਾਬੀ ਦੇ", "pa"],
                  ["Romanian", "Română", "ro"],
                  ["Russian", "русский", "ru"],
                  ["Samoan", "Samoa", "sm"],
                  ["Scottish Gaelic", "Gàidhlig na h-Alba", "gd"],
                  ["Serbian", "Српски", "sr"],
                  ["Sesotho", "Sesotho", "st"],
                  ["Shona", "Shona", "sn"],
                  ["Sindhi", "سنڌي", "sd"],
                  ["Sinhala", "සිංහල", "si"],
                  ["Slovak", "slovenský", "sk"],
                  ["Slovenian", "slovenski", "sl"],
                  ["Somali", "Soomaali", "so"],
                  ["Spanish", "Español", "es"],
                  ["Sundanese", "Sunda", "su"],
                  ["Swahili", "Kiswahili", "sw"],
                  ["Swedish", "svenska", "sv"],
                  ["Tajik", "Тоҷикистон", "tg"],
                  ["Tamil", "தமிழ்", "ta"],
                  ["Telugu", "తెలుగు", "te"],
                  ["Thai", "ไทย", "th"],
                  ["Turkish", "Türk", "tr"],
                  ["Ukrainian", "український", "uk"],
                  ["Urdu", "اردو", "ur"],
                  ["Uzbek", "O'zbekiston", "uz"],
                  ["Vietnamese", "Tiếng Việt", "vi"],
                  ["Welsh", "Cymraeg", "cy"],
                  ["Western Frisian", "Western Frysk", "fy"],
                  ["Xhosa", "isiXhosa", "xh"],
                  ["Yiddish", "ייִדיש", "yi"],
                  ["Yoruba", "yorùbá", "yo"],
                  ["Zulu", "Zulu", "zu"]
              ],
              "showAccount": true,
              "accountMenuText": "User Options",
              "usePageListNavigation": false,
              "extraMenuOptions": [],
              "siteID": "[$siteID$]",
              "allLoaded": function(){}
          });
        },

        "AppAccordion": function() {
            $(".sp-column.one").csAppAccordion({
                "accordionBreakpoints" : [768, 640, 480, 320]
            });
        },

        "Search": function() {
            // FOR SCOPE
            var template = this;

            //OPENING SEARCH
            $(".cs-mystart-button.search .cs-button-selector").on('click keydown', function(event){
                if(template.AllyClick(event) === true){
                    //DONT LET THE PAGE JUMP ON KEYPRESS
                    event.preventDefault();
                    if(!$(this).hasClass("open")){
                        openSearch();
                    }
                }
            })

            //CLOSING SEARCH
            $("#search-control-close").on('click keydown', function(event){
                if(template.AllyClick(event) === true){
                    //DONT LET THE PAGE JUMP ON KEYPRESS
                    event.preventDefault();
                    closeSearch();
                }
            })

            function closeSearch(){
                $(".cs-mystart-button.search .cs-button-selector").removeClass("open").attr("aria-expanded","false");
                $("#gb-search").removeClass("open").attr("aria-hidden","true").fadeOut();
                $("#gb-search-input, #gb-search-button").attr("tabindex","-1");
                $("#gb-page").removeClass("search-open");
                $("#gb-search-button, #search-control-close, #gb-search-input").attr("tabindex","-1");
                $(".cs-mystart-button.search .cs-button-selector").focus();
            }

            function openSearch(){
                $(".cs-mystart-button.search .cs-button-selector").addClass("open").attr("aria-expanded","true");
                $("#gb-search").attr("aria-hidden","false").fadeIn().addClass("open");
                $("#gb-page").addClass("search-open");
                $("#gb-search-button, #search-control-close").attr("tabindex","0");

                setTimeout(function(){
                    $("#gb-search-input").attr("tabindex","0").focus();
                }, 300)
            }

            //SEARCH FORM SUBMISSION
            $("#gb-search-form").submit(function(e){
                e.preventDefault();

                if($.trim($("#gb-search-input").val()) != "I'm looking for..." && $.trim($("#gb-search-input").val()) != "") {
                    window.location.href = "/site/Default.aspx?PageType=6&SiteID=[$SiteID$]&SearchString=" + $("#gb-search-input").val();
                }
            });

            //SEARCH INPUT TEXT
            $("#gb-search-input").focus(function() {
                if($(this).val() == "I'm searching for...") {
                    $(this).val("");
                }
            });

            $("#gb-search-input").blur(function() {
                if($(this).val() == "") {
                    $(this).val("I'm searching for...");
                }
            });

            //SEARCH KEYBOARD NAV
            $("#gb-search-input").on("keydown", function(e){
                // CAPTURE KEY CODE
                switch(e.keyCode) {
                    case template.KeyCodes.tab:
                        if(e.shiftKey){
                            e.preventDefault();
                            $("#search-control-close").focus();
                        }
                    break;
                }
            });

            $("#search-control-close").on("keydown", function(e){
                // CAPTURE KEY CODE
                switch(e.keyCode) {
                    case template.KeyCodes.tab:
                        if(!e.shiftKey){
                            e.preventDefault();
                            $("#gb-search-input").focus();
                        }
                    break;
                }
            });


            $("#search-control-close, #gb-search-input, #gb-search-button, .search-links a").on("keydown", function(e){
                // CAPTURE KEY CODE
                switch(e.keyCode) {
                    case template.KeyCodes.esc:
                        closeSearch();
                    break;
                }
            });

        },

        "AllyClick": function(event) {
            if(event.type == "click") {
                return true;
            } else if(event.type == "keydown") {
                if(event.keyCode == this.KeyCodes.space || event.keyCode == this.KeyCodes.enter) {
                    return true;
                }
            } else {
                return false;
            }
        },

        "GetBreakPoint": function() {
            return window.getComputedStyle(document.querySelector("body"), ":before").getPropertyValue("content").replace(/"|'/g, "");/*"*/
        }
    };
