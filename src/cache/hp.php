

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">

<html lang="en">
<head>
    <title>Dev Site 1 / Homepage</title>
    <!--
    <PageMap>
    <DataObject type="document">
    <Attribute name="siteid">4</Attribute>
    </DataObject>
    </PageMap>
    -->

    
    <meta property="og:type" content="website" />
<meta property="fb:app_id" content="411584262324304" />
<meta property="og:url" content="http%3A%2F%2Fadamcruse.schoolwires.net%2Fsite%2Fdefault.aspx%3FPageID%3D1" />
<meta property="og:title" content="Dev Site 1 / Homepage" />
<meta name="twitter:card" value="summary" />
<meta name="twitter:title" content="Dev Site 1 / Homepage" />
<meta itemprop="name" content="Dev Site 1 / Homepage" />

    <!-- Begin swuc.GlobalJS -->
<script type="text/javascript">
 staticURL = "https://adamcruse.schoolwires.net/Static/";
 SessionTimeout = "500";
 BBHelpURL = "";
</script>
<!-- End swuc.GlobalJS -->

    <script src='https://adamcruse.schoolwires.net/Static/GlobalAssets/Scripts/min/sri-failover.min.js' type='text/javascript'></script>

    <!-- Stylesheets -->
    <link rel="Stylesheet" type="text/css" href="https://adamcruse.schoolwires.net/Static/GlobalAssets/webfonts/OpenSans-Light.css" />
    <link rel="Stylesheet" type="text/css" href="https://adamcruse.schoolwires.net/Static/GlobalAssets/webfonts/OpenSans-Italic.css" />
    <link rel="Stylesheet" type="text/css" href="https://adamcruse.schoolwires.net/Static/GlobalAssets/webfonts/OpenSans-Regular.css" />
    <link rel="Stylesheet" type="text/css" href="https://adamcruse.schoolwires.net/Static/GlobalAssets/webfonts/OpenSans-SemiBold.css" />
    <link rel="Stylesheet" type="text/css" href="https://adamcruse.schoolwires.net/Static/GlobalAssets/Scripts/ThirdParty/shepherd/shepherd-theme-default.css" />
    <link rel="Stylesheet" type="text/css" href="https://adamcruse.schoolwires.net/Static/App_Themes/SW/jquery.jgrowl.css" />
    <link rel="Stylesheet" type="text/css" href="https://adamcruse.schoolwires.net/Static//site/assets/styles/system_2560.css" />
    <link rel="Stylesheet" type="text/css" href="https://adamcruse.schoolwires.net/Static//site/assets/styles/apps_2590.css" />
    <link rel="Stylesheet" type="text/css" href="https://adamcruse.schoolwires.net/Static/App_Themes/SW/jQueryUI.css" />
    <link rel="Stylesheet" type="text/css" href="https://adamcruse.schoolwires.net/Static/GlobalAssets/webfonts/SchoolwiresMobile_2320.css" />
    
    <link rel="Stylesheet" type="text/css" href="https://adamcruse.schoolwires.net/Static/GlobalAssets/Styles/Grid.css" />

    <!-- Scripts -->
    <script src="https://adamcruse.schoolwires.net/Static/GlobalAssets/WCM-2620/WCM.js" type="text/javascript"></script>
    <script src="https://adamcruse.schoolwires.net/Static/GlobalAssets/WCM-2620/API.js" type="text/javascript"></script>
    <script src='https://adamcruse.schoolwires.net/Static/GlobalAssets/Scripts/min/jquery-3.0.0.min.js' type='text/javascript'></script>
    <script src='https://adamcruse.schoolwires.net/Static//GlobalAssets/Scripts/min/jquery-migrate-1.4.1.min.js' type='text/javascript'></script
    <script src='https://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js' type='text/javascript'
        integrity='sha384-JO4qIitDJfdsiD2P0i3fG6TmhkLKkiTfL4oVLkVFhGs5frz71Reviytvya4wIdDW' crossorigin='anonymous'
        data-sri-failover='https://adamcruse.schoolwires.net/Static/GlobalAssets/Scripts/min/swfobject.js'></script>
    <script src='https://adamcruse.schoolwires.net/Static/GlobalAssets/Scripts/ThirdParty/tether/tether.min.js' type='text/javascript'></script>
    <script src='https://adamcruse.schoolwires.net/Static/GlobalAssets/Scripts/ThirdParty/shepherd/shepherd.min.js' type='text/javascript'></script>
   
    <script type="text/javascript">
        $(document).ready(function () {
            SetCookie('SWScreenWidth', screen.width);
            SetCookie('SWClientWidth', document.body.clientWidth);
            
            $("div.ui-article:last").addClass("last-article");
            $("div.region .app:last").addClass("last-app");

            // get on screen alerts
            var isAnyActiveOSA = 'False';
            var onscreenAlertCookie = GetCookie('Alerts');

            if (onscreenAlertCookie == '' || onscreenAlertCookie == undefined) {
                onscreenAlertCookie = "";
            }
            if (isAnyActiveOSA == 'True') {
                GetContent(homeURL + "/cms/Tools/OnScreenAlerts/UserControls/OnScreenAlertDialogListWrapper.aspx?OnScreenAlertCookie=" + onscreenAlertCookie + "&SiteID=4", "onscreenalert-holder", 2, "OnScreenAlertCheckListItem();");
            }            

        });

    // ADA SKIP NAV
    $(document).ready(function () {
        $(document).on('focus', '#skipLink', function () {
            $("div.sw-skipnav-outerbar").animate({
                marginTop: "0px"
            }, 500);
        });

        $(document).on('blur', '#skipLink', function () {
            $("div.sw-skipnav-outerbar").animate({
                marginTop: "-30px"
            }, 500);
        });
    });

    // ADA MYSTART
    $(document).ready(function () {
        var top_level_nav = $('.sw-mystart-nav');

        // Set tabIndex to -1 so that top_level_links can't receive focus until menu is open
        // school dropdown
        $(top_level_nav).find('ul').find('a').attr('tabIndex', -1);

        // my account dropdown
        $(top_level_nav).next('ul').find('a').attr('tabIndex', -1);

        var openNavCallback = function(e, element) {
             // hide open menus
            hideMyStartBarMenu();

            // show school dropdown
            if ($(element).find('ul').length > 0) {
                $(element).find('.sw-dropdown').css('display', 'block').attr('aria-hidden', 'false');
                $(element).find('.sw-dropdown').find('li:first-child a').focus()
            }

            // show my account dropdown
            if ($(element).next('ul').length > 0) {
                $(element).next('.sw-dropdown').css('display', 'block').attr('aria-hidden', 'false');
                $(element).next('.sw-dropdown').find('li:first-child a').focus();
                $('#sw-mystart-account').addClass("clicked-state");
            }
        }

        $(top_level_nav).click(function (e) {
            openNavCallback(e, this);
        });

        $('.sw-dropdown-list li').click(function(e) {
            e.stopImmediatePropagation();
            $(this).focus();
        });
        
        // Bind arrow keys for navigation
        $(top_level_nav).keydown(function (e) {
            if (e.keyCode == 37) { //key left
                e.preventDefault();

                // This is the first item
                if ($(this).prev('.sw-mystart-nav').length == 0) {
                    $(this).parents('div').find('.sw-mystart-nav').last().focus();
                } else {
                    $(this).prev('.sw-mystart-nav').focus();
                }
            } else if (e.keyCode == 38) { //key up
                e.preventDefault();

                // show school dropdown
                if ($(this).find('ul').length > 0) {
                    $(this).find('div.sw-dropdown').css('display', 'block').find('ul').attr('aria-hidden', 'false').find('a').attr('tabIndex', 0).last().focus();
                }

                // show my account dropdown
                if ($(this).find('ul').length > 0) {
                    $(this).find('ul.sw-dropdown').css('display', 'block').attr('aria-hidden', 'false').find('a').attr('tabIndex', 0).last().focus();
                }
            } else if (e.keyCode == 39) { //key right
                e.preventDefault();

                // This is the last item
                if ($(this).next('.sw-mystart-nav').length == 0) {
                    $(this).parents('div').find('.sw-mystart-nav').first().focus();
                } else {
                    $(this).next('.sw-mystart-nav').focus();
                }
            } else if (e.keyCode == 40) { //key down
                e.preventDefault();

                // show school dropdown
                if ($(this).find('ul').length > 0) {
                    $(this).find('div.sw-dropdown').css('display', 'block').find('ul').attr('aria-hidden', 'false').find('a').attr('tabIndex', 0).first().focus();
                }

                // show my account dropdown
                if ($(this).next('ul').length > 0) {
                    $(this).next('ul.sw-dropdown').css('display', 'block').attr('aria-hidden', 'false').find('a').attr('tabIndex', 0).first().focus();
                }
            } else if (e.keyCode == 13 || e.keyCode == 32) { //enter key
                // If submenu is hidden, open it
                e.preventDefault();

                
                openNavCallback(e, this);
                $(this).parent('li').find('ul[aria-hidden=true]').attr('aria-hidden', 'false').find('a').attr('tabIndex', 0).first().focus();
            } else if (e.keyCode == 27) { //escape key
                e.preventDefault();
                hideMyStartBarMenu();
            } else {
                $(this).parent('.sw-mystart-nav').find('ul[aria-hidden=false] a').each(function () {
                    if (typeof keyCodeMap != "undefined" && $(this).text().substring(0, 1).toLowerCase() == keyCodeMap[e.keyCode]) {
                        $(this).focus();
                        return false;
                    }
                });
            }
        });

        // school dropdown
        var startbarlinks = $(top_level_nav).find('ul').find('a');
        bindMyStartBarLinks(startbarlinks);

        // my account dropdown
        var myaccountlinks = $(top_level_nav).next('ul').find('a');
        bindMyStartBarLinks(myaccountlinks);

        function bindMyStartBarLinks(links) {
            $(links).keydown(function (e) {
                e.stopPropagation();

                if (e.keyCode == 38) { //key up
                    e.preventDefault();

                    // This is the first item
                    if ($(this).parent('li').prev('li').length == 0) {
                        if ($(this).parents('ul').parents('.sw-mystart-nav').length > 0) {
                            $(this).parents('ul').parents('.sw-mystart-nav').focus();
                        } else {
                            $(this).parents('ul').prev('.sw-mystart-nav').focus();
                        }
                    } else {
                        $(this).parent('li').prev('li').find('a').first().focus();
                    }
                } else if (e.keyCode == 40) { //key down
                    e.preventDefault();

                    if ($(this).parent('li').next('li').length == 0) {
                        if ($(this).parents('ul').parents('.sw-mystart-nav').length > 0) {
                            $(this).parents('ul').parents('.sw-mystart-nav').focus();
                        } else {
                            $(this).parents('ul').prev('.sw-mystart-nav').focus();
                        }
                    } else {
                        $(this).parent('li').next('li').find('a').first().attr('tabIndex', 0);
                        $(this).parent('li').next('li').find('a').first().focus();
                    }
                } else if (e.keyCode == 27 || e.keyCode == 37) { // escape key or key left
                    e.preventDefault();
                    hideMyStartBarMenu();
                } else if (e.keyCode == 32) { //enter key
                    e.preventDefault();
                    window.location = $(this).attr('href');
                } else {
                    var found = false;

                    $(this).parent('div').nextAll('li').find('a').each(function () {
                        if (typeof keyCodeMap != "undefined" && $(this).text().substring(0, 1).toLowerCase() == keyCodeMap[e.keyCode]) {
                            $(this).focus();
                            found = true;
                            return false;
                        }
                    });

                    if (!found) {
                        $(this).parent('div').prevAll('li').find('a').each(function () {
                            if (typeof keyCodeMap != "undefined" && $(this).text().substring(0, 1).toLowerCase() == keyCodeMap[e.keyCode]) {
                                $(this).focus();
                                return false;
                            }
                        });
                    }
                }
            });
        }
        
        // Hide menu if click or focus occurs outside of navigation
        $('#sw-mystart-inner').find('.sw-mystart-nav').last().keydown(function (e) {
            if (e.keyCode == 9) {
                // If the user tabs out of the navigation hide all menus
                hideMyStartBarMenu();
            }
        });

        /*$(document).click(function() { 
            hideMyStartBarMenu();
        });*/

        // try to capture as many custom MyStart bars as possible
        $('.sw-mystart-button').find('a').focus(function () {
            hideMyStartBarMenu();
        });

        $('#sw-mystart-inner').click(function (e) {
            e.stopPropagation();
        });

        $('ul.sw-dropdown-list').blur(function () {
            hideMyStartBarMenu();
        });

        $('#ui-btn-mypasskey').focus(function () {
            hideMyStartBarMenu();
        });

        $('#ui-btn-sitemanager').focus(function () {
            hideMyStartBarMenu();
        });

        $('#ui-btn-myview').focus(function () {
            hideMyStartBarMenu();
        });

        $('#ui-btn-signin').focus(function () {
            hideMyStartBarMenu();
        });

        $('#ui-btn-register').focus(function () {
            hideMyStartBarMenu();
        });

        // button click events
        $('div.sw-mystart-button.home a').keydown(function (e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).click();
            }
        });

        $('div.sw-mystart-button.pw a').keydown(function (e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).click();
            }
        });

        $('div.sw-mystart-button.manage a').keydown(function (e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).click();
            }
        });

        $('#sw-mystart-account').keydown(function (e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).addClass('clicked-state');
                $('#sw-myaccount-list').show();
            }
        });

        $('#sw-mystart-mypasskey a').keydown(function (e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).click();
            }
        });

        $('div.sw-mystart-button.signin a').keydown(function (e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).click();
            }
        });

        $('div.sw-mystart-button.register a').keydown(function (e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).click();
            }
        });
    });

    function hideMyStartBarMenu() {
        $('.sw-dropdown').attr('aria-hidden', 'true').css('display', 'none');
        $('#sw-mystart-account').removeClass("clicked-state");
    }

    // ADA CHANNEL NAV
    $(document).ready(function() {
        var channelCount;
        var channelIndex = 1;
        var settings = {
            menuHoverClass: 'hover'
        };

        // Add ARIA roles to menubar and menu items
        $('[id="channel-navigation"]').attr('role', 'menubar').find('li a').attr('role', 'menuitem').attr('tabindex', '0');

        var top_level_links = $('[id="channel-navigation"]').find('> li > a');
        channelCount = $(top_level_links).length;


        $(top_level_links).each(function() {
            $(this).attr('aria-posinset', channelIndex).attr('aria-setsize', channelCount);
            $(this).next('ul').attr({ 'aria-hidden': 'true', 'role': 'menu' });

            if ($(this).parent('li.sw-channel-item').children('ul').length > 0) {
                $(this).attr('aria-haspopup', 'true');
            }

            var sectionCount = $(this).next('ul').find('a').length;
            var sectionIndex = 1;
            $(this).next('ul').find('a').each(function() {
                $(this).attr('tabIndex', -1).attr('aria-posinset', sectionIndex).attr('aria-setsize', sectionCount);
                sectionIndex++;
            });
            channelIndex++;

        });

        $(top_level_links).focus(function () {
            //hide open menus
            hideChannelMenu();

            if ($(this).parent('li').find('ul').length > 0) {
                $(this).parent('li').addClass(settings.menuHoverClass).find('ul').attr('aria-hidden', 'false').css('display', 'block');
            }
        });

        // Bind arrow keys for navigation
        $(top_level_links).keydown(function (e) {
            if (e.keyCode == 37) { //key left
                e.preventDefault();

                // This is the first item
                if ($(this).parent('li').prev('li').length == 0) {
                    $(this).parents('ul').find('> li').last().find('a').first().focus();
                } else {
                    $(this).parent('li').prev('li').find('a').first().focus();
                }
            } else if (e.keyCode == 38) { //key up
                e.preventDefault();

                if ($(this).parent('li').find('ul').length > 0) {
                    $(this).parent('li').addClass(settings.menuHoverClass).find('ul').css('display', 'block').attr('aria-hidden', 'false').find('a').attr('tabIndex', 0).last().focus();
                }
            } else if (e.keyCode == 39) { //key right
                e.preventDefault();

                // This is the last item
                if ($(this).parent('li').next('li').length == 0) {
                    $(this).parents('ul').find('> li').first().find('a').first().focus();
                } else {
                    $(this).parent('li').next('li').find('a').first().focus();
                }
            } else if (e.keyCode == 40) { //key down
                e.preventDefault();

                if ($(this).parent('li').find('ul').length > 0) {
                    $(this).parent('li')
                         .addClass(settings.menuHoverClass)
                         .find('ul.sw-channel-dropdown').css('display', 'block')
                         .attr('aria-hidden', 'false')
                         .find('a').attr('tabIndex', 0)
                         .first().focus();
                }
            } else if (e.keyCode == 13 || e.keyCode == 32) { //enter key
                // If submenu is hidden, open it
                e.preventDefault();

                $(this).parent('li').find('ul[aria-hidden=true]').attr('aria-hidden', 'false').addClass(settings.menuHoverClass).find('a').attr('tabIndex', 0).first().focus();
            } else if (e.keyCode == 27) { //escape key
                e.preventDefault();
                hideChannelMenu();
            } else {
                $(this).parent('li').find('ul[aria-hidden=false] a').each(function () {
                    if (typeof keyCodeMap != "undefined" && $(this).text().substring(0, 1).toLowerCase() == keyCodeMap[e.keyCode]) {
                        $(this).focus();
                        return false;
                    }
                });
            }
        });

        var links = $(top_level_links).parent('li').find('ul').find('a');

        $(links).keydown(function (e) {
            if (e.keyCode == 38) {
                e.preventDefault();

                // This is the first item
                if ($(this).parent('li').prev('li').length == 0) {
                    $(this).parents('ul').parents('li').find('a').first().focus();
                } else {
                    $(this).parent('li').prev('li').find('a').first().focus();
                }
            } else if (e.keyCode == 40) {
                e.preventDefault();

                if ($(this).parent('li').next('li').length == 0) {
                    $(this).parents('ul').parents('li').find('a').first().focus();
                } else {
                    $(this).parent('li').next('li').find('a').first().focus();
                }
            } else if (e.keyCode == 27 || e.keyCode == 37) {
                e.preventDefault();
                $(this).parents('ul').first().prev('a').focus().parents('ul').first().find('.' + settings.menuHoverClass).removeClass(settings.menuHoverClass);
            } else if (e.keyCode == 32) {
                e.preventDefault();
                window.location = $(this).attr('href');
            } else {
                var found = false;

                $(this).parent('li').nextAll('li').find('a').each(function () {
                    if (typeof keyCodeMap != "undefined" && $(this).text().substring(0, 1).toLowerCase() == keyCodeMap[e.keyCode]) {
                        $(this).focus();
                        found = true;
                        return false;
                    }
                });

                if (!found) {
                    $(this).parent('li').prevAll('li').find('a').each(function () {
                        if (typeof keyCodeMap != "undefined" && $(this).text().substring(0, 1).toLowerCase() == keyCodeMap[e.keyCode]) {
                            $(this).focus();
                            return false;
                        }
                    });
                }
            }
        });

        function hideChannelMenu() {
            $('li.sw-channel-item.' + settings.menuHoverClass).removeClass(settings.menuHoverClass).find('ul').attr('aria-hidden', 'true').css('display', 'none').find('a').attr('tabIndex', -1);
        }
        
        // Hide menu if click or focus occurs outside of navigation
        $('[id="channel-navigation"]').find('a').last().keydown(function (e) {
            if (e.keyCode == 9) {
                // If the user tabs out of the navigation hide all menus
                hideChannelMenu();
            }
        });

        $('[id="channel-navigation"]').find('a').first().keydown(function (e) {
            if (e.keyCode == 9) {
                // hide open MyStart Bar menus
                hideMyStartBarMenu();
            }
        });

        /*$(document).click(function() {
            hideChannelMenu();
        });*/

        $('[id="channel-navigation"]').click(function (e) {
            e.stopPropagation();
        });
    });

    $(document).ready(function() {
        $('input.required').each(function() {
            if ($('label[for="' + $(this).attr('id') + '"]').length > 0) {
                if ($('label[for="' + $(this).attr('id') + '"]').html().indexOf('recStar') < 0) {
                    $('label[for="' + $(this).attr('id') + '"]').prepend('<span class="recStar" aria-label="required item">*</span> ');
                }
            }
        });

        $(document).ajaxComplete(function() {
            $('input.required').each(function() {
                if ($('label[for="' + $(this).attr('id') + '"]').length > 0) {
                    if ($('label[for="' + $(this).attr('id') + '"]').html().indexOf('recStar') < 0) {
                        $('label[for="' + $(this).attr('id') + '"]').prepend('<span class="recStar" aria-label="required item">*</span> ');
                    }
                }
            });
        });
    });
    </script>

    <!-- Page -->
    
    <style type="text/css">/* MedaiBegin Standard *//* TEMPLATE CSS COMPILED FROM SASS */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
/* ---------------------------- ### GLOBAL ### ---------------------------- */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
/* GroupBegin Global */
/* ----- FONTS ----- */
@font-face {
  font-family: "template-icon";
  src: url("data:application/x-font-ttf;charset=utf-8;base64,AAEAAAALAIAAAwAwT1MvMg8SBkUAAAC8AAAAYGNtYXAXVtKgAAABHAAAAFRnYXNwAAAAEAAAAXAAAAAIZ2x5Zk0BbDIAAAF4AAAU/GhlYWQiAFPhAAAWdAAAADZoaGVhCcAFrwAAFqwAAAAkaG10eHA8AusAABbQAAAAeGxvY2FDqD8UAAAXSAAAAD5tYXhwACQBGAAAF4gAAAAgbmFtZa8Uj4wAABeoAAABknBvc3QAAwAAAAAZPAAAACAAAwQCAZAABQAAApkCzAAAAI8CmQLMAAAB6wAzAQkAAAAAAAAAAAAAAAAAAAABEAAAAAAAAAAAAAAAAAAAAABAAADpGQPA/8AAQAPAAEAAAAABAAAAAAAAAAAAAAAgAAAAAAADAAAAAwAAABwAAQADAAAAHAADAAEAAAAcAAQAOAAAAAoACAACAAIAAQAg6Rn//f//AAAAAAAg6QD//f//AAH/4xcEAAMAAQAAAAAAAAAAAAAAAQAB//8ADwABAAAAAAAAAAAAAgAANzkBAAAAAAEAAAAAAAAAAAACAAA3OQEAAAAAAQAAAAAAAAAAAAIAADc5AQAAAAABANj/vQNAA8AABQAAAScJATcBA0BD/dsCJUP+HQOCPv3+/f8+AcMAAQDY/70DQAPAAAUAAB8BCQEHAdhCAib92kIB4wU+AgICAT7+PAAAAAEAAP/ABAQDwABYAAATPAE1NDc+ATc2NzE2Nz4BNzYzOgEzMToBMzIXHgEXFhcxFhceARcWFRwBFTEcARUUBw4BBwYHMQYHDgEHBiMqASMxKgEjIicuAScmJzEmJy4BJyY1PAE1FQAKCyYcHCIjKClbMjE1AgMBAgMCNDExWigoIiMdHCgKCwoLKBwdIyIoKFoxMTQCAwIBAwI1MTJbKSgjIhwcJgsKAcICAwI0MTFaKCgiIhwcJgsKCgsmHBwiIigpWjIyNQEBAQEDATUyMlwoKSIiHBwmCwoKCyYcHCIiKChaMTE0AwYDAQAAAQAAAHEEMANAAEcAAAE2Nz4BNzY3PgE3Mz4BMzIWFzEeARUUBgcxBgcGBw4BIyImJzEmJy4BJyYnLgEnNTwBNTQ2NzM+ATMyFhcxFhceARcWFx4BFwIYLi0tWi4tLQ4lFQEECAQZKAkFBg0LgIJYWQwpGBkoDTY1Nms1NTUMEAMjGgEHEAgXJgwjIyJFIyIjFi4ZAWE3NzZsNjU2DxcFAQEbFgoWDBIfDJybamoTFhYTQUBAgEBBQA8lFAEBAQEdLQgDAxQRKSopUykqKRs2HgAAAAABAOT/uAMPA8AAHgAAATcjNTQ2OwE1IyImIyIHDgEHBhUUFhU1FSMVMxEzEQLnI6ktO0aLBQoGLikoPRESAa2t0AGZrYYpHa0BEhE9KCkuBgoGAYat/h8B4QAAAAX////ABAYDwABXAK4AzQDbAOcAAAEzHgEXIx4BFzEeAR8BHgEXMR4BFRQGFTUUFhUUBgc3DgEHNw4BDwEOAQcjIS4BJxcuAS8BLgEnNS4BNTwBNRU8ATU0NjcHPgE3Bz4BNzE+ATczPgE3OwE1Iw4BBzcOAQ8BDgEHFQ4BBzEOARUcARc1BhQVFBYXJx4BFyceARczHgE7ASEyNjcHPgE/AT4BNzU+ATU0JjUVNDY1NCYnFS4BJxcuAScxLgEnIy4BKwEHIgcOAQcGFRQXHgEXFjMyNz4BNzY1MTQnLgEnJiMxESImNTQ2MzIWFTEUBiMBFAYjIiY1NDYzMhYB/s4aMhYCER0MDRMGAQgKAQMDAQEEAwEBCgkBDjEgAhUxGQH+YxoyFgIiMQ0BCAoBAwIDAwEBCgkBBxMNDBwQAhUxGQHO0yJAHgMaKxEBEh0KCw0BAgMBAQMDAQENDAEUTTICG0AhAQGnIkAdAjNNFAELDAEDAwEBAwMBDQsBDCIVEisYAhtAIQHTNzAwSBUVFRVIMDA3NjAxRxUVFRVHMTA2RmNjRkVjY0UBUCQaGiUlGhokA2UBCgkGFAwMHBEBFTEaIUwoDx4QAw0dECdOJgcaMhYCIjENAQgKAQEKCQENMiACFTAaASFMJxAeDwMNHg8oTScHGjEXAhEdDAwUBgkKAVsBDQsBCyATARMtGQIcPyIiTSgQIBADDR4QKFAnByJAHgM0TRQLDQ0MARRNMgIcPyEBIk0oECAPAw4eDylPKAciQB4DGy4SEx0KCw31FRVHMTA2NzAwSBUVFRVIMDA3NjAxRxUV/k9jRkVjY0VGYwG7GiUlGhklJQAAAAMAAP/AA+oDwAAbACAALAAAATMVPgEzOgEzIzIXHgEXFhURIxE0JiMiBhURIwEzESMRNxQGIyImNTQ2MzIWAWTMH2M7BAgEAVEvLzEHB8wVUVE3zP6tzMzdRjExRkYxMUYCbWYuOBcWUTg4RP6FAVQ7bmZD/qwCrf1TAq3cMUZGMTFGRgAAAAADAAAAdQPwAxUAHgA9AFwAAAEhLgE1MDQ5ATwBNTQ2NzEhHgEVHAEVMTAUMRQGBzERIS4BNTA0OQE8ATU0NjcxIR4BFRwBFTEwFDEUBgcxESEuATUwNDkBPAE1NDY3MSEeARUcARUxMBQxFAYHMQPG/GQSGBgSA5wRGRkR/GQSGBgSA5wRGRkR/GQSGBgSA5wRGRkRArUCGhICAQEBEhoBARoSAQEBAhIaAv7gAhoSAgEBARIaAQEaEgEBAQISGgL+4AIaEgIBAQESGgEBGhIBAQECEhoCAAAEAAD/wAW1A7oAGwAoADoATAAABSImJyUjIiY1ETQ2OwElPgEXHgEVERQGByIGIwEzMhYfAREHDgErAREFIiYnJjQ3ATYyFxYUBwEOASMhIiYnASY0NzYyFwEWFAcOASMCgggRCP7K9BkeHhn0ATYMIBEMFREQCQ8E/e3NCBAJ7u4JEAjNA5cNEggREQGJEC0QERH+dwQWDAGJDRII/ncRERAtEAGJEREIEgxABwT5HxkBiBkf+QgGCQgZEPxuEBkIBgFzBwS8Aqi8BAf+5m8ICRAtEAGJEREQLRD+dwkICAkBiRAtEBER/ncQLRAJCAACAAD/wAQAA8AAEgAbAAAlIREhNSEiBhURFBYzITI2NREjARUzARcBFTMRA4785AFj/p0vQ0MvAxwvQ3L+x+r9wVACPnIyAxxyQy/85C9DQy8BYwIrcv3CUAI/6gGrAAACAAAAAAQAA4AABwAOAAABFSERIRUhEQEVIRUhFSUBgAIA/gACgP4A/gACAAEAA4CA/YCAA4D/AICAgMAAAAAAAgAA/74CvQO+AAMABwAAEzMRIwEzESMAbGwCUWxsA778AAQA/AAAAAEAWv+sA5wDwABqAAABIgcOAQcGFRQWMzI2NTQmNTQ3PgE3NjMyNjMyFx4BFxYVHAEHNRQHDgEHBiMiJjc+ATc+ATU0JicxKgEjIgYHMR4BFzUwBw4BBwYHBgcOARcWMzI2Nz4BMR4BHwEyNz4BNzY1NCcuAScmIwIdalRUdB4fThkaK0UdHWE+P0QECAQvKio/EhIBEBA4JiUqVR0LCyAZAgIqHgMFAjhQAQESDwwNHw4PAwQICQsBAQ8ePTMHGyVWLgNIOjpRFxYcHGVHRlUDwCQjb0REQIFNCBoaRE4mMTBWHR0BExI+KiowBAgEAUA5OVYZGTcwMDdnBgwHHy0CTTcdNRYBLCxwNTUSEicnUB0ecIUSVRkjCAEgIHBMTVhYQUJWFhUAAwAAAAcD6QNvAAMABwALAAATFSE1ARUhNQEVITUAA+n8FwPp/BcD6QNvfX3+jH19/ol9fQAAAAAFAAAAIgQLA14AFAAvAFsAaQB/AAATETgBMRQWMzgBMTMRIyIGFTgBOQElIzU0JiMxIyIGFTEVFBYzMTMyNjUxNTQmIzE3JS4BIyIGBzEFDgEVMREzNTgBMTQ2MzgBMTM4ATEyFhU4ATkBFTMRNCYnMQUiJjU0NjMyFhUxFAYjJSMRMzgBMTI2NTgBOQEROAExNCYjMQAPC4FnFh4CRicHBhoFCAgFTgUICAXf/v0GDwgIDgb+/QoNzw8LmwsPzw0K/uA1TEw1NkxMNgHSZ4ELDx4WAfT+SAoQAgYeFk5BBQcHBWgFCAgFGgUIZ60EBAQErQcWDv2l6QoPDwrpAlsOFgfpTDY1TEw1Nkxo/foQCgG4Fh4AAAAAAwAA/9gD1APAABsANwBIAAAlIicuAScmNTQ3PgE3NjMyFx4BFxYVFAcOAQcGAyIHDgEHBhUUFx4BFxYzMjc+ATc2NTQnLgEnJgEiJi8BJjQ3NjIfARYUBw4BAcRdUlJ7JCQkI3tTUl1dUlJ7JCQkI3tTUl1FPD1aGhoaGlo8PUVFPD1aGhobGls8PAGkDBcJ1BISEjAS1BISBhY4JCN7U1JdXVJSeyQkJCN7U1JdXVJSeyQkAxAaGlo8PUVFPD1aGhoaGlo8PUVFPD1aGhr8kAcJ1BIwEhIS1BIwEgkHAAABAAEAwAPYAvIAHgAACQE+ATMyFhUUBgcxAQ4BIyImJzEBLgE1NDYzMhYXMQHtAXQKGA4eKQoJ/loKGg4OGAr+VwgLKh0OGAoBagF1CAsqHQ4ZCf5aCgsKCQGoCRkOHSkKCQAAAwAA/8AEZgPAACQALQAxAAABJzc2Nz4BNzY/ATM1ITUjFSEVIQ4BBzMuAS8BIx4BFzEBFwEXASMDMzczFzMDAzcXIwJgggEgGxwwExQOApb+mmb+mQI8HFI1ASI7GAJmH0wu/vtJAQCfAUdm52c58zpm5oZTU6YBI4ACIyYmUywsLgZmZ2dlUIk6JlQtBUN1Mv7/SAEAoAFs/ZqamgJm/prd3QABAAAAAAQUA2YAcQAAAQ4BByM+ATc1DgEPAScuASMqASMxKgEjIgcOAQcGFRwBFTEUFhc1IyInLgEnJicOAQcxHgEfAS4BJzMUFx4BFxYXBiYnHgEzOgEzMQ4BIyoBIzEjFhceARcWMzoBMzE6ATMyNz4BNzY1PAE1FTU+AT8BBBQeRSUEJTUPFzYcBRkZSisBAQEBAgIqJSY3EBACAwVPQkFuLy8pDxICAzgtAR02FwEYGUkrKiYaigsabUQCBAE0gEgCBAIyJCgnVSwtLgQIAwMJBHdoaZstLR80FQEC5A8XBxlHKgIQHg0CGSEmEBE3JSYqAQMBChIIAQkJKSEiMBY1HThbGwEDEg4nKShFGBgIGhYEPEsoLRUREBgGBi0tnGhpdwQIBQEjGDkgAgAAAwAA/8AEAAPAAB0ALwBYAAABIgcOAQcGFRQXHgEXFjMyNz4BNzY1MTQnLgEnJiMVMhYVFAYjIiY1MTQ2MzgBOQERIicuAScmJzE+ATMxMhYXMR4BMzEyNjcHPgEzMTIWFxUGBw4BBwYjMQIAal1eiygoKCiLXl1qal1eiygoKCiLXl1qS2trS0tra0stKytNISIbHWxCBAcEEisXFysUAgQHBEJsHRsiIU0rKy0DwCgoi15dampdXosoKCgoi15dampdXosoKMZrS0tqaktLa/06CgklGhogOEQBAgYICAcBAgFENgIgGholCQoAAAAAAgAAAGcD+gMyAF0AYAAAAS4BJxUuASMxJicuASciMTAjDgEHBgciBgcxDgEHFQ4BFTAUFTEVHAExFBYXNR4BFzEeATM6ATMxFhcWMjcyMTAzFjI3Njc+ATcxPgE3NT4BNTA0NTE1PAE1NCYnFQERBQPwAxUPFDUeNT8+ayQkJCRrPz41HzQUEBQDBQUFBQMVDxY4HwECAR86OnMqKyQkaz8+NR41FBAUAwUFBQX9hAESApUdNBcBFBgEAgIBAQEBAgIEGBQWMx0BIkwoAQFmAQEpUScGHTQWEhUEAQEBAQEBBAEXFBYzHQEjTykBAWEBAQEnTiYG/q8BEokAAQAA/8ADHAPAAAYAAAkBIQkBIQECG/3lAQACHP3k/wACGwHBAf/+Af3/AgEAAAAABQAA/8ADqQPAADkAVgCQAQABFQAAJTY3LgE1NDY3MTwBNTQ2JyYnLgEnJiMiBw4BBwYVHAEVMRwBHQEOARUcARU1FBUcARUUFjM6ATMxMwM2Jjc+ATMwMjkBOgEzMhYXFR4BFRQGBzUhPAE1EzwBNTwBNTE2NDU0JicxLgEnMT4BNzE+ATMyFhcxHgEVFAYHMQ4BFRQWOQEcARUcARU1DgEjIiYnMSUuASMiBw4BBwYVFBYXJxYUFRQGBzEGBw4BFTgBOQEcARUiFBUUFjMwMjkBOgEzMDIxMjY3MT4BNz4BMzE6ATMWNic8ATUzOgExMjY1PAEnFTwBNT4BNzE+ATc+ATM6ARcxHgEzMjY3Iz4BNTQmJxcHIiY1NDYzMhYVFDA5ARQGIzgBIzEBajw9AwQyKwEEBhYWQywrMDUvLkUUFBkiJRoCAgLflgEBAwtePwEBAQE+XQoCAQEB/quIAQgHBAcCARkTBAkFEBsIBQUICAQGAQEVDg4UAgKSCnJOKiQlNxAQBgYBAQICenoEBAEMCQIUKRQBBQoDCA4HAgQDDRoNEg4BOwEBCQwBAQECChQKAQMCAQIBDyMTBw8IAUpjAQEBoxgjIxkaIyMaAcs9PA4dEDxmJAUKBSJFITAoKTwRERQURS8uNQEBAR47Hg8CJRkCAwIBlJUBAgIaJQHYIEEgPVJROwEWMBkUJhMDChIK/swCBAIBAwECBQILFAgGEAkVIQYBAhAMBxEJCxUIBAwHAQIDBwQDBwQBDhMSDm9LZhAQNyUkKhIjEAEBAgECBAF6ewMKBhUoFQEBCAwEBAgOBwICAQ4RDRkNDAgBAQEBEB4PAwQCChQKAgEBBgYBAQtyTAcNBgEtJBkZIyMZARkjAAAAAAIAAAAXAvADaQAVACQAAAE0JicBJgYHDgEVERQWFx4BNwE+ATUzFAYHAQYmNRE0NhcBHgECqwQI/coHDQMECAgEAw0HAjYIBEUWFv3KL19fLwI2FhYBwAQLBQFaBQMCAgkJ/UwJCQICAwUBWgULBBcrDf6mHTU3ArQ3NR3+pg0rAAAAAAT//f/YBQoDnQAbACgARgBkAAAFIiYnJSMiJjURNDY7ASU+ARceARURFAYHJgYjATMyFh8BEQcOASsBEQEiJicmNDc2NzY0JyYnJjQ3NjIXFhcWEAcGBw4BIyciJicmNDc2NzY0JyYnJjQ3NjIXFhcWFAcGBw4BIwJfCA8I/tbkGB0dGOQBKgsfEAwUEBAICwj+DsQIEAjf3wgQCMQD5QwRCBAQVCkqKilUEBAQKhBiMTAwMWIIEQzZDBIIDw8mExMTEyYPDxArEDUbGxsbNQgRDCgGBOodGAFzGB3qCAUICBgQ/KAQGAgEBAFjBgS1AoK0BAf+/P6dCAgQKhBUaGjZaGlTECsPEBBhfHz+/Xx8YQgI2QgIECsQJS8vYi8vJRArEBAQNkNDikJCNAgIAAADAAD/wAYAA8AAHgAuAE0AAAEhLgE1PAE1MTwBNTQ2NzEhHgEVHAEVMRwBFRQGBzEBISImNTE0NjMhMhYVMRQGASEuATU8ATUxPAE1NDY3MSEeARUcARUxHAEVFAYHMQXA+oAbJSUbBYAbJSUb/nr79RMcHBMECxQbGwFy+oAbJSUbBYAbJSUbAy4CJxwBAgEBAgEbKAICKBsBAgEBAgEcJwL+SSseHisrHh4r/kkCKBsBAgEBAgEcJwICJxwBAgEBAgEbKAIAAAEAAAABAAA5oJX5Xw889QALBAAAAAAA3nIHvAAAAADecge8//3/rAYAA8AAAAAIAAIAAAAAAAAAAQAAA8D/wAAABgD//f/QBgAAAQAAAAAAAAAAAAAAAAAAAB4EAAAAAAAAAAAAAAACAAAABAAA2AQAANgEBAAABAAAAAQAAOQEBf//A+kAAAQAAAAF3AAABAAAAAQAAAACvQAABAAAWgQAAAAEAAAABAAAAAQAAAEEZgAABAAAAAQAAAAEAAAAAxwAAAOpAAADRgAABT7//QYAAAAAAAAAAAoAFAAeADIARgC8ASYBVAKSAtYDQgO6A+oECgQeBLIEzgVaBcoF/AZOBuYHXgfcB/QJQAmCChoKfgAAAAEAAAAeARYABQAAAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAOAK4AAQAAAAAAAQAIAAAAAQAAAAAAAgAHAGkAAQAAAAAAAwAIADkAAQAAAAAABAAIAH4AAQAAAAAABQALABgAAQAAAAAABgAIAFEAAQAAAAAACgAaAJYAAwABBAkAAQAQAAgAAwABBAkAAgAOAHAAAwABBAkAAwAQAEEAAwABBAkABAAQAIYAAwABBAkABQAWACMAAwABBAkABgAQAFkAAwABBAkACgA0ALBkaXN0cmljdABkAGkAcwB0AHIAaQBjAHRWZXJzaW9uIDEuMABWAGUAcgBzAGkAbwBuACAAMQAuADBkaXN0cmljdABkAGkAcwB0AHIAaQBjAHRkaXN0cmljdABkAGkAcwB0AHIAaQBjAHRSZWd1bGFyAFIAZQBnAHUAbABhAHJkaXN0cmljdABkAGkAcwB0AHIAaQBjAHRGb250IGdlbmVyYXRlZCBieSBJY29Nb29uLgBGAG8AbgB0ACAAZwBlAG4AZQByAGEAdABlAGQAIABiAHkAIABJAGMAbwBNAG8AbwBuAC4AAAADAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA") format("truetype");
  font-weight: normal;
  font-style: normal;
}
/* ----- GENERAL ----- */
@-ms-viewport {
  width: device-width;
}
body {
  -webkit-text-size-adjust: none;
  -webkit-tap-highlight-color: rgba(255, 255, 255, 0);
}

body.nav-open {
  overflow: hidden;
}

body:before {
  content: "desktop";
  display: none;
}

*[data-toggle=false],
*[data-content=""] {
  display: none !important;
}

/* HIDDEN ELEMENTS */
#sw-mystart-outer,
#sw-footer-outer,
.hidden,
#gb-page div.ui-widget.app div.ui-widget-footer .cs-article-slider-control {
  display: none;
}

.ui-clear:after {
  content: "";
}

.content-wrap {
  max-width: 85vw;
  padding: 0px;
  margin: 0 auto;
  position: relative;
}

button {
  cursor: pointer;
  background: transparent;
  border: 0px;
  padding: 0px;
  margin: 0px;
}

div.sw-special-mode-bar {
  position: fixed !important;
  bottom: 0px;
  left: 0px;
  width: 100%;
}

#gb-page {
  width: 100%;
  position: relative;
  overflow: hidden;
  background: #fff;
}

h1, h2, h3, h4 {
  margin: 0;
}

/* ----- GLOBAL ICONS ----- */
#gb-icons[data-icon-num="0"] {
  display: none;
}

.cs-global-icons {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
}
.cs-global-icons li {
  -ms-flex-preferred-size: 16.666%;
      flex-basis: 16.666%;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  padding: 32px 20px;
  -webkit-box-sizing: border-box;
          box-sizing: border-box;
}
.cs-global-icons a {
  color:#FFFFFF;
  text-decoration: none;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  -webkit-box-pack: justify;
      -ms-flex-pack: justify;
          justify-content: space-between;
  -webkit-box-sizing: border-box;
          box-sizing: border-box;
  padding: 10px 10px 10px;
}
.cs-global-icons a:hover .icon, .cs-global-icons a:focus .icon {
  padding-top: 5px;
  margin-bottom: 20px;
}
.cs-global-icons a:hover .text::after, .cs-global-icons a:focus .text::after {
  width: 70px;
  opacity: 1;
  left: calc(50% - 35px);
}
.cs-global-icons .icon {
  font-size: 3.7rem;
  margin-bottom: 15px;
  padding-top: 10px;
  -webkit-transition: all 0.3s ease 0s;
  transition: all 0.3s ease 0s;
}
.cs-global-icons .text {
  text-transform: uppercase;
  font-size: 1.2rem;
  font-weight: 500;
  max-width: 120px;
  text-align: center;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  position: relative;
}
.cs-global-icons .text::after {
  display: block;
  content: "";
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  -webkit-transition: all 0.3s ease 0s;
  transition: all 0.3s ease 0s;
  height: 2px;
  width: 0;
  left: 50%;
  top: auto;
  bottom: -4px;
  opacity: 0;
  background:#F57E20;
}

/* GroupEnd */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
/* ----------------------------- ### APPS ### ----------------------------- */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
/* GroupBegin Apps */
/* ----- GENERAL ----- */
.flexpage .ui-article a,
.announcements .ui-article a {
  color: #214A9B;
  font-weight: 600;
  text-decoration: underline;
}

.more-link-under {
  display: none;
}

div.ui-widget-header p {
  margin-top: 10px;
}

.sp div.ui-widget-detail h1:not(.ui-article-title),
.sp div.ui-widget-detail h2:not(.ui-article-title),
.sp div.ui-widget-detail h3:not(.ui-article-title),
.sp div.ui-widget-detail h4:not(.ui-article-title) {
  margin-bottom: 5px;
}

.ui-widget-footer .clear {
  display: none;
}

.ui-widget-header.ui-helper-hidden {
  margin: 0px !important;
  padding: 0px !important;
}

div.ui-widget.app div.ui-widget-header:empty {
  display: none;
}

div.ui-widget.app div.ui-widget-header {
  position: relative;
  font-size: 2rem;
  font-weight: 700;
  margin-bottom: 13px;
  color:#132D62;
  text-transform: uppercase;
  letter-spacing: 1px;
}

div.ui-widget.app div.ui-widget-header h1 {
  font-weight: inherit;
  font-size: inherit;
  margin: 0;
}

.ui-article-thumb .img {
  margin: 2px 24px 0px 0px;
  border-radius: 2px;
}

div.ui-widget-detail p:first-of-type {
  margin-top: 0px;
}

div.ui-article {
  margin-bottom: 13px;
}

h1.ui-article-title {
  font-size: 1.3333rem;
  line-height: 1.2;
  font-weight: 600;
  letter-spacing: -0.4px;
  margin-bottom: 2px;
  color:#214A9B;
}

.ui-article-title a {
  color: inherit;
}

.view-calendar-link,
.more-link,
div.app-level-social-rss a.ui-btn-toolbar.rss {
  opacity: 1;
  display: inline-block;
  padding: 14px 32px;
  text-decoration: none;
  -webkit-transition: all 0.3s ease 0s;
  transition: all 0.3s ease 0s;
  position: relative;
  font-weight: 500;
  margin: 0;
  background:#214A9B;
  color:#FFFFFF;
  text-transform: capitalize;
  line-height: 1;
}
.view-calendar-link span,
.more-link span,
div.app-level-social-rss a.ui-btn-toolbar.rss span {
  padding: 0;
  line-height: 1;
  font-size: inherit;
  height: auto;
  background: none;
  display: inline-block;
}
.view-calendar-link:hover, .view-calendar-link:focus,
.more-link:hover,
.more-link:focus,
div.app-level-social-rss a.ui-btn-toolbar.rss:hover,
div.app-level-social-rss a.ui-btn-toolbar.rss:focus {
  background:#132D62;
  color:#FFFFFF;
}

div.app-level-social-rss a.ui-btn-toolbar.rss span::after {
  display: none;
}

/* ----- NAVIGATION (SHORTCUTS & PAGELIST) ----- */
div.ui-widget.app.navigation li {
  border-bottom: 2px solid rgba(0, 0, 0, 0.1);
  padding: 18px 50px 13px 0px;
  position: relative;
}
div.ui-widget.app.navigation li::before {
  display: block;
  content: "";
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  background: #000;
  opacity: 0;
  width: 0;
  height: 2px;
  top: auto;
  bottom: -2px;
  -webkit-transition: all 0.3s ease 0s;
  transition: all 0.3s ease 0s;
}
div.ui-widget.app.navigation li.hover::before {
  width: 100%;
  opacity: 1;
}
div.ui-widget.app.navigation li:last-child {
  border: 0;
}
div.ui-widget.app.navigation li:last-child::before {
  bottom: -3px;
}
div.ui-widget.app.navigation li.collapsible::before {
  display: none;
}
div.ui-widget.app.navigation li.collapsible ul::after {
  display: block;
  content: "";
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  background: #000;
  opacity: 0;
  width: 0;
  top: 3px;
  height: 2px;
  -webkit-transition: all 0.3s ease 0s;
  transition: all 0.3s ease 0s;
}
div.ui-widget.app.navigation li.collapsible.hover ul::after {
  width: 100%;
  opacity: 1;
}
div.ui-widget.app.navigation li a {
  font-size: 1rem;
  font-weight: 700;
  color:#214A9B;
  display: table;
  position: relative;
  -webkit-transition: all 0.3s ease 0s;
  transition: all 0.3s ease 0s;
}
div.ui-widget.app.navigation li div.bullet {
  background: none;
  position: absolute;
  right: -11px;
  top: 6px;
  height: 40px;
  font-size: 11px;
  width: 40px;
  text-align: center;
  -webkit-box-sizing: border-box;
          box-sizing: border-box;
  padding-top: 20px;
}
div.ui-widget.app.navigation li div.bullet::before {
  font-family: "template-icon" !important;
  speak: none;
  font-style: normal;
  font-weight: normal;
  font-variant: normal;
  text-transform: none;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  -webkit-transition: all 0.3s ease 0s;
  transition: all 0.3s ease 0s;
}
div.ui-widget.app.navigation li div.bullet.expandable {
  cursor: pointer;
  background: none;
}
div.ui-widget.app.navigation li div.bullet.expandable::before {
  content: "\e910";
}
div.ui-widget.app.navigation li div.bullet.collapsible {
  cursor: pointer;
  background: none;
}
div.ui-widget.app.navigation li div.bullet.collapsible::before {
  content: "\e910";
  display: block;
  -webkit-transform: rotate(180deg);
          transform: rotate(180deg);
}
div.ui-widget.app.navigation li ul {
  position: relative;
  margin: 10px 0 -11px;
  padding: 6px 0 0;
  width: calc(100% + 50px);
}
div.ui-widget.app.navigation li ul::before {
  display: block;
  content: "";
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  height: 2px;
  background: rgba(0, 0, 0, 0.1);
  left: 0px;
  top: 3px;
}
div.ui-widget.app.navigation li ul li {
  padding-left: 10px;
}
div.ui-widget.app.navigation.pagenavigation li {
  border-bottom: 1px solid #cee1df;
}
div.ui-widget.app.navigation.pagenavigation li:last-child {
  border: 0;
}
div.ui-widget.app.navigation.pagenavigation li.active a {
  color:#000000;
}
div.ui-widget.app.navigation.pagenavigation li a {
  color:#214A9B;
}
div.ui-widget.app.navigation.pagenavigation li ul::before {
  height: 1px;
}

/* ----- ANNOUNCEMENTS ----- */
.announcements li .ui-article {
  position: relative;
}
.announcements li .ui-article::after {
  display: block;
  content: "";
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  height: 1px;
  top: auto;
  bottom: 3px;
  background: rgba(0, 0, 0, 0.25);
}
.announcements li:last-child .ui-article::after {
  display: none;
}

/* ----- UPCOMING EVENTS ----- */
.upcomingevents .ui-article {
  margin-bottom: 6px;
}

h1.sw-calendar-block-date {
  font-weight: 400;
  border: 2px solid rgba(0, 0, 0, 0.1);
  border-radius: 2px;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: horizontal;
  -webkit-box-direction: reverse;
      -ms-flex-direction: row-reverse;
          flex-direction: row-reverse;
  -webkit-box-pack: justify;
      -ms-flex-pack: justify;
          justify-content: space-between;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  padding: 0px 19px;
  color:#214A9B;
  margin-bottom: 5px;
}
h1.sw-calendar-block-date span {
  display: block;
}
h1.sw-calendar-block-date .adam-hug {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: horizontal;
  -webkit-box-direction: reverse;
      -ms-flex-direction: row-reverse;
          flex-direction: row-reverse;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
}
h1.sw-calendar-block-date .joel-month {
  font-size: 1.0666rem;
  text-transform: uppercase;
  font-weight: 400;
}
h1.sw-calendar-block-date .jeremy-date {
  font-size: 2.6666rem;
  font-weight: 300;
  margin-right: 16px;
}
h1.sw-calendar-block-date .joel-day {
  font-size: 0.9333rem;
  font-weight: 400;
  text-transform: capitalize;
}

.upcoming-column.right .ui-article-description {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
  margin-bottom: 16px;
  border-bottom: 2px solid rgba(0, 0, 0, 0.1);
  margin: 0;
  padding: 8px 0;
}
.upcoming-column.right .ui-article-description:last-child {
  border: 0;
}
.upcoming-column.right .sw-calendar-block-title {
  font-weight: 500;
  color:#214A9B;
  font-size: 1.0666rem;
}
.upcoming-column.right .sw-calendar-block-title a:hover,
.upcoming-column.right .sw-calendar-block-title a:focus {
  text-decoration: underline;
}
.upcoming-column.right .sw-calendar-block-time {
  font-size: 0.8666rem;
  font-weight: 300;
  color:#214A9B;
}

/* ----- HEADLINES ----- */
.headlines h1.ui-article-title {
  margin-top: 4px;
}

/* ----- HOMEPAGE APPS ----- */
.ui-hp div.ui-widget.app div.ui-widget-header {
  font-size: 2.6666rem;
  letter-spacing: 1.2px;
}
.ui-hp div.ui-widget.app div.ui-widget-header p {
  font-size: 100%;
  text-transform: none;
  font-family: Poppins, sans-serif;
  letter-spacing: 0;
}
.ui-hp div.ui-widget.app div.ui-widget-header a {
  text-transform: capitalize;
  font-size: 0.94rem;
  font-family: Poppins, sans-serif;
  letter-spacing: 0;
}

#gb-page.hp .site-shortcuts-columns {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: justify;
      -ms-flex-pack: justify;
          justify-content: space-between;
  margin: 0 -20px;
}

#gb-page.hp .site-shortcuts-column {
  margin: 0;
  padding: 0;
  -ms-flex-preferred-size: 25%;
      flex-basis: 25%;
  padding: 0 20px;
  -webkit-box-sizing: border-box;
          box-sizing: border-box;
}
#gb-page.hp .site-shortcuts-column li:last-child {
  border-bottom: 2px solid rgba(0, 0, 0, 0.1);
}

/* ----- HEADLINES ----- */
/* ----- ANNOUNCEMENTS ----- */
/* ----- UPCOMING EVENTS ----- */
/* GroupEnd */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
/* ---------------------------- ### HEADER ### ---------------------------- */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
/* GroupBegin Header */
#cs-contrast-bar {
  display: none;
  background-color: #000;
}

.mobile-menu {
  display: none;
}

#gb-header {
  z-index: 101;
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
}

/*.sticky-header #gb-header {
  z-index: 101;
}*/

.header-row {
  position: relative;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: justify;
      -ms-flex-pack: justify;
          justify-content: space-between;
}
.header-row.one {
  background:#214A9B;
  min-height: 49px;
  z-index: 10001;
}
.header-row.one #cs-contrast-bar {
  display: block;
  padding: 16px 18px 8px;
  -webkit-box-sizing: border-box;
          box-sizing: border-box;
}
.header-row.two {
  background: rgba(0, 0, 0, 0.25);
  border-bottom: 1px solid rgba(255, 255, 255, 0.6);
  z-index: 10000;
}
.header-row.two::before {
  display: block;
  content: "";
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  height: 0;
  background: rgba(0, 0, 0, 0.8);
  opacity: 0;
  -webkit-transition: all 0.2s ease 0s;
  transition: all 0.2s ease 0s;
}
.sticky-header .header-row.two {
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
}
.sticky-header .header-row.two::before {
  height: 100%;
  opacity: 1;
}
.header-row.two .header-column.one {
  -webkit-box-flex: 1;
      -ms-flex-positive: 1;
          flex-grow: 1;
}
.header-row.three {
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
}
.header-row.three::before {
  display: block;
  content: "";
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  height: 407px;
  top: -78px;
  background: url("https://adamcruse.schoolwires.net/cms/lib/SWCS000009/Centricity/template/176/defaults/sitename-glow.svg") no-repeat;
}

.header-column {
  position: relative;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
}

/* ----- MYSTART ----- */
.cs-mystart-dropdown,
.cs-mystart-button {
  -webkit-box-flex: 1;
      -ms-flex-positive: 1;
          flex-grow: 1;
  position: relative;
}
.cs-mystart-dropdown.so,
.cs-mystart-button.so {
  margin-right: -11px;
}
.cs-mystart-dropdown.translate,
.cs-mystart-button.translate {
  margin-right: -13px;
}

.cs-dropdown-selector,
.cs-button-selector {
  display: block;
  text-decoration: none;
  color:#FFFFFF;
  font-family: Poppins, sans-serif;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  font-size: 1.0666rem;
  padding: 8px 24px;
  line-height: 1;
  cursor: pointer;
  -webkit-transition: all 0.3s ease 0s;
  transition: all 0.3s ease 0s;
  height: 100%;
  -webkit-box-sizing: border-box;
          box-sizing: border-box;
  position: relative;
}
.cs-dropdown-selector::before,
.cs-button-selector::before {
  font-family: "template-icon" !important;
  speak: none;
  font-style: normal;
  font-weight: normal;
  font-variant: normal;
  text-transform: none;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  position: absolute;
}
.open .cs-dropdown-selector, .cs-dropdown-selector:hover, .cs-dropdown-selector:focus,
.open .cs-button-selector,
.cs-button-selector:hover,
.cs-button-selector:focus {
  color: #fff;
}
.home .cs-dropdown-selector,
.home .cs-button-selector {
  background:#F57E20;
  color: #000;
}
.home .cs-dropdown-selector:hover, .home .cs-dropdown-selector:focus,
.home .cs-button-selector:hover,
.home .cs-button-selector:focus {
  color:#F57E20;
  background: #000;
  text-decoration: underline;
}
.schools .cs-dropdown-selector,
.schools .cs-button-selector {
  padding-left: 56px;
}
.schools .cs-dropdown-selector::before,
.schools .cs-button-selector::before {
  content: "\e90e";
  left: 25px;
  font-size: 21px;
  top: 13px;
}
.schools .cs-dropdown-selector:hover, .schools .cs-dropdown-selector:focus,
.schools .cs-button-selector:hover,
.schools .cs-button-selector:focus {
  background: #fff;
  color: #000;
}
.translate .cs-dropdown-selector,
.translate .cs-button-selector {
  padding: 8px 14px 8px 34px;
  margin-left: 10px;
  margin-right: 10px;
}
.translate .cs-dropdown-selector:hover, .translate .cs-dropdown-selector:focus,
.translate .cs-button-selector:hover,
.translate .cs-button-selector:focus {
  color: #000;
  background: #fff;
}
.translate .cs-dropdown-selector::before,
.translate .cs-button-selector::before {
  content: "\e911";
  left: 14px;
  font-size: 14px;
  top: 17px;
}
.user-options .cs-dropdown-selector,
.user-options .cs-button-selector {
  width: 75px;
}
.user-options .cs-dropdown-selector::before,
.user-options .cs-button-selector::before {
  content: "\e913";
  font-size: 21px;
  right: 39px;
}
.user-options .cs-dropdown-selector::after,
.user-options .cs-button-selector::after {
  font-family: "template-icon" !important;
  speak: none;
  font-style: normal;
  font-weight: normal;
  font-variant: normal;
  text-transform: none;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  content: "\e903";
  position: absolute;
  font-size: 8px;
  right: 26px;
  display: block;
  -webkit-transition: all 0.3s ease 0s;
  transition: all 0.3s ease 0s;
}
.user-options .cs-dropdown-selector .uo-text-wrapper,
.user-options .cs-button-selector .uo-text-wrapper {
  position: absolute;
  top: calc(100% - 3px);
  right: 0;
  display: block;
  width: 95px;
}
.user-options .cs-dropdown-selector .uo-text-wrapper::before,
.user-options .cs-button-selector .uo-text-wrapper::before {
  display: block;
  content: "";
  height: 0;
  width: 0;
  border-bottom: 8px solid #000;
  border-right: 8px solid transparent;
  border-left: 8px solid transparent;
  position: absolute;
  bottom: calc(100% - 2px);
  left: calc(50% - 9px);
  -webkit-transition: all 0.3s ease 0s;
  transition: all 0.3s ease 0s;
  opacity: 0;
}
.user-options .cs-dropdown-selector .uo-text,
.user-options .cs-button-selector .uo-text {
  background: #000;
  color: #fff;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  font-size: 0.8666rem;
  text-align: center;
  height: 0;
  overflow: hidden;
  -webkit-transition: all 0.3s ease 0s;
  transition: all 0.3s ease 0s;
  border-radius: 2px;
}
.user-options .cs-dropdown-selector:hover .uo-text-wrapper::before, .user-options .cs-dropdown-selector:focus .uo-text-wrapper::before,
.user-options .cs-button-selector:hover .uo-text-wrapper::before,
.user-options .cs-button-selector:focus .uo-text-wrapper::before {
  opacity: 1;
  bottom: calc(100% - 2px);
}
.user-options .cs-dropdown-selector:hover .uo-text, .user-options .cs-dropdown-selector:focus .uo-text,
.user-options .cs-button-selector:hover .uo-text,
.user-options .cs-button-selector:focus .uo-text {
  height: 26px;
}
.open.user-options .cs-dropdown-selector::after,
.open.user-options .cs-button-selector::after {
  -webkit-transform: rotate(180deg);
          transform: rotate(180deg);
}
.quick-translate .cs-dropdown-selector,
.quick-translate .cs-button-selector {
  padding: 8px 12px;
}
.quick-translate .cs-button-selector span {
	position:relative;
}
.quick-translate .cs-button-selector span::after {
	display:block;
    position:absolute;
    left:-1px;
    top:0;
    font-weight:700;
    text-decoration:underline;
    opacity:0;
}
.quick-translate .cs-button-selector:hover span::after,
.quick-translate .cs-button-selector:focus span::after{
	opacity:1;
}
.quick-translate.es .cs-button-selector span::after {
	content: "Español";
}
.quick-translate.so .cs-button-selector span::after {
	content: "Soomaali";
}
.quick-translate .cs-dropdown-selector:hover, .quick-translate .cs-dropdown-selector:focus,
.quick-translate .cs-button-selector:hover,
.quick-translate .cs-button-selector:focus {
  text-decoration: underline;
}
.quick-translate.es .cs-dropdown-selector::after,
.quick-translate.es .cs-button-selector::after {
  display: block;
  content: "";
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  background:#FFFFFF;
  width: 1px;
  height: 18px;
  left: auto;
  right: 0;
  top: 15px;
}
.search .cs-dropdown-selector,
.search .cs-button-selector {
  width: 78px;
  border-left: 1px solid rgba(255, 255, 255, 0.6);
}
.search .cs-dropdown-selector::before,
.search .cs-button-selector::before {
  content: "\e90f";
  font-size: 26px;
}
.search .cs-dropdown-selector:hover, .search .cs-dropdown-selector:focus,
.search .cs-button-selector:hover,
.search .cs-button-selector:focus {
  background-color: rgba(0, 0, 0, 0.5);
}
.search .cs-dropdown-selector.open,
.search .cs-button-selector.open {
  background-color: rgba(0, 0, 0, 0.9);
}
.staffnet .cs-dropdown-selector,
.staffnet .cs-button-selector {
  width: 78px;
  border-left: 1px solid rgba(255, 255, 255, 0.6);
}
.staffnet .cs-dropdown-selector::before,
.staffnet .cs-button-selector::before {
  content: "\e916";
  font-size: 26px;
}
.staffnet .cs-dropdown-selector:hover, .staffnet .cs-dropdown-selector:focus,
.staffnet .cs-button-selector:hover,
.staffnet .cs-button-selector:focus {
  background-color: rgba(0, 0, 0, 0.5);
}
.staffnet .cs-dropdown-selector:hover span, .staffnet .cs-dropdown-selector:focus span,
.staffnet .cs-button-selector:hover span,
.staffnet .cs-button-selector:focus span {
  height: 26px;
}
.staffnet .cs-dropdown-selector span,
.staffnet .cs-button-selector span {
  background: #000;
  color: #fff;
  position: absolute;
  top: 100%;
  left: 0;
  width: 100%;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  font-size: 0.8666rem;
  text-align: center;
  height: 0;
  overflow: hidden;
  -webkit-transition: all 0.3s ease 0s;
  transition: all 0.3s ease 0s;
}
.quick-links .cs-dropdown-selector,
.quick-links .cs-button-selector {
  font-size: 1.0666rem;
  text-transform: uppercase;
  font-weight: 600;
  padding-right: 59px;
  border-left: 1px solid rgba(255, 255, 255, 0.6);
  white-space: nowrap;
}
.quick-links .cs-dropdown-selector::after,
.quick-links .cs-button-selector::after {
  font-family: "template-icon" !important;
  speak: none;
  font-style: normal;
  font-weight: normal;
  font-variant: normal;
  text-transform: none;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  content: "\e90d";
  position: absolute;
  right: 23px;
  top: calc(50% - 11px);
  font-size: 20px;
}
.quick-links .cs-dropdown-selector:hover, .quick-links .cs-dropdown-selector:focus,
.quick-links .cs-button-selector:hover,
.quick-links .cs-button-selector:focus {
  background:#F57E20;
  color: #000;
}
.quick-links.open .cs-dropdown-selector,
.quick-links.open .cs-button-selector {
  background:#F57E20;
  color: #000;
}

.cs-dropdown {
  position: absolute;
  top: 100%;
  width: 100%;
  left: 0;
  background: #FFF;
}
.cs-dropdown .cs-dropdown-list {
  list-style: none;
  margin: 0;
  padding: 4px;
  position: relative;
}
.schools .cs-dropdown {
  width: auto;
}
.schools .cs-dropdown .cs-dropdown-list {
  -webkit-column-count: 3;
     -moz-column-count: 3;
          column-count: 3;
  -webkit-column-gap: 0;
     -moz-column-gap: 0;
          column-gap: 0;
  padding: 29px 18px;
}
.schools .cs-dropdown li {
  -webkit-box-sizing: border-box;
          box-sizing: border-box;
  width: 217px;
  padding: 7px 12px;
  -webkit-column-break-inside: avoid;
     -moz-column-break-inside: avoid;
          break-inside: avoid;
}
.schools .cs-dropdown li a {
  font-weight: 500;
  border-bottom: 2px solid #F57E20;
  padding-bottom: 8px;
  position: relative;
  padding-right: 5px;
}
.schools .cs-dropdown li a::after {
  display: block;
  content: "";
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  height: 2px;
  background: #000;
  top: auto;
  bottom: -2px;
  width: 0;
  opacity: 0;
  -webkit-transition: all 0.3s ease 0s;
  transition: all 0.3s ease 0s;
}
.schools .cs-dropdown li a:hover, .schools .cs-dropdown li a:focus {
  color: #000;
  text-decoration: none;
  padding-left: 5px;
  padding-right: 0px;
}
.schools .cs-dropdown li a:hover::after, .schools .cs-dropdown li a:focus::after {
  width: 100%;
  opacity: 1;
}
.user-options .cs-dropdown {
  left: auto;
  right: 0;
  background: #000;
  width: 148px;
  border-radius: 4px;
  top: calc(100% - 3px);
}
.user-options .cs-dropdown .cs-dropdown-list {
  padding: 12px 4px 11px;
}
.user-options .cs-dropdown li {
  padding: 2px 18px;
}
.user-options .cs-dropdown li a {
  font-size: 0.8666rem;
  color: #fff;
  padding-right: 5px;
}
.user-options .cs-dropdown li a:hover, .user-options .cs-dropdown li a:focus {
  color: #fff;
  padding-left: 5px;
  padding-right: 0px;
}
.translate .cs-dropdown {
  left: auto;
  right: 10px;
  width: auto;
}
.cs-dropdown li {
  width: auto;
  padding: 7px 0px;
}
.cs-dropdown li a {
  font-size: 1.0666rem;
  color: #000;
  -webkit-transition: all 0.3s ease 0s;
  transition: all 0.3s ease 0s;
  width: auto;
  display: block;
  padding: 0px;
  line-height: 1.2;
  text-decoration: none;
}
.cs-dropdown li a:hover, .cs-dropdown li a:focus {
  background: transparent;
  color:#214A9B;
  text-decoration: underline;
}

.cs-mystart-dropdown.translate .cs-dropdown {
  padding: 20px;
  -webkit-box-sizing: border-box;
          box-sizing: border-box;
  text-align: center;
}

#google_translate_element .goog-te-gadget,
#google_translate_element .goog-logo-link,
#google_translate_element .goog-logo-link:link,
#google_translate_element .goog-logo-link:visited,
#google_translate_element .goog-logo-link:hover,
#google_translate_element .goog-logo-link:active {
  color: #231f20;
}

#google_translate_element {
  text-transform: none;
  position: relative;
}

a#ui-btn-mypasskey {
  height: 100%;
  color:#FFFFFF;
  text-decoration: none;
}
a#ui-btn-mypasskey::before {
  font-family: "template-icon" !important;
  speak: none;
  font-style: normal;
  font-weight: normal;
  font-variant: normal;
  text-transform: none;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  content: "\e90a";
  font-size: 24px;
}
a#ui-btn-mypasskey span {
  display: none;
}

div#ui-mypasskey-overlay {
  margin-top: 30px;
  padding: 0;
}

.quick-links .cs-dropdown {
  width: auto;
  left: auto;
  right: 0;
}
.quick-links .cs-dropdown li a:not(.gb-social-media-icon) {
  padding: 0px 5px 0px 0px;
}
.quick-links .cs-dropdown li a:not(.gb-social-media-icon):hover, .quick-links .cs-dropdown li a:not(.gb-social-media-icon):focus {
  padding: 0px 0px 0px 5px;
}
.quick-links .csmc-section-columns {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  padding: 7px 22px;
}
.quick-links .csmc-column {
  width: 284px;
  -webkit-box-sizing: border-box;
          box-sizing: border-box;
  padding: 14px;
  list-style: none;
}
.quick-links .csmc-column .column-heading {
  font-size: 1.2rem;
  text-transform: uppercase;
  font-weight: 600;
}
.quick-links .csmc-column .column-heading.social-icons {
  padding-top: 13px;
  margin-bottom: 5px;
}
.quick-links .csmc-column .column-heading h3, .quick-links .csmc-column .column-heading h2:not(.csmc-title) {
  font-size: 1.2rem;
  border-bottom: 2px solid #F57E20;
  padding-bottom: 6px;
  margin-bottom: 2px;
  width: 100%;
}
.quick-links .csmc-column .social-icons {
  position: relative;
}
.quick-links .csmc-column .social-icons:not(.column-heading) {
  padding-bottom: 43px;
}
.quick-links .csmc-column .social-icons > span {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: justify;
      -ms-flex-pack: justify;
          justify-content: space-between;
  position: absolute;
  left: -7px;
  width: 100%;
}
.quick-links .csmc-column .social-icons > span .gb-social-media-icon {
  color: #fff;
  padding: 10px 7px;
}
.quick-links .csmc-column .social-icons > span .gb-social-media-icon:hover, .quick-links .csmc-column .social-icons > span .gb-social-media-icon:focus {
  text-decoration: none;
  padding: 0px 7px 20px;
}

/* ----- SCHOOLS ----- */
/* ----- SEARCH ----- */
#gb-search {
  position: absolute;
  width: 0;
  height: 100%;
  right: 100%;
  background: #fff;
  z-index: 10000;
  overflow: hidden;
  -webkit-transition: all 0.3s ease 0s;
  transition: all 0.3s ease 0s;
  opacity: 0;
  top: 0;
}
.open #gb-search {
  width: 400px;
  opacity: 1;
}

#gb-search-form {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  background: #fff;
  -webkit-box-flex: 1;
      -ms-flex-positive: 1;
          flex-grow: 1;
  height: 100%;
}
#gb-search-form label {
  width: 0 !important;
  height: 0 !important;
  display: block !important;
  padding: 0 !important;
  margin: 0 !important;
  border: 0 !important;
  overflow: hidden !important;
}
#gb-search-form input {
  -webkit-box-flex: 1;
      -ms-flex-positive: 1;
          flex-grow: 1;
  border: 0;
  font-size: 1.312rem;
  font-family: Poppins, sans-serif;
  margin: 0;
  height: auto;
  padding: 10px 33px;
}

#logo-sitename {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  padding: 8px 24px;
  position: relative;
}
#logo-sitename .logo {
  padding-right: 21px;
  -ms-flex-negative: 0;
      flex-shrink: 0;
}
#logo-sitename .logo a {
  display: block;
}
#logo-sitename .logo img {
  max-width: 100%;
  height: auto;
}
#logo-sitename .sitename h1 {
  font-size: 3.6666rem;
  line-height: 1.018;
  font-weight: 400;
  color: #000;
  font-family: "Times New Roman", serif;
  margin: 0;
}
#logo-sitename .sitename span {
  display: block;
}

[data-show-sitename=false] .sitename,
[data-show-logo=false] .logo {
  display: none;
}

/* ----- CHANNELS ----- */
#gb-channel-bar {
  position: static;
  margin: 0;
}

#sw-channel-list-container {
  width: 100%;
  padding: 0 9px;
  position: static;
}

ul.sw-channel-list li.sw-channel-item {
  height: 78px;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  padding: 0px;
  margin: 0;
}
ul.sw-channel-list li.sw-channel-item.csmc {
  position: static;
}
ul.sw-channel-list li.sw-channel-item > a {
  color: #FFF;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  padding: 0 20px;
  text-decoration: none;
  font-size: 1.0666rem;
  font-weight: 600;
  text-transform: uppercase;
  -webkit-box-sizing: border-box;
          box-sizing: border-box;
  position: relative;
  -webkit-transition: all 0.3s ease 0s;
  transition: all 0.3s ease 0s;
  z-index: 8000;
}
ul.sw-channel-list li.sw-channel-item > a::before {
  display: block;
  content: "";
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  -webkit-transition: all 0.3s ease 0s;
  transition: all 0.3s ease 0s;
  background:#F57E20;
  width: 0;
  left: 50%;
  height: 2px;
  opacity: 0;
  top: auto;
  bottom: -1px;
}
ul.sw-channel-list li.sw-channel-item > a span {
  position: relative;
  padding: 0;
}
ul.sw-channel-list li.sw-channel-item.hover > a::before, ul.sw-channel-list li.sw-channel-item.active > a::before {
  opacity: 1;
  width: 100%;
  left: 0;
}

/* ----- DROPDOWNS ----- */
.csmc ul.sw-channel-dropdown {
  width: calc(100% - 40px);
  left: 20px;
}

ul.sw-channel-dropdown {
  top: 100%;
  left: 0px;
  position: absolute;
  height: auto;
  width: 76vw;
  max-height: 500px;
  overflow: auto;
  border: none;
  padding: 11px 4px;
  text-align: left;
  background: #fff;
  -webkit-box-sizing: border-box;
          box-sizing: border-box;
}
ul.sw-channel-dropdown li.csmc-section-columns {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  padding: 0 18px;
}
ul.sw-channel-dropdown li.csmc-section-columns .column-heading {
  font-size: 1.2rem;
  text-transform: uppercase;
  font-weight: 600;
}
ul.sw-channel-dropdown li.csmc-section-columns .csmc-column {
  margin: 0;
  padding: 15px 14px;
  -ms-flex-preferred-size: 25%;
      flex-basis: 25%;
  display: block !important;
  -webkit-box-sizing: border-box;
          box-sizing: border-box;
  min-width: 256px;
}
ul.sw-channel-dropdown li.csmc-section-columns .csmc-column h3, ul.sw-channel-dropdown li.csmc-section-columns .csmc-column h2:not(.csmc-title) {
  font-size: 1.2rem;
  border-bottom: 2px solid #F57E20;
  padding-bottom: 6px;
  margin-bottom: 2px;
}
ul.sw-channel-dropdown li.csmc-section-columns .csmc-column.extra {
  -ms-flex-preferred-size: 50%;
      flex-basis: 50%;
}
ul.sw-channel-dropdown li.csmc-section-columns .csmc-column.extra li {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-align: start;
      -ms-flex-align: start;
          align-items: flex-start;
}
ul.sw-channel-dropdown .csmc-image {
  margin-right: 28px;
  max-width: 100%;
}
ul.sw-channel-dropdown .csmc-title {
  font-size: 1.6rem;
}
ul.sw-channel-dropdown .csmc-description {
  font-size: 0.9333rem;
  line-height: 1.8571;
}
ul.sw-channel-dropdown li {
  width: auto;
  padding: 8px 0px;
}
ul.sw-channel-dropdown li a {
  font-size: 1.0666rem;
  color: #000;
  -webkit-transition: all 0.3s ease 0s;
  transition: all 0.3s ease 0s;
  width: auto;
  padding: 0px 5px 0px 0px;
  line-height: 1.2;
}
ul.sw-channel-dropdown li a:hover, ul.sw-channel-dropdown li a:focus {
  background: transparent;
  color:#214A9B;
  text-decoration: underline;
  padding: 0px 0px 0px 5px;
}

.sw-channel-item:not(.csmc) ul.sw-channel-dropdown {
  width: 256px;
  padding: 26px 28px;
}

.sw-channel-item[data-column-count="1"] ul.sw-channel-dropdown {
  width: 256px;
}
.sw-channel-item[data-column-count="1"] ul.sw-channel-dropdown li.csmc-section-columns .csmc-column {
  -ms-flex-preferred-size: 100%;
      flex-basis: 100%;
}

.sw-channel-item[data-column-count="2"] ul.sw-channel-dropdown li.csmc-section-columns .csmc-column {
  -ms-flex-preferred-size: 50%;
      flex-basis: 50%;
}

.sw-channel-more-li {
  background: none transparent;
}

/* GroupEnd */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
/* -------------------------- ### HOMEPAGE ### ---------------------------- */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
/* GroupBegin Homepage */
.hp-row {
  position: relative;
}
.hp-row:not(.one) .app {
  margin-bottom: 50px;
}

.hp-column {
  position: relative;
}

/* ----- SLIDESHOW ----- */
[data-slideshow-option="Streaming Video"] .multimedia-gallery {
  display: none;
}

#hp-slideshow-outer {
  position: relative;
  overflow: hidden;
  padding-top: 49px;
}
#hp-slideshow-outer[data-slideshow-gradient=false] #hp-slideshow .mmg-image::after,
#hp-slideshow-outer[data-slideshow-gradient=false] #hp-slideshow .cs-streaming-video::after {
  display: none;
}
#hp-slideshow-outer #hp-slideshow .mmg-description,
#hp-slideshow-outer #hp-slideshow .cs-streaming-video-description {
  background: transparent;
  bottom: 120px;
  width: 72vw;
  margin: 0 auto;
  right: 0;
  display: block;
  position: absolute;
  left: 0;
  z-index: 10;
}
#hp-slideshow-outer #hp-slideshow .mmg-description-inner {
	height:100%;
    display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
    flex-direction:column;
    -webkit-flex-direction:column;
    justify-content:center;
    -webkit-justify-content:center;
}
#hp-slideshow-outer #hp-slideshow .cs-streaming-video-description {
  -webkit-animation: fade-in 3.5s cubic-bezier(0.39, 0.575, 0.565, 1) 3.5s both;
  animation: fade-in 3.5s cubic-bezier(0.39, 0.575, 0.565, 1) 3.5s both;
}
#hp-slideshow-outer #hp-slideshow [data-transition=fade][data-is-animating=true] .mmg-slide.next .mmg-description {
  -webkit-animation: fade-in 2s cubic-bezier(0.390, 0.575, 0.565, 1.000) 1s both;
  animation: fade-in 2s cubic-bezier(0.390, 0.575, 0.565, 1.000) 1s both;
}
#hp-slideshow-outer #hp-slideshow [data-transition=fade][data-is-animating=true] .mmg-slide.active .mmg-description {
  display: block;
  -webkit-animation: fade-out 2s ease-out both;
  animation: fade-out 2s ease-out both;
}
#hp-slideshow-outer #hp-slideshow .mmg-image::after,
#hp-slideshow-outer #hp-slideshow .cs-streaming-video::after {
  display: block;
  content: "";
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  top: auto;
  bottom: 0;
  height: 80%;
  background: -webkit-gradient(linear, left top, left bottom, from(rgba(0, 0, 0, 0)), color-stop(1%, rgba(0, 0, 0, 0)), color-stop(50%, rgba(0, 0, 0, 0.4)), to(black));
  background: linear-gradient(to bottom, rgba(0, 0, 0, 0) 0%, rgba(0, 0, 0, 0) 1%, rgba(0, 0, 0, 0.4) 50%, black 100%);
  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr="#00000000", endColorstr="#000000",GradientType=0 );
  opacity: 0.5;
  z-index: 2;
}
#hp-slideshow-outer #hp-slideshow .mmg-description-title,
#hp-slideshow-outer #hp-slideshow .cs-streaming-video-title {
  font-size: 3.3333rem;
  font-weight: 700;
  font-style: normal;
  color: #fff;
  font-family: Poppins, sans-serif;
  text-shadow: 0px 2px 6px rgba(0, 0, 0, 0.25);
  max-width: 60%;
  line-height: 1.1;
  display: block;
  background: transparent;
}
#hp-slideshow-outer #hp-slideshow .mmg-description-caption,
#hp-slideshow-outer #hp-slideshow .cs-streaming-video-caption {
  color: #fff;
  font-family: Poppins, sans-serif;
  font-size: 1.3333rem;
  font-weight: 500;
  padding-top: 9px;
  line-height: 1.5;
  text-shadow: 0px 3px 6px rgba(0, 0, 0, 0.25);
  max-width: 60%;
  display: block;
  background: transparent;
}
#hp-slideshow-outer #hp-slideshow .mmg-description-links,
#hp-slideshow-outer #hp-slideshow .cs-streaming-video-links {
  padding-top: 22px;
  margin: 0;
}
#hp-slideshow-outer #hp-slideshow .mmg-description-link,
#hp-slideshow-outer #hp-slideshow .cs-streaming-video-link {
  display: inline-block;
  padding: 14px 32px;
  text-decoration: none;
  -webkit-transition: all 0.3s ease 0s;
  transition: all 0.3s ease 0s;
  font-family: Poppins, sans-serif;
  font-size: 0.9333rem;
  position: relative;
  font-weight: 500;
  background:#214A9B;
  color:#FFFFFF;
  text-transform: capitalize;
  line-height: 1;
}
#hp-slideshow-outer #hp-slideshow .mmg-description-link:hover, #hp-slideshow-outer #hp-slideshow .mmg-description-link:focus,
#hp-slideshow-outer #hp-slideshow .cs-streaming-video-link:hover,
#hp-slideshow-outer #hp-slideshow .cs-streaming-video-link:focus {
  background:#FFFFFF;
  color:#214A9B;
}
#hp-slideshow-outer #hp-slideshow .mmg-control,
#hp-slideshow-outer #hp-slideshow .cs-streaming-video-control {
  background: #214A9B;
  position: relative;
  width: 48px;
  height: 48px;
  border-radius: 100%;
  top: auto;
  left: auto;
  color:#FFFFFF;
  margin: 0 6px;
  font-size: 18px;
  -webkit-transition: all 0.3s ease 0s;
  transition: all 0.3s ease 0s;
}
#hp-slideshow-outer #hp-slideshow .mmg-control:hover, #hp-slideshow-outer #hp-slideshow .mmg-control:focus,
#hp-slideshow-outer #hp-slideshow .cs-streaming-video-control:hover,
#hp-slideshow-outer #hp-slideshow .cs-streaming-video-control:focus {
  color:#214A9B;
  background:#FFFFFF;
}
#hp-slideshow-outer #hp-slideshow .mmg-control::before,
#hp-slideshow-outer #hp-slideshow .cs-streaming-video-control::before {
  font-family: "template-icon" !important;
  speak: none;
  font-style: normal;
  font-weight: normal;
  font-variant: normal;
  text-transform: none;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}
#hp-slideshow-outer #hp-slideshow .mmg-control.back::before,
#hp-slideshow-outer #hp-slideshow .cs-streaming-video-control.back::before {
  content: "\e900";
}
#hp-slideshow-outer #hp-slideshow .mmg-control.next::before,
#hp-slideshow-outer #hp-slideshow .cs-streaming-video-control.next::before {
  content: "\e901";
}
#hp-slideshow-outer #hp-slideshow .mmg-control.play-pause.playing::before,
#hp-slideshow-outer #hp-slideshow .cs-streaming-video-control.play-pause.playing::before {
  content: "\e90b";
}
#hp-slideshow-outer #hp-slideshow .mmg-control.play-pause.paused::before,
#hp-slideshow-outer #hp-slideshow .cs-streaming-video-control.play-pause.paused::before {
  content: "\e917";
  padding-left:4px;
}
#hp-slideshow-outer #hp-slideshow .mmg-control.video-control span,
#hp-slideshow-outer #hp-slideshow .cs-streaming-video-control.video-control span {
  display: none;
}
#hp-slideshow-outer #hp-slideshow .mmg-control.video-control.paused::before,
#hp-slideshow-outer #hp-slideshow .cs-streaming-video-control.video-control.paused::before {
  content: "\e917";
}
#hp-slideshow-outer #hp-slideshow .mmg-control.video-control.playing::before,
#hp-slideshow-outer #hp-slideshow .cs-streaming-video-control.video-control.playing::before {
  content: "\e90b";
}
#hp-slideshow-outer #hp-slideshow .mmg-control.audio-control span,
#hp-slideshow-outer #hp-slideshow .cs-streaming-video-control.audio-control span {
  display: none;
}
#hp-slideshow-outer #hp-slideshow .mmg-control.audio-control.paused::before,
#hp-slideshow-outer #hp-slideshow .cs-streaming-video-control.audio-control.paused::before {
  /* UNMUTE */
  content: "\e918";
}
#hp-slideshow-outer #hp-slideshow .mmg-control.audio-control.playing::before,
#hp-slideshow-outer #hp-slideshow .cs-streaming-video-control.audio-control.playing::before {
  /* MUTE */
  content: "\e908";
}
#hp-slideshow-outer #hp-slideshow .mmg-control.watch-video::before,
#hp-slideshow-outer #hp-slideshow .cs-streaming-video-control.watch-video::before {
  content: "\e909";
}
#hp-slideshow-outer #hp-slideshow .mmg-control.play-pause span {
  display: none;
}

.mmg-control-wrapper,
.cs-streaming-video-controls {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  position: absolute;
  padding: 41px 21px;
  bottom: 0;
  left: 0;
  z-index: 100;
}

.mmg-fixed-btns .mmg-control-wrapper,
.mmg-fixed-btns .cs-streaming-video-controls,
.mmg-fixed-btns .mmg-description,
.mmg-fixed-btns .cs-streaming-video-description {
  position: fixed !important;
}

#hp-slideshow-outer:not(.mmg-fixed-btns) .mmg-description {
	top:auto !important;
}

/* GroupEnd */
.hp-row.slider .ui-widget-header {
  text-align: center;
}
.hp-row.slider .ui-widget-header::before {
  display: block;
  content: "";
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.1);
  height: 2px;
  top: 50%;
}
.hp-row.slider .ui-widget-header h1 {
  display: inline-block;
  background: #fff;
  position: relative;
  padding: 0 36px;
}
.hp-row.slider .upcomingevents .ui-widget-header {
  margin-bottom: 22px;
}
.hp-row.slider .upcomingevents li {
  padding: 0 15px;
}
.hp-row.slider .upcomingevents li.hover h1.sw-calendar-block-date .joel-month {
  border-color: rgba(0, 0, 0, 0.4);
}
.hp-row.slider .upcomingevents h1.sw-calendar-block-date {
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
  border: 0;
  padding: 0;
}
.hp-row.slider .upcomingevents h1.sw-calendar-block-date .adam-hug {
  -webkit-box-orient: vertical;
  -webkit-box-direction: reverse;
      -ms-flex-direction: column-reverse;
          flex-direction: column-reverse;
  width: 100%;
}
.hp-row.slider .upcomingevents h1.sw-calendar-block-date .jeremy-date {
  margin: 0;
  font-size: 4rem;
}
.hp-row.slider .upcomingevents h1.sw-calendar-block-date .joel-month {
  border: 2px solid rgba(0, 0, 0, 0.1);
  border-radius: 2px;
  width: 100%;
  -webkit-box-sizing: border-box;
          box-sizing: border-box;
  text-align: center;
  padding: 4px 0;
  -webkit-transition: all 0.3s ease 0s;
  transition: all 0.3s ease 0s;
}
.hp-row.slider .upcomingevents h1.sw-calendar-block-date .joel-day {
  margin-bottom: -5px;
}
.hp-row.slider .upcomingevents .upcoming-column.right {
  text-align: center;
  padding-top: 7px;
}
.hp-row.slider .headlines .ui-widget-header {
  margin-bottom: 19px;
}
.hp-row.slider .headlines li {
  padding: 5px 20px 0px;
  -webkit-transition: all 0.3s ease 0s;
  transition: all 0.3s ease 0s;
}
.hp-row.slider .headlines li.hover {
  padding: 0px 20px 5px;
}
.hp-row.slider .headlines .ui-articles {
  margin: 0 -7px;
}
.hp-row.slider .headlines .ui-article {
  background:#214A9B;
  -webkit-box-sizing: border-box;
          box-sizing: border-box;
  height: 100%;
  padding: 0;
}
.hp-row.slider .headlines .ui-article-thumb {
  float: none;
}
.hp-row.slider .headlines .ui-article-thumb .img {
  margin: 0 0 10px;
  border-radius: 0;
}
.hp-row.slider .headlines .ui-article-thumb .img img {
  max-width: none !important;
  max-height: none !important;
}
.hp-row.slider .headlines h1.ui-article-title {
  color:#FFFFFF;
  padding: 22px 27px 6px;
  margin: 0;
}
.hp-row.slider .headlines h1.ui-article-title a {
  padding-bottom: 5px;
  display: inline-block;
  position: relative;
}
.hp-row.slider .headlines h1.ui-article-title a::after {
  display: block;
  content: "";
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  height: 1px;
  background:#F57E20;
  top: calc(100% - 5px);
  width: 0;
  opacity: 0;
  -webkit-transition: all 0.3s ease 0s;
  transition: all 0.3s ease 0s;
}
.hp-row.slider .headlines h1.ui-article-title a:hover, .hp-row.slider .headlines h1.ui-article-title a:focus {
  text-decoration: none;
}
.hp-row.slider .headlines h1.ui-article-title a:hover::after, .hp-row.slider .headlines h1.ui-article-title a:focus::after {
  width: 100%;
  opacity: 1;
}
.hp-row.slider .headlines .ui-article-description {
  color:#FFFFFF;
  padding: 0px 27px 11px;
}
.hp-row.slider .headlines .has-slider-controls {
  padding-top: 32px;
}
.hp-row.slider .announcements li .ui-article::after {
  display: none;
}
.hp-row.slider .has-slider-controls {
  text-align: center;
}
.hp-row.slider .ui-widget.app.no-slider .ui-articles {
	justify-content:center;
    -webkit-justify-content:center;
}

#gb-page .hp-row.slider div.ui-widget.app.upcomingevents .cs-article-slider-control {
  background:#214A9B;
  color:#FFFFFF;
}
#gb-page .hp-row.slider div.ui-widget.app.upcomingevents .cs-article-slider-control:hover, #gb-page .hp-row.slider div.ui-widget.app.upcomingevents .cs-article-slider-control:focus {
  background:#132D62;
  color:#FFFFFF;
}
#gb-page .hp-row.slider div.ui-widget.app.headlines .cs-article-slider-control {
  background:#214A9B;
  color:#FFFFFF;
  top: calc(25% + 22px);
}
#gb-page .hp-row.slider div.ui-widget.app.headlines .cs-article-slider-control:hover, #gb-page .hp-row.slider div.ui-widget.app.headlines .cs-article-slider-control:focus {
  background:#132D62;
  color:#FFFFFF;
}

#gb-icons {
  background:#214A9B;
}
#gb-icons .content-wrap {
  display: block;
}

.hp-row.three {
  padding: 60px 0 25px;
}

.hp-row.four {
  padding: 25px 0 45px;
}

.hp-row.five .hp-column-wrap {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  margin: 0 -25px;
}
.hp-row.five .hp-column {
  padding: 0 25px;
  -webkit-box-sizing: border-box;
          box-sizing: border-box;
  width: 0;
  -webkit-box-flex: 1;
      -ms-flex-positive: 1;
          flex-grow: 1;
  display: none;
}
.hp-row.five .hp-column.has-app {
  display: block;
}
.hp-row.five div.ui-widget.app div.ui-widget-header {
  font-size: 2rem;
  padding-bottom: 30px;
}
.hp-row.five div.ui-widget.app div.ui-widget-header::after {
  display: block;
  content: "";
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  height: 2px;
  background: rgba(0, 0, 0, 0.1);
  top: auto;
  bottom: 16px;
}

/* CHOOSE 742 */
.choose-section {
  color:#FFFFFF;
  padding: 96px 0 79px;
  background-size: cover;
  background-attachment: fixed;
}
.choose-section::before {
  display: block;
  content: "";
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  background-color:#214A9B;
  opacity: 0.8;
}
.choose-section img {
  max-width: 100%;
  margin-bottom: 24px;
}
.choose-section .content-wrap {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: justify;
      -ms-flex-pack: justify;
          justify-content: space-between;
}
.choose-section .choose-column.one {
  max-width: 31%;
  padding-right: 82px;
}
.choose-section .choose-column.two {
  -webkit-box-flex: 1;
      -ms-flex-positive: 1;
          flex-grow: 1;
}
.choose-section h2 {
  font-size: 2rem;
}
.choose-section p {
  font-size: 1.0666rem;
  line-height: 1.75;
}
.choose-section .choose-section-links {
  list-style: none;
  margin: 0;
  padding: 0;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -ms-flex-wrap: wrap;
      flex-wrap: wrap;
}
.choose-section .choose-section-links li {
  position: relative;
  -ms-flex-preferred-size: 33.3333%;
      flex-basis: 33.3333%;
  min-width: 33.3333%;
  -webkit-box-sizing: border-box;
          box-sizing: border-box;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  padding: 11px;
}
.choose-section .choose-section-links li a {
  color:#FFFFFF;
  position: relative;
  font-size: 1.1vw;
  text-decoration: none;
  font-weight: 600;
  line-height: 1.5;
  height: 110px;
  -webkit-box-sizing: border-box;
          box-sizing: border-box;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  text-align: center;
  width: 100%;
  padding: 0 35px;
  border-radius: 2px;
}
.choose-section .choose-section-links li a::before {
  display: block;
  content: "";
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  background:#000000;
  opacity: 0.4;
  border-radius: 2px;
}
.choose-section .choose-section-links li a::after {
  display: block;
  content: "";
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  height: 2px;
  top: auto;
  bottom: 2px;
  background:#F57E20;
  width: calc(100% - 80px);
  left: 40px;
  -webkit-transition: all 0.3s ease 0s;
  transition: all 0.3s ease 0s;
}
.choose-section .choose-section-links li a span {
  position: relative;
  padding: 5px 0px;
  -webkit-transition: all 0.3s ease 0s;
  transition: all 0.3s ease 0s;
}
.choose-section .choose-section-links li a:hover::after, .choose-section .choose-section-links li a:focus::after {
  width: calc(100% - 4px);
  left: 2px;
}
.choose-section .choose-section-links li a:hover span, .choose-section .choose-section-links li a:focus span {
  padding: 0 0 10px;
}

#gb-page .hp-row div.ui-widget.app .cs-article-slider-control {
  width: 48px;
  height: 48px;
  border-radius: 50%;
  text-decoration: none;
  -webkit-transition: all 0.3s ease 0s;
  transition: all 0.3s ease 0s;
  position: absolute;
  top: 25%;
}
#gb-page .hp-row div.ui-widget.app .cs-article-slider-control::before {
  font-family: "template-icon" !important;
  speak: none;
  font-style: normal;
  font-weight: normal;
  font-variant: normal;
  text-transform: none;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  color: inherit;
  font-size: 18px;
}
#gb-page .hp-row div.ui-widget.app .cs-article-slider-control.back {
  left: -53px;
}
#gb-page .hp-row div.ui-widget.app .cs-article-slider-control.back::before {
  content: "\e900";
}
#gb-page .hp-row div.ui-widget.app .cs-article-slider-control.next {
  right: -53px;
}
#gb-page .hp-row div.ui-widget.app .cs-article-slider-control.next::before {
  content: "\e901";
}

/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
/* --------------------------- ### SUBPAGE ### ---------------------------- */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
/* GroupBegin Subpage */
#sw-content-layout-wrapper {
  background-size:0 0 !important;
}
.sp #gb-header {
  background-size: cover;
  background-position: center center;
  position: relative;
}

.sp #gb-icons {
  border-bottom: 1px solid #FFF;
}

.sp .header-row.two::before {
  background: rgba(0, 0, 0, 0.9);
}
.sp .header-row.three {
  -webkit-box-align: inherit;
      -ms-flex-align: inherit;
          align-items: inherit;
}
.sp .header-row.three::before {
  height: calc(100% + 78px);
  top: auto;
  bottom: 0;
  -webkit-transition: all 0.3s ease 0s;
  transition: all 0.3s ease 0s;
}
.sp .header-row.three::after {
  background: rgba(0, 0, 0, 0.12);
  display: block;
  content: "";
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  z-index: -1;
}

.header-row.four {
  display: none;
}

#sp-content {
  min-height: 500px;
  padding-top: 63px;
  padding-bottom: 50px;
}
#sp-content .content-wrap {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
}

.ui-return {
  display: block;
  margin-bottom: 30px;
}

.breadcrumb-wrapper {
  padding-right: 7.5vw;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-align: end;
      -ms-flex-align: end;
          align-items: flex-end;
  position: relative;
}

ul.ui-breadcrumbs {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  width: 100%;
  -ms-flex-wrap: wrap;
      flex-wrap: wrap;
  background-color: #000000;
  margin: 0;
  padding: 12px 0 10px 12px;
}
ul.ui-breadcrumbs > li {
  position: relative;
  color: #FFFFFF;
  font-size: 1.0666rem;
  margin: 0 10px;
  font-weight: 400;
}
ul.ui-breadcrumbs > li.ui-breadcrumb-first > a > span {
  display: none;
}
ul.ui-breadcrumbs > li > a {
  background: none;
  text-decoration: none;
  color: #FFFFFF;
  padding-right: 5px;
}
ul.ui-breadcrumbs > li > a:hover, ul.ui-breadcrumbs > li > a:focus {
  text-decoration: underline;
}
ul.ui-breadcrumbs > li::after {
  content: "\e915";
  position: absolute;
  right: -10px;
  top: 9px;
  font-size: 8px;
  font-family: "template-icon" !important;
  speak: none;
  font-style: normal;
  font-weight: normal;
  font-variant: normal;
  text-transform: none;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}
ul.ui-breadcrumbs > li:last-child {
  font-weight: 600;
}
ul.ui-breadcrumbs > li:last-child::after {
  display: none;
}

.sp-column.one {
  min-width: 320px;
  padding-top: 6px;
}
.sp-column.one div.ui-widget.app div.ui-widget-header {
  margin-bottom: 11px;
  font-size: 1.3333rem;
  text-transform: none;
  color:#132D62;
}

.sp-column.two {
  -webkit-box-flex: 1;
      -ms-flex-positive: 1;
          flex-grow: 1;
  -webkit-flex-grow: 1;
  padding-left: 25px;
}
.sp-column.two #sw-content-layout-wrapper > div[id*=sw-content-layout] {
  width: calc(100% + 25px);
}
.sp-column.two div.ui-widget.app {
  padding: 0px 25px;
  margin-bottom: 40px;
}

.sp.spn .sp-column.two {
  padding-left: 0;
}

/* GroupEnd */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
/* ---------------------------- ### FOOTER ### ---------------------------- */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
/* GroupBegin Footer */
.cs-bb-footer-outer {
	font-size:0.9333rem;
}
.footer-column {
  position: relative;
}

.footer-row {
  position: relative;
}
.footer-row .content-wrap {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: justify;
      -ms-flex-pack: justify;
          justify-content: space-between;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
}
.footer-row.one {
  background:#214A9B;
  color: #FFFFFF;
  padding: 27px 0;
}
.footer-row.one .footer-column.one {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
}
.footer-row.two {
  background:#132D62;
  color: #FFFFFF;
  padding: 5px 0;
}

.contact-info {
  font-size: 1.3333rem;
  line-height: 1.7;
}
.contact-info:first-child {
  margin-right: 108px;
}

.social-icons {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  margin-right: -7px;
}

.gb-social-media-icon {
  padding: 10px 7px;
  -webkit-transition: all 0.3s ease 0s;
  transition: all 0.3s ease 0s;
  color: #fff;
  text-decoration: none;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
}
.gb-social-media-icon span {
  width: 32px;
  height: 32px;
  -webkit-transition: all 0.3s ease 0s;
  transition: all 0.3s ease 0s;
  border-radius: 50%;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
}
.gb-social-media-icon span::before {
  font-family: "template-icon" !important;
  speak: none;
  font-style: normal;
  font-weight: normal;
  font-variant: normal;
  text-transform: none;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}
.gb-social-media-icon:hover, .gb-social-media-icon:focus {
  padding: 0px 7px 20px;
}
.gb-social-media-icon:hover span, .gb-social-media-icon:focus span {
  -webkit-transform: scale(1.3);
          transform: scale(1.3);
}
.gb-social-media-icon.facebook span {
  background: #1E77F2;
}
.gb-social-media-icon.facebook span::before {
  content: "\e904";
}
.gb-social-media-icon.twitter span {
  background: #55ACEE;
}
.gb-social-media-icon.twitter span::before {
  content: "\e912";
}
.gb-social-media-icon.youtube span {
  background: #FF0000;
}
.gb-social-media-icon.youtube span::before {
  content: "\e914";
}
.gb-social-media-icon.instagram span {
  background: #BC2A8D;
}
.gb-social-media-icon.instagram span::before {
  content: "\e905";
}
.gb-social-media-icon.linkedin span {
  background: #0077B5;
}
.gb-social-media-icon.linkedin span::before {
  content: "\e906";
}
.gb-social-media-icon.pinterest span {
  background: #CB2027;
}
.gb-social-media-icon.pinterest span::before {
  content: "\e90c";
}

.cs-bb-footer-outer {
  padding-left: 0;
}

.footer-link {
  color: #fff;
  text-decoration: none;
  position: relative;
  display: inline-block;
  padding: 5px;
  font-size: 0.9333rem;
  font-weight: 600;
  -webkit-transition: all 0.3s ease 0s;
  transition: all 0.3s ease 0s;
  margin-right: -5px;
}
.footer-link:hover, .footer-link:focus {
  text-decoration: underline;
}

/* GroupEnd */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
/* ------------------------ ### EDITOR STYLES ### ------------------------- */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
html {
  font-size: 15px;
  line-height: 1.666;
}

/* GroupBegin EditorStyles */
body {
  font-family: Poppins, sans-serif;
  font-weight: 400;
  color: #000;
  text-rendering: optimizeLegibility !important;
  -webkit-font-smoothing: antialiased !important;
  -moz-font-smoothing: antialiased !important;
  font-smoothing: antialiased !important;
  -moz-osx-font-smoothing: grayscale;
}

.Border_Box {
  display: block;
  padding: 30px;
  border: 1px solid #D5D5D5;
}

.Primary_Box {
  background:#214A9B;
  color:#FFFFFF;
  display: block;
  padding: 30px;
}

.Secondary_Box {
  background:#000000;
  color:#FFFFFF;
  display: block;
  padding: 30px;
}

.Box_Title {
  font-weight: 700;
  font-size: 1.3333rem;
  display: inline-block;
  margin-bottom: 10px;
}

.Featured_Content_Button {
  display: inline-block;
  
}

/* GroupEnd */
.Featured_Content_Button {
  
}
.Featured_Content_Button::before {
	background:#214A9B;
    position:absolute;
    left:0px;
    top:0px;
    width:100%;
    height:100%;
    content:"";
    display:block;
    z-index:-1;
    -webkit-transition: all 0.3s ease 0s;
  transition: all 0.3s ease 0s;
}
.Featured_Content_Button:hover, .Featured_Content_Button:focus {
  color:#FFFFFF;
}
.Featured_Content_Button:hover::before, .Featured_Content_Button:focus::before {
  background:#132D62;
  height:calc(100% + 8px);
  top:-4px;
}

.Primary_Box a {
  color:#FFFFFF !important;
  text-decoration: underline;
}

.Secondary_Box a {
  color:#FFFFFF !important;
  text-decoration: underline;
}

.Border_Box a {
  color:#214A9B !important;
  text-decoration: underline;
}

/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
/* ------------------------ ### HIGH CONTRAST ### ------------------------- */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
.cs-high-contrast #gb-page .header-row.one,
.cs-high-contrast #gb-page #hp-slideshow-outer #hp-slideshow .mmg-description-link,
.cs-high-contrast #gb-page #hp-slideshow-outer #hp-slideshow .cs-streaming-video-link,
.cs-high-contrast #gb-page #hp-slideshow-outer #hp-slideshow .mmg-control,
.cs-high-contrast #gb-page #hp-slideshow-outer #hp-slideshow .cs-streaming-video-control,
.cs-high-contrast #gb-page #gb-icons,
.cs-high-contrast #gb-page .view-calendar-link, .cs-high-contrast #gb-page .more-link, .cs-high-contrast #gb-page div.app-level-social-rss a.ui-btn-toolbar.rss,
.cs-high-contrast #gb-page .hp-row.slider div.ui-widget.app.upcomingevents .cs-article-slider-control,
.cs-high-contrast #gb-page .hp-row.slider div.ui-widget.app.headlines .cs-article-slider-control,
.cs-high-contrast #gb-page .hp-row.slider .headlines .ui-article,
.cs-high-contrast #gb-page .choose-section::before,
.cs-high-contrast #gb-page .choose-section .choose-section-links li a::before,
.cs-high-contrast #gb-page .footer-row.two,
.cs-high-contrast #gb-page .gb-social-media-icon span,
.cs-high-contrast #gb-page .Featured_Content_Button,
.cs-high-contrast #gb-page .Secondary_Box,
.cs-high-contrast #gb-page .quick-links .cs-dropdown-selector:hover,
.cs-high-contrast #gb-page .quick-links .cs-dropdown-selector:focus,
.cs-high-contrast #gb-page .quick-links .cs-button-selector:hover,
.cs-high-contrast #gb-page .quick-links .cs-button-selector:focus,
.cs-high-contrast #gb-page #rs-menu-btn,
.cs-high-contrast #gb-page .Featured_Content_Button::before {
  background: #000;
}
.cs-high-contrast #gb-page .home .cs-dropdown-selector,
.cs-high-contrast #gb-page .home .cs-button-selector,
.cs-high-contrast #gb-page div.ui-widget.app div.ui-widget-header,
.cs-high-contrast #gb-page div.ui-widget.app.navigation li a,
.cs-high-contrast #gb-page h1.sw-calendar-block-date,
.cs-high-contrast #gb-page .upcoming-column.right .sw-calendar-block-title,
.cs-high-contrast #gb-page .upcoming-column.right .sw-calendar-block-time,
.cs-high-contrast #gb-page .flexpage .ui-article a,
.cs-high-contrast #gb-page .announcements .ui-article a,
.cs-high-contrast #gb-page h1.ui-article-title,
.cs-high-contrast #gb-page .schools .cs-dropdown-selector:hover,
.cs-high-contrast #gb-page .schools .cs-dropdown-selector:focus,
.cs-high-contrast #gb-page .schools .cs-button-selector:hover,
.cs-high-contrast #gb-page .schools .cs-button-selector:focus,
.cs-high-contrast #gb-page .translate .cs-dropdown-selector:hover,
.cs-high-contrast #gb-page .translate .cs-dropdown-selector:focus,
.cs-high-contrast #gb-page .translate .cs-button-selector:hover,
.cs-high-contrast #gb-page .translate .cs-button-selector:focus,
.cs-high-contrast #gb-page ul.sw-channel-dropdown li a:hover, 
.cs-high-contrast #gb-page ul.sw-channel-dropdown li a:focus {
  color: #000;
}
.cs-high-contrast #gb-page .Border_Box a {
	color: #000 !important;
}
.cs-high-contrast #gb-page .cs-dropdown-selector,
.cs-high-contrast #gb-page .cs-button-selector,
.cs-high-contrast #gb-page ul.sw-channel-list li.sw-channel-item > a,
.cs-high-contrast #gb-page #hp-slideshow-outer #hp-slideshow .mmg-description-link,
.cs-high-contrast #gb-page #hp-slideshow-outer #hp-slideshow .cs-streaming-video-link,
.cs-high-contrast #gb-page #hp-slideshow-outer #hp-slideshow .mmg-control,
.cs-high-contrast #gb-page #hp-slideshow-outer #hp-slideshow .cs-streaming-video-control,
.cs-high-contrast #gb-page .cs-global-icons a,
.cs-high-contrast #gb-page .view-calendar-link, .cs-high-contrast #gb-page .more-link, .cs-high-contrast #gb-page div.app-level-social-rss a.ui-btn-toolbar.rss,
.cs-high-contrast #gb-page .hp-row.slider div.ui-widget.app.upcomingevents .cs-article-slider-control,
.cs-high-contrast #gb-page .hp-row.slider div.ui-widget.app.headlines .cs-article-slider-control,
.cs-high-contrast #gb-page .hp-row.slider .headlines .ui-article-description,
.cs-high-contrast #gb-page .hp-row.slider .headlines h1.ui-article-title,
.cs-high-contrast #gb-page .choose-section .choose-section-links li a,
.cs-high-contrast #gb-page .footer-row.two,
.cs-high-contrast #gb-page .footer-link,
.cs-high-contrast #gb-page .footer-row.one,
.cs-high-contrast #gb-page .Featured_Content_Button,
.cs-high-contrast #gb-page .Secondary_Box,
.cs-high-contrast #gb-page .Primary_Box,
.cs-high-contrast #gb-page #rs-menu-btn {
  color: #fff;
}
.cs-high-contrast #gb-page .home .cs-dropdown-selector,
.cs-high-contrast #gb-page .home .cs-button-selector,
.cs-high-contrast #gb-page .cs-global-icons .text::after,
.cs-high-contrast #gb-page .hp-row.slider .headlines h1.ui-article-title a::after {
  background: #fff;
}
.cs-high-contrast #gb-page .footer-row.one,
.cs-high-contrast #gb-page .Primary_Box {
  background: #212121;
}
.cs-high-contrast #gb-page ul.sw-channel-list li.sw-channel-item > a::before,
.cs-high-contrast #gb-page .choose-section .choose-section-links li a::after {
  background: #A5A5A5;
}
.cs-high-contrast #gb-page .header-row.two,
.cs-high-contrast #gb-page #hp-slideshow-outer #hp-slideshow .mmg-description-outer, 
.cs-high-contrast #gb-page #hp-slideshow-outer #hp-slideshow .cs-streaming-video-description{
  background: rgba(0, 0, 0, 0.75);
}
.cs-high-contrast #gb-page #logo-sitename {
  background: rgba(255, 255, 255, 0.8);
}
.cs-high-contrast #gb-page #hp-slideshow-outer #hp-slideshow .mmg-description-title,
.cs-high-contrast #gb-page #hp-slideshow-outer #hp-slideshow .cs-streaming-video-title,
.cs-high-contrast #gb-page #hp-slideshow-outer #hp-slideshow .mmg-description-caption,
.cs-high-contrast #gb-page #hp-slideshow-outer #hp-slideshow .cs-streaming-video-caption {
  background: rgba(0, 0, 0, 0.8);
}
.cs-high-contrast #gb-page #hp-slideshow-outer #hp-slideshow .mmg-description-title,
.cs-high-contrast #gb-page #hp-slideshow-outer #hp-slideshow .cs-streaming-video-title,
.cs-high-contrast #gb-page #hp-slideshow-outer #hp-slideshow .mmg-description-caption,
.cs-high-contrast #gb-page #hp-slideshow-outer #hp-slideshow .cs-streaming-video-caption {
  margin: 0;
  padding: 10px;
}
.cs-high-contrast #gb-page .choose-section::before {
  opacity: 0.8;
}
.cs-high-contrast #gb-page ul.sw-channel-dropdown li.csmc-section-columns .csmc-column h2,
.cs-high-contrast #gb-page ul.sw-channel-dropdown li.csmc-section-columns .csmc-column h3,
.cs-high-contrast #gb-page .quick-links .csmc-column .column-heading h2:not(.csmc-title),
.cs-high-contrast #gb-page .quick-links .csmc-column .column-heading h3 {
  border-bottom-color: #000;
}
.cs-high-contrast #gb-page .view-calendar-link, 
.cs-high-contrast #gb-page .more-link, 
.cs-high-contrast #gb-page div.app-level-social-rss a.ui-btn-toolbar.rss {
	border:2px solid transparent;
}
.cs-high-contrast #gb-page .view-calendar-link:hover, 
.cs-high-contrast #gb-page .more-link:hover, 
.cs-high-contrast #gb-page div.app-level-social-rss a.ui-btn-toolbar.rss:hover,
.cs-high-contrast #gb-page .view-calendar-link:focus , 
.cs-high-contrast #gb-page .more-link:focus , 
.cs-high-contrast #gb-page div.app-level-social-rss a.ui-btn-toolbar.rss:focus {
	border-color:#000;
    background:#fff;
    color:#000;
}
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
/* ------------------------ ### MEDIA QUERIES ### ------------------------- */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
/* GroupBegin Media Queries */
@media (max-width: 1399px) {
  ul.sw-channel-dropdown li.csmc-section-columns .csmc-column {
    min-width: 0;
    -ms-flex-preferred-size: 33.3333%;
        flex-basis: 33.3333%;
  }
  ul.sw-channel-dropdown li.csmc-section-columns .csmc-column.extra {
    -ms-flex-preferred-size: 33.3333%;
        flex-basis: 33.3333%;
  }
  ul.sw-channel-dropdown li.csmc-section-columns .csmc-column.extra li {
    display: block;
  }
  ul.sw-channel-dropdown li.csmc-section-columns .csmc-column.extra li img {
    max-width: 50%;
  }
  ul.sw-channel-dropdown li.csmc-section-columns .csmc-column.extra li .csmc-title {
    margin-bottom: -17px;
  }

  .csmc[data-image-only=true] ul.sw-channel-dropdown li.csmc-section-columns .csmc-column.extra li img {
    max-width: 100%;
  }
  #hp-slideshow-outer #hp-slideshow .mmg-description, #hp-slideshow-outer #hp-slideshow .cs-streaming-video-description {
  	width:85vw;
  }
  #hp-slideshow-outer #hp-slideshow .mmg-description-title, #hp-slideshow-outer #hp-slideshow .cs-streaming-video-title,
  #hp-slideshow-outer #hp-slideshow .mmg-description-caption, #hp-slideshow-outer #hp-slideshow .cs-streaming-video-caption {
  	max-width:none;
  }
}
@media (max-width: 1099px) {
  .quick-links .csmc-column {
    width: 260px;
  }

  .schools .cs-dropdown li {
    width: 210px;
  }
}
/* GroupEnd */
/* ----------------------------------------------
 * Generated by Animista on 2022-4-8 10:9:37
 * Licensed under FreeBSD License.
 * See http://animista.net/license for more info.
 * w: http://animista.net, t: @cssanimista
 * ---------------------------------------------- */
/**
 * ----------------------------------------
 * animation fade-in
 * ----------------------------------------
 */
@-webkit-keyframes fade-in {
  0% {
    opacity: 0;
  }
  100% {
    opacity: 1;
  }
}
@keyframes fade-in {
  0% {
    opacity: 0;
  }
  100% {
    opacity: 1;
  }
}
/* ----------------------------------------------
 * Generated by Animista on 2022-4-8 10:27:23
 * Licensed under FreeBSD License.
 * See http://animista.net/license for more info.
 * w: http://animista.net, t: @cssanimista
 * ---------------------------------------------- */
/**
 * ----------------------------------------
 * animation fade-out
 * ----------------------------------------
 */
@-webkit-keyframes fade-out {
  0% {
    opacity: 1;
  }
  100% {
    opacity: 0;
  }
}
@keyframes fade-out {
  0% {
    opacity: 1;
  }
  100% {
    opacity: 0;
  }
}/* MediaEnd *//* MediaBegin 768+ */ @media (max-width: 1023px) {/* GroupBegin Global */
body:before {
  content: "768";
}

.show768 {
  display: block;
}

.hide768 {
  display: none;
}

.ui-column-one-quarter.region {
  width: 50%;
  float: left;
  clear: left;
}

.ui-column-one-third.region {
  width: 50%;
  float: left;
}

.ui-column-two-thirds.region {
  width: 50%;
  float: left;
}

.region.right {
  float: left;
}

.region.clearleft {
  clear: none;
}

.cs-global-icons {
  -ms-flex-wrap: wrap;
      flex-wrap: wrap;
}
.cs-global-icons li {
  -ms-flex-preferred-size: 33.3333%;
      flex-basis: 33.3333%;
  padding: 0 20px 11px;
}

.content-wrap {
  max-width: none;
  padding: 0 24px;
}

/* GroupEnd */
/* GroupBegin Header */
.cs-mystart-button.home,
.cs-mystart-dropdown.schools,
.cs-mystart-dropdown.user-options,
.header-row.two {
  display: none;
}

#gb-header {
  position: relative;
}

.header-row.one #cs-contrast-bar {
  padding-left: 24px;
}
.header-row.one .header-column.two {
  padding-right: 12px;
}
.sticky-header .header-row.two {
  position: relative;
}
.header-row.three {
  background-color: #FFFFFF;
  padding-bottom: 12px;
  padding-top: 0 !important;
}
.header-row.three::before {
  display: none;
}

#logo-sitename .sitename h1 {
  color:#000000;
  font-size: 2.2666rem;
}

.mobile-menu {
  display: block;
}

#rs-menu-btn {
  background:#214A9B;
  color:#FFFFFF;
  font-size: 0.9333rem;
  font-weight: 700;
  padding: 11px 50px 11px 10px;
}
#rs-menu-btn::before {
  font-family: "template-icon" !important;
  speak: none;
  font-style: normal;
  font-weight: normal;
  font-variant: normal;
  text-transform: none;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  content: "\e907";
  background: transparent;
  padding: 0;
  position: absolute;
  right: 12px;
  top: 11px;
  font-size: 24px;
}
#rs-menu-btn span {
  padding: 0;
}

/* GroupEnd */
/* GroupBegin Footer */
.footer-row.one .content-wrap {
  -webkit-box-align: start;
      -ms-flex-align: start;
          align-items: flex-start;
}
.footer-row.one .footer-column.one {
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
}
.footer-row.one .social-icons {
  padding-top: 13px;
}

.contact-info:first-child {
  margin-right: 0;
  margin-bottom: -9px;
}

/* GroupEnd */
/* GroupBegin Apps */
#gb-page div.ui-widget.app .ui-widget-detail .cs-article-slider-control {
  display: none;
}

#gb-page div.ui-widget.app div.ui-widget-footer .cs-article-slider-control {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
}

#gb-page.hp .site-shortcuts-column {
  -ms-flex-preferred-size: 50%;
      flex-basis: 50%;
}

/* GroupEnd */
/* GroupBegin Homepage */
#hp-slideshow-outer {
  padding: 0;
}
#hp-slideshow-outer #hp-slideshow .mmg-description-outer,
#hp-slideshow-outer #hp-slideshow .cs-streaming-video-description {
  background:#132D62;
  -webkit-box-sizing: border-box;
          box-sizing: border-box;
}
#hp-slideshow-outer #hp-slideshow .mmg-description-inner {
	display:block;
    height:auto;
}
#hp-slideshow-outer #hp-slideshow .mmg-description,
#hp-slideshow-outer #hp-slideshow .cs-streaming-video-description {
  width: 100%;
  position: relative;
  bottom: auto;
  padding: 20px 24px 19px;
}
#hp-slideshow-outer #hp-slideshow .mmg-description {
	height: auto !important;
}
#hp-slideshow-outer #hp-slideshow .mmg-description-title,
#hp-slideshow-outer #hp-slideshow .cs-streaming-video-title {
  font-size: 2rem;
  max-width: none;
}
#hp-slideshow-outer #hp-slideshow .mmg-description-caption,
#hp-slideshow-outer #hp-slideshow .cs-streaming-video-caption {
  font-size: 1.2rem;
  max-width: none;
  line-height: 1.7;
}
#hp-slideshow-outer #hp-slideshow .mmg-description-links,
#hp-slideshow-outer #hp-slideshow .cs-streaming-video-links {
  padding-top: 12px;
  padding-right: 190px;
}
#hp-slideshow-outer #hp-slideshow .cs-streaming-video-links {
  padding-right: 0;
}
#hp-slideshow-outer #hp-slideshow .cs-streaming-video-description {
  -webkit-animation: none;
  animation: none;
}
#hp-slideshow-outer #hp-slideshow [data-active-slide-link=false][data-active-slide-video=false] .mmg-description-outer {
  padding-bottom: 58px;
}

.mmg-control-wrapper {
  bottom: 0;
  right: 0;
  left: auto;
  -webkit-box-pack: end;
      -ms-flex-pack: end;
          justify-content: flex-end;
  padding: 10px 24px 21px;
  margin-right: -6px;
}

.hp-row.two {
  padding: 32px 0px 28px;
}

.hp-row.slider .ui-widget-header h1 {
  padding: 0 18px;
}
.hp-row.slider .has-slider-controls {
  position: relative;
}
.hp-row.slider .upcomingevents .ui-articles {
  margin: 0 -15px;
}
.hp-row.slider .headlines .ui-articles {
  margin: 0 -20px;
}

#gb-page .hp-row div.ui-widget.app .cs-article-slider-control.next {
  right: 0;
  top: 3px;
}

#gb-page .hp-row div.ui-widget.app .cs-article-slider-control.back {
  left: auto;
  right: 64px;
  top: 3px;
}

#gb-page .hp-row.slider div.ui-widget.app.headlines .cs-article-slider-control {
  top: 30px;
}

.hp-row.three {
  padding: 75px 0 40px;
}

.hp-row.four {
  padding: 40px 0 40px;
}

.choose-section {
  padding: 50px 0 69px;
}
.choose-section .content-wrap {
  display: block;
}
.choose-section img {
  margin-bottom: 29px;
}
.choose-section p {
  margin-bottom: 20px;
}
.choose-section .choose-column.one {
  text-align: center;
  padding: 0;
  max-width: none;
}
.choose-section .choose-section-links li {
  -ms-flex-preferred-size: 50%;
      flex-basis: 50%;
}
.choose-section .choose-section-links li a {
  font-size: 1.2rem;
}

/* GroupEnd */
/* GroupBegin Subpage */
#sp-content {
  padding-top: 36px;
}
#sp-content .content-wrap {
  display: block;
}

.sp-column.one {
  padding-bottom: 34px;
}
.sp-column.one div.ui-widget.app.open div.ui-widget-header::before {
  content: "\e910";
}
.sp-column.one div.ui-widget.app div.ui-widget-header {
  padding-left: 44px;
  cursor: pointer;
}
.sp-column.one div.ui-widget.app div.ui-widget-header::before {
  content: "\e919";
  font-family: "template-icon" !important;
  speak: none;
  font-style: normal;
  font-weight: normal;
  font-variant: normal;
  text-transform: none;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  position: absolute;
  left: 0;
  top: 8px;
  font-size: 16px;
}
.sp-column.two {
  padding-left: 0;
}
.sp-column.two #sw-content-layout-wrapper > div[id*=sw-content-layout] {
  margin-left: -25px;
  width: calc(100% + 50px);
}

.sp .header-row.three {
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
}
.sp .header-row.three .breadcrumb-wrapper {
  display: none;
}
.sp .header-row.four {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  height: 178px;
}
.sp .header-row.four .breadcrumb-wrapper {
  padding: 0;
  width: 100%;
}

ul.ui-breadcrumbs {
  -webkit-box-pack: start;
      -ms-flex-pack: start;
          justify-content: flex-start;
}

.sp #gb-header {
  background-image: none !important;
}

/* GroupEnd */} /* MediaEnd *//* MediaBegin 640+ */ @media (max-width: 767px) {/* GroupBegin Global */
body:before {
  content: "640";
}

.show640 {
  display: block;
}

.hide640 {
  display: none;
}

.ui-spn .ui-column-one-third {
  width: auto !important;
  float: none !important;
}

.ui-column-one.region {
  width: auto;
  clear: none;
}

.ui-column-one-quarter.region {
  width: auto;
  float: none;
}

.ui-column-one-half.region {
  width: auto;
  float: none;
}

.ui-column-one-third.region {
  width: auto;
  float: none;
}

.ui-column-two-thirds.region {
  width: auto;
  float: none;
}

.region.right {
  float: none;
}

/* GroupEnd */
/* GroupBegin Header */
/* GroupEnd */
/* GroupBegin Footer */
.gb-bb-footer.links li:nth-child(2) a::after {
  display: none;
}

.gb-bb-footer.links ul {
  -ms-flex-wrap: wrap;
      flex-wrap: wrap;
  -webkit-flex-wrap: wrap;
}

/* GroupEnd */
/* GroupBegin Apps */
/* GroupEnd */
/* GroupBegin Homepage */
/* GroupEnd */
/* GroupBegin Subpage */
/* GroupEnd */} /* MediaEnd *//* MediaBegin 480+ */ @media (max-width: 639px) {/* GroupBegin Global */
body:before {
  content: "480";
}

.show480 {
  display: block;
}

.hide480 {
  display: none;
}

.cs-global-icons {
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
}
.cs-global-icons li {
  width: 100%;
  -webkit-box-pack: start;
      -ms-flex-pack: start;
          justify-content: flex-start;
  padding: 9px 28px;
}
.cs-global-icons a {
  -webkit-box-orient: horizontal;
  -webkit-box-direction: normal;
      -ms-flex-direction: row;
          flex-direction: row;
  padding: 0;
}
.cs-global-icons .icon {
  font-size: 2.6rem;
  padding-top: 0;
  margin: 0 22px 0 0;
  width: 40px;
}

.content-wrap {
  padding: 0 8px;
}

/* GroupEnd */
/* GroupBegin Header */
.header-row.one #cs-contrast-bar {
  padding-left: 8px;
  padding-right: 8px;
}
.header-row.one #cs-contrast-bar p {
  letter-spacing: 0px;
}
.header-row.one #cs-contrast-bar #cs-contrast-toggle {
  margin-left: 4px;
}
.header-row.one .header-column {
  -ms-flex-preferred-size: 50%;
      flex-basis: 50%;
}
.header-row.one .header-column.two {
  padding-right: 0;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
}
[data-show-sitename=true] .header-row.three {
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  padding-bottom: 0;
}

.cs-mystart-dropdown.passkeys {
  display: none;
}

.cs-mystart-button.quick-translate,
.cs-mystart-dropdown.translate {
  display: none;
}

#logo-sitename {
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  padding: 14px 8px;
}
#logo-sitename .logo {
  padding-right: 0px;
}
[data-show-sitename=true] #logo-sitename .logo {
  margin-bottom: 15px;
}
[data-show-sitename=true] #logo-sitename {
  text-align: center;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
}

/* GroupEnd */
/* GroupBegin Footer */
.footer-row .content-wrap {
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
}
.footer-row.one {
  padding-bottom: 41px;
}
.footer-row.one .footer-column.one {
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  width: 100%;
  text-align: center;
}
.footer-row.one .footer-column.two {
  width: 100%;
}
.footer-row.one .social-icons {
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
}
.footer-row.two {
  padding: 27px 0 0;
}
.footer-row.two .content-wrap {
  -webkit-box-orient: vertical;
  -webkit-box-direction: reverse;
      -ms-flex-direction: column-reverse;
          flex-direction: column-reverse;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
}

.cs-bb-footer-outer {
  padding-right: 0;
}

.footer-link {
  margin-right: 0;
}

/* GroupEnd */
/* GroupBegin Apps */
.has-slider-controls * {
  -webkit-box-ordinal-group: 3;
      -ms-flex-order: 2;
          order: 2;
}

.has-slider-controls .back {
  -webkit-box-ordinal-group: 2;
      -ms-flex-order: 1;
          order: 1;
}

.has-slider-controls .next {
  -webkit-box-ordinal-group: 4;
      -ms-flex-order: 3;
          order: 3;
}

.ui-hp div.ui-widget.app div.ui-widget-header {
  font-size: 1.8666rem;
}

.view-calendar-link,
.more-link,
div.app-level-social-rss a.ui-btn-toolbar.rss {
  padding: 14px 24px;
}

#gb-page.hp .site-shortcuts-column {
  -ms-flex-preferred-size: 100%;
      flex-basis: 100%;
}

/* GroupEnd */
/* GroupBegin Homepage */
#hp-slideshow-outer #hp-slideshow .mmg-container {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
}
#hp-slideshow-outer #hp-slideshow .mmg-viewer {
  -webkit-box-ordinal-group: 2;
      -ms-flex-order: 1;
          order: 1;
}
#hp-slideshow-outer #hp-slideshow .mmg-description-outer {
  -webkit-box-ordinal-group: 4;
      -ms-flex-order: 3;
          order: 3;
  text-align: center;
}
#hp-slideshow-outer #hp-slideshow [data-active-slide-link=false][data-active-slide-video=false] .mmg-description-outer {
  padding-bottom: 0;
}
#hp-slideshow-outer #hp-slideshow .mmg-description,
#hp-slideshow-outer #hp-slideshow .cs-streaming-video-description {
  padding: 15px 8px 10px;
}
#hp-slideshow-outer #hp-slideshow .mmg-description-title,
#hp-slideshow-outer #hp-slideshow .cs-streaming-video-title {
  font-size: 1.3333rem;
}
#hp-slideshow-outer #hp-slideshow .mmg-description-caption,
#hp-slideshow-outer #hp-slideshow .cs-streaming-video-caption {
  font-size: 1.0666rem;
  line-height: 1.625;
  padding-top: 12px;
}
#hp-slideshow-outer #hp-slideshow .mmg-control-wrapper {
  -webkit-box-ordinal-group: 3;
      -ms-flex-order: 2;
          order: 2;
  position: relative;
  background:#132D62;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  padding: 20px 8px 0px;
  margin-right: 0;
}
#hp-slideshow-outer #hp-slideshow .mmg-description-links {
  padding-right: 0;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  flex-direction:column;
  -webkit-flex-direction:column;
  align-items:center;
  -webkit-align-items:center;
  justify-content:center;
  -webkit-justify-content:center;
}
#hp-slideshow-outer #hp-slideshow .mmg-description-link,
#hp-slideshow-outer #hp-slideshow .cs-streaming-video-link {
  margin: 0 0 10px 0;
}

.hp-row.two {
  padding: 23px 0 29px;
}

.hp-row.slider .ui-widget-header h1 {
  padding: 0;
}
.hp-row.slider .ui-widget-header::before {
  display: none;
}
.hp-row.slider .has-slider-controls {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: justify;
      -ms-flex-pack: justify;
          justify-content: space-between;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
}

#gb-page .hp-row div.ui-widget.app .cs-article-slider-control {
  position: relative;
}
#gb-page .hp-row div.ui-widget.app .cs-article-slider-control.next, #gb-page .hp-row div.ui-widget.app .cs-article-slider-control.back {
  top: auto;
  right: auto;
}

#gb-page .hp-row.slider div.ui-widget.app.headlines .cs-article-slider-control {
  top: auto;
}

#gb-page .hp-row div.ui-widget.app.upcomingevents li {
  width: 100%;
}

.hp-row.three {
  padding: 75px 0 35px;
}

.hp-row.four {
  padding: 35px 0 43px;
}

.choose-section {
  padding: 33px 0;
}
.choose-section img {
  margin-bottom: 16px;
}
.choose-section .choose-section-links {
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
}
.choose-section .choose-section-links li {
  padding: 4px 0px;
}

/* GroupEnd */
/* GroupBegin Subpage */
/* GroupEnd */} /* MediaEnd *//* MediaBegin 320+ */ @media (max-width: 479px) {/* GroupBegin Default */
body:before {
  content: "320";
}

.show320 {
  display: block;
}

.hide320 {
  display: none;
}

/* GroupEnd */
/* GroupBegin Header */
.header-row.one .header-column.one {
	background:#000;
}
/* GroupEnd */
/* GroupBegin Footer */
/* GroupEnd */
/* GroupBegin Apps */
/* GroupEnd */
/* GroupBegin Homepage */
/* GroupEnd */
/* GroupBegin Subpage */
/* GroupEnd */} /* MediaEnd */</style>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="format-detection" content="telephone=no">
<link rel="stylesheet" type="text/css" href="https://extend.schoolwires.com/creative/scripts/creative/tools/creative-icons-v4/css/creativeIcons.v4.min.css" />
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">
<link type="text/css" rel="stylesheet" href="https://extend.schoolwires.com/creative/scripts/creative/global/css/cs.global.min.css" />

<script type="text/javascript" src="https://extend.schoolwires.com/creative/scripts/creative/global/js/cs.global.min.js"></script>
<script type="text/javascript" src="https://extend.schoolwires.com/creative/scripts/creative/tools/head.min.js"></script>
<script type="text/javascript" src="https://extend.schoolwires.com/creative/scripts/creative/tools/streaming-video/cs.streaming.video.min.js"></script>
<script type="text/javascript" src="https://extend.schoolwires.com/creative/scripts/creative/tools/creative-icons-v4/creativeIcons.v4.min.js"></script>
<script type="text/javascript" src="https://extend.schoolwires.com/creative/scripts/creative/responsive/creative-responsive-menu-v3/creative.responsive.menu.v3.min.js"></script>
<script type="text/javascript" src="https://extend.schoolwires.com/creative/scripts/joel/mod-events/joel.mod-events.min.js"></script>
<script type="text/javascript" src="https://extend.schoolwires.com/creative/scripts/creative/tools/creative-translate/creative.translate.min.js"></script>
<script type="text/javascript" src="https://extend.schoolwires.com/creative/scripts/creative/responsive/creative-app-accordion/creative.app.accordion.min.js"></script>
<script type="text/javascript" src="https://extend.schoolwires.com/creative/scripts/creative/responsive/creative-article-slider/cs.article.slider.min.js"></script>
<script type="text/javascript" src="https://extend.schoolwires.com/creative/creative_services/blackboard_footer/cs.blackboard.footer.min.js"></script>
<script type="text/javascript" src="https://extend.schoolwires.com/creative/scripts/creative/tools/tcw-upgrade/cs.tcw.upgrade.min.js"></script>
<script type="text/javascript" src="https://extend.schoolwires.com/creative/subscription_library/tools/cs.contrast.bar.min.js"></script>
<script type="text/javascript" src="https://extend.schoolwires.com/creative/scripts/creative/tools/mega-channel-menu/cs.mega.channel.menu.min.js"></script>


<script type="text/javascript">/******/ (function() { // webpackBootstrap
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

$(window).on("load", function () {
  CreativeTemplate.WindowLoad();
});
$(function () {
  CreativeTemplate.Init();
});
window.CreativeTemplate = {
  // PROPERTIES
  "KeyCodes": {
    "tab": 9,
    "enter": 13,
    "esc": 27,
    "space": 32,
    "end": 35,
    "home": 36,
    "left": 37,
    "up": 38,
    "right": 39,
    "down": 40
  },
  "IsMyViewPage": false,
  // UPDATES IN SetTemplateProps METHOD
  "ShowDistrictHome":false,
  "ShowSchoolList":false,
  // UPDATES IN SetTemplateProps METHOD
  "ShowTranslate": true,
  // METHODS
  "Init": function Init() {
    // FOR SCOPE
    var template = this;
    csGlobalJs.OpenInNewWindowWarning();
    this.SetTemplateProps();
    this.SocialIcons();
    this.MyStart();
    this.RsMenu();
    this.JsMediaQueries();
    this.SchoolList();
    this.Translate();
    this.Header();
    this.Search();
    this.ChannelBar();
    this.StickyChannelBar();
    this.ModEvents();

    if ($("#gb-page.hp").length) {
      this.Slideshow();
      this.StickySlideshow();
      this.CheckSlideshow();
      this.Homepage();
      this.Shortcuts();
    }

    this.Body();
    this.GlobalIcons();
    
    this.AppAccordion();
    this.Footer();
    $(window).resize(function () {
      template.WindowResize();
    });
    $(window).scroll(function () {
      template.WindowScroll();
    });
  },
  "SetTemplateProps": function SetTemplateProps() {
    // MYVIEW PAGE CHECK
    if ($("#pw-body").length) this.IsMyViewPage = true;
  },
  "WindowLoad": function WindowLoad() {
    var loadHighContrast = setInterval(loadHighContrastElement, 100);

    function loadHighContrastElement() {
      if ($("#cs-contrast-bar").length) {
        $("#cs-contrast-bar").prependTo(".header-row.one .header-column.one");
        clearInterval(loadHighContrast);
      }
    } //SETUP PAGE HEADER


    if ($(".sp").length) {
      if($("#sw-content-layout-wrapper").css("background-image") != "none"){ //THERE IS A HEADER BANNER IMAGE
          var imgURL = $("#sw-content-layout-wrapper").css("background-image");
          $("#gb-header, .header-row.four").css("background-image",imgURL).addClass("has-bg-img");
      }
    }

    this.csMegaChannelMenu();
  },
  "WindowResize": function WindowResize() {
    this.JsMediaQueries();
    if ($(".hp").length) {
    	this.StickySlideshow();
        this.CheckSlideshow();
        this.Shortcuts();
    }
    
  },
  "WindowScroll": function WindowScroll() {
    if ($(".hp").length) {
      this.StickySlideshow();
      this.CheckSlideshow();
    }
  },
  "JsMediaQueries": function JsMediaQueries() {
    switch (this.GetBreakPoint()) {
      case "desktop":
        break;

      case "768":
        if (!$(".mobile-menu #rs-menu-btn").length) {
          $("#rs-menu-btn").appendTo(".mobile-menu");
        }

        break;

      case "640":
        if (!$(".mobile-menu #rs-menu-btn").length) {
          $("#rs-menu-btn").appendTo(".mobile-menu");
        }

        break;

      case "480":
        if (!$(".mobile-menu").is(":empty")) {
          $("#rs-menu-btn").appendTo(".header-row.one .header-column.two");
        }

        break;

      case "320":
        if (!$(".mobile-menu").is(":empty")) {
          $("#rs-menu-btn").appendTo(".header-row.one .header-column.two");
        }

        break;
    }
  },
  "MyStart": function MyStart() {
    // FOR SCOPE
    var template = this; // BUILD USER OPTIONS DROPDOWN

    var userOptionsItems = "";
    var loggedin = "true";
    
    // SIGNIN BUTTON


    if ($(".sw-mystart-button.signin").length) {
      userOptionsItems += "<li>" + $(".sw-mystart-button.signin").html() + "</li>";
    } // REGISTER BUTTON


    if ($(".sw-mystart-button.register").length) {
      userOptionsItems += "<li>" + $(".sw-mystart-button.register").html() + "</li>";
    }
    
    // ADD USER OPTIONS DROPDOWN TO THE DOM


    $(".cs-mystart-dropdown.user-options .cs-dropdown-list").html(userOptionsItems); // BIND DROPDOWN EVENTS

    this.DropdownActions({
      "dropdownParent": ".cs-mystart-dropdown.user-options",
      "dropdownSelector": ".cs-dropdown-selector",
      "dropdown": ".cs-dropdown",
      "dropdownList": ".cs-dropdown-list"
    }); // BIND DROPDOWN EVENTS

    this.DropdownActions({
      "dropdownParent": ".cs-mystart-dropdown.quick-links",
      "dropdownSelector": ".cs-dropdown-selector",
      "dropdown": ".cs-dropdown",
      "dropdownList": ".cs-dropdown-list"
    });
  },
  "SchoolList": function SchoolList() {
    // ADD SCHOOL LIST
    if (this.ShowSchoolList) {
      var schoolDropdown = '<div class="cs-mystart-dropdown schools">' + '<div class="cs-dropdown-selector" tabindex="0" aria-label="Schools" role="button" aria-expanded="false" aria-haspopup="true">Schools</div>' + '<div class="cs-dropdown" aria-hidden="true" style="display:none;">' + '<ul class="cs-dropdown-list">' + $(".sw-mystart-dropdown.schoollist .sw-dropdown-list").html() + '</ul>' + '</div>' + '</div>'; // ADD SCHOOL LIST TO THE DOM

      $(".cs-mystart-button.home").after(schoolDropdown); // BIND DROPDOWN EVENTS

      this.DropdownActions({
        "dropdownParent": ".cs-mystart-dropdown.schools",
        "dropdownSelector": ".cs-dropdown-selector",
        "dropdown": ".cs-dropdown",
        "dropdownList": ".cs-dropdown-list"
      });
    }
  },
  "Translate": function Translate() {
    var template = this; // ADD TRANSLATE

    if (this.ShowTranslate) {
      $(".cs-mystart-dropdown.translate .cs-dropdown").creativeTranslate({
        "type": 2,
        // 1 = FRAMESET, 2 = BRANDED, 3 = API
        "languages": [// ["ENGLISH LANGUAGE NAME", "TRANSLATED LANGUAGE NAME", "LANGUAGE CODE"]
        ["Afrikaans", "Afrikaans", "af"], ["Albanian", "shqiptar", "sq"], ["Amharic", "አማርኛ", "am"], ["Arabic", "العربية", "ar"], ["Armenian", "հայերեն", "hy"], ["Azerbaijani", "Azərbaycan", "az"], ["Basque", "Euskal", "eu"], ["Belarusian", "Беларуская", "be"], ["Bengali", "বাঙালি", "bn"], ["Bosnian", "bosanski", "bs"], ["Bulgarian", "български", "bg"], ["Burmese", "မြန်မာ", "my"], ["Catalan", "català", "ca"], ["Cebuano", "Cebuano", "ceb"], ["Chichewa", "Chichewa", "ny"], ["Chinese Simplified", "简体中文", "zh-CN"], ["Chinese Traditional", "中國傳統的", "zh-TW"], ["Corsican", "Corsu", "co"], ["Croatian", "hrvatski", "hr"], ["Czech", "čeština", "cs"], ["Danish", "dansk", "da"], ["Dutch", "Nederlands", "nl"], ["Esperanto", "esperanto", "eo"], ["Estonian", "eesti", "et"], ["Filipino", "Pilipino", "tl"], ["Finnish", "suomalainen", "fi"], ["French", "français", "fr"], ["Galician", "galego", "gl"], ["Georgian", "ქართული", "ka"], ["German", "Deutsche", "de"], ["Greek", "ελληνικά", "el"], ["Gujarati", "ગુજરાતી", "gu"], ["Haitian Creole", "kreyòl ayisyen", "ht"], ["Hausa", "Hausa", "ha"], ["Hawaiian", "ʻŌlelo Hawaiʻi", "haw"], ["Hebrew", "עִברִית", "iw"], ["Hindi", "हिंदी", "hi"], ["Hmong", "Hmong", "hmn"], ["Hungarian", "Magyar", "hu"], ["Icelandic", "Íslenska", "is"], ["Igbo", "Igbo", "ig"], ["Indonesian", "bahasa Indonesia", "id"], ["Irish", "Gaeilge", "ga"], ["Italian", "italiano", "it"], ["Japanese", "日本語", "ja"], ["Javanese", "Jawa", "jw"], ["Kannada", "ಕನ್ನಡ", "kn"], ["Kazakh", "Қазақ", "kk"], ["Khmer", "ភាសាខ្មែរ", "km"], ["Korean", "한국어", "ko"], ["Kurdish", "Kurdî", "ku"], ["Kyrgyz", "Кыргызча", "ky"], ["Lao", "ລາວ", "lo"], ["Latin", "Latinae", "la"], ["Latvian", "Latvijas", "lv"], ["Lithuanian", "Lietuvos", "lt"], ["Luxembourgish", "lëtzebuergesch", "lb"], ["Macedonian", "Македонски", "mk"], ["Malagasy", "Malagasy", "mg"], ["Malay", "Malay", "ms"], ["Malayalam", "മലയാളം", "ml"], ["Maltese", "Malti", "mt"], ["Maori", "Maori", "mi"], ["Marathi", "मराठी", "mr"], ["Mongolian", "Монгол", "mn"], ["Myanmar", "မြန်မာ", "my"], ["Nepali", "नेपाली", "ne"], ["Norwegian", "norsk", "no"], ["Nyanja", "madambwe", "ny"], ["Pashto", "پښتو", "ps"], ["Persian", "فارسی", "fa"], ["Polish", "Polskie", "pl"], ["Portuguese", "português", "pt"], ["Punjabi", "ਪੰਜਾਬੀ ਦੇ", "pa"], ["Romanian", "Română", "ro"], ["Russian", "русский", "ru"], ["Samoan", "Samoa", "sm"], ["Scottish Gaelic", "Gàidhlig na h-Alba", "gd"], ["Serbian", "Српски", "sr"], ["Sesotho", "Sesotho", "st"], ["Shona", "Shona", "sn"], ["Sindhi", "سنڌي", "sd"], ["Sinhala", "සිංහල", "si"], ["Slovak", "slovenský", "sk"], ["Slovenian", "slovenski", "sl"], ["Somali", "Soomaali", "so"], ["Spanish", "Español", "es"], ["Sundanese", "Sunda", "su"], ["Swahili", "Kiswahili", "sw"], ["Swedish", "svenska", "sv"], ["Tajik", "Тоҷикистон", "tg"], ["Tamil", "தமிழ்", "ta"], ["Telugu", "తెలుగు", "te"], ["Thai", "ไทย", "th"], ["Turkish", "Türk", "tr"], ["Ukrainian", "український", "uk"], ["Urdu", "اردو", "ur"], ["Uzbek", "O'zbekiston", "uz"], ["Vietnamese", "Tiếng Việt", "vi"], ["Welsh", "Cymraeg", "cy"], ["Western Frisian", "Western Frysk", "fy"], ["Xhosa", "isiXhosa", "xh"], ["Yiddish", "ייִדיש", "yi"], ["Yoruba", "yorùbá", "yo"], ["Zulu", "Zulu", "zu"]],
        "advancedOptions": {
          "addMethod": "append",
          // PREPEND OR APPEND THE TRANSLATE ELEMENT
          "dropdownHandleText": "Translate",
          // ONLY FOR FRAMESET AND API VERSIONS AND NOT USING A CUSTOM ELEMENT
          "customElement": {
            // ONLY FOR FRAMESET AND API VERSIONS
            "useCustomElement": false,
            "translateItemsList": true,
            // true = THE TRANSLATE ITEMS WILL BE AN UNORDERED LIST, false = THE TRANSLATE ITEMS WILL JUST BE A COLLECTION OF <a> TAGS
            "customElementMarkup": "" // CUSTOM HTML MARKUP THAT MAKES THE CUSTOM TRANSLATE ELEMENT/STRUCTURE - USE [$CreativeTranslateListItems$] ACTIVE BLOCK IN THE MARKUP WHERE THE TRANSLATE ITEMS SHOULD BE ADDED

          },
          "apiKey": "",
          // ONLY FOR API VERSION
          "brandedLayout": 1,
          // 1 = VERTICAL (TEXT UNDER SELECT), 2 = HORIZONTAL (TEXT BESIDE SELECT), 3 = NON-RESPONSIVE MEGA DROPDOWN
          "removeBrandedDefaultStyling": false // 1 = VERTICAL (TEXT UNDER SELECT), 2 = HORIZONTAL (TEXT BESIDE SELECT), 3 = NON-RESPONSIVE MEGA DROPDOWN

        },
        "translateLoaded": function translateLoaded() {
          document.cookie = 'googtrans=; path=/; domain=.schoolwires.net; expires=' + new Date(0).toUTCString();
          $(document).on("click keydown", ".cs-mystart-button.quick-translate .cs-button-selector", function (e) {
            if (template.AllyClick(e)) {
              e.preventDefault(); // SET LANGUAGE PAIR COOKIE

              var langPair = $(this).attr("data-lang-code");
              csGlobalJs.CreateCookie({
                "name": "googtrans",
                "value": langPair
              }); // RELOAD THE PAGE

              location.reload();
            }
          });
        }
      }); // BIND DROPDOWN EVENTS

      this.DropdownActions({
        "dropdownParent": ".cs-mystart-dropdown.translate",
        "dropdownSelector": ".cs-dropdown-selector",
        "dropdown": ".cs-dropdown",
        "dropdownList": ".cs-dropdown-list"
      });
    }
  },
  "DropdownActions": function DropdownActions(params) {
    // FOR SCOPE
    var template = this;
    var dropdownParent = params.dropdownParent;
    var dropdownSelector = params.dropdownSelector;
    var dropdown = params.dropdown;
    var dropdownList = params.dropdownList;
    $(dropdownParent + " " + dropdownList + " a").attr("tabindex", "-1"); // MYSTART DROPDOWN SELECTOR CLICK EVENT

    $(dropdownParent).on("click", dropdownSelector, function (e) {
      e.preventDefault();

      if ($(this).parent().hasClass("open")) {
        $("+ " + dropdownList + " a").attr("tabindex", "-1");
        $(this).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
      } else {
        $(this).attr("aria-expanded", "true").parent().addClass("open").find(dropdown).attr("aria-hidden", "false").slideDown(300, "swing");
      }
    }); // MYSTART DROPDOWN SELECTOR KEYDOWN EVENTS

    $(dropdownParent).on("keydown", dropdownSelector, function (e) {
      // CAPTURE KEY CODE
      switch (e.keyCode) {
        // CONSUME LEFT AND UP ARROWS
        case template.KeyCodes.enter:
        case template.KeyCodes.space:
          e.preventDefault(); // IF THE DROPDOWN IS OPEN, CLOSE IT

          if ($(dropdownParent).hasClass("open")) {
            $("+ " + dropdown + " " + dropdownList + " a").attr("tabindex", "-1");
            $(this).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
          } else {
            $(this).attr("aria-expanded", "true").parent().addClass("open").find(dropdown).attr("aria-hidden", "false").slideDown(300, "swing", function () {
              if ($(dropdownParent).hasClass("quick-links")) {
                if ($(dropdownParent + " .csmc-column:first-child").find("li:first-child").hasClass("column-heading")) {
                  $(dropdownParent + " .csmc-column:first-child li:nth-child(2) > a").focus();
                } else {
                  $(dropdownParent + " .csmc-column:first-child li:first-child > a").focus();
                }
              } else {
              	if($(dropdownParent).hasClass("translate")){
                	$("#cs-branded-translate-dropdown").focus();
                } else {
                	$(dropdownList + " li:first-child a", this).attr("tabindex", "0").focus();
                }
                
              }
            });
          }

          break;
        // CONSUME TAB KEY

        case template.KeyCodes.tab:
          if ($("+ " + dropdown + " " + dropdownList + " a").length) {
            $("+ " + dropdown + " " + dropdownList + " a").attr("tabindex", "-1");
            $(this).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
          }

          break;
        // CONSUME LEFT AND UP ARROWS

        case template.KeyCodes.down:
        case template.KeyCodes.right:
          e.preventDefault();
          $("+ " + dropdown + " " + dropdownList + " a").attr("tabindex", "-1");

          if ($(dropdownParent).hasClass("quick-links")) {
            if ($(dropdownParent + " .csmc-column:first-child").find("li:first-child").hasClass("column-heading")) {
              $(dropdownParent + " .csmc-column:first-child li:nth-child(2) > a").attr("tabindex", "0").focus();
            } else {
              $(dropdownParent + " .csmc-column:first-child li:first-child > a").attr("tabindex", "0").focus();
            }
          } else {
            $("+ " + dropdown + " " + dropdownList + " li:first-child > a", this).attr("tabindex", "0").focus();
          }

          break;
        // CONSUME LEFT AND UP ARROWS

        case template.KeyCodes.up:
        case template.KeyCodes.left:
          e.preventDefault();
          $("+ " + dropdown + " " + dropdownList + " a").attr("tabindex", "-1");

          if ($(dropdownParent).hasClass("quick-links")) {
            $(".csmc-section.social-icons .gb-social-media-icon:last-child").attr("tabindex", "0").focus();
          } else {
            $("+ " + dropdown + " " + dropdownList + " li:last-child > a", this).attr("tabindex", "0").focus();
          }

          break;
      }
    }); // MYSTART DROPDOWN LINK KEYDOWN EVENTS

    $(dropdownParent).on("keydown", dropdownList + " li a", function (e) {
      // CAPTURE KEY CODE
      switch (e.keyCode) {
        // CONSUME LEFT AND UP ARROWS
        case template.KeyCodes.left:
        case template.KeyCodes.up:
          e.preventDefault();

          if ($(dropdownParent).hasClass("quick-links")) {
            if ($(this).hasClass("gb-social-media-icon")) {
              if ($(this).is(":first-child")) {
                $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                $(this).parent().parent().prev().prev().find("a:first-child").attr("tabindex", "0").focus();
              } else {
                $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                $(this).prev().attr("tabindex", "0").focus();
              }
            } else {
              if ($(this).parent().prev().hasClass("column-heading")) {
                //CHECK TO SEE IF HEADING IS THE FIRST HEADING IN THE COLUMN
                if ($(this).parent().prev().is(":first-child")) {
                  //CHECK FOR OTHER SECTION COLUMNS
                  if (!$(this).parent().parent().is(":first-child")) {
                    $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                    $(this).parent().parent().prev().find("li:last-child a:first-child").attr("tabindex", "0").focus();
                  } else {
                    $(this).closest(dropdownParent).find(dropdownSelector).focus();
                  }
                } else {
                  $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                  $(this).parent().prev().prev().find("a:first-child").attr("tabindex", "0").focus();
                } // FOCUS FIRST ITEM

              } else {
                // FOCUS NEXT ITEM
                $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                $(this).parent().prev().find("a:first-child").attr("tabindex", "0").focus();
              }
            }
          } else {
            // IS FIRST ITEM
            if ($(this).parent().is(":first-child")) {
              // FOCUS DROPDOWN BUTTON
              $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
              $(this).closest(dropdownParent).find(dropdownSelector).focus();
            } else {
              // FOCUS PREVIOUS ITEM
              $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
              $(this).parent().prev("li").find("> a").attr("tabindex", "0").focus();
            }
          }

          break;
        // CONSUME RIGHT AND DOWN ARROWS

        case template.KeyCodes.right:
        case template.KeyCodes.down:
          e.preventDefault();

          if ($(dropdownParent).hasClass("quick-links")) {
            if ($(this).hasClass("gb-social-media-icon")) {
              if ($(this).is(":last-child")) {
                $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                $(this).closest(dropdownParent).find(dropdownSelector).focus();
              } else {
                $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                $(this).next().attr("tabindex", "0").focus();
              }
            } else {
              if ($(this).parent().is(":last-child")) {
                //CHECK FOR OTHER SECTION COLUMNS
                if (!$(this).parent().parent().is(":last-child") && !$(this).parent().parent().next().hasClass("extra")) {
                  if ($(this).parent().parent().next().find("li:first-child").hasClass("column-heading")) {
                    $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                    $(this).parent().parent().next().find("li:nth-child(2) a:first-child").attr("tabindex", "0").focus();
                  } else {
                    $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                    $(this).parent().parent().next().find("li:first-child a:first-child").attr("tabindex", "0").focus();
                  }
                } else {
                  //LAST LINK IN LAST SECTION COLUMN
                  $(this).closest(dropdownParent).find(dropdownSelector).focus();
                } // FOCUS FIRST ITEM

              } else {
                // FOCUS NEXT ITEM
                if ($(this).parent().next().hasClass("column-heading")) {
                  $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                  $(this).parent().next().next().find("a:first-child").attr("tabindex", "0").focus();
                } else {
                  $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                  $(this).parent().next().find("a:first-child").attr("tabindex", "0").focus();
                }
              }
            }
          } else {
            // IS LAST ITEM
            if ($(this).parent().is(":last-child")) {
              // FOCUS FIRST ITEM
              $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
              $(this).closest(dropdownParent).find(dropdownSelector).focus();
            } else {
              // FOCUS NEXT ITEM
              $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
              $(this).parent().next("li").find("> a").attr("tabindex", "0").focus();
            }
          }

          break;
        // CONSUME TAB KEY

        case template.KeyCodes.tab:
          if (e.shiftKey) {
            e.preventDefault(); // FOCUS DROPDOWN BUTTON

            $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
            $(this).closest(dropdownParent).find(dropdownSelector).focus();
          }

          break;
        // CONSUME HOME KEY

        case template.KeyCodes.home:
          e.preventDefault(); // FOCUS FIRST ITEM

          $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
          $(this).closest(dropdownList).find("li:first-child > a").attr("tabindex", "0").focus();
          break;
        // CONSUME END KEY

        case template.KeyCodes.end:
          e.preventDefault(); // FOCUS LAST ITEM

          $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
          $(this).closest(dropdownList).find("li:last-child > a").attr("tabindex", "0").focus();
          break;
        // CONSUME ESC KEY

        case template.KeyCodes.esc:
          e.preventDefault(); // FOCUS DROPDOWN BUTTON AND CLOSE DROPDOWN

          $(this).closest(dropdownParent).find(dropdownSelector).focus();
          $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
          $(dropdownSelector).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
          break;
      }
    });
    $(dropdownParent).mouseleave(function () {
      $(dropdownList + " a", this).attr("tabindex", "-1");
      $(dropdownSelector, this).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
    }).focusout(function () {
      var thisDropdown = this;
      setTimeout(function () {
        if (!$(thisDropdown).find(":focus").length) {
          $(dropdownSelector, thisDropdown).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
        }
      }, 500);
    });
  },
  "Header": function Header() {
    // ADD LOGO
    var logoSrc = jQuery.trim("https://adamcruse.schoolwires.net/cms/lib/SWCS000009/Centricity/Template/GlobalAssets/images///Faces/default-man.jpg");
    var srcSplit = logoSrc.split("/");
    var srcSplitLen = srcSplit.length;

    if (logoSrc != "" && srcSplit[srcSplitLen - 1] != "default-man.jpg") {
    	if(false){
        	$("#logo-sitename .logo").append("<a href='https://adamcruse.schoolwires.net/eisd'><img src='" + logoSrc + "' alt='St. Cloud Area School District Logo' /></a>");
        } else {
        	$("#logo-sitename .logo").append("<h1><a href='https://adamcruse.schoolwires.net/eisd'><img src='" + logoSrc + "' alt='St. Cloud Area School District Logo' /></a></h1>");
        }
    } else {
    	if(false){
        	$("#logo-sitename .logo").append("<a href='https://adamcruse.schoolwires.net/eisd'><img width='509' height='104' src='https://adamcruse.schoolwires.net/cms/lib/SWCS000009/Centricity/Template/176/defaults/logo.svg' alt='St. Cloud Area School District Logo' /></a>");
        } else {
        	$("#logo-sitename .logo").append("<h1><a href='https://adamcruse.schoolwires.net/eisd'><img width='509' height='104' src='https://adamcruse.schoolwires.net/cms/lib/SWCS000009/Centricity/Template/176/defaults/logo.svg' alt='St. Cloud Area School District Logo' /></a></h1>");
        }
      
    }

    $.csHighContrastBar({
      "show": true,
      "alternateLogo": {
        "selector": "",
        // ID OR CLASS OF DEFAULT LOGO <img>
        "src": "",
        // ALTERNATE LOGO SRC
        "alt": "" // ALTERNATE LOGO ALT TEXT

      },
      "toggle": function toggle(on) {} // CUSTOM CALLBACK WHEN CONTRAST IS TOGGLED (ALSO FIRES ON DOC READY) - on PARAMETER IS EITHER true OR false (boolean)

    });
  },
  "ChannelBar": function ChannelBar() {
    $(".sw-channel-item").off('hover');
    $(".sw-channel-item").hover(function () {
      //GET TOP POSITION OF CHANNEL FOR MEGA DROPDOWN POSITIONING
      var childPos = $(this).offset();
      var parentPos = $(this).parent().offset();
      var childOffset = {
        top: childPos.top - parentPos.top
      };
      $(this).find(".sw-channel-dropdown").css("top", childOffset.top + 78 + "px");
      $(".sw-channel-item ul").stop(true, true);
      var subList = $(this).children('ul');

      if ($.trim(subList.html()) !== "") {
        subList.slideDown(300, "swing");
      }

      $(this).addClass("hover");
    }, function () {
      $(".sw-channel-dropdown").slideUp(300, "swing");
      $(this).removeClass("hover");
    });
  },
  "StickyChannelBar": function StickyChannelBar() {
    var headerHeight = $(".header-row.one").outerHeight();
    var navHeight = $(".header-row.two").outerHeight();
    $(document).scroll(function () {
      distanceFromTop = $(this).scrollTop();

      if (distanceFromTop >= headerHeight) {
        $("#gb-page").addClass("sticky-header");
        $(".header-row.three").css("padding-top", navHeight);
      } else {
        $("#gb-page").removeClass("sticky-header");
        $(".header-row.three").css("padding-top", "0px");
      }

      if ($(".sp").length && $(".ui-widget.app.calendar").length) {
        if ($(".wcm-controls").hasClass("wcm-stuck")) {
          $(".wcm-controls").css("margin-top", navHeight);
        }
      }

      $("#sw-maincontent").css({
        "display": "block",
        "position": "relative",
        "top": "-" + navHeight + "px"
      });
    });
  },
  "Body": function Body() {
    // FOR SCOPE
    var template = this; // AUTO FOCUS SIGN IN FIELD

    $("#swsignin-txt-username").focus(); // APPLY RESPONSIVE DIMENSIONS TO CONTENT IMAGES

    $(".ui-widget.app .ui-widget-detail img").not($(".ui-widget.app.multimedia-gallery .ui-widget-detail img")).each(function () {
      if ($(this).attr('width') !== undefined && $(this).attr('height') !== undefined) {
        // IMAGE HAS INLINE DIMENSIONS
        $(this).css({
          "display": "inline-block",
          "width": "100%",
          "max-width": $(this).attr("width") + "px",
          "height": "auto",
          "max-height": $(this).attr("height") + "px"
        });
      }
    }); //SHORTCUTS HOVERS

    $(".navigation a").hover(function () {
      $(this).closest("li").addClass("hover");
    }, function () {
      $(this).closest("li").removeClass("hover");
    });
    $(".navigation a").focus(function () {
      $(this).closest("li").addClass("hover");
    });
    $(".navigation a").focusout(function () {
      $(this).closest("li").removeClass("hover");
    }); // ADJUST FIRST BREADCRUMB

    $("li.ui-breadcrumb-first > a > span").text("Home").show(); // CHECK PAGELIST HEADER

    if ($.trim($(".ui-widget.app.pagenavigation .ui-widget-header").text()) == "") {
      $(".ui-widget.app.pagenavigation .ui-widget-header").html("<h1></h1>");
    } //REMOVE UNUSED ELEMENTS


    $("[data-content=''], [data-content='#'], [data-toggle='false']").remove(); //CHOOSE 742 SECTION
    //BACKGROUND

    var bgSrc = jQuery.trim("https://adamcruse.schoolwires.net/cms/lib/SWCS000009/Centricity/Template/GlobalAssets/images///Faces/default-man.jpg");
    var bgSrcSplit = bgSrc.split("/");
    var bgSrcSplitLen = bgSrcSplit.length;

    if (bgSrc != "" && bgSrcSplit[bgSrcSplitLen - 1] != "default-man.jpg") {
      $(".choose-section").css("background-image", "url('" + bgSrc + "')");
    } else {
      $(".choose-section").css("background-image", "url('https://adamcruse.schoolwires.net/cms/lib/SWCS000009/Centricity/Template/176/defaults/choose-bg.jpg')");
    } //LOGO


    var logoSrc = jQuery.trim("https://adamcruse.schoolwires.net/cms/lib/SWCS000009/Centricity/Template/GlobalAssets/images///Faces/default-man.jpg");
    var srcSplit = logoSrc.split("/");
    var srcSplitLen = srcSplit.length;

    if (logoSrc != "" && srcSplit[srcSplitLen - 1] != "default-man.jpg") {
      $(".choose-section .choose-column.one").prepend("<img src='" + logoSrc + "' alt=' Image' />");
    } else {
      $(".choose-section .choose-column.one").prepend("<img width='318' height='194' src='https://adamcruse.schoolwires.net/cms/lib/SWCS000009/Centricity/Template/176/defaults/choose.svg' alt=' Image' />");
    }
  },
  "Homepage": function Homepage() {
    // FOR SCOPE
    var template = this; //ARTICLE SLIDERS

    $(".slider .upcomingevents").csArticleSlider({
      "viewerNum": [5, 2, 2, 1, 1],
      // NUMERICAL - [960 view, 768 view, 640 view, 480 view, 320 view]
      "transitionSpeed": .5,
      // SECONDS
      "slideFullView": false,
      "extend": function extend(config, element, ArticleSlider) {}
    });
    $(".slider .headlines").csArticleSlider({
      "viewerNum": [4, 2, 2, 1, 1],
      // NUMERICAL - [960 view, 768 view, 640 view, 480 view, 320 view]
      "transitionSpeed": .5,
      // SECONDS
      "slideFullView": false,
      "extend": function extend(config, element, ArticleSlider) {}
    });
    $(".slider .app").each(function () {
      var thisApp = this;
      $(thisApp).find(".ui-widget-footer").addClass("has-slider-controls");
      $(thisApp).find(".view-calendar-link, .ui-read-more").prependTo($(thisApp).find(".ui-widget-footer"));
      $(".ui-widget-detail .cs-article-slider-control", thisApp).each(function () {
        $(this).clone().appendTo($(thisApp).find(".ui-widget-footer"));
      });
      if($(thisApp).find(".cs-article-slider-control.back").attr("aria-hidden") == "true"){
      	$(thisApp).addClass("no-slider");
      }
    }); //CHECK HP COLUMNS FOR APPS

    $(".hp-row.five .hp-column").each(function () {
      if ($(this).find(".app").length) {
        $(this).addClass("has-app");
      }
    }); //HF HOVERS

    $(".slider .ui-article-title a").hover(function () {
      $(this).closest("li").addClass("hover");
    }, function () {
      $(this).closest("li").removeClass("hover");
    });
    $(".slider .ui-article-title a").focus(function () {
      $(this).closest("li").addClass("hover");
    });
    $(".slider .ui-article-title a").focusout(function () {
      $(this).closest("li").removeClass("hover");
    }); //EVENTS HOVERS

    $(".sw-calendar-block-title a").hover(function () {
      $(this).closest("li").addClass("hover");
    }, function () {
      $(this).closest("li").removeClass("hover");
    });
    $(".sw-calendar-block-title a").focus(function () {
      $(this).closest("li").addClass("hover");
    });
    $(".sw-calendar-block-title a").focusout(function () {
      $(this).closest("li").removeClass("hover");
    });
  },
  "Shortcuts": function Shortcuts() {
    var template = this; // EDIT THESE TWO VARS

    var columnNums = [4, 2, 2, 1, 1]; // [960, 768, 640, 480, 320]

    var selector = ".hp-row.three .siteshortcuts, .hp-row.four .siteshortcuts"; // RETURN BREAKPOINT INDEX

    var bp = function bp() {
      switch (template.GetBreakPoint()) {
        case "desktop":
          return 0;
          break;

        case "768":
          return 1;
          break;

        case "640":
          return 2;
          break;

        case "480":
          return 3;
          break;

        case "320":
          return 4;
          break;
      }
    }; // SET COLUMN NUM AND OTHER VARS


    var columnNum = columnNums[bp()];
    var endRange;
    $(selector).each(function () {
      // RETURN THE LI'S TO THE ORIGINAL UL
      $(".ui-widget-detail > ul.site-shortcuts", this).append($(".site-shortcuts-column > li", this)); // REMOVE COLUMN CONTAINER FOR REBUILD

      $(".site-shortcuts-columns", this).remove(); // GET SHORTCUT NUM

      var shortcutNum = $(".ui-widget-detail > ul.site-shortcuts > li", this).length; // ADD COLUMN CONTAINER

      $(".ui-widget-detail", this).prepend('<div class="site-shortcuts-columns"></div>'); // LOOP TO BUILD COLUMNS

      for (var i = 0; i < columnNum; i++) {
        // KEEP FROM ADDING EMPTY UL'S TO THE DOM
        if (i < shortcutNum) {
          // IF shortcutNum / columnNum REMAINDER IS BETWEEN .0 AND .5 AND THIS IS THE FIRST LOOP ITERATION
          // WE'LL ADD 1 TO THE END RANGE SO THAT THE EXTRA LINK GOES INTO THE FIRST COLUMN
          if (shortcutNum / columnNum % 1 > 0.0 && shortcutNum / columnNum % 1 < 0.5 && i == 0) {
            endRange = Math.round(shortcutNum / columnNum) + 1;
          } else if (shortcutNum / columnNum % 1 == 0.5 && i >= columnNum / i) {
            endRange = Math.round(shortcutNum / columnNum) - 1;
          } else {
            endRange = Math.round(shortcutNum / columnNum);
          } // ADD THE COLUMN UL


          $(".site-shortcuts-columns", this).append('<ul class="site-shortcuts-column column-' + (i + 1) + '"></ul>'); // MOVE THE RANGE OF LI'S TO THE COLUMN UL

          $(".site-shortcuts-column.column-" + (i + 1), this).append($(".ui-widget-detail > ul.site-shortcuts > li:nth-child(n+1):nth-child(-n+" + endRange + ")", this));
        }
      } // HIDE THE ORIGINAL UL


      $(".ui-widget-detail > ul.site-shortcuts", this).hide();
    });
  },
  "Footer": function Footer() {
    // FOR SCOPE
    var template = this;
    $(document).csBbFooter({
      'footerContainer': '.footer-row.two .footer-column.one',
      'useDisclaimer': false,
      'disclaimerText': ''
    }); //BACK TO TOP

    $(".to-top").click(function (e) {
      e.preventDefault();
      $("html, body").animate({
        scrollTop: 0
      }, "slow", function () {
        $("li.sw-channel-item:first-child > a").focus();
      });
    });

    if ($("#gb-footer-disclaimer span").text().trim() == "") {
      $("#gb-footer-disclaimer").hide();
    }
  },
  "Slideshow": function Slideshow() {
    // FOR SCOPE
    var template = this;

    if("Multimedia Gallery" == "Multimedia Gallery;Streaming Video" || "Multimedia Gallery" == "Multimedia Gallery") {
        if($("#sw-content-container10 .ui-widget.app.multimedia-gallery").length) {
            this.MMGPlugin();
        }
    } else {
        this.StreamingVideo();
    }
  },
  "MMGPlugin": function MMGPlugin() {
    var _$$csMultimediaGaller;

    // FOR SCOPE
    var template = this;
    var mmg = eval("multimediaGallery" + $("#sw-content-container10 .ui-widget.app.multimedia-gallery:first").attr("data-pmi"));
    mmg.props.defaultGallery = false;
    $("#sw-content-container10 .ui-widget.app.multimedia-gallery:first").csMultimediaGallery((_$$csMultimediaGaller = {
      "efficientLoad": true,
      "imageWidth": 1500,
      "imageHeight": 878,
      "mobileDescriptionContainer": [768, 640, 480, 320],
      // [960, 768, 640, 480, 320]
      "galleryOverlay": false,
      "linkedElement": [],
      // ["image", "title", "overlay"]
      "playPauseControl": true,
      "backNextControls": true,
      "bullets": false,
      "thumbnails": false,
      "thumbnailViewerNum": [3, 4, 3, 3, 2],
      // NUMERICAL - [960 view, 768 view, 640 view, 480 view, 320 view]
      "autoRotate": true,
      "hoverPause": true,
      "transitionType": "fade",
      "transitionSpeed":2,
      "transitionDelay":6
    }, _defineProperty(_$$csMultimediaGaller, "transitionSpeed", 3), _defineProperty(_$$csMultimediaGaller, "transitionDelay", 8), _defineProperty(_$$csMultimediaGaller, "fullScreenRotator", false), _defineProperty(_$$csMultimediaGaller, "fullScreenBreakpoints", [960]), _defineProperty(_$$csMultimediaGaller, "onImageLoad", function onImageLoad(props) {}), _defineProperty(_$$csMultimediaGaller, "allImagesLoaded", function allImagesLoaded(props) {}), _defineProperty(_$$csMultimediaGaller, "onTransitionStart", function onTransitionStart(props) {}), _defineProperty(_$$csMultimediaGaller, "onTransitionEnd", function onTransitionEnd(props) {
    	 $(".active .mmg-description").css("height", $(".active .mmg-description").outerHeight());
    }), _defineProperty(_$$csMultimediaGaller, "allLoaded", function allLoaded(props) {
      //WRAP CONTROLS
      $(".mmg-control.play-pause", props.element).insertAfter(".mmg-control.back", props.element);
      $(".mmg-control", props.element).wrapAll("<div class='mmg-control-wrapper'></div>"); //MOVE CONTROLS
      
      $(".mmg-description").wrapInner("<div class='mmg-description-inner'></div>");
      $(".mmg-description").css("top", $("#gb-header").outerHeight());
      
      setTimeout(function(){
      	$(".mmg-slide:first-child .mmg-description").css("height", $(".mmg-slide:first-child .mmg-description").outerHeight());
      }, 100);
      

      if (template.GetBreakPoint() != "desktop") {
        $(".mmg-control-wrapper", props.element).appendTo(".mmg-container", props.element);
      }
    }), _defineProperty(_$$csMultimediaGaller, "onWindowResize", function onWindowResize(props) {
      switch (template.GetBreakPoint()) {
        case "desktop":
          if (!$(".mmg-viewer .mmg-control-wrapper", props.element).length) {
            $(".mmg-control-wrapper", props.element).insertAfter(".mmg-slides-outer", props.element);
          }

          break;

        case "768":
          if ($(".mmg-viewer .mmg-control-wrapper", props.element).length) {
            $(".mmg-control-wrapper", props.element).appendTo(".mmg-container", props.element);
          }

          break;

        case "640":
          if ($(".mmg-viewer .mmg-control-wrapper", props.element).length) {
            $(".mmg-control-wrapper", props.element).appendTo(".mmg-container", props.element);
          }

          break;

        case "480":
          if ($(".mmg-viewer .mmg-control-wrapper", props.element).length) {
            $(".mmg-control-wrapper", props.element).appendTo(".mmg-container", props.element);
          }

          break;

        case "320":
          if ($(".mmg-viewer .mmg-control-wrapper", props.element).length) {
            $(".mmg-control-wrapper", props.element).appendTo(".mmg-container", props.element);
          }

          break;
      }
    }), _$$csMultimediaGaller));
  },
  "StickySlideshow": function StickySlideshow() {
    var windowHeight = $(window).height();
    var scrollTop = $(document).scrollTop();
    var headerHeight = $(".header-row.one").outerHeight();
    var slideshowHeight = $("#hp-slideshow").outerHeight();
    var maxToScroll = -1 * (windowHeight - (headerHeight + slideshowHeight));

    if (this.GetBreakPoint() == "desktop") {
      if (scrollTop < maxToScroll) {
        $("#hp-slideshow-outer").addClass("mmg-fixed-btns");
      } else {
        $("#hp-slideshow-outer").removeClass("mmg-fixed-btns");
      }
    }
  },
  "CheckSlideshow": function CheckSlideshow() {
    // FOR SCOPE
    var template = this;

    if ($(".hp").length && template.GetBreakPoint() != "desktop" &&"Multimedia Gallery" != "Streaming Video") {
      if ($(window).scrollTop() <= $("#hp-slideshow").offset().top + $("#hp-slideshow").height()) {
        if (this.SlideshowDescFixed) {
          $("#hp-slideshow").removeAttr("style");
          this.SlideshowDescFixed = false;
        }
      } else {
        if (!this.SlideshowDescFixed) {
          $("#hp-slideshow").css({
            "height": $("#hp-slideshow").height(),
            "overflow": "hidden"
          });
          this.SlideshowDescFixed = true;
        }
      }
    }
  },
  "StreamingVideo": function StreamingVideo() {
    // FOR SCOPE
    var template = this;
    var videoVendor ="YouTube";
    var mobilePhoto ="https://adamcruse.schoolwires.net/cms/lib/SWCS000009/Centricity/Template/GlobalAssets/images///Faces/default-man.jpg";

    if ($.trim(mobilePhoto) == "" || mobilePhoto.indexOf("default-man.jpg") > -1) {
      mobilePhoto = "https://adamcruse.schoolwires.net/cms/lib/SWCS000009/Centricity/Template/176/defaults/default-video-image.jpg";
    }

    $("#hp-slideshow").csStreamingVideo({
      // USER CONFIGURATIONS
      "videoSource": videoVendor,
      // OPTIONS ARE: YouTube, Vimeo, MyVRSpot
      "videoID":'giXco2jaZ_4',
      // DEFAULT IS BLANK TO AUTOFILL THE PROPER ID FOR THE VENDOR
      "myVRSpotVideoWidth": "853.5",
      "myVRSpotVideoHeight": "480",
      "fullWidthBreakpoints": [],
      // OPTIONS ARE: ["Desktop", 1024, 768, 640, 480, 320]
      "fullWidthCSSPosition": "",
      // OPITONS ARE: static, relative, fixed, absolute, sticky
      "fullScreenBreakpoints": [],
      // OPTIONS ARE: ["Desktop", 1024, 768, 640, 480, 320]
      "fullScreenCSSPosition": "",
      // OPITONS ARE: static, relative, fixed, absolute, sticky
      "showVideoTitle":true,
      "videoTitleText":'Video Title',
      "showVideoDescription":true,
      "videoDescriptionText":'Video Caption',
      "showVideoLinks":true,
      "videoLinks": [{
        "text":'Video Link Text',
        "link":'#',
        "target":'_self'
      }],
      //{ "text": "Video Text", "link": "Video Link", "target": "Video Target"  }
      "showAudioButtons": true,
      "showWatchVideoButton": true,
      "useBackgroundPhoto": true,
      "backgroundPhoto": mobilePhoto,
      "backgroundPhotoBreakpoints": [768, 640, 480, 320],
      // OPTIONS ARE: ["Desktop", 1024, 768, 640, 480, 320]
      "backgroundPhotoCSSPosition": "",
      // OPITONS ARE: static, relative, fixed, absolute, sticky
      "useCustomResize": false,
      "extendResize": function extendResize(props) {},
      //element, videoHeight, videoWidth
      "onReady": function onReady(props) {},
      // IF NOEMBED REQUEST RETURNS VIDEO DATA, PROPS WILL BE THE RETURNED VIDEO DATA OBJECT. OTHERWISE, PROPS WILL BE props.width AND props.height FOR VIDEO WIDTH AND HEIGHT
      "allLoaded": function allLoaded(props) {} // props.element

    });
  },
  "csMegaChannelMenu": function csMegaChannelMenu() {
  	var template = this;
  
    $("#sw-channel-list-container").megaChannelMenu({
      "numberOfChannels": 10,
      // integer MAX 10 CHANNELS
      "maxSectionColumnsPerChannel": 3,
      // integer, MAX 4 COLUMNS (this does NOT include the extra, non-section column)
      "extraColumn": true,
      // boolean
      "extraColumnTitle": true,
      //bolean
      "extraColumnDescription": true,
      //bolean
      "extraColumnImage": true,
      //bolean
      "extraColumnLink": false,
      //bolean
      "extraColumnPosition": "RIGHT",
      // string, "ABOVE", "BELOW", "LEFT", "RIGHT"
      "sectionHeadings": true,
      // boolean
      "sectionHeadingLinks": false,
      // boolean
      "maxNumberSectionHeadingsPerColumn": 1,
      // integer, MAX 3 HEADINGS
      "megaMenuElements": [{
        //CHANNEL 1
        "channelName":'Parents',
        "channelTitle":'Menu Title',
        "channelDescription":'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no.',
        "channelImage":'https://adamcruse.schoolwires.net/cms/lib/SWCS000009/Centricity/Template/GlobalAssets/images///images/channel-dd-image.jpg',
        "channelImageAlt":'alt text',
        "channelColumns": [[//COLUMN 1
        {
          //SECTION 1
          "heading": [{
            "text":'Column Header 1'
          }],
          "sections":3
        }], [//COLUMN 2
        {
          //SECTION 1
          "heading": [{
            "text":'Column Header 2'
          }],
          "sections":4
        }], [//COLUMN 3
        {
          //SECTION 1
          "heading": [{
            "text": ''
          }],
          "sections": 0
        }]]
      }, {
        //CHANNEL 2
        "channelName":'Our District',
        "channelTitle":'',
        "channelDescription":'',
        "channelImage":'https://adamcruse.schoolwires.net/cms/lib/SWCS000009/Centricity/Template/GlobalAssets/images///images/channel-dd-image.jpg',
        "channelImageAlt":'',
        "channelColumns": [[//COLUMN 1
        {
          //SECTION 1
          "heading": [{
            "text":'Column Heading'
          }],
          "sections":4
        }], [//COLUMN 2
        {
          //SECTION 1
          "heading": [{
            "text":''
          }],
          "sections":0
        }], [//COLUMN 3
        {
          //SECTION 1
          "heading": [{
            "text": ''
          }],
          "sections": 0
        }]]
      }, {
        //CHANNEL 3
        "channelName":'',
        "channelTitle":'',
        "channelDescription":'',
        "channelImage":'https://adamcruse.schoolwires.net/cms/lib/SWCS000009/Centricity/Template/GlobalAssets/images///Faces/default-man.jpg',
        "channelImageAlt":'',
        "channelColumns": [[//COLUMN 1
        {
          //SECTION 1
          "heading": [{
            "text":''
          }],
          "sections":0
        }], [//COLUMN 2
        {
          //SECTION 1
          "heading": [{
            "text":''
          }],
          "sections":0
        }], [//COLUMN 3
        {
          //SECTION 1
          "heading": [{
            "text": ''
          }],
          "sections": 0
        }]]
      }, {
        //CHANNEL 4
        "channelName":'',
        "channelTitle":'',
        "channelDescription":'',
        "channelImage":'https://adamcruse.schoolwires.net/cms/lib/SWCS000009/Centricity/Template/GlobalAssets/images///Faces/default-man.jpg',
        "channelImageAlt":'',
        "channelColumns": [[//COLUMN 1
        {
          //SECTION 1
          "heading": [{
            "text":''
          }],
          "sections":0
        }], [//COLUMN 2
        {
          //SECTION 1
          "heading": [{
            "text":''
          }],
          "sections":0
        }], [//COLUMN 3
        {
          //SECTION 1
          "heading": [{
            "text": ''
          }],
          "sections": 0
        }]]
      }, {
        //CHANNEL 5
        "channelName":'',
        "channelTitle":'',
        "channelDescription":'',
        "channelImage":'https://adamcruse.schoolwires.net/cms/lib/SWCS000009/Centricity/Template/GlobalAssets/images///Faces/default-man.jpg',
        "channelImageAlt":'',
        "channelColumns": [[//COLUMN 1
        {
          //SECTION 1
          "heading": [{
            "text":''
          }],
          "sections":0
        }], [//COLUMN 2
        {
          //SECTION 1
          "heading": [{
            "text":''
          }],
          "sections":0
        }], [//COLUMN 3
        {
          //SECTION 1
          "heading": [{
            "text": ''
          }],
          "sections": 0
        }]]
      }, {
        //CHANNEL 6
        "channelName":'',
        "channelTitle":'',
        "channelDescription":'',
        "channelImage":'https://adamcruse.schoolwires.net/cms/lib/SWCS000009/Centricity/Template/GlobalAssets/images///Faces/default-man.jpg',
        "channelImageAlt":'',
        "channelColumns": [[//COLUMN 1
        {
          //SECTION 1
          "heading": [{
            "text":''
          }],
          "sections":0
        }], [//COLUMN 2
        {
          //SECTION 1
          "heading": [{
            "text":''
          }],
          "sections":0
        }], [//COLUMN 3
        {
          //SECTION 1
          "heading": [{
            "text": ''
          }],
          "sections": 0
        }]]
      }, {
        //CHANNEL 7
        "channelName":'',
        "channelTitle":'',
        "channelDescription":'',
        "channelImage":'https://adamcruse.schoolwires.net/cms/lib/SWCS000009/Centricity/Template/GlobalAssets/images///Faces/default-man.jpg',
        "channelImageAlt":'',
        "channelColumns": [[//COLUMN 1
        {
          //SECTION 1
          "heading": [{
            "text":''
          }],
          "sections":0
        }], [//COLUMN 2
        {
          //SECTION 1
          "heading": [{
            "text":''
          }],
          "sections":0
        }], [//COLUMN 3
        {
          //SECTION 1
          "heading": [{
            "text": ''
          }],
          "sections": 0
        }]]
      }, {
        //CHANNEL 8
        "channelName":'',
        "channelTitle":'',
        "channelDescription":'',
        "channelImage":'https://adamcruse.schoolwires.net/cms/lib/SWCS000009/Centricity/Template/GlobalAssets/images///Faces/default-man.jpg',
        "channelImageAlt":'',
        "channelColumns": [[//COLUMN 1
        {
          //SECTION 1
          "heading": [{
            "text":''
          }],
          "sections":0
        }], [//COLUMN 2
        {
          //SECTION 1
          "heading": [{
            "text":''
          }],
          "sections":0
        }], [//COLUMN 3
        {
          //SECTION 1
          "heading": [{
            "text": ''
          }],
          "sections": 0
        }]]
      }, {
        //CHANNEL 9
        "channelName":'',
        "channelTitle":'',
        "channelDescription":'',
        "channelImage":'https://adamcruse.schoolwires.net/cms/lib/SWCS000009/Centricity/Template/GlobalAssets/images///Faces/default-man.jpg',
        "channelImageAlt":'',
        "channelColumns": [[//COLUMN 1
        {
          //SECTION 1
          "heading": [{
            "text":''
          }],
          "sections":0
        }], [//COLUMN 2
        {
          //SECTION 1
          "heading": [{
            "text":''
          }],
          "sections":0
        }], [//COLUMN 3
        {
          //SECTION 1
          "heading": [{
            "text": ''
          }],
          "sections": 0
        }]]
      }, {
        //CHANNEL 10 - CALL OUT CHANNEL
        "channelName":'Departments',
        "channelTitle": '',
        "channelDescription": '',
        "channelImage": '',
        "channelImageAlt": '',
        "channelColumns": [[//COLUMN 1
        {
          //SECTION 1
          "heading": [{
            "text":'Column Header 1'
          }],
          "sections":2
        }], [//COLUMN 2
        {
          //SECTION 2
          "heading": [{
            "text":'Column Header 2'
          }],
          "sections":2
        }], [//COLUMN 3
        {
          //SECTION 2
          "heading": [{
            "text":'Column Header 3'
          }],
          "sections":3
        }]]
      }],
      "allLoaded": function allLoaded() {
        var useQuickLinks =true; //WRAP CHANNEL TITLES AND DESCRIPTIONS

        $(".csmc").each(function () {
          if (!$(this).find(".csmc-title").length && !$(this).find(".csmc-description").length) {
            $(this).attr("data-image-only", "true");
          } else {
            $(this).find(".csmc-title, .csmc-description").wrapAll("<span class='csmc-title-description'></span>");
          } //LABEL QUICK LINKS CHANNEL


          if (useQuickLinks) {
            var channelName = $.trim($(this).find("> a span").text());
            var quickLinksChannelName = $.trim('Departments');

            if (channelName == quickLinksChannelName) {
              $(this).addClass("quick-links-channel");
            }
          }
        });

        if (useQuickLinks) {
          var quickLinksDD = $(".quick-links-channel .sw-channel-dropdown").html();
          $(".cs-mystart-dropdown.quick-links .cs-dropdown-list").append(quickLinksDD);
          var quickLinksDDCol1 = $(".quick-links-channel .sw-channel-dropdown .csmc-column:nth-child(1)").html();
          var quickLinksDDCol2 = $(".quick-links-channel .sw-channel-dropdown .csmc-column:nth-child(2)").html();
          var quickLinksDDCol3 = $(".quick-links-channel .sw-channel-dropdown .csmc-column:nth-child(3)").html(); //ADD SOCIAL

          //var socialIcons = $(".footer-column.two .social-icons").html();
          $(".cs-mystart-dropdown.quick-links .cs-dropdown-list .csmc-column:last-child").append("<li class='csmc-section column-heading social-icons'><h2>SOCIAL MEDIA</h2></li><li class='csmc-section social-icons'><span></span></li>"); //ADD QUICK LINKS TO MOBILE MENU

          $(".rs-menu-group.quicklinks .rs-menu-group-list").append("<ul>" + quickLinksDDCol1 + quickLinksDDCol2 + quickLinksDDCol3 + "</ul>");
       		
            template.SocialIconsQuickLinks();
        }
      }
    });
  },
  "ModEvents": function ModEvents() {
    // FOR SCOPE
    var template = this;
    $(".ui-widget.app.upcomingevents").modEvents({
      columns: "yes",
      monthLong: "yes",
      dayWeek: "yes"
    });
    eventsByDay(".upcomingevents .ui-articles");

    function eventsByDay(container) {
      $(".ui-article", container).each(function () {
        if (!$(this).find("h1.ui-article-title.sw-calendar-block-date").size()) {
          var moveArticle = $(this).html();
          $(this).parent().prev().children().children().next().append(moveArticle);
          $(this).parent().remove();
        }

        ;
      });
      $(".ui-article", container).each(function () {
        var newDateTime = $('.upcoming-column.left h1', this).html().toLowerCase().split("</span>");
        var newDate = '';
        var dateIndex = newDateTime.length > 2 ? 2 : 1; //if the dayWeek is set to yes we need to account for that in our array indexing
        //if we have the day of the week make sure to include it.

        if (dateIndex > 1) {
          newDate += newDateTime[0] + "</span>";
        } //add in the month


        newDate += newDateTime[dateIndex - 1] + '</span>'; //the month is always the in the left array position to the date
        //wrap the date in a new tag

        newDate += '<span class="jeremy-date">' + newDateTime[dateIndex] + '</span>'; //append the date and month back into their columns

        $('.upcoming-column.left h1', this).html(newDate); //add an ALL DAY label if no time was given

        $('.upcoming-column.right .ui-article-description', this).each(function () {
          if ($('.sw-calendar-block-time', this).length < 1) {
            //if it doesnt exist add it
            $(this).prepend('<span class="sw-calendar-block-time">ALL DAY</span>');
          }

          $(".sw-calendar-block-time", this).appendTo($(this));
        }); //WRAP DATE AND MONTH IN A CONTAINER

        $(".jeremy-date, .joel-month", this).wrapAll("<span class='adam-hug'></span>"); //ABBREVIATE DAY

        var origDay = $.trim($(this).find(".joel-day").text());
        var shortDay = origDay.substring(0, 3);
        $(this).find(".joel-day").text(shortDay);
      });
    } //ADD NO EVENTS TEXT


    $(".upcomingevents").each(function () {
      if (!$(this).find(".ui-article").length) {
        $("<li><p>There are no upcoming events to display.</p></l1>").appendTo($(this).find(".ui-articles"));
      }
    });
  },
  "GlobalIcons": function GlobalIcons() {
    $("#gb-icons .content-wrap").creativeIcons({
      "iconNum":"6",
      "defaultIconSrc": "",
      "icons": [{
        "image":"Site Utility/28.png",
        "showText": true,
        "text":"Employment",
        "url":"#",
        "target":"_self"
      }, {
        "image":"Site Utility/27.png",
        "showText": true,
        "text":"Directory",
        "url":"#",
        "target":"_self"
      }, {
        "image":"People/25.png",
        "showText": true,
        "text":"Families",
        "url":"#",
        "target":"_self"
      }, {
        "image":"People/54.png",
        "showText": true,
        "text":"Community",
        "url":"#",
        "target":"_self"
      }, {
        "image":"Student Program/31.png",
        "showText": true,
        "text":"Staff Net",
        "url":"#",
        "target":"_self"
      }, {
        "image":"Site Utility/33.png",
        "showText": true,
        "text":"Calendar",
        "url":"#",
        "target":"_self"
      }],
      "siteID": "4",
      "siteAlias": "eisd",
      "calendarLink": "/Page/2",
      "contactEmail": "webmaster@email.com",
      "allLoaded": function allLoaded() {}
    });
  },
  "SocialIcons": function SocialIcons() {
    var socialIcons = [{
      "show":true,
      "label": "Facebook",
      "class": "facebook",
      "url":"#",
      "target":"_blank"
    }, {
      "show":true,
      "label": "Twitter",
      "class": "twitter",
      "url":"#",
      "target":"_blank"
    }, {
      "show":true,
      "label": "Instagram",
      "class": "instagram",
      "url":"#",
      "target":"_blank"
    }, {
      "show":true,
      "label": "LinkedIn",
      "class": "linkedin",
      "url":"#",
      "target":"_blank"
    }, {
      "show":true,
      "label": "YouTube",
      "class": "youtube",
      "url":"#",
      "target":"_blank"
    }, {
      "show":true,
      "label": "Pinterest",
      "class": "pinterest",
      "url":"#",
      "target":"_blank"
    }];
    var icons = '';
    $.each(socialIcons, function (index, icon) {
      if (icon.show) {
        icons += '<a class="gb-social-media-icon ' + icon.class + ' district" href="' + icon.url + '" target="' + icon.target + '" aria-label="' + icon.label + '"><span></span></a>';
      }
    });

    if (icons.length) {
        $(".footer-column.two .social-icons").prepend(icons);
    }
  },
  "SocialIconsQuickLinks": function SocialIcons() {
    var socialIcons = [{
      "show":true,
      "label": "Facebook",
      "class": "facebook",
      "url":"#",
      "target":"_blank"
    }, {
      "show":true,
      "label": "Twitter",
      "class": "twitter",
      "url":"#",
      "target":"_blank"
    }, {
      "show":true,
      "label": "Instagram",
      "class": "instagram",
      "url":"#",
      "target":"_blank"
    }, {
      "show":true,
      "label": "LinkedIn",
      "class": "linkedin",
      "url":"#",
      "target":"_blank"
    }, {
      "show":true,
      "label": "YouTube",
      "class": "youtube",
      "url":"#",
      "target":"_blank"
    }, {
      "show":true,
      "label": "Pinterest",
      "class": "pinterest",
      "url":"#",
      "target":"_blank"
    }];
    var icons = '';
    $.each(socialIcons, function (index, icon) {
      if (icon.show) {
        icons += '<a class="gb-social-media-icon ' + icon.class + ' district" href="' + icon.url + '" target="' + icon.target + '" aria-label="' + icon.label + '"><span></span></a>';
      }
    });

    if (icons.length) {
        $(".quick-links .social-icons:not(.column-heading) span").prepend(icons);
    }
  },
  "RsMenu": function RsMenu() {
    // FOR SCOPE
    var template = this;
    var useQuickLinksMobile =true;
    var rsExtraMenuOptions = {};

    if (useQuickLinksMobile) {
      rsExtraMenuOptions = {
        "Quick Links": []
      };
    }

    $.csRsMenu({
      "breakPoint": 768,
      // SYSTEM BREAK POINTS - 768, 640, 480, 320
      "slideDirection": "right-to-left",
      // OPTIONS - left-to-right, right-to-left
      "menuButtonParent": ".mobile-menu",
      "menuBtnText": "MENU",
      "colors": {
        "pageOverlay":"#000000",
        // DEFAULT #000000
        "menuBackground":"#FFFFFF",
        // DEFAULT #FFFFFF
        "menuText":"#333333",
        // DEFAULT #333333
        "menuTextAccent":"#333333",
        // DEFAULT #333333
        "dividerLines":"#e6e6e6",
        // DEFAULT #E6E6E6
        "buttonBackground":"#e6e6e6",
        // DEFAULT #E6E6E6
        "buttonText":"#333333" // DEFAULT #333333

      },
      "showDistrictHome": template.ShowDistrictHome,
      "districtHomeText": "District",
      "showSchools": template.ShowSchoolList,
      "schoolMenuText": "Schools",
      "showTranslate": true,
      "translateMenuText": "Translate",
      "translateVersion": 2,
      // 1 = FRAMESET, 2 = BRANDED
      "translateId": "",
      "translateLanguages": [// ["ENGLISH LANGUAGE NAME", "TRANSLATED LANGUAGE NAME", "LANGUAGE CODE"]
      ["Afrikaans", "Afrikaans", "af"], ["Albanian", "shqiptar", "sq"], ["Amharic", "አማርኛ", "am"], ["Arabic", "العربية", "ar"], ["Armenian", "հայերեն", "hy"], ["Azerbaijani", "Azərbaycan", "az"], ["Basque", "Euskal", "eu"], ["Belarusian", "Беларуская", "be"], ["Bengali", "বাঙালি", "bn"], ["Bosnian", "bosanski", "bs"], ["Bulgarian", "български", "bg"], ["Burmese", "မြန်မာ", "my"], ["Catalan", "català", "ca"], ["Cebuano", "Cebuano", "ceb"], ["Chichewa", "Chichewa", "ny"], ["Chinese Simplified", "简体中文", "zh-CN"], ["Chinese Traditional", "中國傳統的", "zh-TW"], ["Corsican", "Corsu", "co"], ["Croatian", "hrvatski", "hr"], ["Czech", "čeština", "cs"], ["Danish", "dansk", "da"], ["Dutch", "Nederlands", "nl"], ["Esperanto", "esperanto", "eo"], ["Estonian", "eesti", "et"], ["Filipino", "Pilipino", "tl"], ["Finnish", "suomalainen", "fi"], ["French", "français", "fr"], ["Galician", "galego", "gl"], ["Georgian", "ქართული", "ka"], ["German", "Deutsche", "de"], ["Greek", "ελληνικά", "el"], ["Gujarati", "ગુજરાતી", "gu"], ["Haitian Creole", "kreyòl ayisyen", "ht"], ["Hausa", "Hausa", "ha"], ["Hawaiian", "ʻŌlelo Hawaiʻi", "haw"], ["Hebrew", "עִברִית", "iw"], ["Hindi", "हिंदी", "hi"], ["Hmong", "Hmong", "hmn"], ["Hungarian", "Magyar", "hu"], ["Icelandic", "Íslenska", "is"], ["Igbo", "Igbo", "ig"], ["Indonesian", "bahasa Indonesia", "id"], ["Irish", "Gaeilge", "ga"], ["Italian", "italiano", "it"], ["Japanese", "日本語", "ja"], ["Javanese", "Jawa", "jw"], ["Kannada", "ಕನ್ನಡ", "kn"], ["Kazakh", "Қазақ", "kk"], ["Khmer", "ភាសាខ្មែរ", "km"], ["Korean", "한국어", "ko"], ["Kurdish", "Kurdî", "ku"], ["Kyrgyz", "Кыргызча", "ky"], ["Lao", "ລາວ", "lo"], ["Latin", "Latinae", "la"], ["Latvian", "Latvijas", "lv"], ["Lithuanian", "Lietuvos", "lt"], ["Luxembourgish", "lëtzebuergesch", "lb"], ["Macedonian", "Македонски", "mk"], ["Malagasy", "Malagasy", "mg"], ["Malay", "Malay", "ms"], ["Malayalam", "മലയാളം", "ml"], ["Maltese", "Malti", "mt"], ["Maori", "Maori", "mi"], ["Marathi", "मराठी", "mr"], ["Mongolian", "Монгол", "mn"], ["Myanmar", "မြန်မာ", "my"], ["Nepali", "नेपाली", "ne"], ["Norwegian", "norsk", "no"], ["Nyanja", "madambwe", "ny"], ["Pashto", "پښتو", "ps"], ["Persian", "فارسی", "fa"], ["Polish", "Polskie", "pl"], ["Portuguese", "português", "pt"], ["Punjabi", "ਪੰਜਾਬੀ ਦੇ", "pa"], ["Romanian", "Română", "ro"], ["Russian", "русский", "ru"], ["Samoan", "Samoa", "sm"], ["Scottish Gaelic", "Gàidhlig na h-Alba", "gd"], ["Serbian", "Српски", "sr"], ["Sesotho", "Sesotho", "st"], ["Shona", "Shona", "sn"], ["Sindhi", "سنڌي", "sd"], ["Sinhala", "සිංහල", "si"], ["Slovak", "slovenský", "sk"], ["Slovenian", "slovenski", "sl"], ["Somali", "Soomaali", "so"], ["Spanish", "Español", "es"], ["Sundanese", "Sunda", "su"], ["Swahili", "Kiswahili", "sw"], ["Swedish", "svenska", "sv"], ["Tajik", "Тоҷикистон", "tg"], ["Tamil", "தமிழ்", "ta"], ["Telugu", "తెలుగు", "te"], ["Thai", "ไทย", "th"], ["Turkish", "Türk", "tr"], ["Ukrainian", "український", "uk"], ["Urdu", "اردو", "ur"], ["Uzbek", "O'zbekiston", "uz"], ["Vietnamese", "Tiếng Việt", "vi"], ["Welsh", "Cymraeg", "cy"], ["Western Frisian", "Western Frysk", "fy"], ["Xhosa", "isiXhosa", "xh"], ["Yiddish", "ייִדיש", "yi"], ["Yoruba", "yorùbá", "yo"], ["Zulu", "Zulu", "zu"]],
      "showAccount": true,
      "accountMenuText": "User Options",
      "usePageListNavigation": false,
      "extraMenuOptions": rsExtraMenuOptions,
      "siteID": "4",
      "allLoaded": function allLoaded() {
        if (template.GetBreakPoint() == "480" || template.GetBreakPoint() == "320") {
          $("#rs-menu-btn").appendTo(".header-row.one .header-column.two");
        }

        if (useQuickLinksMobile) {
          $(".rs-menu-group.quicklinks").insertAfter(".rs-menu-group.channels");
        }
      }
    });
  },
  "AppAccordion": function AppAccordion() {
    $(".sp-column.one").csAppAccordion({
      "accordionBreakpoints": [768, 640, 480, 320]
    });
  },
  "Search": function Search() {
    // FOR SCOPE
    var template = this;
    $(".cs-mystart-button.search .cs-button-selector").on('click keydown', function (event) {
      if (template.AllyClick(event) === true) {
        //DONT LET THE PAGE JUMP ON KEYPRESS
        event.preventDefault();

        if (!$(this).parent().hasClass("open")) {
          openSearch();
        } else {
          closeSearch();
        }
      }
    });
    $(".cs-mystart-button.search .cs-button-selector, #gb-search-input, #gb-search-button").on("focusout", function () {
      setTimeout(function () {
        if (!$(".cs-mystart-button.search").find(":focus").length) {
          closeSearch();
        }
      }, 500);
    });

    function closeSearch() {
      $(".cs-mystart-button.search .cs-button-selector").parent().removeClass("open");
      $(".cs-mystart-button.search .cs-button-selector").removeClass("open").attr("aria-expanded", "false");
      $("#gb-search").removeClass("open").attr("aria-hidden", "true");
      $("#gb-search-input, #gb-search-button").attr("tabindex", "-1");
    }

    function openSearch() {
      $(".cs-mystart-button.search .cs-button-selector").addClass("open").attr("aria-expanded", "true");
      $(".cs-mystart-button.search .cs-button-selector").parent().addClass("open");
      $("#gb-search").addClass("open").attr("aria-hidden", "false");
      $("#gb-search-input").focus().attr("tabindex", "0");
      $("#gb-search-button").attr("tabindex", "0");
    } //SEARCH FORM SUBMISSION


    $("#gb-search-form").submit(function (e) {
      e.preventDefault();

      if ($.trim($("#gb-search-input").val()) != "I'm looking for..." && $.trim($("#gb-search-input").val()) != "") {
        window.location.href = "/site/Default.aspx?PageType=6&SiteID=4&SearchString=" + $("#gb-search-input").val();
      }
    }); //SEARCH INPUT TEXT

    $("#gb-search-input").focus(function () {
      if ($(this).val() == "I'm looking for...") {
        $(this).val("");
      }
    });
    $("#gb-search-input").blur(function () {
      if ($(this).val() == "") {
        $(this).val("I'm looking for...");
      }
    });
  },
  "AllyClick": function AllyClick(event) {
    if (event.type == "click") {
      return true;
    } else if (event.type == "keydown") {
      if (event.keyCode == this.KeyCodes.space || event.keyCode == this.KeyCodes.enter) {
        return true;
      }
    } else {
      return false;
    }
  },
  "GetBreakPoint": function GetBreakPoint() {
    return window.getComputedStyle(document.querySelector("body"), ":before").getPropertyValue("content").replace(/"|'/g, "");
    /*"*/
  }
};
/******/ })()
;</script>

    <!-- App Preview -->
    


    <style type="text/css">
        /* HOMEPAGE EDIT THUMBNAIL STYLES */

        div.region {
            ;
        }

            div.region span.homepage-thumb-region-number {
                font: bold 100px verdana;
                color: #fff;
            }

        div.homepage-thumb-region {
            background: #264867; /*dark blue*/
            border: 5px solid #fff;
            text-align: center;
            padding: 40px 0 40px 0;
            display: block;
        }

            div.homepage-thumb-region.region-1 {
                background: #264867; /*dark blue*/
            }

            div.homepage-thumb-region.region-2 {
                background: #5C1700; /*dark red*/
            }

            div.homepage-thumb-region.region-3 {
                background: #335905; /*dark green*/
            }

            div.homepage-thumb-region.region-4 {
                background: #B45014; /*dark orange*/
            }

            div.homepage-thumb-region.region-5 {
                background: #51445F; /*dark purple*/
            }

            div.homepage-thumb-region.region-6 {
                background: #3B70A0; /*lighter blue*/
            }

            div.homepage-thumb-region.region-7 {
                background: #862200; /*lighter red*/
            }

            div.homepage-thumb-region.region-8 {
                background: #417206; /*lighter green*/
            }

            div.homepage-thumb-region.region-9 {
                background: #D36929; /*lighter orange*/
            }

            div.homepage-thumb-region.region-10 {
                background: #6E5C80; /*lighter purple*/
            }

        /* END HOMEPAGE EDIT THUMBNAIL STYLES */
    </style>

    <style type="text/css" media="print">
        .noprint {
            display: none !important;
        }
    </style>

    <style type ="text/css">
        .ui-txt-validate {
            display: none;
        }
    </style>

    

<!-- Begin Schoolwires Traffic Code --> 

<script type="text/javascript">

    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date(); a = s.createElement(o),
        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-5173826-6', 'auto', 'BBTracker' );
    ga('BBTracker.set', 'dimension1', 'AWS');
    ga('BBTracker.set', 'dimension2', 'False');
    ga('BBTracker.set', 'dimension3', 'SWCS000009');
    ga('BBTracker.set', 'dimension4', '4');
    ga('BBTracker.set', 'dimension5', '1');
    ga('BBTracker.set', 'dimension6', '1');

    ga('BBTracker.send', 'pageview');

</script>

<!-- End Schoolwires Traffic Code --> 

    <!-- Ally Alternative Formats Loader    START   -->
    
    <!-- Ally Alternative Formats Loader    END     -->

</head>
<script type="text/javascript">window.NREUM||(NREUM={});NREUM.info = {"beacon":"bam-cell.nr-data.net","errorBeacon":"bam-cell.nr-data.net","licenseKey":"e84461d315","applicationID":"14859287","transactionName":"Z1MEZEtSVkoFBxIKX14ZJ2NpHEtQEAFJB1VWVxNcTR1ZShQc","queueTime":0,"applicationTime":211,"agent":"","atts":""}</script><script type="text/javascript">(window.NREUM||(NREUM={})).init={ajax:{deny_list:["bam-cell.nr-data.net"]}};(window.NREUM||(NREUM={})).loader_config={licenseKey:"e84461d315",applicationID:"14859287"};window.NREUM||(NREUM={}),__nr_require=function(t,e,n){function r(n){if(!e[n]){var i=e[n]={exports:{}};t[n][0].call(i.exports,function(e){var i=t[n][1][e];return r(i||e)},i,i.exports)}return e[n].exports}if("function"==typeof __nr_require)return __nr_require;for(var i=0;i<n.length;i++)r(n[i]);return r}({1:[function(t,e,n){function r(){}function i(t,e,n,r){return function(){return s.recordSupportability("API/"+e+"/called"),o(t+e,[u.now()].concat(c(arguments)),n?null:this,r),n?void 0:this}}var o=t("handle"),a=t(9),c=t(10),f=t("ee").get("tracer"),u=t("loader"),s=t(4),d=NREUM;"undefined"==typeof window.newrelic&&(newrelic=d);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],l="api-",v=l+"ixn-";a(p,function(t,e){d[e]=i(l,e,!0,"api")}),d.addPageAction=i(l,"addPageAction",!0),d.setCurrentRouteName=i(l,"routeName",!0),e.exports=newrelic,d.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(t,e){var n={},r=this,i="function"==typeof e;return o(v+"tracer",[u.now(),t,n],r),function(){if(f.emit((i?"":"no-")+"fn-start",[u.now(),r,i],n),i)try{return e.apply(this,arguments)}catch(t){throw f.emit("fn-err",[arguments,this,t],n),t}finally{f.emit("fn-end",[u.now()],n)}}}};a("actionText,setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(t,e){m[e]=i(v,e)}),newrelic.noticeError=function(t,e){"string"==typeof t&&(t=new Error(t)),s.recordSupportability("API/noticeError/called"),o("err",[t,u.now(),!1,e])}},{}],2:[function(t,e,n){function r(t){if(NREUM.init){for(var e=NREUM.init,n=t.split("."),r=0;r<n.length-1;r++)if(e=e[n[r]],"object"!=typeof e)return;return e=e[n[n.length-1]]}}e.exports={getConfiguration:r}},{}],3:[function(t,e,n){var r=!1;try{var i=Object.defineProperty({},"passive",{get:function(){r=!0}});window.addEventListener("testPassive",null,i),window.removeEventListener("testPassive",null,i)}catch(o){}e.exports=function(t){return r?{passive:!0,capture:!!t}:!!t}},{}],4:[function(t,e,n){function r(t,e){var n=[a,t,{name:t},e];return o("storeMetric",n,null,"api"),n}function i(t,e){var n=[c,t,{name:t},e];return o("storeEventMetrics",n,null,"api"),n}var o=t("handle"),a="sm",c="cm";e.exports={constants:{SUPPORTABILITY_METRIC:a,CUSTOM_METRIC:c},recordSupportability:r,recordCustom:i}},{}],5:[function(t,e,n){function r(){return c.exists&&performance.now?Math.round(performance.now()):(o=Math.max((new Date).getTime(),o))-a}function i(){return o}var o=(new Date).getTime(),a=o,c=t(11);e.exports=r,e.exports.offset=a,e.exports.getLastTimestamp=i},{}],6:[function(t,e,n){function r(t,e){var n=t.getEntries();n.forEach(function(t){"first-paint"===t.name?l("timing",["fp",Math.floor(t.startTime)]):"first-contentful-paint"===t.name&&l("timing",["fcp",Math.floor(t.startTime)])})}function i(t,e){var n=t.getEntries();if(n.length>0){var r=n[n.length-1];if(u&&u<r.startTime)return;var i=[r],o=a({});o&&i.push(o),l("lcp",i)}}function o(t){t.getEntries().forEach(function(t){t.hadRecentInput||l("cls",[t])})}function a(t){var e=navigator.connection||navigator.mozConnection||navigator.webkitConnection;if(e)return e.type&&(t["net-type"]=e.type),e.effectiveType&&(t["net-etype"]=e.effectiveType),e.rtt&&(t["net-rtt"]=e.rtt),e.downlink&&(t["net-dlink"]=e.downlink),t}function c(t){if(t instanceof y&&!w){var e=Math.round(t.timeStamp),n={type:t.type};a(n),e<=v.now()?n.fid=v.now()-e:e>v.offset&&e<=Date.now()?(e-=v.offset,n.fid=v.now()-e):e=v.now(),w=!0,l("timing",["fi",e,n])}}function f(t){"hidden"===t&&(u=v.now(),l("pageHide",[u]))}if(!("init"in NREUM&&"page_view_timing"in NREUM.init&&"enabled"in NREUM.init.page_view_timing&&NREUM.init.page_view_timing.enabled===!1)){var u,s,d,p,l=t("handle"),v=t("loader"),m=t(8),g=t(3),y=NREUM.o.EV;if("PerformanceObserver"in window&&"function"==typeof window.PerformanceObserver){s=new PerformanceObserver(r);try{s.observe({entryTypes:["paint"]})}catch(h){}d=new PerformanceObserver(i);try{d.observe({entryTypes:["largest-contentful-paint"]})}catch(h){}p=new PerformanceObserver(o);try{p.observe({type:"layout-shift",buffered:!0})}catch(h){}}if("addEventListener"in document){var w=!1,b=["click","keydown","mousedown","pointerdown","touchstart"];b.forEach(function(t){document.addEventListener(t,c,g(!1))})}m(f)}},{}],7:[function(t,e,n){function r(t,e){if(!i)return!1;if(t!==i)return!1;if(!e)return!0;if(!o)return!1;for(var n=o.split("."),r=e.split("."),a=0;a<r.length;a++)if(r[a]!==n[a])return!1;return!0}var i=null,o=null,a=/Version\/(\S+)\s+Safari/;if(navigator.userAgent){var c=navigator.userAgent,f=c.match(a);f&&c.indexOf("Chrome")===-1&&c.indexOf("Chromium")===-1&&(i="Safari",o=f[1])}e.exports={agent:i,version:o,match:r}},{}],8:[function(t,e,n){function r(t){function e(){t(c&&document[c]?document[c]:document[o]?"hidden":"visible")}"addEventListener"in document&&a&&document.addEventListener(a,e,i(!1))}var i=t(3);e.exports=r;var o,a,c;"undefined"!=typeof document.hidden?(o="hidden",a="visibilitychange",c="visibilityState"):"undefined"!=typeof document.msHidden?(o="msHidden",a="msvisibilitychange"):"undefined"!=typeof document.webkitHidden&&(o="webkitHidden",a="webkitvisibilitychange",c="webkitVisibilityState")},{}],9:[function(t,e,n){function r(t,e){var n=[],r="",o=0;for(r in t)i.call(t,r)&&(n[o]=e(r,t[r]),o+=1);return n}var i=Object.prototype.hasOwnProperty;e.exports=r},{}],10:[function(t,e,n){function r(t,e,n){e||(e=0),"undefined"==typeof n&&(n=t?t.length:0);for(var r=-1,i=n-e||0,o=Array(i<0?0:i);++r<i;)o[r]=t[e+r];return o}e.exports=r},{}],11:[function(t,e,n){e.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(t,e,n){function r(){}function i(t){function e(t){return t&&t instanceof r?t:t?u(t,f,a):a()}function n(n,r,i,o,a){if(a!==!1&&(a=!0),!l.aborted||o){t&&a&&t(n,r,i);for(var c=e(i),f=m(n),u=f.length,s=0;s<u;s++)f[s].apply(c,r);var p=d[w[n]];return p&&p.push([b,n,r,c]),c}}function o(t,e){h[t]=m(t).concat(e)}function v(t,e){var n=h[t];if(n)for(var r=0;r<n.length;r++)n[r]===e&&n.splice(r,1)}function m(t){return h[t]||[]}function g(t){return p[t]=p[t]||i(n)}function y(t,e){l.aborted||s(t,function(t,n){e=e||"feature",w[n]=e,e in d||(d[e]=[])})}var h={},w={},b={on:o,addEventListener:o,removeEventListener:v,emit:n,get:g,listeners:m,context:e,buffer:y,abort:c,aborted:!1};return b}function o(t){return u(t,f,a)}function a(){return new r}function c(){(d.api||d.feature)&&(l.aborted=!0,d=l.backlog={})}var f="nr@context",u=t("gos"),s=t(9),d={},p={},l=e.exports=i();e.exports.getOrSetContext=o,l.backlog=d},{}],gos:[function(t,e,n){function r(t,e,n){if(i.call(t,e))return t[e];var r=n();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(t,e,{value:r,writable:!0,enumerable:!1}),r}catch(o){}return t[e]=r,r}var i=Object.prototype.hasOwnProperty;e.exports=r},{}],handle:[function(t,e,n){function r(t,e,n,r){i.buffer([t],r),i.emit(t,e,n)}var i=t("ee").get("handle");e.exports=r,r.ee=i},{}],id:[function(t,e,n){function r(t){var e=typeof t;return!t||"object"!==e&&"function"!==e?-1:t===window?0:a(t,o,function(){return i++})}var i=1,o="nr@id",a=t("gos");e.exports=r},{}],loader:[function(t,e,n){function r(){if(!M++){var t=T.info=NREUM.info,e=m.getElementsByTagName("script")[0];if(setTimeout(u.abort,3e4),!(t&&t.licenseKey&&t.applicationID&&e))return u.abort();f(x,function(e,n){t[e]||(t[e]=n)});var n=a();c("mark",["onload",n+T.offset],null,"api"),c("timing",["load",n]);var r=m.createElement("script");0===t.agent.indexOf("http://")||0===t.agent.indexOf("https://")?r.src=t.agent:r.src=l+"://"+t.agent,e.parentNode.insertBefore(r,e)}}function i(){"complete"===m.readyState&&o()}function o(){c("mark",["domContent",a()+T.offset],null,"api")}var a=t(5),c=t("handle"),f=t(9),u=t("ee"),s=t(7),d=t(2),p=t(3),l=d.getConfiguration("ssl")===!1?"http":"https",v=window,m=v.document,g="addEventListener",y="attachEvent",h=v.XMLHttpRequest,w=h&&h.prototype,b=!1;NREUM.o={ST:setTimeout,SI:v.setImmediate,CT:clearTimeout,XHR:h,REQ:v.Request,EV:v.Event,PR:v.Promise,MO:v.MutationObserver};var E=""+location,x={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1216.min.js"},O=h&&w&&w[g]&&!/CriOS/.test(navigator.userAgent),T=e.exports={offset:a.getLastTimestamp(),now:a,origin:E,features:{},xhrWrappable:O,userAgent:s,disabled:b};if(!b){t(1),t(6),m[g]?(m[g]("DOMContentLoaded",o,p(!1)),v[g]("load",r,p(!1))):(m[y]("onreadystatechange",i),v[y]("onload",r)),c("mark",["firstbyte",a.getLastTimestamp()],null,"api");var M=0}},{}],"wrap-function":[function(t,e,n){function r(t,e){function n(e,n,r,f,u){function nrWrapper(){var o,a,s,p;try{a=this,o=d(arguments),s="function"==typeof r?r(o,a):r||{}}catch(l){i([l,"",[o,a,f],s],t)}c(n+"start",[o,a,f],s,u);try{return p=e.apply(a,o)}catch(v){throw c(n+"err",[o,a,v],s,u),v}finally{c(n+"end",[o,a,p],s,u)}}return a(e)?e:(n||(n=""),nrWrapper[p]=e,o(e,nrWrapper,t),nrWrapper)}function r(t,e,r,i,o){r||(r="");var c,f,u,s="-"===r.charAt(0);for(u=0;u<e.length;u++)f=e[u],c=t[f],a(c)||(t[f]=n(c,s?f+r:r,i,f,o))}function c(n,r,o,a){if(!v||e){var c=v;v=!0;try{t.emit(n,r,o,e,a)}catch(f){i([f,n,r,o],t)}v=c}}return t||(t=s),n.inPlace=r,n.flag=p,n}function i(t,e){e||(e=s);try{e.emit("internal-error",t)}catch(n){}}function o(t,e,n){if(Object.defineProperty&&Object.keys)try{var r=Object.keys(t);return r.forEach(function(n){Object.defineProperty(e,n,{get:function(){return t[n]},set:function(e){return t[n]=e,e}})}),e}catch(o){i([o],n)}for(var a in t)l.call(t,a)&&(e[a]=t[a]);return e}function a(t){return!(t&&t instanceof Function&&t.apply&&!t[p])}function c(t,e){var n=e(t);return n[p]=t,o(t,n,s),n}function f(t,e,n){var r=t[e];t[e]=c(r,n)}function u(){for(var t=arguments.length,e=new Array(t),n=0;n<t;++n)e[n]=arguments[n];return e}var s=t("ee"),d=t(10),p="nr@original",l=Object.prototype.hasOwnProperty,v=!1;e.exports=r,e.exports.wrapFunction=c,e.exports.wrapInPlace=f,e.exports.argsToArray=u},{}]},{},["loader"]);</script><body>

    <input type="hidden" id="hidFullPath" value="https://adamcruse.schoolwires.net/" />
    <input type="hidden" id="hidActiveChannelNavType" value="-1" />
    <input type="hidden" id="hidActiveChannel" value ="0" />
    <input type="hidden" id="hidActiveSection" value="0" />

    <!-- OnScreen Alert Dialog Start -->
    <div id="onscreenalert-holder"></div>
    <!-- OnScreen Alert Dialog End -->

    <!-- ADA Skip Nav -->
    <div class="sw-skipnav-outerbar">
        <a href="#sw-maincontent" id="skipLink" class="sw-skipnav" tabindex="0">Skip to Main Content</a>
    </div>

    <!-- DashBoard SideBar Start -->
    
    <!-- DashBoard SideBar End -->

    <!-- off-canvas menu enabled-->
    

    

<style type="text/css">
	/* SPECIAL MODE BAR */
	div.sw-special-mode-bar {
		background: #FBC243 url('https://adamcruse.schoolwires.net/Static//GlobalAssets/Images/special-mode-bar-background.png') no-repeat;
		height: 30px;
		text-align: left;
		font-size: 12px;
		position: relative;
		z-index: 10000;
	}
	div.sw-special-mode-bar > div {
		padding: 8px 0 0 55px;
		font-weight: bold;
	}
	div.sw-special-mode-bar > div > a {
		margin-left: 20px;
		background: #A0803D;
		-moz-border-radius: 5px;
		-webkit-border-radius: 5px;
		color: #fff;
		padding: 4px 6px 4px 6px;
		font-size: 11px;
	}

	/* END SPECIAL MODE BAR */
</style>

<script type="text/javascript">
	
	function SWEndPreviewMode() { 
		var data = "{}";
		var success = "window.location='';";
		var failure = "CallControllerFailure(result[0].errormessage);";
		CallController("https://adamcruse.schoolwires.net/site/SiteController.aspx/EndPreviewMode", data, success, failure);
	}
	
    function SWEndEmulationMode() {
        var data = "{}";
        var success = "DeleteCookie('SourceEmulationUserID');DeleteCookie('SidebarIsClosed');window.location='https://adamcruse.schoolwires.net/ums/Users/Users.aspx';";
        var failure = "CallControllerFailure(result[0].errormessage);";
        CallController("https://adamcruse.schoolwires.net/site/SiteController.aspx/EndEmulationMode", data, success, failure);
	}

	function SWEndPreviewConfigMode() {
	    var data = "{}";
	    var success = "window.location='';";
	    var failure = "CallControllerFailure(result[0].errormessage);";
	    CallController("https://adamcruse.schoolwires.net/site/SiteController.aspx/EndPreviewConfigMode", data, success, failure);
	}
</script>
            

    <!-- BEGIN - MYSTART BAR -->
<div id='sw-mystart-outer' class='noprint'>
<div id='sw-mystart-inner'>
<div id='sw-mystart-left'>
<div class='sw-mystart-nav sw-mystart-button home'><a tabindex="0" href="https://adamcruse.schoolwires.net/Domain/4" alt="District Home" title="Return to the homepage on the district site."><span>District Home<div id='sw-home-icon'></div>
</span></a></div>
<div class='sw-mystart-nav sw-mystart-dropdown schoollist' tabindex='0' aria-label='Select a School' role='navigation'>
<div class='selector' aria-hidden='true'>Select a School...</div>
<div class='sw-dropdown' aria-hidden='false'>
<div class='sw-dropdown-selected' aria-hidden='true'>Select a School</div>
<ul class='sw-dropdown-list' aria-hidden='false' aria-label='Schools'>
<li><a href="https://adamcruse.schoolwires.net/Domain/9">Dev Site 2 (es)</a></li>
<li><a href="https://adamcruse.schoolwires.net/Domain/10">Raph (hs)</a></li>
<li><a href="https://adamcruse.schoolwires.net/Domain/11">Mikey (ea)</a></li>
<li><a href="https://adamcruse.schoolwires.net/Domain/8">Leo (ms) (es)</a></li>
<li><a href="https://adamcruse.schoolwires.net/Domain/29">Styler Training (ct) (hs) (es)</a></li>
</ul>
</div>
<div class='sw-dropdown-arrow' aria-hidden='true'></div>
</div>
</div>
<div id='sw-mystart-right'>
<div id='ui-btn-signin' class='sw-mystart-button signin'><a href="https://adamcruse.schoolwires.net/site/Default.aspx?PageType=7&SiteID=4&IgnoreRedirect=true"><span>Sign In</span></a></div>
<div id='ui-btn-register' class='sw-mystart-button register'><a href="https://adamcruse.schoolwires.net/site/Default.aspx?PageType=10&SiteID=4"><span>Register</span></a></div>
<div id='sw-mystart-search' class='sw-mystart-nav'>
<script type="text/javascript">
$(document).ready(function() {
    $('#sw-search-input').keyup(function(e) {        if (e.keyCode == 13) {
            SWGoToSearchResultsPageswsearchinput();
        }
    });
    $('#sw-search-input').val($('#swsearch-hid-word').val())});
function SWGoToSearchResultsPageswsearchinput() {
window.location.href="https://adamcruse.schoolwires.net/site/Default.aspx?PageType=6&SiteID=4&SearchString=" + $('#sw-search-input').val();
}
</script>
<label for="sw-search-input" class="hidden-label">Search Our Site</label>
<input id="sw-search-input" type="text" title="Search Term" aria-label="Search Our Site" placeholder="Search this Site..." />
<a tabindex="0" id="sw-search-button" title="Search" href="javascript:;" role="button" aria-label="Submit Site Search" onclick="SWGoToSearchResultsPageswsearchinput();"><span><img src="https://adamcruse.schoolwires.net/Static//globalassets/images/sw-mystart-search.png" alt="Search" /></span></a><script type="text/javascript">
$(document).ready(function() {
    $('#sw-search-button').keyup(function(e) {        if (e.keyCode == 13) {
            SWGoToSearchResultsPageswsearchinput();        }
    });
});
</script>

</div>
<div class='clear'></div>
</div>
</div>
</div>
<!-- END - MYSTART BAR -->
<div id="gb-page" class="hp eisd" data-slideshow-option='Multimedia Gallery' data-show-logo='true' data-show-sitename='false'>
	<header id="gb-header">
		<section class="header-row one">
			<div class="header-column one">
				<div class="cs-mystart-button home" data-toggle='false'>
					<a class="cs-button-selector" href="https://adamcruse.schoolwires.net/">District Home</a>
				</div>
			</div>
			<div class="header-column two">
				<div class="cs-mystart-button quick-translate es">
					<div tabindex="0" role="button" class="cs-button-selector" data-lang="es" data-lang-code="/en/es" aria-label="traducir esta página al español"><span>Español</span></div>
				</div>
				<div class="cs-mystart-button quick-translate so">
					<div tabindex="0" role="button" class="cs-button-selector" data-lang="so" data-lang-code="/en/so" aria-label="boggaan u tarjun somali"><span>Soomaali</span></div>
				</div>
				<div class="cs-mystart-dropdown translate">
					<div class="cs-dropdown-selector" tabindex="0" aria-label='Translate' role="button" aria-expanded="false" aria-haspopup="true">Translate</div>
					<div class="cs-dropdown" aria-hidden="true" style="display:none;"></div>
				</div>
				<div class="cs-mystart-dropdown user-options">
					<div class="cs-dropdown-selector" tabindex="0" aria-label='User Options' role="button" aria-expanded="false" aria-haspopup="true"><span class="uo-text-wrapper"><span class="uo-text">User Options</span></span></div>
					<div class="cs-dropdown" aria-hidden="true" style="display:none;">
						<ul class="cs-dropdown-list"></ul>
					</div>
				</div>
			</div>
		</section>
		<section class="header-row two">
			<div class="header-column one">
				<nav id="gb-channel-bar" aria-label="Site Navigation" class="content-wrap">
					<div id="sw-channel-list-container" role="navigation">
<ul id="channel-navigation" class="sw-channel-list" role="menubar">
<li id="navc-HP" class="sw-channel-item" role="none"><a tabindex="-1" href="https://adamcruse.schoolwires.net/Page/1" aria-label="Home"><span>Home</span></a></li>
<li id="navc-146" class="sw-channel-item" >
<a tabindex="-1" href="https://adamcruse.schoolwires.net/domain/154"">
<span>Parents</span></a>
<ul class="hidden-sections">

</ul>
<ul role="menu" aria-label="Parents" class="sw-channel-dropdown">
<li role="none" id="navs-154"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/154"><span>Parents - Section 1</span></a></li>
<li role="none" id="navs-155"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/155"><span>Parents - Section 2</span></a></li>
<li role="none" id="navs-156"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/156"><span>Parents - Section 3</span></a></li>
<li role="none" id="navs-157"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/157"><span>Parents - Section 4</span></a></li>
<li role="none" id="navs-158"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/158"><span>Parents - Section 5</span></a></li>
<li role="none" id="navs-159"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/159"><span>Parents - Section 6</span></a></li>
<li role="none" id="navs-176"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/176"><span>Parents - Section 7</span></a></li>
<li role="none" id="navs-209"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/209"><span>Section 8</span></a></li>
<li role="none" id="navs-210"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/210"><span>Section 9</span></a></li>
<li role="none" id="navs-211"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/211"><span>Section 10</span></a></li>
<li role="none" id="navs-212"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/212"><span>Section 11</span></a></li>
<li role="none" id="navs-213"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/213"><span>Section 12</span></a></li>
<li role="none" id="navs-214"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/214"><span>Section 13</span></a></li>
<li role="none" id="navs-215"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/215"><span>Section 14</span></a></li>
<li role="none" id="navs-216"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/216"><span>Another Section Dude</span></a></li>
</ul>
</li><li id="navc-55" class="hidden-channel">
<a tabindex="-1" href="https://adamcruse.schoolwires.net/domain/59"">
<span>Our Parents</span></a>
<ul class="dropdown-hidden">
<li role="none" id="navs-59"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/59"><span>Section 1</span></a></li>
</ul>
</li><li id="navc-6" class="sw-channel-item" >
<a tabindex="-1" href="https://adamcruse.schoolwires.net/site/Default.aspx?PageType=1&SiteID=4&ChannelID=6&DirectoryType=6
"">
<span>About Us</span></a>
<ul class="hidden-sections">
<li id="navs-12" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/12"><span>Employment</span></a></li>
<li id="navs-13" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/13"><span>Embed Code</span></a></li>
<li id="navs-14" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/14"><span>Section 3</span></a></li>
<li id="navs-75" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/75"><span>Breaking Bad</span></a></li>
<li id="navs-76" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/76"><span>30 Rock</span></a></li>
<li id="navs-77" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/77"><span>Parks and Rec</span></a></li>
<li id="navs-102" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/102"><span>Ulysses</span></a></li>
<li id="navs-103" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/103"><span>The Great Gatsby</span></a></li>
<li id="navs-104" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/104"><span>A portrait of the Artist as a Young Man</span></a></li>
<li id="navs-105" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/105"><span>Lolita</span></a></li>
<li id="navs-106" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/106"><span>Brave New World</span></a></li>
<li id="navs-107" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/107"><span>The Sound and the Fury</span></a></li>
<li id="navs-108" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/108"><span>Catch 22</span></a></li>
<li id="navs-109" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/109"><span>Darkness at Noon</span></a></li>
<li id="navs-110" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/110"><span>Sons and Lovers</span></a></li>
<li id="navs-111" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/111"><span>The Grapes of Wrath</span></a></li>
<li id="navs-112" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/112"><span>Under the Volcano</span></a></li>
<li id="navs-114" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/114"><span>1984</span></a></li>
<li id="navs-115" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/115"><span>I, Claudius</span></a></li>
<li id="navs-116" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/116"><span>To the Lighthouse</span></a></li>
<li id="navs-117" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/117"><span>An American Tragedy</span></a></li>
<li id="navs-118" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/118"><span>The Heart is a Lonely Hunter</span></a></li>
<li id="navs-119" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/119"><span>Slaughterhouse-Five</span></a></li>
<li id="navs-120" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/120"><span>Invisible Man</span></a></li>
<li id="navs-121" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/121"><span>Native Son</span></a></li>
<li id="navs-122" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/122"><span>Henderson the Rain King</span></a></li>
<li id="navs-123" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/123"><span>Appointment in Samarra</span></a></li>
<li id="navs-124" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/124"><span>U.S.A.</span></a></li>
<li id="navs-125" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/125"><span>Winesburg, Ohio</span></a></li>
<li id="navs-113" class="hidden-section"><a href="https://adamcruse.schoolwires.net/domain/113"><span>The Way of All Flesh</span></a></li>

</ul>
<ul class="dropdown-hidden">
</ul>
</li><li id="navc-203" class="hidden-channel">
<a tabindex="-1" href="https://adamcruse.schoolwires.net/domain/204"">
<span>I Want To</span></a>
<ul class="hidden-sections">

</ul>
<ul role="menu" aria-label="I Want To" class="sw-channel-dropdown">
<li role="none" id="navs-204"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/204"><span>Apps</span></a></li>
<li role="none" id="navs-205"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/205"><span>Another Section</span></a></li>
<li role="none" id="navs-206"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/206"><span>One More Section</span></a></li>
<li role="none" id="navs-207"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/207"><span>Last Section</span></a></li>
</ul>
</li><li id="navc-19" class="hidden-channel">
<a tabindex="-1" href="https://adamcruse.schoolwires.net/domain/28"">
<span>Teachers</span></a>
<ul class="dropdown-hidden">
<li role="none" id="navs-28"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/28"><span>Section 1</span></a></li>
</ul>
</li><li id="navc-56" class="hidden-channel">
<a tabindex="-1" href="https://adamcruse.schoolwires.net/domain/60"">
<span>Our Students</span></a>
<ul class="dropdown-hidden">
<li role="none" id="navs-60"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/60"><span>Section 1</span></a></li>
</ul>
</li><li id="navc-18" class="sw-channel-item" >
<a tabindex="-1" href="https://adamcruse.schoolwires.net/domain/25"">
<span>Employment</span></a>
<ul class="hidden-sections">

</ul>
<ul role="menu" aria-label="Employment" class="sw-channel-dropdown">
<li role="none" id="navs-25"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/25"><span>Section 1</span></a></li>
<li role="none" id="navs-26"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/26"><span>Section 2</span></a></li>
<li role="none" id="navs-27"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/27"><span>Section 3</span></a></li>
<li role="none" id="navs-177"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/177"><span>Section 4</span></a></li>
</ul>
</li><li id="navc-57" class="hidden-channel">
<a tabindex="-1" href="https://adamcruse.schoolwires.net/domain/61"">
<span>For Staff</span></a>
<ul class="dropdown-hidden">
<li role="none" id="navs-61"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/61"><span>Section 1</span></a></li>
</ul>
</li><li id="navc-73" class="hidden-channel">
<a tabindex="-1" href="https://adamcruse.schoolwires.net/domain/74"">
<span>Schools</span></a>
<ul class="hidden-sections">

</ul>
<ul role="menu" aria-label="Schools" class="sw-channel-dropdown">
<li role="none" id="navs-74"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/74"><span>School Name 1</span></a></li>
<li role="none" id="navs-133"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/133"><span>School Name 2</span></a></li>
<li role="none" id="navs-134"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/134"><span>School Name 3</span></a></li>
<li role="none" id="navs-135"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/135"><span>School Name 4</span></a></li>
<li role="none" id="navs-136"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/136"><span>School Name 5</span></a></li>
</ul>
</li><li id="navc-145" class="hidden-channel">
<a tabindex="-1" href="https://adamcruse.schoolwires.net/domain/151"">
<span>Athletics</span></a>
<ul class="hidden-sections">

</ul>
<ul role="menu" aria-label="Athletics" class="sw-channel-dropdown">
<li role="none" id="navs-151"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/151"><span>Athletics - Section 1</span></a></li>
<li role="none" id="navs-152"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/152"><span>Athletics - Section 2</span></a></li>
<li role="none" id="navs-153"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/153"><span>Athletics - Section 3</span></a></li>
</ul>
</li><li id="navc-147" class="hidden-channel">
<a tabindex="-1" href="https://adamcruse.schoolwires.net/domain/160"">
<span>Upcoming Events</span></a>
<ul class="hidden-sections">

</ul>
<ul role="menu" aria-label="Upcoming Events" class="sw-channel-dropdown">
<li role="none" id="navs-160"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/160"><span>Upcoming Events - Section 1</span></a></li>
<li role="none" id="navs-161"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/161"><span>Upcoming Events - Section 2</span></a></li>
</ul>
</li><li id="navc-54" class="sw-channel-item" >
<a tabindex="-1" href="https://adamcruse.schoolwires.net/domain/58"">
<span>Our District</span></a>
<ul class="hidden-sections">

</ul>
<ul role="menu" aria-label="Our District" class="sw-channel-dropdown">
<li role="none" id="navs-58"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/58"><span>Our Community</span></a></li>
<li role="none" id="navs-62"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/62"><span>Section 2</span></a></li>
<li role="none" id="navs-63"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/63"><span>Section 3</span></a></li>
<li role="none" id="navs-64"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/64"><span>Section 4</span></a></li>
<li role="none" id="navs-65"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/65"><span>Section 5</span></a></li>
<li role="none" id="navs-66"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/66"><span>Section 6</span></a></li>
<li role="none" id="navs-67"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/67"><span>Section 7</span></a></li>
<li role="none" id="navs-68"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/68"><span>Section 8</span></a></li>
<li role="none" id="navs-69"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/69"><span>Section 9</span></a></li>
<li role="none" id="navs-70"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/70"><span>Section 10</span></a></li>
<li role="none" id="navs-71"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/71"><span>Section 11</span></a></li>
<li role="none" id="navs-72"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/72"><span>Section 12</span></a></li>
</ul>
</li><li id="navc-148" class="hidden-channel">
<a tabindex="-1" href="https://adamcruse.schoolwires.net/domain/162"">
<span>Communities</span></a>
<ul class="hidden-sections">

</ul>
<ul role="menu" aria-label="Communities" class="sw-channel-dropdown">
<li role="none" id="navs-162"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/162"><span>Communities - Section 1</span></a></li>
<li role="none" id="navs-163"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/163"><span>Communities - Section 2</span></a></li>
<li role="none" id="navs-164"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/164"><span>Communities - Section 3</span></a></li>
</ul>
</li><li id="navc-149" class="hidden-channel">
<a tabindex="-1" href="https://adamcruse.schoolwires.net/domain/165"">
<span>Contact Us</span></a>
<ul class="hidden-sections">

</ul>
<ul role="menu" aria-label="Contact Us" class="sw-channel-dropdown">
<li role="none" id="navs-165"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/165"><span>Contact Us - Section 1</span></a></li>
<li role="none" id="navs-166"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/166"><span>Contact Us - Section 2</span></a></li>
</ul>
</li><li id="navc-150" class="hidden-channel">
<a tabindex="-1" href="https://adamcruse.schoolwires.net/domain/167"">
<span>Students</span></a>
<ul class="hidden-sections">

</ul>
<ul role="menu" aria-label="Students" class="sw-channel-dropdown">
<li role="none" id="navs-167"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/167"><span>Students - Section 1</span></a></li>
<li role="none" id="navs-168"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/168"><span>Students - Section 2</span></a></li>
<li role="none" id="navs-169"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/169"><span>Students - Section 3</span></a></li>
</ul>
</li><li id="navc-17" class="hidden-channel">
<a tabindex="-1" href="https://adamcruse.schoolwires.net/domain/20"">
<span>Departments</span></a>
<ul class="hidden-sections">

</ul>
<ul role="menu" aria-label="Departments" class="sw-channel-dropdown">
<li role="none" id="navs-20"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/20"><span>Section 1</span></a></li>
<li role="none" id="navs-21"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/21"><span>Section 2</span></a></li>
<li role="none" id="navs-22"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/22"><span>Section 3</span></a></li>
<li role="none" id="navs-23"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/23"><span>Section 4</span></a></li>
<li role="none" id="navs-24"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/24"><span>Section 5</span></a></li>
<li role="none" id="navs-126"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/126"><span>Another Test</span></a></li>
<li role="none" id="navs-127"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/domain/127"><span>Testing</span></a></li>
</ul>
</li><li id="navc-178" class="hidden-channel">
<a tabindex="-1" href="https://adamcruse.schoolwires.net/site/Default.aspx?PageID=272"">
<span>Community</span></a>
<ul class="hidden-sections">

</ul>
<ul role="menu" aria-label="Community" class="sw-channel-dropdown">
<li role="none" id="navs-179"><a role="menuitem" tabindex="-1" href="https://adamcruse.schoolwires.net/site/Default.aspx?PageID=272"><span>District Application Programs</span></a></li>
<li role="none" id="navs-180"><a role="menuitem" tabindex="-1" href="http://www.google.com"><span>Test Section</span></a></li>
</ul>
</li><li id="navc-CA" class="sw-channel-item" role="none"><a tabindex="-1" href="https://adamcruse.schoolwires.net/Page/2"><span>Calendar</span></a></li>
</ul><div class='clear'></div>
</div>



<script type="text/javascript">
    $(document).ready(function() {
        channelHoverIE();
        channelTouch();
        closeMenuByPressingKey();
    });

    function channelTouch() {
        // this will change the dropdown behavior when it is touched vs clicked.
        // channels will be clickable on second click. first click simply opens the menu.
        $('#channel-navigation > .sw-channel-item > a').on({
            'touchstart': function (e) {
                
                // see if has menu
                if ($(this).siblings('ul.sw-channel-dropdown').children('li').length > 0)  {
                    var button = $(this);

                    // add href as property if not already set
                    // then remove href attribute
                    if (!button.prop('linkHref')) {
                        button.prop('linkHref', button.attr('href'));
                        button.removeAttr('href');
                    }

                    // check to see if menu is already open
                    if ($(this).siblings('ul.sw-channel-dropdown').is(':visible')) {
                        // if open go to link
                        window.location.href = button.prop('linkHref');
                    } 

                } 
            }
        });
    }


    
    function channelHoverIE(){
		// set z-index for IE7
		var parentZindex = $('#channel-navigation').parents('div:first').css('z-index');
		var zindex = (parentZindex > 0 ? parentZindex : 8000);
		$(".sw-channel-item").each(function(ind) {
			$(this).css('z-index', zindex - ind);
			zindex --;
		});
	    $(".sw-channel-item").hover(function(){
	        var subList = $(this).children('ul');
	        if ($.trim(subList.html()) !== "") {
	            subList.show();
	            subList.attr("aria-hidden", "false").attr("aria-expanded", "true");
		    }
		    $(this).addClass("hover");
	    }, function() {
	        $(".sw-channel-dropdown").hide();
	        $(this).removeClass("hover");
	        var subList = $(this).children('ul');
	        if ($.trim(subList.html()) !== "") {
	            subList.attr("aria-hidden", "true").attr("aria-expanded", "false");
	        }
	    });
    }

    function closeMenuByPressingKey() {
        $(".sw-channel-item").each(function(ind) {
            $(this).keyup(function (event) {
                if (event.keyCode == 27) { // ESC
                    $(".sw-channel-dropdown").hide();
                    $(this).removeClass("hover");
                    var subList = $(this).children('ul');
                    if ($.trim(subList.html()) !== "") {
                        subList.attr("aria-hidden", "true").attr("aria-expanded", "false");
                    }
                }
                if (event.keyCode == 13 || event.keyCode == 32) { //enter or space
                    $(this).find('a').get(0).click();
                }
            }); 
        });

        $(".sw-channel-item a").each(function (ind) {
            $(this).parents('.sw-channel-item').keydown(function (e) {
                if (e.keyCode == 9) { // TAB
                    $(".sw-channel-dropdown").hide();
                    $(this).removeClass("hover");
                    var subList = $(this).children('ul');
                    if ($.trim(subList.html()) !== "") {
                        subList.attr("aria-hidden", "true").attr("aria-expanded", "false");
                    }
                }
            });
        });

        $(".sw-channel-dropdown li").each(function(ind) {
            $(this).keydown(function (event) {
                if (event.keyCode == 9) { // TAB
                    $(".sw-channel-dropdown").hide();
                    var parentMenuItem = $(this).parent().closest('li');
                    parentMenuItem.removeClass("hover");
                    var subList = parentMenuItem.children('ul');
                    if ($.trim(subList.html()) !== "") {
                        subList.attr("aria-hidden", "true").attr("aria-expanded", "false");
                    }
                    parentMenuItem.next().find('a:first').focus();
                    event.preventDefault();
                    event.stopPropagation();
                }

                if (event.keyCode == 37 || // left arrow
                    event.keyCode == 39) { // right arrow
                    $(".sw-channel-dropdown").hide();
                    var parentMenuItem = $(this).parent().closest('li');
                    parentMenuItem.removeClass("hover");
                    var subList = parentMenuItem.children('ul');
                    if ($.trim(subList.html()) !== "") {
                        subList.attr("aria-hidden", "true").attr("aria-expanded", "false");
                    }
                    if (event.keyCode == 37) {
                        parentMenuItem.prev().find('a:first').focus();
                    } else {
                        parentMenuItem.next().find('a:first').focus();
                    }
                    event.preventDefault();
                    event.stopPropagation();
                }
            });
        });
    }

</script>


				</nav>
			</div>
			<div class="header-column two">
				<div class="cs-mystart-dropdown quick-links" data-toggle='true'>
					<div class="cs-dropdown-selector" tabindex="0" aria-label='Quick Links' role="button" aria-expanded="false" aria-haspopup="true">Quick Links</div>
					<div class="cs-dropdown" aria-hidden="true" style="display:none;">
						<ul class="cs-dropdown-list"></ul>
					</div>
				</div>
				<div class="cs-mystart-button search">
					<div class="cs-button-selector" tabindex="0" aria-label='Search' role="button" aria-expanded="false" aria-haspopup="true"></div>
					<div id="gb-search" aria-hidden="true">
						<form id="gb-search-form">
		                    <label for="gb-search-input">Search</label>
		                    <input type="text" id="gb-search-input" tabindex="-1" value="I'm looking for...">
		                </form>
	                </div>
				</div>
				<div class="cs-mystart-button staffnet" data-content='#'>
					<a class="cs-button-selector" target='_self' aria-label="Staff Net" href='#'><span>StaffNet</span></a>
				</div>
			</div>
		</section>
		<section class="header-row three">
			<div id="logo-sitename">
				<div class="logo"></div>
				<div class="sitename" data-toggle='false'>
					<h1>
						<span></span>
						<span></span>
					</h1>
				</div>
			</div>
			<div class="mobile-menu">

			</div>
		</section>
	</header>
	<main id="hp-content">
		<section class="hp-row one" id="hp-slideshow-outer" data-use-mobile-background-photo='true
' data-slideshow-gradient='true'>
			<div id="hp-slideshow">
				<div id="sw-content-container10" class="region ui-hp"><div id='pmi-2873'>



<div id='sw-module-2990'>
    <script type="text/javascript">
        $(document).ready(function() {
            var DomainID = '4';
            var PageID = '1';
            var RenderLoc = '0';
            var MIID = '299';

            //added to check to make sure moderated content doesn't bleed through the dialog
            if ($('#dialog-overlay-WindowLargeModal-body.moderateContent').length > 0) {
                $("#module-content-" + MIID).find(".ui-widget-detail").find(".ui-article").append("<p>&nbsp;</p>");
            }
        });
    </script>

    <script type="text/javascript">$(document).ready(function() {CheckScript('ModuleView');CheckScript('Mustache');
 });</script>
    
    <div id="module-content-299" >
<div class="ui-widget app multimedia-gallery" data-pmi="2873" data-mi="299">
	<div class="ui-widget-header ui-helper-hidden">
		
	</div>
    <div class="ui-widget-detail">
    	<div id="mmg-container-2873" class="mmg-container" data-gallery-type="default" data-transition="fade" data-record-num="0" data-is-animating="false" data-play-state="playing" data-is-hovering="false" data-has-focus="false" data-is-touching="false" data-active-record-index="0" data-active-gallery-index="0">
            <div class="mmg-viewer">
                <div class="mmg-slides-outer">
                    <ul class="mmg-slides"></ul>
                </div>
                <div class="mmg-live-feedback mmg-ally-hidden" aria-live="polite"></div>
            </div>
        </div>
        
        <script type="text/javascript">
            var multimediaGallery2873 = {
            	"props": {
                	"defaultGallery": true,
                    "imageWidth" : 960,
                	"imageHeight" : 500,
                	"pageModuleInstanceId": "2873",
                    "moduleInstanceId": "299",
                    "virtualFolderPath": "https://adamcruse.schoolwires.net/cms/lib/SWCS000009/Centricity/ModuleInstance/299/"
                },
				"records": [
{
                    "flexDataId": "1152",
                    "hideTitle": "False",
                    "title": "Robotics Club",
                    "hideCaption": "False",
                    "caption": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida.",
                    "imageSrc": "https://adamcruse.schoolwires.net/cms/lib/SWCS000009/Centricity/Domain/4/cgsc-mmg-1.jpg",
                    "imageWidth": "910",
                    "imageHeight": "550",
                    "imageAltText": "test",
                    "isLinked": "True",
                    "linkUrl": "http://www.google.com",
                    "linkText": "Discover More",
                    "openLinkInNewWindow": "False",
                    "videoLinkText": "Watch Our Video.",
                    "videoIsEmbedded": "False",
                    "videoIframe": "<iframe width=&quot;560&quot; height=&quot;315&quot; src=&quot;https://www.youtube.com/embed/oRdxUFDoQe0&quot; frameborder=&quot;0&quot; allow=&quot;autoplay; encrypted-media&quot; allowfullscreen></iframe>"
                },
{
                    "flexDataId": "1155",
                    "hideTitle": "True",
                    "title": "Hidden",
                    "hideCaption": "False",
                    "caption": "",
                    "imageSrc": "https://adamcruse.schoolwires.net/cms/lib/SWCS000009/Centricity/Domain/4/cgsc-mmg-2.jpg",
                    "imageWidth": "910",
                    "imageHeight": "550",
                    "imageAltText": "test",
                    "isLinked": "False",
                    "linkUrl": "",
                    "linkText": "",
                    "openLinkInNewWindow": "False",
                    "videoLinkText": "",
                    "videoIsEmbedded": "False",
                    "videoIframe": ""
                },
{
                    "flexDataId": "1154",
                    "hideTitle": "False",
                    "title": "This shows a longer title with the image.",
                    "hideCaption": "False",
                    "caption": "Here is a caption to see how it works with a long title. Check out our new website... Pretty neat huh? This shows an extra long title with the image. Pretium dolor augue.",
                    "imageSrc": "https://adamcruse.schoolwires.net/cms/lib/SWCS000009/Centricity/Domain/4/cgsc-mmg-3.jpg",
                    "imageWidth": "910",
                    "imageHeight": "550",
                    "imageAltText": "test",
                    "isLinked": "True",
                    "linkUrl": "/#",
                    "linkText": "Link goes here",
                    "openLinkInNewWindow": "False",
                    "videoLinkText": "Link Goes here",
                    "videoIsEmbedded": "True",
                    "videoIframe": ""
                },
{
                    "flexDataId": "1153",
                    "hideTitle": "False",
                    "title": "No Title",
                    "hideCaption": "True",
                    "caption": "I am a caption that does not have a title",
                    "imageSrc": "https://adamcruse.schoolwires.net/cms/lib/SWCS000009/Centricity/Domain/4/cgsc-mmg-1.jpg",
                    "imageWidth": "910",
                    "imageHeight": "550",
                    "imageAltText": "test",
                    "isLinked": "False",
                    "linkUrl": "http://www.google.com",
                    "linkText": "Link Text Here",
                    "openLinkInNewWindow": "False",
                    "videoLinkText": "Play Video",
                    "videoIsEmbedded": "False",
                    "videoIframe": "<iframe width=&quot;560&quot; height=&quot;315&quot; src=&quot;https://www.youtube.com/embed/xCwwxNbtK6Y&quot; frameborder=&quot;0&quot; allow=&quot;autoplay; encrypted-media&quot; allowfullscreen></iframe>"
                },
{}]
			};
		</script>
	</div>
	<div class="ui-widget-footer ui-clear"></div>
</div>

<script type="text/javascript" src="//extend.schoolwires.com/creative/scripts/creative/rotate/multimedia-gallery/cs.multimedia.gallery.min.js"></script>
<script type="text/javascript">
	$(function() {
    	if(multimediaGallery2873.props.defaultGallery) {
        	$("#pmi-2873 .ui-widget.app.multimedia-gallery").csMultimediaGallery({
                "pageModuleInstanceId": 2873,
                "moduleInstanceId": 299,
                "imageWidth" : multimediaGallery2873.props.imageWidth,
                "imageHeight" : multimediaGallery2873.props.imageHeight,
                "playPauseControl" : true,
                "bullets" : true
            });
        }
    });
</script>

<style type="text/css">
#pmi-2873:before {
    content: "960";
    display: none;
}
@media (max-width: 1023px) {
    #pmi-2873:before {
        content: "768";
    }
}
@media (max-width: 767px) {
    #pmi-2873:before {
        content: "640";
    }
}
@media (max-width: 639px) {
    #pmi-2873:before {
        content: "480";
    }
}
@media (max-width: 479px) {
    #pmi-2873:before {
        content: "320";
    }
}
</style>
<script type="text/javascript">
$(document).ready(function (){
$(".tag-list li a").keypress(function(e) { if(e.which == 13) { $(this).click();   }});
});
function LoadGroupedData(container, MIID, PMI, groupYear, groupMonth, groupBy, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://adamcruse.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&EnableQuirksMode=0&ViewID=' + viewToUse + '&Tag=' + tag, container, 2, 'chkSidebar();');
}
function LoadData(container, MIID, PMI, flexDataID, groupYear, groupMonth, groupBy, targetView, tag) {
  if(targetView !== undefined || targetView.Length() == 0){
  //targetView looks at the hidden Detail View defined in the Builder View.
      targetView = $('#hid-'+MIID+'-DetailView').val();
   }
  GetContent('https://adamcruse.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&FlexDataID=' + flexDataID + '&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&RenderLoc=0&FromRenderLoc=0&EnableQuirksMode=0&Tag=' + tag + '&ViewID=' + targetView, container, 2, 'chkSidebar();');
}
function LoadTaggedData(container, MIID, PMI, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://adamcruse.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&Tag=' + tag + '&EnableQuirksMode=0&ViewID='+viewToUse, container, 2, 'chkSidebar();');
  setTimeout(function(){ $("#module-content-"+ MIID +"").find('[tabindex]:first').focus(); }, 200);
}
</script>
</div>

</div></div>
</div>
			</div>
		</section>
		<section class="hp-row two" id="gb-icons" data-icon-num='6'>
			<div class="content-wrap"></div>
		</section>
		<section class="hp-row three slider">
			<div class="content-wrap">
            	<a id="sw-maincontent" name="sw-maincontent" tabindex="-1"></a>
				<div id="sw-content-container1" class="region ui-hp"><div id='pmi-2791'>



<div id='sw-module-13750'>
    <script type="text/javascript">
        $(document).ready(function() {
            var DomainID = '4';
            var PageID = '1';
            var RenderLoc = '0';
            var MIID = '1375';

            //added to check to make sure moderated content doesn't bleed through the dialog
            if ($('#dialog-overlay-WindowLargeModal-body.moderateContent').length > 0) {
                $("#module-content-" + MIID).find(".ui-widget-detail").find(".ui-article").append("<p>&nbsp;</p>");
            }
        });
    </script>

    <script type="text/javascript">$(document).ready(function() {CheckScript('ModuleView');CheckScript('Mustache');
 });</script>
    
    <div id="module-content-1375" >
<div class="ui-widget app headlines">
	
	<div class="ui-widget-header">
		<h1>Latest News</h1>
	</div>
	
	<div class="ui-widget-detail" id="sw-app-headlines-1375">
		<ul class="ui-articles">
<li>  
    <div  class="ui-article">   
        <span class="ui-article-thumb" aria-hidden="true">
            <span class="img">
                <img alt=" space" height="270" width="380" src="https://adamcruse.schoolwires.net/..//cms/lib/SWCS000009/Centricity/Domain/4/Headlines-1.png" />
            </span>
        </span>   
        
        <h1 class="ui-article-title">
        	<a href="https://adamcruse.schoolwires.net/../site/default.aspx?PageType=3&DomainID=4&ModuleInstanceID=1375&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=2226&PageID=1"><span>Mission</span></a>
            </h1> 
        <p class="ui-article-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p>   
        <div class="ui-article-controls">                                    
            <a class="sub-link" title="Go to comments for Mission" href="https://adamcruse.schoolwires.net/../site/default.aspx?PageType=3&DomainID=4&ModuleInstanceID=1375&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=2226&PageID=1&Comments=true"><span>Comments (-1)</span></a>
               
        </div>   
        <div class="clear"></div>  
    </div>
</li>
<li>  
    <div  class="ui-article">   
        <span class="ui-article-thumb" aria-hidden="true">
            <span class="img">
                <img alt=" also space" height="270" width="380" src="https://adamcruse.schoolwires.net/..//cms/lib/SWCS000009/Centricity/Domain/4/Headlines-2.png" />
            </span>
        </span>   
        
        <h1 class="ui-article-title">
        	<a href="https://adamcruse.schoolwires.net/../site/default.aspx?PageType=3&DomainID=4&ModuleInstanceID=1375&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=2225&PageID=1"><span>Vision</span></a>
            </h1> 
        <p class="ui-article-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.   Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.   Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p>   
        <div class="ui-article-controls">                                    
            <a class="sub-link" title="Go to comments for Vision" href="https://adamcruse.schoolwires.net/../site/default.aspx?PageType=3&DomainID=4&ModuleInstanceID=1375&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=2225&PageID=1&Comments=true"><span>Comments (-1)</span></a>
               
        </div>   
        <div class="clear"></div>  
    </div>
</li>
<li>  
    <div  class="ui-article">   
        <span class="ui-article-thumb" aria-hidden="true">
            <span class="img">
                <img alt=" more space" height="270" width="380" src="https://adamcruse.schoolwires.net/..//cms/lib/SWCS000009/Centricity/Domain/4/Headlines-3.png" />
            </span>
        </span>   
        
        <h1 class="ui-article-title">
        	<a href="https://adamcruse.schoolwires.net/../site/default.aspx?PageType=3&DomainID=4&ModuleInstanceID=1375&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=2224&PageID=1"><span>Values</span></a>
            </h1> 
        <p class="ui-article-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p>   
        <div class="ui-article-controls">                                    
            <a class="sub-link" title="Go to comments for Values" href="https://adamcruse.schoolwires.net/../site/default.aspx?PageType=3&DomainID=4&ModuleInstanceID=1375&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=2224&PageID=1&Comments=true"><span>Comments (-1)</span></a>
               
        </div>   
        <div class="clear"></div>  
    </div>
</li>
<li>  
    <div  class="ui-article">   
        <span class="ui-article-thumb" aria-hidden="true">
            <span class="img">
                <img alt="" height="276" width="388"  />
            </span>
        </span>   
        
        <h1 class="ui-article-title">
        	<a href="https://adamcruse.schoolwires.net/../site/default.aspx?PageType=3&DomainID=4&ModuleInstanceID=1375&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=2228&PageID=1"><span>Portrait of a Student</span></a>
            </h1> 
        <p class="ui-article-description">I am a headline with no image.</p>   
        <div class="ui-article-controls">                                    
            <a class="sub-link" title="Go to comments for Portrait of a Student" href="https://adamcruse.schoolwires.net/../site/default.aspx?PageType=3&DomainID=4&ModuleInstanceID=1375&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=2228&PageID=1&Comments=true"><span>Comments (-1)</span></a>
               
        </div>   
        <div class="clear"></div>  
    </div>
</li>
<li>  
    <div  class="ui-article">   
        <span class="ui-article-thumb" aria-hidden="true">
            <span class="img">
                <img alt="" height="276" width="388"  />
            </span>
        </span>   
        
        <h1 class="ui-article-title">
        	<a href="https://adamcruse.schoolwires.net/../site/default.aspx?PageType=3&DomainID=4&ModuleInstanceID=1375&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=2227&PageID=1"><span>Transparency</span></a>
            </h1> 
        <p class="ui-article-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p>   
        <div class="ui-article-controls">                                    
            <a class="sub-link" title="Go to comments for Transparency" href="https://adamcruse.schoolwires.net/../site/default.aspx?PageType=3&DomainID=4&ModuleInstanceID=1375&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=2227&PageID=1&Comments=true"><span>Comments (-1)</span></a>
               
        </div>   
        <div class="clear"></div>  
    </div>
</li>
</ul><div class="ui-read-more"><a id='MoreLinkButton1375' class='more-link' aria-label='Go to more records' onclick='MoreViewClick(this);' href='https://adamcruse.schoolwires.net/site/default.aspx?PageType=14&DomainID=4&PageID=1&ModuleInstanceID=1375&ViewID=c83d46ac-74fe-4857-8c9a-5922a80225e2&IsMoreExpandedView=True'><span>more</span></a><div class='more-link-under'>&nbsp;</div></div>
</ul>
	</div>

	<div class="ui-widget-footer">
		
		<div class="app-level-social-rss"><a title='Subscribe to RSS Feed - Latest News' tabindex='0' class='ui-btn-toolbar rss' href="https://adamcruse.schoolwires.net/site/Default.aspx?PageType=4&DomainID=4&ModuleInstanceID=1375&PageID=1&PMIID=2791"><span>Subscribe to RSS Feed - Latest News </span></a></div>
		<div class="clear"></div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
        /*$(document).on('click', 'a.ui-article-thumb', function() {
        	window.location = $(this).attr('href');
    	});*/
    		
		$('#sw-app-headlines-1375').find('img').each(function() {
			if ($.trim(this.src) == '' ) {
				$(this).parent().parent().remove();
			}
		});
        
        // Jason Smith - 12/9/2014 - Removed due to bandwidth implications
		
	});

</script>
<script type="text/javascript">
$(document).ready(function (){
$(".tag-list li a").keypress(function(e) { if(e.which == 13) { $(this).click();   }});
});
function LoadGroupedData(container, MIID, PMI, groupYear, groupMonth, groupBy, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://adamcruse.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&EnableQuirksMode=0&ViewID=' + viewToUse + '&Tag=' + tag, container, 2, 'chkSidebar();');
}
function LoadData(container, MIID, PMI, flexDataID, groupYear, groupMonth, groupBy, targetView, tag) {
  if(targetView !== undefined || targetView.Length() == 0){
  //targetView looks at the hidden Detail View defined in the Builder View.
      targetView = $('#hid-'+MIID+'-DetailView').val();
   }
  GetContent('https://adamcruse.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&FlexDataID=' + flexDataID + '&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&RenderLoc=0&FromRenderLoc=0&EnableQuirksMode=0&Tag=' + tag + '&ViewID=' + targetView, container, 2, 'chkSidebar();');
}
function LoadTaggedData(container, MIID, PMI, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://adamcruse.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&Tag=' + tag + '&EnableQuirksMode=0&ViewID='+viewToUse, container, 2, 'chkSidebar();');
  setTimeout(function(){ $("#module-content-"+ MIID +"").find('[tabindex]:first').focus(); }, 200);
}
</script>
</div>

</div></div>
</div>
			</div>
		</section>
		<section class="hp-row four slider">
			<div class="content-wrap">
				<div id="sw-content-container2" class="region ui-hp"><div id='pmi-2916'>



<div id='sw-module-230'>
    <script type="text/javascript">
        $(document).ready(function() {
            var DomainID = '4';
            var PageID = '1';
            var RenderLoc = '0';
            var MIID = '23';

            //added to check to make sure moderated content doesn't bleed through the dialog
            if ($('#dialog-overlay-WindowLargeModal-body.moderateContent').length > 0) {
                $("#module-content-" + MIID).find(".ui-widget-detail").find(".ui-article").append("<p>&nbsp;</p>");
            }
        });
    </script>

    <script type="text/javascript">$(document).ready(function() {CheckScript('ModuleView');CheckScript('Mustache');
 });</script>
    
    <div id="module-content-23" >
<div class="ui-widget app flexpage">
	<div class="ui-widget-header">
		<h1>Twitter</h1>
	</div>
	
	<div class="ui-widget-detail">
<div class="ui-article"><span ><a class="twitter-timeline" data-height="460" href="https://twitter.com/MereMortalsWW">Tweets by MereMortalsWW</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script></span></div> 
<div class="clear"></div>
</div>
	<div class="ui-widget-footer">
		
			
		
		<div class="clear"></div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function (){
$(".tag-list li a").keypress(function(e) { if(e.which == 13) { $(this).click();   }});
});
function LoadGroupedData(container, MIID, PMI, groupYear, groupMonth, groupBy, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://adamcruse.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&EnableQuirksMode=0&ViewID=' + viewToUse + '&Tag=' + tag, container, 2, 'chkSidebar();');
}
function LoadData(container, MIID, PMI, flexDataID, groupYear, groupMonth, groupBy, targetView, tag) {
  if(targetView !== undefined || targetView.Length() == 0){
  //targetView looks at the hidden Detail View defined in the Builder View.
      targetView = $('#hid-'+MIID+'-DetailView').val();
   }
  GetContent('https://adamcruse.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&FlexDataID=' + flexDataID + '&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&RenderLoc=0&FromRenderLoc=0&EnableQuirksMode=0&Tag=' + tag + '&ViewID=' + targetView, container, 2, 'chkSidebar();');
}
function LoadTaggedData(container, MIID, PMI, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://adamcruse.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&Tag=' + tag + '&EnableQuirksMode=0&ViewID='+viewToUse, container, 2, 'chkSidebar();');
  setTimeout(function(){ $("#module-content-"+ MIID +"").find('[tabindex]:first').focus(); }, 200);
}
</script>
</div>

</div></div>
<div id='pmi-2917'>



<div id='sw-module-240'>
    <script type="text/javascript">
        $(document).ready(function() {
            var DomainID = '4';
            var PageID = '1';
            var RenderLoc = '0';
            var MIID = '24';

            //added to check to make sure moderated content doesn't bleed through the dialog
            if ($('#dialog-overlay-WindowLargeModal-body.moderateContent').length > 0) {
                $("#module-content-" + MIID).find(".ui-widget-detail").find(".ui-article").append("<p>&nbsp;</p>");
            }
        });
    </script>

    <script type="text/javascript">$(document).ready(function() {CheckScript('ModuleView');CheckScript('Mustache');
 });</script>
    
    <div id="module-content-24" >
<div class="ui-widget app flexpage">
	<div class="ui-widget-header">
		<h1>Facebook</h1>
	</div>
	
	<div class="ui-widget-detail">
<div class="ui-article"><span ><center><div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10&appId=276431879052753";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="fb-page" data-href="https://www.facebook.com/adamcruseweddingdj/" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/adamcruseweddingdj/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/adamcruseweddingdj/">Adam Cruse Wedding DJ</a></blockquote></div>
</center></span></div> 
<div class="clear"></div>
</div>
	<div class="ui-widget-footer">
		
			
		
		<div class="clear"></div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function (){
$(".tag-list li a").keypress(function(e) { if(e.which == 13) { $(this).click();   }});
});
function LoadGroupedData(container, MIID, PMI, groupYear, groupMonth, groupBy, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://adamcruse.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&EnableQuirksMode=0&ViewID=' + viewToUse + '&Tag=' + tag, container, 2, 'chkSidebar();');
}
function LoadData(container, MIID, PMI, flexDataID, groupYear, groupMonth, groupBy, targetView, tag) {
  if(targetView !== undefined || targetView.Length() == 0){
  //targetView looks at the hidden Detail View defined in the Builder View.
      targetView = $('#hid-'+MIID+'-DetailView').val();
   }
  GetContent('https://adamcruse.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&FlexDataID=' + flexDataID + '&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&RenderLoc=0&FromRenderLoc=0&EnableQuirksMode=0&Tag=' + tag + '&ViewID=' + targetView, container, 2, 'chkSidebar();');
}
function LoadTaggedData(container, MIID, PMI, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://adamcruse.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&Tag=' + tag + '&EnableQuirksMode=0&ViewID='+viewToUse, container, 2, 'chkSidebar();');
  setTimeout(function(){ $("#module-content-"+ MIID +"").find('[tabindex]:first').focus(); }, 200);
}
</script>
</div>

</div></div>
<div id='pmi-2922'>



<div id='sw-module-3220'>
    <script type="text/javascript">
        $(document).ready(function() {
            var DomainID = '4';
            var PageID = '1';
            var RenderLoc = '0';
            var MIID = '322';

            //added to check to make sure moderated content doesn't bleed through the dialog
            if ($('#dialog-overlay-WindowLargeModal-body.moderateContent').length > 0) {
                $("#module-content-" + MIID).find(".ui-widget-detail").find(".ui-article").append("<p>&nbsp;</p>");
            }
        });
    </script>

    <script type="text/javascript">$(document).ready(function() {CheckScript('ModuleView');CheckScript('Mustache');
 });</script>
    
    <div id="module-content-322" >
<div class="ui-widget app flexpage">
	<div class="ui-widget-header">
		<h1>YouTube</h1>
	</div>
	
	<div class="ui-widget-detail">
<div class="ui-article"><span ><iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PLCsuqbR8ZoiAlyZL_ZfhzNb3JOn2Ks9NV" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></span></div> 
<div class="clear"></div>
</div>
	<div class="ui-widget-footer">
		
			
		
		<div class="clear"></div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function (){
$(".tag-list li a").keypress(function(e) { if(e.which == 13) { $(this).click();   }});
});
function LoadGroupedData(container, MIID, PMI, groupYear, groupMonth, groupBy, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://adamcruse.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&EnableQuirksMode=0&ViewID=' + viewToUse + '&Tag=' + tag, container, 2, 'chkSidebar();');
}
function LoadData(container, MIID, PMI, flexDataID, groupYear, groupMonth, groupBy, targetView, tag) {
  if(targetView !== undefined || targetView.Length() == 0){
  //targetView looks at the hidden Detail View defined in the Builder View.
      targetView = $('#hid-'+MIID+'-DetailView').val();
   }
  GetContent('https://adamcruse.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&FlexDataID=' + flexDataID + '&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&RenderLoc=0&FromRenderLoc=0&EnableQuirksMode=0&Tag=' + tag + '&ViewID=' + targetView, container, 2, 'chkSidebar();');
}
function LoadTaggedData(container, MIID, PMI, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://adamcruse.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&Tag=' + tag + '&EnableQuirksMode=0&ViewID='+viewToUse, container, 2, 'chkSidebar();');
  setTimeout(function(){ $("#module-content-"+ MIID +"").find('[tabindex]:first').focus(); }, 200);
}
</script>
</div>

</div></div>
<div id='pmi-2921'>



<div id='sw-module-3210'>
    <script type="text/javascript">
        $(document).ready(function() {
            var DomainID = '4';
            var PageID = '1';
            var RenderLoc = '0';
            var MIID = '321';

            //added to check to make sure moderated content doesn't bleed through the dialog
            if ($('#dialog-overlay-WindowLargeModal-body.moderateContent').length > 0) {
                $("#module-content-" + MIID).find(".ui-widget-detail").find(".ui-article").append("<p>&nbsp;</p>");
            }
        });
    </script>

    <script type="text/javascript">$(document).ready(function() {CheckScript('ModuleView');CheckScript('Mustache');
 });</script>
    
    <div id="module-content-321" >
<div class="ui-widget app flexpage">
	<div class="ui-widget-header">
		<h1>Instagram</h1>
	</div>
	
	<div class="ui-widget-detail">
<div class="ui-article"><span ><blockquote class="instagram-media" data-instgrm-captioned data-instgrm-version="7" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:41.66666666666667% 0; text-align:center; width:100%;"> <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div> <p style=" margin:8px 0 0 0; padding:0 4px;"> <a href="https://www.instagram.com/p/BYwPhCpFrro/" style=" color:#000; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none; word-wrap:break-word;" target="_blank">#BethelDay1</a></p> <p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;">A post shared by @bethelschools on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2017-09-07T20:48:50+00:00">Sep 7, 2017 at 1:48pm PDT</time></p></div></blockquote>
<script async defer src="//platform.instagram.com/en_US/embeds.js"></script></span></div> 
<div class="clear"></div>
</div>
	<div class="ui-widget-footer">
		
			
		
		<div class="clear"></div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function (){
$(".tag-list li a").keypress(function(e) { if(e.which == 13) { $(this).click();   }});
});
function LoadGroupedData(container, MIID, PMI, groupYear, groupMonth, groupBy, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://adamcruse.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&EnableQuirksMode=0&ViewID=' + viewToUse + '&Tag=' + tag, container, 2, 'chkSidebar();');
}
function LoadData(container, MIID, PMI, flexDataID, groupYear, groupMonth, groupBy, targetView, tag) {
  if(targetView !== undefined || targetView.Length() == 0){
  //targetView looks at the hidden Detail View defined in the Builder View.
      targetView = $('#hid-'+MIID+'-DetailView').val();
   }
  GetContent('https://adamcruse.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&FlexDataID=' + flexDataID + '&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&RenderLoc=0&FromRenderLoc=0&EnableQuirksMode=0&Tag=' + tag + '&ViewID=' + targetView, container, 2, 'chkSidebar();');
}
function LoadTaggedData(container, MIID, PMI, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://adamcruse.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&Tag=' + tag + '&EnableQuirksMode=0&ViewID='+viewToUse, container, 2, 'chkSidebar();');
  setTimeout(function(){ $("#module-content-"+ MIID +"").find('[tabindex]:first').focus(); }, 200);
}
</script>
</div>

</div></div>
</div>
			</div>
		</section>
		<section class="hp-row five">
			<div class="content-wrap">
				<div class="hp-column-wrap">
					<div class="hp-column one">
						<div id="sw-content-container3" class="region ui-hp"><div id='pmi-2914'>



<div id='sw-module-1650'>
    <script type="text/javascript">
        $(document).ready(function() {
            var DomainID = '4';
            var PageID = '1';
            var RenderLoc = '0';
            var MIID = '165';

            //added to check to make sure moderated content doesn't bleed through the dialog
            if ($('#dialog-overlay-WindowLargeModal-body.moderateContent').length > 0) {
                $("#module-content-" + MIID).find(".ui-widget-detail").find(".ui-article").append("<p>&nbsp;</p>");
            }
        });
    </script>

    <script type="text/javascript">$(document).ready(function() {CheckScript('ModuleView');CheckScript('Mustache');
 });</script>
    
    <div id="module-content-165" >
<div class="ui-widget app headlines">
	
	<div class="ui-widget-header ui-helper-hidden">
		
	</div>
	
	<div class="ui-widget-detail" id="sw-app-headlines-165">
		<ul class="ui-articles">
<li>  
    <div  class="ui-article">   
        <span class="ui-article-thumb" aria-hidden="true">
            <span class="img">
                <img alt=" space again" height="270" width="380" src="https://adamcruse.schoolwires.net/..//cms/lib/SWCS000009/Centricity/Domain/4/Headlines-4.png" />
            </span>
        </span>   
        
        <h1 class="ui-article-title">
        	<a href="https://adamcruse.schoolwires.net/../site/default.aspx?PageType=3&DomainID=4&ModuleInstanceID=165&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=1161&PageID=1"><span>Headline Title Text Example</span></a>
            </h1> 
        <p class="ui-article-description">Academics</p>   
        <div class="ui-article-controls">                                    
            <a class="sub-link" title="Go to comments for Headline Title Text Example" href="https://adamcruse.schoolwires.net/../site/default.aspx?PageType=3&DomainID=4&ModuleInstanceID=165&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=1161&PageID=1&Comments=true"><span>Comments (-1)</span></a>
               
        </div>   
        <div class="clear"></div>  
    </div>
</li>
<li>  
    <div  class="ui-article">   
        <span class="ui-article-thumb" aria-hidden="true">
            <span class="img">
                <img alt=" space again!" height="270" width="380" src="https://adamcruse.schoolwires.net/..//cms/lib/SWCS000009/Centricity/Domain/4/Headlines-5.png" />
            </span>
        </span>   
        
        <h1 class="ui-article-title">
        	<a href="https://adamcruse.schoolwires.net/../site/default.aspx?PageType=3&DomainID=4&ModuleInstanceID=165&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=1138&PageID=1"><span>Feature Title</span></a>
            </h1> 
        <p class="ui-article-description">Newsroom</p>   
        <div class="ui-article-controls">                                    
            <a class="sub-link" title="Go to comments for Feature Title" href="https://adamcruse.schoolwires.net/../site/default.aspx?PageType=3&DomainID=4&ModuleInstanceID=165&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=1138&PageID=1&Comments=true"><span>Comments (-1)</span></a>
               
        </div>   
        <div class="clear"></div>  
    </div>
</li>
<li>  
    <div  class="ui-article">   
        <span class="ui-article-thumb" aria-hidden="true">
            <span class="img">
                <img alt=" banananana-batman" height="270" width="380" src="https://adamcruse.schoolwires.net/..//cms/lib/SWCS000009/Centricity/Domain/4/Headlines-8.png" />
            </span>
        </span>   
        
        <h1 class="ui-article-title">
        	<a href="https://adamcruse.schoolwires.net/../site/default.aspx?PageType=3&DomainID=4&ModuleInstanceID=165&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=1137&PageID=1"><span>Alumni Honored</span></a>
            </h1> 
        <p class="ui-article-description">Research</p>   
        <div class="ui-article-controls">                                    
            <a class="sub-link" title="Go to comments for Alumni Honored" href="https://adamcruse.schoolwires.net/../site/default.aspx?PageType=3&DomainID=4&ModuleInstanceID=165&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=1137&PageID=1&Comments=true"><span>Comments (-1)</span></a>
               
        </div>   
        <div class="clear"></div>  
    </div>
</li>
<li>  
    <div  class="ui-article">   
        <span class="ui-article-thumb" aria-hidden="true">
            <span class="img">
                <img alt=" hawk" height="270" width="380" src="https://adamcruse.schoolwires.net/..//cms/lib/SWCS000009/Centricity/Domain/4/Headlines-6.png" />
            </span>
        </span>   
        
        <h1 class="ui-article-title">
        	<a href="https://adamcruse.schoolwires.net/../site/default.aspx?PageType=3&DomainID=4&ModuleInstanceID=165&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=1139&PageID=1"><span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut tempus elit id ex fermentum, id rhoncus magna suscipit. Aliquam pharetra metus risus</span></a>
            </h1> 
        <p class="ui-article-description">Legislative</p>   
        <div class="ui-article-controls">                                    
            <a class="sub-link" title="Go to comments for Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut tempus elit id ex fermentum, id rhoncus magna suscipit. Aliquam pharetra metus risus" href="https://adamcruse.schoolwires.net/../site/default.aspx?PageType=3&DomainID=4&ModuleInstanceID=165&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=1139&PageID=1&Comments=true"><span>Comments (-1)</span></a>
               
        </div>   
        <div class="clear"></div>  
    </div>
</li>
</ul><div class="ui-read-more"><a id='MoreLinkButton165' class='more-link' aria-label='Go to more records' onclick='MoreViewClick(this);' href='https://adamcruse.schoolwires.net/site/default.aspx?PageType=14&DomainID=4&PageID=1&ModuleInstanceID=165&ViewID=c83d46ac-74fe-4857-8c9a-5922a80225e2&IsMoreExpandedView=True'><span>more</span></a><div class='more-link-under'>&nbsp;</div></div>
</ul>
	</div>

	<div class="ui-widget-footer">
		
		<div class="app-level-social-rss"><a title='Subscribe to RSS Feed - Latest News' tabindex='0' class='ui-btn-toolbar rss' href="https://adamcruse.schoolwires.net/site/Default.aspx?PageType=4&DomainID=4&ModuleInstanceID=165&PageID=1&PMIID=2914"><span>Subscribe to RSS Feed - Latest News </span></a></div>
		<div class="clear"></div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
        /*$(document).on('click', 'a.ui-article-thumb', function() {
        	window.location = $(this).attr('href');
    	});*/
    		
		$('#sw-app-headlines-165').find('img').each(function() {
			if ($.trim(this.src) == '' ) {
				$(this).parent().parent().remove();
			}
		});
        
        // Jason Smith - 12/9/2014 - Removed due to bandwidth implications
		
	});

</script>
<script type="text/javascript">
$(document).ready(function (){
$(".tag-list li a").keypress(function(e) { if(e.which == 13) { $(this).click();   }});
});
function LoadGroupedData(container, MIID, PMI, groupYear, groupMonth, groupBy, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://adamcruse.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&EnableQuirksMode=0&ViewID=' + viewToUse + '&Tag=' + tag, container, 2, 'chkSidebar();');
}
function LoadData(container, MIID, PMI, flexDataID, groupYear, groupMonth, groupBy, targetView, tag) {
  if(targetView !== undefined || targetView.Length() == 0){
  //targetView looks at the hidden Detail View defined in the Builder View.
      targetView = $('#hid-'+MIID+'-DetailView').val();
   }
  GetContent('https://adamcruse.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&FlexDataID=' + flexDataID + '&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&RenderLoc=0&FromRenderLoc=0&EnableQuirksMode=0&Tag=' + tag + '&ViewID=' + targetView, container, 2, 'chkSidebar();');
}
function LoadTaggedData(container, MIID, PMI, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://adamcruse.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&Tag=' + tag + '&EnableQuirksMode=0&ViewID='+viewToUse, container, 2, 'chkSidebar();');
  setTimeout(function(){ $("#module-content-"+ MIID +"").find('[tabindex]:first').focus(); }, 200);
}
</script>
</div>

</div></div>
</div>
					</div>
					<div class="hp-column two">
						<div id="sw-content-container4" class="region ui-hp"><div id='pmi-2881'>
<div id="module-content-1535" ><div class="ui-widget app upcomingevents">
 <div class="ui-widget-header">
     <h1>Upcoming Events</h1>
 </div>
 <div class="ui-widget-detail">
		<ul class="ui-articles">
<li>
<div class="ui-article">
     <h1 class="ui-article-title sw-calendar-block-date">Monday</h1>
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">12:00 PM - 2:00 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=4#calendar1/20220516/event/3948">Name of Event</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">7:00 PM - 8:30 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=4#calendar1/20220516/event/4260">Name of Event</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">9:00 PM - 10:00 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=4#calendar1/20220516/event/4535">The Third Event of the Day</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <h1 class="ui-article-title sw-calendar-block-date">Tuesday</h1>
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">8:00 AM - 9:00 AM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=4#calendar1/20220517/event/4664">Soccer Practice</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">11:00 PM - 11:30 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=4#calendar1/20220517/event/4820">Movie Night</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <h1 class="ui-article-title sw-calendar-block-date">May 23, 2022</h1>
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">12:00 PM - 2:00 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=4#calendar1/20220523/event/3949">Name of Event</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">7:00 PM - 8:30 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=4#calendar1/20220523/event/4261">Name of Event</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">9:00 PM - 10:00 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=4#calendar1/20220523/event/4536">The Third Event of the Day</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <h1 class="ui-article-title sw-calendar-block-date">May 24, 2022</h1>
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">8:00 AM - 9:00 AM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=4#calendar1/20220524/event/4665">Soccer Practice</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">11:00 PM - 11:30 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=4#calendar1/20220524/event/4821">Movie Night</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <h1 class="ui-article-title sw-calendar-block-date">May 30, 2022</h1>
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">12:00 PM - 2:00 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=4#calendar1/20220530/event/3950">Name of Event</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">7:00 PM - 8:30 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=4#calendar1/20220530/event/4262">Name of Event</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">9:00 PM - 10:00 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=4#calendar1/20220530/event/4537">The Third Event of the Day</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <h1 class="ui-article-title sw-calendar-block-date">May 31, 2022</h1>
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">8:00 AM - 9:00 AM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=4#calendar1/20220531/event/4666">Soccer Practice</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">11:00 PM - 11:30 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://adamcruse.schoolwires.net/site/Default.aspx?PageID=2&DomainID=4#calendar1/20220531/event/4822">Movie Night</a></span>
     </p>
</div>
</li>
	</ul>
<a class='view-calendar-link' href="https://adamcruse.schoolwires.net/Page/2"><span>View Calendar</span></a>
 </div>
 <div class="ui-widget-footer">
 </div>
</div>
</div>
</div>
</div>
					</div>
					<div class="hp-column three">
						<div id="sw-content-container5" class="region ui-hp"><div id='pmi-2936'>



<div id='sw-module-15770'>
    <script type="text/javascript">
        $(document).ready(function() {
            var DomainID = '4';
            var PageID = '1';
            var RenderLoc = '0';
            var MIID = '1577';

            //added to check to make sure moderated content doesn't bleed through the dialog
            if ($('#dialog-overlay-WindowLargeModal-body.moderateContent').length > 0) {
                $("#module-content-" + MIID).find(".ui-widget-detail").find(".ui-article").append("<p>&nbsp;</p>");
            }
        });
    </script>

    <script type="text/javascript">$(document).ready(function() {CheckScript('ModuleView');CheckScript('Mustache');
 });</script>
    
    <div id="module-content-1577" >
<div class="ui-widget app flexpage">
	<div class="ui-widget-header ui-helper-hidden">
		
	</div>
	
	<div class="ui-widget-detail">
<div class="ui-article"><span ><iframe style="position:absolute; height:100%; width:100%;" src="https://adamcruse.schoolwires.net//cms/module/selectsurvey/TakeSurvey.aspx?SurveyID=101"></iframe></span></div> 
<div class="clear"></div>
</div>
	<div class="ui-widget-footer">
		
			
		
		<div class="clear"></div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function (){
$(".tag-list li a").keypress(function(e) { if(e.which == 13) { $(this).click();   }});
});
function LoadGroupedData(container, MIID, PMI, groupYear, groupMonth, groupBy, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://adamcruse.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&EnableQuirksMode=0&ViewID=' + viewToUse + '&Tag=' + tag, container, 2, 'chkSidebar();');
}
function LoadData(container, MIID, PMI, flexDataID, groupYear, groupMonth, groupBy, targetView, tag) {
  if(targetView !== undefined || targetView.Length() == 0){
  //targetView looks at the hidden Detail View defined in the Builder View.
      targetView = $('#hid-'+MIID+'-DetailView').val();
   }
  GetContent('https://adamcruse.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&FlexDataID=' + flexDataID + '&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&RenderLoc=0&FromRenderLoc=0&EnableQuirksMode=0&Tag=' + tag + '&ViewID=' + targetView, container, 2, 'chkSidebar();');
}
function LoadTaggedData(container, MIID, PMI, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://adamcruse.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&Tag=' + tag + '&EnableQuirksMode=0&ViewID='+viewToUse, container, 2, 'chkSidebar();');
  setTimeout(function(){ $("#module-content-"+ MIID +"").find('[tabindex]:first').focus(); }, 200);
}
</script>
</div>

</div></div>
</div>
					</div>
				</div>
			</div>
		</section>
		<section class="hp-row six choose-section" data-toggle='true'>
			<div class="content-wrap">
				<div class="choose-column one">
					<h2>Choose 742</h2>
					<p>Choosing 742 means choosing more options and opportunities for your child. Among the many choices, you will find both Spanish and Chinese Language Immersion Programs, 19 Advanced Placement classes, 33 High School League sponsored activities, countless clubs and groups, rigorous academics and comprehensive Choir, Band and Orchestra programs. Explore and discover why 742 is for you!</p>
				</div>
				<div class="choose-column two">
					<ul class="choose-section-links">
						<li data-content='Athletics, Activities, Clubs and Groups'><a href='#' target='_self'><span>Athletics, Activities, Clubs and Groups</span></a></li>
						<li data-content='Career and College Prep'><a href='#' target='_self'><span>Career and College Prep</span></a></li>
						<li data-content='Career and Technical Education'><a href='#' target='_self'><span>Career and Technical Education</span></a></li>
						<li data-content='Electives and Advanced Placement'><a href='#' target='_self'><span>Electives and Advanced Placement</span></a></li>
						<li data-content='English Learners'><a href='#' target='_self'><span>English Learners</span></a></li>
						<li data-content='Fine and Performing Arts'><a href='#' target='_self'><span>Fine and Performing Arts</span></a></li>
						<li data-content='Language Immersion'><a href='#' target='_self'><span>Language Immersion</span></a></li>
						<li data-content='Talent Development and Accelerated Services'><a href='#' target='_self'><span>Talent Development and Accelerated Services</span></a></li>
						<li data-content='Technology in the Classroom'><a href='#' target='_self'><span>Technology in the Classroom</span></a></li>
						<li data-content='Youth Enrichment with Community Education'><a href='#' target='_self'><span>Youth Enrichment with Community Education</span></a></li>
					</ul>
				</div>
			</div>
		</section>
	</main>
	<footer id="gb-footer">
		<section class="footer-row one">
			<div class="content-wrap">
				<div class="footer-column one">
					<p class="contact-info address" data-content="1628 19th Street">
						<span class="contact-column one">1628 19th Street</span><br>
						<span class="contact-column two">Lubbock, TX 79401</span>
					</p>
					<p class="contact-info phone-fax">
						<span class="contact-column one" data-content="806-219-0000"><span>806-219-0000</span></span><br>
						<span class="contact-column two" data-content="501.450.4898">Fax: <span>501.450.4898</span></span>
					</p>
				</div>
				<div class="footer-column two">
					<div class="social-icons"></div>
				</div>
			</div>
		</section>
		<section class="footer-row two">
			<div class="content-wrap">
				<div class="footer-column one"></div>
				<div class="footer-column two">
					<a class="footer-link" href="https://adamcruse.schoolwires.net/site/Default.aspx?PageType=15&SiteID=4&SectionMax=15&DirectoryType=6">
						<span>SITE MAP</span>
					</a>
				</div>
			</div>
		</section>
	</footer>
</div>
<!-- BEGIN - STANDARD FOOTER -->
<div id='sw-footer-outer'>
<div id='sw-footer-inner'>
<div id='sw-footer-left'></div>
<div id='sw-footer-right'>
<div id='sw-footer-links'>
<ul>
<li><a title='Click to email the primary contact' href='mailto:changeme@changeme.com'>Questions or Feedback?</a> | </li>
<li><a href='https://www.blackboard.com/blackboard-web-community-manager-privacy-statement' target="_blank">Blackboard Web Community Manager Privacy Policy (Updated)</a> | </li>
<li><a href='https://help.blackboard.com/Terms_of_Use' target="_blank">Terms of Use</a></li>
</ul>
</div>
<div id='sw-footer-copyright'>Copyright &copy; 2002-2022 Blackboard, Inc. All rights reserved.</div>
<div id='sw-footer-logo'><a href='http://www.blackboard.com' title="Blackboard, Inc. All rights reserved.">
<img src='https://adamcruse.schoolwires.net/Static//GlobalAssets/Images/Navbar/blackboard_logo.png'
 alt="Blackboard, Inc. All rights reserved."/>
</a></div>
</div>
</div>
</div>
<!-- END - STANDARD FOOTER -->
<script type="text/javascript">
   $(document).ready(function(){
      var beaconURL='https://analytics.schoolwires.com/analytics.asmx/Insert?AccountNumber=6boZxfPfyUY810QWRpwW3A%3d%3d&SessionID=3874552a-acb7-4af8-bad7-107f1ef77ace&SiteID=4&ChannelID=0&SectionID=0&PageID=1&HitDate=5%2f12%2f2022+1%3a25%3a16+PM&Browser=Chrome+87.0&OS=Unknown&IPAddress=10.61.81.55';
      try {
         $.getJSON(beaconURL + '&jsonp=?', function(myData) {});
      } catch(err) { 
         // prevent site error for analytics
      }
   });
</script>

    <input type="hidden" id="hid-pageid" value="1" />

    

    <div id='dialog-overlay-WindowMedium-base' class='ui-dialog-overlay-base' ><div id='WindowMedium' role='dialog' tabindex='-1'  class='ui-dialog-overlay medium' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-WindowMedium-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-WindowMedium-body' ></div><button class='ui-dialog-overlay-close' title='Close' aria-label='Close' onclick='CloseDialogOverlay("WindowMedium");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-WindowMedium-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-WindowMedium-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>
    <div id='dialog-overlay-WindowSmall-base' class='ui-dialog-overlay-base' ><div id='WindowSmall' role='dialog' tabindex='-1'  class='ui-dialog-overlay small' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-WindowSmall-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-WindowSmall-body' ></div><button class='ui-dialog-overlay-close' title='Close' aria-label='Close' onclick='CloseDialogOverlay("WindowSmall");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-WindowSmall-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-WindowSmall-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>
    <div id='dialog-overlay-WindowLarge-base' class='ui-dialog-overlay-base' ><div id='WindowLarge' role='dialog' tabindex='-1'  class='ui-dialog-overlay large' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-WindowLarge-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-WindowLarge-body' ></div><button class='ui-dialog-overlay-close' title='Close' aria-label='Close' onclick='CloseDialogOverlay("WindowLarge");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-WindowLarge-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-WindowLarge-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>

    <div id='dialog-overlay-WindowMediumModal-base' class='ui-dialog-overlay-base-modal' ><div id='WindowMediumModal' role='dialog' tabindex='-1'  class='ui-dialog-overlay medium' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-WindowMediumModal-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-WindowMediumModal-body' ></div><button class='ui-dialog-overlay-close modal' title='Close' aria-label='Close' onclick='CloseDialogOverlay("WindowMediumModal");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-WindowMediumModal-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-WindowMediumModal-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>
    <div id='dialog-overlay-WindowSmallModal-base' class='ui-dialog-overlay-base-modal' ><div id='WindowSmallModal' role='dialog' tabindex='-1'  class='ui-dialog-overlay small' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-WindowSmallModal-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-WindowSmallModal-body' ></div><button class='ui-dialog-overlay-close modal' title='Close' aria-label='Close' onclick='CloseDialogOverlay("WindowSmallModal");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-WindowSmallModal-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-WindowSmallModal-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>
    <div id='dialog-overlay-WindowLargeModal-base' class='ui-dialog-overlay-base-modal' ><div id='WindowLargeModal' role='dialog' tabindex='-1'  class='ui-dialog-overlay large' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-WindowLargeModal-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-WindowLargeModal-body' ></div><button class='ui-dialog-overlay-close modal' title='Close' aria-label='Close' onclick='CloseDialogOverlay("WindowLargeModal");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-WindowLargeModal-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-WindowLargeModal-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>
    <div id='dialog-overlay-WindowXLargeModal-base' class='ui-dialog-overlay-base-modal' ><div id='WindowXLargeModal' role='dialog' tabindex='-1'  class='ui-dialog-overlay xlarge' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-WindowXLargeModal-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-WindowXLargeModal-body' ></div><button class='ui-dialog-overlay-close modal' title='Close' aria-label='Close' onclick='CloseDialogOverlay("WindowXLargeModal");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-WindowXLargeModal-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-WindowXLargeModal-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>

    <div id='dialog-overlay-MyAccountSubscriptionOverlay-base' class='ui-dialog-overlay-base-modal' ><div id='MyAccountSubscriptionOverlay' role='dialog' tabindex='-1'  class='ui-dialog-overlay medium' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-MyAccountSubscriptionOverlay-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-MyAccountSubscriptionOverlay-body' ></div><button class='ui-dialog-overlay-close modal' title='Close' aria-label='Close' onclick='CloseDialogOverlay("MyAccountSubscriptionOverlay");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-MyAccountSubscriptionOverlay-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-MyAccountSubscriptionOverlay-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>

    <div id='dialog-overlay-InsertOverlay-base' class='ui-dialog-overlay-base-modal' ><div id='InsertOverlay' role='dialog' tabindex='-1'  class='ui-dialog-overlay large' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-InsertOverlay-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-InsertOverlay-body' ></div><button class='ui-dialog-overlay-close modal' title='Close' aria-label='Close' onclick='CloseDialogOverlay("InsertOverlay");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-InsertOverlay-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-InsertOverlay-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>
    <div id='dialog-overlay-InsertOverlay2-base' class='ui-dialog-overlay-base-modal' ><div id='InsertOverlay2' role='dialog' tabindex='-1'  class='ui-dialog-overlay large' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-InsertOverlay2-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-InsertOverlay2-body' ></div><button class='ui-dialog-overlay-close modal' title='Close' aria-label='Close' onclick='CloseDialogOverlay("InsertOverlay2");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-InsertOverlay2-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-InsertOverlay2-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>
    
    <div id="videowrapper" class="ui-helper-hidden">
        <div id="videodialog" role="application">
            <a id="videodialog-close" role="button" href="javascript:;" aria-label="Close Overlay" class="close-btn" onclick="closeVideoDialog();">CLOSE</a>
            <div id="videodialog-video" ></div>
            <div id="videodialog-foot" tabindex="0"></div>
        </div>
    </div>
    <div id="attachmentwrapper" class="ui-helper-hidden">
        <div id="attachmentdialog" role="application">
            <a id="attachmentdialog-close" role="button" href="javascript:;" aria-label="Close Overlay" class="close-btn" onclick="closeAttachmentDialog();">CLOSE</a>
            <div id="attachmentdialog-container"></div>
        </div>
    </div>
    <script type="text/javascript">

        $(document).ready(function () {

            removeBrokenImages();
            checkSidebar();
            RemoveCookie();

            $('div.bullet').attr('tabindex', '');

            $('.navigation li.collapsible').each(function () {
                if ($(this).find('ul').length == 0) {
                    $(this).removeClass('collapsible');
                }
            });

            // find page nav state cookie and add open chevron
            var arrValues = GetCookie('SWPageNavState').split('~');

            $.each(arrValues, function () {
                if (this != '') {
                    $('#' + this).addClass('collapsible').prepend("<div class='bullet collapsible' aria-label='Close Page Submenu'/>");
                }
            });

            // find remaining sub menus and add closed chevron and close menu
            $('.navigation li > ul').each(function () {
                var list = $(this);

                if (list.parent().hasClass('active') && !list.parent().hasClass('collapsible')) {
                    // open sub for currently selected page                    
                    list.parent().addClass('collapsible').prepend("<div class='bullet collapsible'aria-label='Close Page Submenu' />");
                } else {
                    if (list.parent().hasClass('collapsible') && !list.siblings('div').hasClass('collapsible')) {
                        // open sub for page with auto expand
                        list.siblings('div.expandable').remove();
                        list.parent().prepend("<div class='bullet collapsible' aria-label='Close Page Submenu' />");
                    }
                }

                if (!list.siblings('div').hasClass('collapsible')) {
                    // keep all closed that aren't already set to open
                    list.parent().addClass('expandable').prepend("<div class='bullet expandable' aria-label='Open Page Submenu' />");
                    ClosePageSubMenu(list.parent());
                } else {
                    OpenPageSubMenu(list.parent());
                }
            });

            // remove bullet from hierarchy if no-bullet set
            $('.navigation li.collapsible').each(function () {
                if ($(this).hasClass('no-bullet')) {
                    if (!$(this).hasClass('navigationgroup')) { $(this).removeClass('collapsible'); }
                    $(this).children('div.collapsible').remove();
                }
            });

            $('.navigation li.expandable').each(function () {
                if ($(this).hasClass('no-bullet')) {
                    if (!$(this).hasClass('navigationgroup')) { $(this).removeClass('expandable'); }
                    $(this).children('div.expandable').remove();
                }
            });

            $('.navigation li:not(.collapsible,.expandable,.no-bullet)').each(function () {
                $(this).prepend("<div class='bullet'/>");
            });

            $('.navigation li.active').parents('ul').each(function () {
                if (!$(this).hasClass('page-navigation')) {
                    OpenPageSubMenu($(this).parent());
                }
            });

            // Set aria ttributes
            $('li.collapsible').each(function () {
                $(this).attr("aria-expanded", "true");
                $(this).find('div:first').attr('aria-pressed', 'true');
            });

            $('li.expandable').each(function () {
                $(this).attr("aria-expanded", "false");
                $(this).find('div:first').attr('aria-pressed', 'false');
            });

            $('div.bullet').each(function () {
                $(this).attr("aria-hidden", "true");
            });

            // set click event for chevron
            $(document).on('click', '.navigation div.collapsible', function () {
                ClosePageSubMenu($(this).parent());
            });

            $(document).on('click', '.navigation div.expandable', function () {
                OpenPageSubMenu($(this).parent());
            });

            // set navigation grouping links
            $(document).on('click', '.navigationgroup.collapsible > a', function () {
                ClosePageSubMenu($(this).parent());
            });

            $(document).on('click', '.navigationgroup.expandable > a', function () {
                OpenPageSubMenu($(this).parent());
            });

            //SW MYSTART DROPDOWNS
            $(document).on('click', '.sw-mystart-dropdown', function () {
                $(this).children(".sw-dropdown").css("display", "block");
            });

            $(".sw-mystart-dropdown").hover(function () { }, function () {
                $(this).children(".sw-dropdown").hide();
                $(this).blur();
            });

            //SW ACCOUNT DROPDOWN
            $(document).on('click', '#sw-mystart-account', function () {
                $(this).children("#sw-myaccount-list").show();
                $(this).addClass("clicked-state");
            });

            $("#sw-mystart-account, #sw-myaccount-list").hover(function () { }, function () {
                $(this).children("#sw-myaccount-list").hide();
                $(this).removeClass("clicked-state");
                $("#sw-myaccount").blur();
            });

            // set hover class for page and section navigation
            $('.ui-widget.app.pagenavigation, .ui-widget.app.sectionnavigation').find('li > a').hover(function () {
                $(this).addClass('hover');
            }, function () {
                $(this).removeClass('hover');
            });

            //set aria-label for home
            $('#navc-HP > a').attr('aria-label', 'Home');

            // set active class on channel and section
            var activeChannelNavType = $('input#hidActiveChannelNavType').val();
            if (activeChannelNavType == -1) {
                // homepage is active
                $('#navc-HP').addClass('active');
            } else if (activeChannelNavType == 1) {
                // calendar page is active
                $('#navc-CA').addClass('active');
            } else {
                // channel is active - set the active class on the channel
                var activeSelectorID = $('input#hidActiveChannel').val();
                $('#navc-' + activeSelectorID).addClass('active');

                // set the breadcrumb channel href to the channel nav href
                $('li[data-bccID=' + activeSelectorID + '] a').attr('href', $('#navc-' + activeSelectorID + ' a').attr('href'));
                $('li[data-bccID=' + activeSelectorID + '] a span').text($('#navc-' + activeSelectorID + ' a span').first().text());

                // set the active class on the section
                activeSelectorID = $('input#hidActiveSection').val();
                $('#navs-' + activeSelectorID).addClass('active');

                // set the breadcrumb section href to the channel nav href
                $('li[data-bcsID=' + activeSelectorID + '] a').attr('href', $('#navs-' + activeSelectorID + ' a').attr('href'));
                if ($('#navs-' + activeSelectorID + ' a').attr('target') !== undefined) {
                    $('li[data-bcsID=' + activeSelectorID + '] a').attr('target', $('#navs-' + activeSelectorID + ' a').attr('target'));
                }
                $('li[data-bcsID=' + activeSelectorID + '] span').text($('#navs-' + activeSelectorID + ' a span').text());

                if ($('.sw-directory-columns').length > 0) {
                    $('ul.ui-breadcrumbs li:last-child').remove();
                    $('ul.ui-breadcrumbs li:last-child a').replaceWith(function() { return $('span', this); });
                    $('ul.ui-breadcrumbs li:last-child span').append(' Directory');
                }
            }
        }); // end document ready

        function OpenPageSubMenu(li) {
            if (li.prop('tagName').toLowerCase() == "li") {
                if (li.hasClass('expandable')) {
                    li.removeClass('expandable').addClass('collapsible');
                }
                if (li.find('div:first').hasClass('expandable')) {
                    li.find('div:first').removeClass('expandable').addClass('collapsible').attr('aria-pressed', 'true').attr('aria-label','Close Page Submenu');
                }
                li.find('ul:first').attr('aria-hidden', 'false').show();

                li.attr("aria-expanded", "true");

                PageNavigationStateCookie();
            }
        }

        function ClosePageSubMenu(li) {
            if (li.prop('tagName').toLowerCase() == "li") {
                li.removeClass('collapsible').addClass('expandable');
                li.find('div:first').removeClass('collapsible').addClass('expandable').attr('aria-pressed', 'false').attr('aria-label','Open Page Submenu');
                li.find('ul:first').attr('aria-hidden', 'true').hide();

                li.attr("aria-expanded", "false");

                PageNavigationStateCookie();
            }
        }

        function PageNavigationStateCookie() {
            var strCookie = "";

            $('.pagenavigation li > ul').each(function () {
                var item = $(this).parent('li');
                if (item.hasClass('collapsible') && !item.hasClass('no-bullet')) {
                    strCookie += $(this).parent().attr('id') + '~';
                }
            });

            SetCookie('SWPageNavState', strCookie);
        }

        function checkSidebar() {
            $(".ui-widget-sidebar").each(function () {
                if ($.trim($(this).html()) != "") {
                    $(this).show();
                    $(this).siblings(".ui-widget-detail").addClass("with-sidebar");
                }
            });
        }

        function removeBrokenImages() {
            //REMOVES ANY BROKEN IMAGES
            $("span.img img").each(function () {
                if ($(this).attr("src") !== undefined && $(this).attr("src") != '../../') {
                    $(this).parent().parent().show();
                    $(this).parent().parent().siblings().addClass("has-thumb");
                }
            });
        }

        function LoadEventDetailUE(moduleInstanceID, eventDateID, userRegID, isEdit) {
            (userRegID === undefined ? userRegID = 0 : '');
            (isEdit === undefined ? isEdit = false : '');
            OpenDialogOverlay("WindowMediumModal", { LoadType: "U", LoadURL: "https://adamcruse.schoolwires.net//site/UserControls/Calendar/EventDetailWrapper.aspx?ModuleInstanceID=" + moduleInstanceID + "&EventDateID=" + eventDateID + "&UserRegID=" + userRegID + "&IsEdit=" + isEdit });
        }

        function RemoveCookie() {
            // There are no sub page            
            if ($('.pagenavigation li li').length == 0) {
                //return false;
                PageNavigationStateCookie();
            }
        }
    </script>

    <script type="text/javascript">

        function AddOffCanvasMenuHeightForSiteNav() {
            var sitenavulHeight = 0;

            if ($('#sw-pg-sitenav-ul').length > 0) {
                sitenavulHeight = parseInt($("#sw-pg-sitenav-ul").height());
            }

            var swinnerwrapHeight = 0;

            if ($('#sw-inner-wrap').length > 0) {
                swinnerwrapHeight = parseInt($("#sw-inner-wrap").height());
            }

            // 360px is abount 5 li height
            if (sitenavulHeight + 360 >= swinnerwrapHeight) {
                $("#sw-inner-wrap").height(sitenavulHeight + 360);
            }
        }

        function AddOffCanvasMenuHeightForSelectSchool() {
            var selectschoolulHeight = 0;

            if ($('#sw-pg-selectschool-ul').length > 0) {
                selectschoolulHeight = parseInt($("#sw-pg-selectschool-ul").height());
            }

            var swinnerwrapHeight = 0;

            if ($('#sw-inner-wrap').length > 0) {
                swinnerwrapHeight = parseInt($("#sw-inner-wrap").height());
            }

            // 360px is abount 5 li height
            if (selectschoolulHeight + 360 >= swinnerwrapHeight) {
                $("#sw-inner-wrap").height(selectschoolulHeight + 360);
            }
        }

        $(document).ready(function () {
            if ($("#sw-pg-sitenav-a").length > 0) {
                $(document).on('click', '#sw-pg-sitenav-a', function () {
                    if ($("#sw-pg-sitenav-ul").hasClass('sw-pgmenu-closed')) {
                        AddOffCanvasMenuHeightForSiteNav();

                        $("ul.sw-pgmenu-toplevel").removeClass('sw-pgmenu-open').addClass('sw-pgmenu-closed');
                        $("#sw-pg-sitenav-ul").removeClass('sw-pgmenu-closed');
                        $("#sw-pg-sitenav-ul").addClass('sw-pgmenu-open');
                    } else {
                        $("#sw-pg-sitenav-ul").removeClass('sw-pgmenu-open');
                        $("#sw-pg-sitenav-ul").addClass('sw-pgmenu-closed');
                    }
                });

                $(document).on('click', '#sw-pg-selectschool-a', function () {
                    if ($("#sw-pg-selectschool-ul").hasClass('sw-pgmenu-closed')) {
                        AddOffCanvasMenuHeightForSelectSchool();

                        $("ul.sw-pgmenu-toplevel").removeClass('sw-pgmenu-open').addClass('sw-pgmenu-closed');
                        $("#sw-pg-selectschool-ul").removeClass('sw-pgmenu-closed');
                        $("#sw-pg-selectschool-ul").addClass('sw-pgmenu-open');
                    } else {
                        $("#sw-pg-selectschool-ul").removeClass('sw-pgmenu-open');
                        $("#sw-pg-selectschool-ul").addClass('sw-pgmenu-closed');
                    }
                });

                $(document).on('click', '#sw-pg-myaccount-a', function () {
                    if ($("#sw-pg-myaccount-ul").hasClass('sw-pgmenu-closed')) {
                        $("ul.sw-pgmenu-toplevel").removeClass('sw-pgmenu-open').addClass('sw-pgmenu-closed');
                        $("#sw-pg-myaccount-ul").removeClass('sw-pgmenu-closed');
                        $("#sw-pg-myaccount-ul").addClass('sw-pgmenu-open');
                    } else {
                        $("#sw-pg-myaccount-ul").removeClass('sw-pgmenu-open');
                        $("#sw-pg-myaccount-ul").addClass('sw-pgmenu-closed');
                    }
                });

                $(document).on('click', '.pg-list-bullet', function () {
                    $(this).prev().toggle();

                    if ($(this).hasClass('closed')) {
                        AddOffCanvasMenuHeightForSiteNav();

                        $(this).removeClass('closed');
                        $(this).addClass('open');
                    } else {
                        $(this).removeClass('open');
                        $(this).addClass('closed');
                    }
                });

                $(document).on('mouseover', '#sw-pg-selectschool', function () {
                    $("#sw-pg-selectschool-firstli").removeClass('sw-pg-selectschool-firstli-mouseout').addClass('sw-pg-selectschool-firstli-mouseover');
                    $("#sw-pg-selectschool-firstli a").addClass('sw-pg-selectschool-firstli-a-mouseover').removeClass('sw-pg-selectschool-firstli-a-mouseout');
                });

                $(document).on('mouseout', '#sw-pg-selectschool', function () {
                    $("#sw-pg-selectschool-firstli").removeClass('sw-pg-selectschool-firstli-mouseover').addClass('sw-pg-selectschool-firstli-mouseout');
                    $("#sw-pg-selectschool-firstli a").addClass('sw-pg-selectschool-firstli-a-mouseout').removeClass('sw-pg-selectschool-firstli-a-mouseover');
                });

                $(document).on('mouseover', '#sw-pg-myaccount', function () {
                    $("#sw-pg-myaccount-firstli").removeClass('sw-pg-myaccount-firstli-mouseout').addClass('sw-pg-myaccount-firstli-mouseover');
                    $("#sw-pg-myaccount-firstli a").addClass('sw-pg-myaccount-firstli-a-mouseover').removeClass('sw-pg-myaccount-firstli-a-mouseout');
                });

                $(document).on('mouseout', '#sw-pg-myaccount', function () {
                    $("#sw-pg-myaccount-firstli").removeClass('sw-pg-myaccount-firstli-mouseover').addClass('sw-pg-myaccount-firstli-mouseout');
                    $("#sw-pg-myaccount-firstli a").addClass('sw-pg-myaccount-firstli-a-mouseout').removeClass('sw-pg-myaccount-firstli-a-mouseover');
                });

                $(document).on('mouseover', '#sw-pg-sitenav', function () {
                    $("#sw-pg-sitenav-firstli").removeClass('sw-pg-sitenav-firstli-mouseout').addClass('sw-pg-sitenav-firstli-mouseover');
                    $("#sw-pg-sitenav-firstli a").addClass('sw-pg-sitenav-firstli-a-mouseover').removeClass('sw-pg-sitenav-firstli-a-mouseout');
                });

                $(document).on('mouseout', '#sw-pg-sitenav', function () {
                    $("#sw-pg-sitenav-firstli").removeClass('sw-pg-sitenav-firstli-mouseover').addClass('sw-pg-sitenav-firstli-mouseout');
                    $("#sw-pg-sitenav-firstli a").addClass('sw-pg-sitenav-firstli-a-mouseout').removeClass('sw-pg-sitenav-firstli-a-mouseover');
                });

                $(document).on('mouseover', '#sw-pg-district', function () {
                    $("#sw-pg-district-firstli").removeClass('sw-pg-district-firstli-mouseout').addClass('sw-pg-district-firstli-mouseover');
                    $("#sw-pg-district-firstli a").addClass('sw-pg-district-firstli-a-mouseover').removeClass('sw-pg-district-firstli-a-mouseout');
                });

                $(document).on('mouseout', '#sw-pg-district', function () {
                    $("#sw-pg-district-firstli").removeClass('sw-pg-district-firstli-mouseover').addClass('sw-pg-district-firstli-mouseout');
                    $("#sw-pg-district-firstli a").addClass('sw-pg-district-firstli-a-mouseout').removeClass('sw-pg-district-firstli-a-mouseover');
                });
            }
        });


    </script>
    <script src='https://adamcruse.schoolwires.net/Static//GlobalAssets/Scripts/min/jquery-ui-1.12.0.min.js' type='text/javascript'></script>
    <script src="https://adamcruse.schoolwires.net/Static/GlobalAssets/Scripts/min/SW-UI.min.js" type='text/javascript'></script>
    <script src="https://adamcruse.schoolwires.net/Static/GlobalAssets/Scripts/jquery.sectionlayer.js" type='text/javascript'></script>
    
    <script src="https://adamcruse.schoolwires.net/Static/GlobalAssets/Scripts/min/swfobject.min.js" type="text/javascript"></script>
    <script src="https://adamcruse.schoolwires.net/Static/GlobalAssets/Scripts/min/jquery.ajaxupload_2440.min.js" type="text/javascript"></script>

    <!-- Begin swuc.CheckScript -->
  <script type="text/javascript" src="https://adamcruse.schoolwires.net/Static/GlobalAssets/Scripts/ThirdParty/json2.js"></script>
  
<script>var homeURL = location.protocol + "//" + window.location.hostname;

function parseXML(xml) {
    if (window.ActiveXObject && window.GetObject) {
        var dom = new ActiveXObject('Microsoft.XMLDOM');
        dom.loadXML(xml);
        return dom;
    }

    if (window.DOMParser) {
        return new DOMParser().parseFromString(xml, 'text/xml');
    } else {
        throw new Error('No XML parser available');
    }
}

function GetContent(URL, TargetClientID, Loadingtype, SuccessCallback, FailureCallback, IsOverlay, Append) {
    (Loadingtype === undefined ? LoadingType = 3 : '');
    (SuccessCallback === undefined ? SuccessCallback = '' : '');
    (FailureCallback === undefined ? FailureCallback = '' : '');
    (IsOverlay === undefined ? IsOverlay = '0' : '');
    (Append === undefined ? Append = false : '');

    var LoadingHTML;
    var Selector;

    switch (Loadingtype) {
        //Small
        case 1:
            LoadingHTML = "SMALL LOADER HERE";
            break;
            //Large
        case 2:
            LoadingHTML = "<div class='ui-loading large' role='alert' aria-label='Loading content'></div>";
            break;
            // None
        case 3:
            LoadingHTML = "";
            break;
    }

    Selector = "#" + TargetClientID;

    ajaxCall = $.ajax({
        url: URL,
        cache: false,
        beforeSend: function () {
            if (Loadingtype != 3) { BlockUserInteraction(TargetClientID, LoadingHTML, Loadingtype, 0, IsOverlay); }
        },
        success: function (strhtml) {

            // check for calendar and empty div directly surrounding eventlist uc first 
            //      to avoid memory error in IE (Access violation reading location 0x00000018)
            //      need to figure out exactly why this is happening..
            //      Partially to do with this? http://support.microsoft.com/kb/927917/en-us
            //      The error/crash happens when .empty() is called on Selector 
            //          (.html() calls .empty().append() in jquery)
            //      * no one has come across this issue anywhere else in the product so far

            if ($(Selector).find('#calendar-pnl-calendarlist').length > 0) {
                $('#calendar-pnl-calendarlist').empty();
                $(Selector).html(strhtml);
            } else if (Append) {
                $(Selector).append(strhtml);
            }
            else {
                $(Selector).html(strhtml);
            }


            // check for tabindex 
            if ($(Selector).find(":input[tabindex='1']:first").length > 0) {
                $(Selector).find(":input[tabindex='1']:first").focus();
            } else {
                $(Selector).find(":input[type='text']:first").not('.nofocus').focus();
            }

            //if (CheckDirty(Selector) === true) { BindSetDirty(Selector); }
            //CheckDirty(Selector);
            BlockUserInteraction(TargetClientID, '', '', 1);
            (SuccessCallback != '' ? eval(SuccessCallback) : '');
        },
        failure: function () {
            BlockUserInteraction(TargetClientID, '', '', 1);
            (FailureCallback != '' ? eval(FailureCallback) : '');
        }
    });
}

function BlockUserInteraction(TargetClientID, LoadingHTML, Loadingtype, Unblock, IsOverlay) {
    if (LoadingHTML === undefined) {
        LoadingHTML = "<div class='ui-loading large'></div>";
    }

    if (Unblock == 1) {
        $('#' + TargetClientID).unblockinteraction();
    } else {
        if (IsOverlay == 1) {
            $('#' + TargetClientID).blockinteraction({ message: LoadingHTML, type: Loadingtype, isOverlay: true });
        } else {
            $('#' + TargetClientID).blockinteraction({ message: LoadingHTML, type: Loadingtype });
        }
    }
}

function OpenUltraDialogOverlay(OverlayClientID, options, Callback) {
    lastItemClicked = document.activeElement;
    var defaults = {
        LoadType: "U",
        LoadURL: "",
        TargetDivID: "",
        LoadContent: "",
        NoResize: false,
        ScrollTop: false,
        CloseCallback: undefined
    };

    jQuery.extend(defaults, options);

    // check what browser/version we're on
    var isIE = GetIEVersion();

    if (isIE == 0) {
        if ($.browser.mozilla) {
            //Firefox/Chrome
            $("body").css("overflow", "hidden");
        } else {
            //Safari
            $("html").css("overflow", "hidden");
        }
    } else {
        // IE
        $("html").css("overflow", "hidden");
    }

    var OverlaySelector;
    var BodyClientID;
    var TargetDivSelector;
    var CurrentScrollPosition;

    if (defaults.ScrollTop) {
        $.scrollTo(0, { duration: 0 });
    }

    OverlaySelector = "#dialog-ultra-overlay-" + OverlayClientID + "-base";
    BodyClientID = "dialog-ultra-overlay-" + OverlayClientID + "-holder";
    TargetDivSelector = "#" + defaults.TargetDivID;

    if ($.trim($("#dialog-ultra-overlay-" + OverlayClientID + "-holder").html()) != "") {
        CloseUltraDialogOverlay(OverlayClientID);
    }

    // U = URL
    // H - HTML
    // D - Divider

    var success = "";

    if (Callback !== undefined) {
        success += Callback;
    }

    // block user interaction
    BlockUserInteraction(BodyClientID, "<div class='ui-loading large'></div>", 2, 0, 1);

    switch (defaults.LoadType) {
        case 'U':
            GetContent(defaults.LoadURL, BodyClientID, 3, success);
            break;
        case 'H':
            $("#" + BodyClientID).html(defaults.LoadContent);
            break;
        case 'D':
            $("#" + BodyClientID).html($(TargetDivSelector).html());
            break;
    };

    // open the lateral panel
    var browserWidth = $(document).width();

    if (OverlayClientID == "UltraOverlayLarge") {
        browserWidth = browserWidth - 200;
    } else {
        browserWidth = browserWidth - 280;
    }

    $("#dialog-ultra-overlay-" + OverlayClientID + "-body").css("width", browserWidth);
    $(OverlaySelector).addClass("is-visible");

    // hide 'X' button if second window opened
    if (OverlayClientID == "UltraOverlayMedium") {
        $("#dialog-ultra-overlay-UltraOverlayLarge-close").hide();
    }
}

function CloseUltraDialogOverlay(OverlayClientID) {
    $("#dialog-ultra-overlay-" + OverlayClientID + "-base").removeClass("is-visible");

    // show 'X' button if second window closed
    if (OverlayClientID == "UltraOverlayMedium") {
        $("#dialog-ultra-overlay-UltraOverlayLarge-close").show();
    }

    if (OverlayClientID != "UltraOverlayMedium") {
        // check what browser/version we're on
        var isIE = GetIEVersion();

        if (isIE == 0) {
            if ($.browser.mozilla) {
                //Firefox/Chrome
                $("body").css("overflow", "auto");
            } else {
                //Safari
                $("html").css("overflow", "auto");
            }
        } else {
            // IE
            $("html").css("overflow", "auto");
        }
    }
    //focus back on the element clicked to open the dialog
    if (lastItemClicked !== undefined) {
        lastItemClicked.focus();
        lastItemClicked = undefined;
    }
}

//Save the last item clicked when opening a dialog overlay so the focus can go there upon close
var lastItemClicked;
function OpenDialogOverlay(OverlayClientID, options, Callback) {
    lastItemClicked = document.activeElement;
    var defaults = {
        LoadType: 'U',
        LoadURL: '',
        TargetDivID: '',
        LoadContent: '',
        NoResize: false,
        ScrollTop: false,
        CloseCallback: undefined
    };

    jQuery.extend(defaults, options);

    //check what browser/version we're on
    var isIE = GetIEVersion();

    //if (isIE == 0) {
    //    if ($.browser.mozilla) {
    //        //Firefox/Chrome
    //        $('body').css('overflow', 'hidden');
    //    } else {
    //        //Safari
    //        $('html').css('overflow', 'hidden');
    //    }
    //} else {
    //    // IE
    //    $('html').css('overflow', 'hidden');
    //}
    $('html').css('overflow', 'hidden');

    var OverlaySelector;
    var BodyClientID;
    var TargetDivSelector;

    if (defaults.ScrollTop) {
        $.scrollTo(0, { duration: 0 });
    }

    OverlaySelector = "#dialog-overlay-" + OverlayClientID + "-base";
    BodyClientID = "dialog-overlay-" + OverlayClientID + "-body";
    TargetDivSelector = "#" + defaults.TargetDivID;

    $(OverlaySelector).appendTo('body');

    $("#" + OverlayClientID).css("top", "5%");

    $("#" + BodyClientID).html("");

    if (isIEorEdge()) {
        $(OverlaySelector).show();
    } else {
        $(OverlaySelector).fadeIn();
    }

    if ($.trim($('#dialog-overlay-' + OverlayClientID + '-body').html()) != "") {
        CloseDialogOverlay(OverlayClientID);
    }

    // U = URL
    // H - HTML
    // D - Divider

    var success = "";

    if (Callback !== undefined) {
        success += Callback;
    }

    // Block user interaction
    $('#' + BodyClientID).css({ 'min-height': '100px' });
    BlockUserInteraction(BodyClientID, "<div class='ui-loading large'></div>", 2, 0, 1);

    switch (defaults.LoadType) {
        case 'U':
            GetContent(defaults.LoadURL, BodyClientID, 3, success);
            break;
        case 'H':
            $("#" + BodyClientID).html(defaults.LoadContent);
            break;
        case 'D':
            $("#" + BodyClientID).html($(TargetDivSelector).html());
            break;

    };

    if (defaults.CloseCallback !== undefined) {
        $(OverlaySelector + ' .ui-dialog-overlay-close').attr('onclick', 'CloseDialogOverlay(\'' + OverlayClientID + '\',' + defaults.CloseCallback + ')')
    }

    // check for tabindex 
    if ($("#" + BodyClientID).find(":input[tabindex='1']:first").length > 0) {
        $("#" + BodyClientID).find(":input[tabindex='1']:first").focus();
    } else if ($("#" + BodyClientID).find(":input[type='text']:first").length > 0) {
        $("#" + BodyClientID).find(":input[type='text']:first").focus();
    } else {
        $("#" + BodyClientID).parent().focus();
    }

    $('#dialog-overlay-' + OverlayClientID + '-base').css({'overflow': 'auto', 'top': '0', 'width': '100%', 'left': '0' });
}

function GetIEVersion() {
    var sAgent = window.navigator.userAgent;
    var Idx = sAgent.indexOf("MSIE");

    if (Idx > 0) {
        // If IE, return version number
        return parseInt(sAgent.substring(Idx + 5, sAgent.indexOf(".", Idx)));
    } else if (!!navigator.userAgent.match(/Trident\/7\./)) {
        // If IE 11 then look for Updated user agent string
        return 11;
    } else {
        //It is not IE
        return 0;
    }
}

function isIEorEdge() {
    var agent = window.navigator.userAgent;

    var ie = agent.indexOf('MSIE ');
    if (ie > 0) {
        // IE <= 10
        return parseInt(agent.substring(ie + 5, uaagentindexOf('.', ie)), 10);
    }

    var gum = agent.indexOf('Trident/');
    if (gum > 0) {
        // IE 11
        var camper = agent.indexOf('rv:');
        return parseInt(agent.substring(camper + 3, agent.indexOf('.', camper)), 10);
    }

    var linkinPark = agent.indexOf('Edge/');
    if (linkinPark > 0) {
        return parseInt(agent.substring(linkinPark + 5, agent.indexOf('.', linkinPark)), 10);
    }

    // other browser
    return false;
}

function SendEmail(to, from, subject, body, callback) {

    var data = "{ToEmailAddress: '" + to + "', " +
        "FromEmailAddress: '" + from + "', " +
        "Subject: '" + subject + "', " +
        "Body: '" + body + "'}";
    var url = homeURL + "/GlobalUserControls/SE/SEController.aspx/SE";

    $.ajax({
        type: "POST",
        url: url,
        data: data,
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,
        success: function (msg) {
            callback(msg.d);
        },
        headers: { 'X-Csrf-Token': GetCookie('CSRFToken') }
    });
}

// DETERMINE TEXT COLOR 

function rgbstringToTriplet(rgbstring) {
    var commadelim = rgbstring.substring(4, rgbstring.length - 1);
    var strings = commadelim.split(",");
    var numeric = [];

    for (var i = 0; i < 3; i++) {
        numeric[i] = parseInt(strings[i]);
    }

    return numeric;
}

function adjustColour(someelement) {
    var rgbstring = someelement.css('background-color');
    var triplet = [];
    var newtriplet = [];

    if (rgbstring != 'transparent') {
        if (/rgba\(0, 0, 0, 0\)/.exec(rgbstring)) {
            triplet = [255, 255, 255];
        } else {
            if (rgbstring.substring(0, 1).toLowerCase() != 'r') {
                CheckScript('RGBColor', staticURL + '/GlobalAssets/Scripts/ThirdParty/rgbcolor.js');
                // not rgb, convert it
                var color = new RGBColor(rgbstring);
                rgbstring = color.toRGB();
            }
            triplet = rgbstringToTriplet(rgbstring);
        }
    } else {
        triplet = [255, 255, 255];
    }

    // black or white:
    var total = 0; for (var i = 0; i < triplet.length; i++) { total += triplet[i]; }

    if (total > (3 * 256 / 2)) {
        newtriplet = [0, 0, 0];
    } else {
        newtriplet = [255, 255, 255];
    }

    var newstring = "rgb(" + newtriplet.join(",") + ")";

    someelement.css('color', newstring);
    someelement.find('*').css('color', newstring);

    return true;
}


// END DETERMINE TEXT color

function CheckScript2(ModuleName, ScriptSRC) {
    $.ajax({
        url: ScriptSRC,
        async: false,
        //context: document.body,
        success: function (html) {
            var script =
				document.createElement('script');
            document.getElementsByTagName('head')[0].appendChild(script);
            script.text = html;
        }
    });
}

// AREA / SCREEN CODES

function setCurrentScreenCode(screenCode) {
    SetCookie('currentScreenCode', screenCode);
    AddAnalyticsEvent(getCurrentAreaCode(), screenCode, 'Page View');
}

function getCurrentScreenCode() {
    var cookieValue = GetCookie('currentScreenCode');
    return (cookieValue != '' ? cookieValue : 0);
}

function setCurrentAreaCode(areaCode) {
    SetCookie('currentAreaCode', areaCode);
}

function getCurrentAreaCode() {
    var cookieValue = GetCookie('currentAreaCode');
    return (cookieValue != '' ? cookieValue : 0);
}

// END AREA / SCREEN CODES

// CLICK HOME TAB IN HEADER SECTION

function GoHome() {
    window.location.href = homeURL + "/cms/Workspace";
}

// END CLICK HOME TAB IN HEADER SECTION

// HELP PANEL

function OpenHelpPanel() {
    var URLScreenCode = getCurrentScreenCode();
    
    if (URLScreenCode == "" || URLScreenCode == 0) {
        URLScreenCode = getCurrentAreaCode();
        if (URLScreenCode == 0) {
            URLScreenCode = "";
        }
    } 

    AddAnalyticsEvent("Help", "How Do I...?", URLScreenCode);

    //help site url stored in webconfig, passed in to BBHelpURL from GlobalJSVar and GlobalJS.cs
    var HelpURL = BBHelpURL + URLScreenCode;
    window.open(HelpURL,"_blank");
}

// END HELP PANEL

// COOKIES
function SetCookie(name, value, days, ms) {
    var expires = "";

    if (ms) {
        var date = new Date();

        date.setMilliseconds(date.getMilliseconds() + ms);
        expires = "; expires=" + date.toGMTString();
    } else if (days) {
        var date = new Date();

        date.setDate(date.getDate() + days);
        expires = "; expires=" + date.toGMTString();
    }

    document.cookie = name + "=" + escape(value) + expires + "; path=/";
}

function GetCookie(name) {
    var value = "";

    if (document.cookie.length > 0) {
        var start = document.cookie.indexOf(name + "=");

        if (start != -1) {
            start = start + name.length + 1;

            var end = document.cookie.indexOf(";", start);

            if (end == -1) end = document.cookie.length;
            value = unescape(document.cookie.substring(start, end));
        }
    }

    return value;
}

function DeleteCookie(name) {
    SetCookie(name, '', -1);
}

function SetUnescapedCookie(name, value, days, ms) {
    var expires = "";

    if (ms) {
        var date = new Date();

        date.setMilliseconds(date.getMilliseconds() + ms);
        expires = "; expires=" + date.toGMTString();
    } else if (days) {
        var date = new Date();

        date.setDate(date.getDate() + days);
        expires = "; expires=" + date.toGMTString();
    }

    document.cookie = name + "=" + value + expires + "; path=/";
}
// END COOKIES



// IFRAME FUNCTIONS
function BindResizeFrame(FrameID) {
    $(document).on('load', "#" + FrameID, function () {
        var bodyHeight = $("#" + FrameID).contents().height() + 40;
        $("#" + FrameID).attr("height", bodyHeight + "px");
    });
}

function AdjustLinkTarget(FrameID) {
    $(document).on('load', "#" + FrameID, function () {
        $("#" + FrameID).contents().find("a").attr("target", "_parent");
    });
}

function ReloadDocViewer(moduleInstanceID, retryCount) {
    var iframe = document.getElementById('doc-viewer-' + moduleInstanceID);

    //if document failed to load and retry count is less or equal to 3, reload document and check again
    if (iframe.contentWindow.frames.length == 0 && retryCount <= 3) {
        retryCount = retryCount + 1;

        //reload the iFrame
        document.getElementById('doc-viewer-' + moduleInstanceID).src += '';

        //Check if document loaded again in 7.5 seconds
        setTimeout(ReloadDocViewer, (1000 * retryCount), moduleInstanceID, retryCount);

    } else if (iframe.contentWindow.frames.length == 0 && retryCount > 3) {
        $('#doc-viewer-' + moduleInstanceID).css('background', '');
        iframe.src = "/Errors/ReloadPage.aspx";
        iframe.height = 200;
    }

    if (iframe.contentWindow.frames.length == 1) {
        $('#doc-viewer-' + moduleInstanceID).css('background', '');
    }
}
// END IFRAME FUNCTIONS


// SCROLL TOP

function ScrollTop() {
    $.scrollTo(0, { duration: 1000 });
}

// END SCROLL TOP

// ANALYTICS TRACKING

function AddAnalyticsEvent(category, action, label) {
    ga('BBTracker.send', 'event', category, action, label);
}

// END ANYLYTICS TRACKING


// BEGIN INCLUDE DOC READY SCRIPTS

function IncludeDocReadyScripts() {
    var arrScripts = [
		staticURL + '/GlobalAssets/Scripts/min/external-combined.min.js',
        staticURL + '/GlobalAssets/Scripts/Utilities_2560.js'

    ];

    var script = document.createElement('script');
    script.type = 'text/javascript';
    $.each(arrScripts, function () {script.src = this;$('head').append(script);;
        
    });

}
// END INCLUDE DOC READY SCRIPTS

//BEGIN ONSCREEN ALERT SCRIPTS
function OnScreenAlertDialogInit() {
    $("#onscreenalert-message-dialog").hide();
    $(".titleMessage").hide();
    $("#onscreenalert-ctrl-msglist").hide();
    $(".onscreenalert-ctrl-footer").hide();

    $(".icon-icon_alert_round_message").addClass("noBorder");
    $('.onscreenalert-ctrl-modal').addClass('MoveIt2 MoveIt1 Smaller');
    $('.onscreenalert-ctrl-modal').css({ "top": "88%" });
    $("#onscreenalert-message-dialog").show();
    $('#onscreenalert-message-dialog-icon').focus();
}

function OnScreenAlertGotItDialog(serverTime) {
    var alertsID = '';
    alertsID = GetCookie("Alerts");
    var arr = [];

    if (alertsID.length > 0) {
        arr = alertsID.split(',')
    }

    $("#onscreenalertdialoglist li").each(function () {
        arr.push($(this).attr('id'));
    });

    arr.sort();
    var newarr = $.unique(arr);

    SetUnescapedCookie("Alerts", newarr.join(','), 365);
    OnScreenAlertMaskShow(false);

    $('.onscreenalert-ctrl-modal').removeClass("notransition");
    $(".onscreenalert-ctrl-footer").slideUp(200);
    $("#onscreenalert-ctrl-msglist").slideUp(200);

    // check what browser/version we're on
    // if IE 9 or less, don't do CSS 3 transitions (not supported)
    var isIE = GetIEVersion();

    if (isIE == 0) {
        // not IE
        hideOnScreenAlertTransitions();
    } else {
        // IE
        if (isIE == 8 || isIE == 9) {
            hideOnScreenAlertNoTransitions();
        } else if (isIE >= 10) {
            hideOnScreenAlertTransitions();
        }
    }
}

function OnScreenAlertOpenDialog(SiteID) {
    $('#sw-important-message-tooltip').hide();
    var onscreenAlertCookie = GetCookie('Alerts');

    if (onscreenAlertCookie != '' && onscreenAlertCookie != undefined) {
        GetContent(homeURL + "/cms/Tools/OnScreenAlerts/UserControls/OnScreenAlertDialogListWrapper.aspx?OnScreenAlertCookie=" + onscreenAlertCookie + "&SiteID=" + SiteID, "onscreenalert-ctrl-cookielist", 2, "OnScreenAlertCookieSuccess();");
        $('.onscreenalert-ctrl-content').focus();
    }
}

function OnScreenAlertCookieSuccess() {
    OnScreenAlertMaskShow(true);

    $('.onscreenalert-ctrl-modal').removeClass("notransition");
    $('.onscreenalert-ctrl-modal').removeClass("Smaller");

    // check what browser/version we're on
    // if IE 9 or less, don't do CSS 3 transitions (not supported)
    var isIE = GetIEVersion();

    if (isIE == 0) {
        // not IE
        showOnScreenAlertTransitions();
    } else {
        // IE
        if (isIE == 8 || isIE == 9) {
            showOnScreenAlertNoTransitions();
        } else if (isIE >= 10) {
            showOnScreenAlertTransitions();
        }
    }

    $('.onscreenalert-ctrl-modal').addClass('MoveIt3');
    $('.onscreenalert-ctrl-modal').attr('style', '');

    // move div to top of viewport
    $('.onscreenalert-ctrl-modal').one('transitionend', function (e) {
        var top = $('.onscreenalert-ctrl-modal').offset().top;

        $('.onscreenalert-ctrl-modal').addClass("notransition").removeClass('MoveIt1').addClass('MoveIt4');

        $('.onscreenalert-ctrl-modal').removeClass('MoveIt2');
    });
}

function showOnScreenAlertTransitions() {
    $('.onscreenalert-ctrl-modal').one('transitionend', function (e) {
        $(".icon-icon_alert_round_message").removeClass("noBorder");
        $(".titleMessage").show();
        $("#onscreenalert-ctrl-msglist").slideDown(200);
        $(".onscreenalert-ctrl-footer").slideDown(200);
    });
}

function showOnScreenAlertNoTransitions() {
    $(".icon-icon_alert_round_message").removeClass("noBorder");
    $(".titleMessage").show();
    $("#onscreenalert-ctrl-msglist").slideDown(200);
    $(".onscreenalert-ctrl-footer").slideDown(200);
}

function hideOnScreenAlertTransitions() {
    $(".titleMessage").fadeOut(200, function () {
        $('.onscreenalert-ctrl-modal').addClass("notransition").removeClass('MoveIt4').addClass('MoveIt1');
        $('.onscreenalert-ctrl-modal').attr('style', '');
        $('.onscreenalert-ctrl-modal').addClass("Smaller");
        $(".icon-icon_alert_round_message").addClass("noBorder");

        $('.onscreenalert-ctrl-modal').one('transitionend', function (e) {
            $('.onscreenalert-ctrl-modal').removeClass("notransition");
            $('.onscreenalert-ctrl-modal').removeClass('MoveIt3').addClass('MoveIt2');
            $('.onscreenalert-ctrl-modal').css({
                "top": "88%"
            });
        });
    });
}

function hideOnScreenAlertNoTransitions() {
    $(".titleMessage").fadeOut(200, function () {
        $('.onscreenalert-ctrl-modal').addClass("notransition").removeClass('MoveIt4').addClass('MoveIt1');
        $('.onscreenalert-ctrl-modal').attr('style', '');
        $('.onscreenalert-ctrl-modal').addClass("Smaller");
        $(".icon-icon_alert_round_message").addClass("noBorder");

        $('.onscreenalert-ctrl-modal').removeClass("notransition");
        $('.onscreenalert-ctrl-modal').removeClass('MoveIt3').addClass('MoveIt2');
        $('.onscreenalert-ctrl-modal').css({
            "top": "88%"
        });
    });
}

function OnScreenAlertCheckListItem() {
    var ShowOkGotIt = $("#onscreenalertdialoglist-hid-showokgotit").val();
    var ShowStickyBar = $("#onscreenalertdialoglist-hid-showstickybar").val();

    if (ShowOkGotIt == "True" && ShowStickyBar == "False") {
        OnScreenAlertShowCtrls(true, true, false);
    } else if (ShowOkGotIt == "False" && ShowStickyBar == "True") {
        OnScreenAlertShowCtrls(true, false, true);
    } else {
        OnScreenAlertShowCtrls(false, false, false);
    }
}

function OnScreenAlertShowCtrls(boolOkGotIt, boolMask, boolStickyBar) {
    if (boolOkGotIt == false) {
        $("#onscreenalert-message-dialog").hide()
    }

    (boolMask == true) ? (OnScreenAlertMaskShow(true)) : (OnScreenAlertMaskShow(false));

    if (boolStickyBar == true) {
        OnScreenAlertDialogInit();
    }
    else {
        $('.onscreenalert-ctrl-content').focus();
    }
    
}

function OnScreenAlertMaskShow(boolShow) {
    if (boolShow == true) {
        $("#onscreenalert-ctrl-mask").css({ "position": "absolute", "background": "#000", "filter": "alpha(opacity=70)", "-moz-opacity": "0.7", "-khtml-opacity": "0.7", "opacity": "0.7", "z-index": "9991", "top": "0", "left": "0" });
        $("#onscreenalert-ctrl-mask").css({ "height": function () { return $(document).height(); } });
        $("#onscreenalert-ctrl-mask").css({ "width": function () { return $(document).width(); } });
        $("#onscreenalert-ctrl-mask").show();
        $('body').css('overflow', 'hidden');
    } else {
        $("#onscreenalert-ctrl-mask").hide();
        $('body').css('overflow', 'scroll');
    }
}
// END ONSCREEN ALERT SCRIPTS

// Encoding - obfuscation
function swrot13(s) {
    return s.replace(/[a-zA-Z]/g, function(c) {return String.fromCharCode((c<="Z"?90:122)>=(c=c.charCodeAt(0)+13)?c:c-26);})
}

// START SIDEBAR TOUR SCRIPTS
/* 
 *  Wrapping sidebar tour in a function 
 *  to be called when needed.
 */

var hasPasskeys = false;
var hasStudents = false;
var isParentLinkStudent = false;
var hasNotifications = false;

function startSidebarTour() {
    // define tour
    var tour = new Shepherd.Tour({
        defaults: {
            classes: 'shepherd-theme-default'
        }
    });

    if ($('#dashboard-sidebar-student0').length > 0) {
        hasStudents = true;
    }

    if ($('#dashboard-sidebar-profile').length > 0) {
        isParentLinkStudent = true;
    }

    if ($("#dashboard-sidebar-passkeys").get(0)) {
        hasPasskeys = true;
    }

    if ($("#dashboard-sidebar-notification").get(0)) {
        hasNotifications = true;
    }


    // define steps

    tour.addStep('Intro', {
        //title: '',
        text: '<div id="tour-lightbulb-icon"></div><p>Introducing your new personalized dashboard. Would you like to take a quick tour?</p>',
        attachTo: 'body',
        classes: 'dashboard-sidebar-tour-firststep shepherd-theme-default',
        when: {
            show: function () {
                AddAnalyticsEvent('Dashboard', 'Tour', 'View');
            }
        },
        buttons: [
          {
              text: 'Yes! Let\'s go.',
              action: function () {
                  AddAnalyticsEvent('Dashboard', 'Tour', 'Continue');
                  tour.next();
                  TourADAFocus();
              },
              events: {
                  'keydown': function (e) {
                      TourButtonPress(e, tour);
                  }
              }
          },
          {
              text: 'Maybe later.',
              classes: 'shepherd-button-sec',
              action: function () {
                  AddAnalyticsEvent('Dashboard', 'Tour', 'Hide For Now');
                  tour.show('Later');
                  TourADAFocus();
              },
              events: {
                  'keydown': function (e) {
                      TourButtonPress(e, tour, 'Later');
                  }
              }
          },
          {
              text: 'No thanks. I\'ll explore on my own.',
              classes: 'shepherd-button-suboption',
              action: function () {
                  AddAnalyticsEvent('Dashboard', 'Tour', 'Hide Forever');
                  tour.show('Never');
                  TourADAFocus();
              },
              events: {
                  'keydown': function (e) {
                      TourButtonPress(e, tour, 'Never');
                  }
              }
          }
        ]
    }).addStep('Avatar', {
        text: 'Click on your user avatar to access and update your personal information and subscriptions.',
        attachTo: '#dashboard-sidebar-avatar-container right',
        when: {
            show: function() {
                $('h3.shepherd-title').attr('role', 'none'); //ADA compliance
            }
        },
        classes: 'shepherd-theme-default shepherd-element-custom',
        buttons: [
          {
              text: 'Continue',
              action: function () {
                tour.next();
                TourADAFocus();

              },
              events: {
                  'keydown': function (e) {
                      if (hasPasskeys) {
                          TourButtonPress(e, tour);
                      } else {
                          TourButtonPress(e, tour, 'Finish');
                      }
                  }
              }
          }
        ]
    }).addStep('Stream', {
        text: 'Go to your stream to see updates from your district and schools.',
        attachTo: '#dashboard-sidebar-stream right',
        buttons: [
          {
              text: 'Continue',
              action: function () {
                  if (hasPasskeys) {
                      tour.next();
                      TourADAFocus();
                  } else {
                      tour.show('Finish');
                      TourADAFocus();
                  }
              },
              events: {
                  'keydown': function (e) {
                      if(hasPasskeys){
                          TourButtonPress(e, tour);
                      } else {
                          TourButtonPress(e, tour, 'Finish');
                      }
                  }
              }
          }
        ]
    });

    if (hasPasskeys) {
        tour.addStep('Passkeys', {
            text: 'Use your passkeys to log into other district applications or websites.',
            attachTo: '#dashboard-sidebar-passkeys right',
            buttons: [
              {
                  text: 'Continue',
                  action: function () {
                      if (hasNotifications) {
                          tour.show('Notifications');
                          TourADAFocus();
                      }
                      else if (hasStudents) {
                          tour.show('Students');
                          TourADAFocus();
                      }
                      else if (isParentLinkStudent) {
                          tour.show('ParentLinkStudent');
                          TourADAFocus();
                      }
                      else {
                          tour.show('Finish');
                          TourADAFocus();
                      }
                  },
                  events: {
                      'keydown': function (e) {
                          if (hasStudents) {
                              TourButtonPress(e, tour, 'Notifications');
                          }
                          else {
                              TourButtonPress(e, tour, 'Finish');
                          }
                      }
                  }
              }
            ]
        })
    }

    if (hasNotifications) {
        tour.addStep('Notifications', {
            text: 'Open your notifications to review messages.',
            attachTo: '#dashboard-sidebar-notification right',
            buttons: [
              {
                  text: 'Continue',
                  action: function () {
                      if (hasStudents) {
                          tour.show('Students');
                          TourADAFocus();
                      }
                      else if (isParentLinkStudent) {
                          tour.show('ParentLinkStudent');
                          TourADAFocus();
                      }
                      else {
                          tour.show('Finish');
                          TourADAFocus();
                      }
                  },
                  events: {
                      'keydown': function (e) {
                          if (hasStudents) {
                              TourButtonPress(e, tour, 'Students');
                          }
                          else {
                              TourButtonPress(e, tour, 'Finish');
                          }
                      }
                  }
              }
            ]
        })
    }

    if (hasStudents) {
        tour.addStep('Students', {
            text: 'Select a student to view his or her information and records.',
            attachTo: '#dashboard-sidebar-student0 right',
            buttons: [
              {
                  text: 'Continue',
                  action: function () {
                      tour.show('Finish');
                      TourADAFocus();
                  },
                  events: {
                      'keydown': function (e) {
                          TourButtonPress(e, tour, 'Finish');
                      }
                  }
              }
            ]
        })
    }

    if (isParentLinkStudent) {
        tour.addStep('ParentLinkStudent', {
            text: 'View your information or other helpful resources.',
            attachTo: '#dashboard-sidebar-profile right',
            buttons: [
              {
                  text: 'Continue',
                  action: function () {
                      tour.show('Finish');
                      TourADAFocus();
                  },
                  events: {
                      'keydown': function (e) {
                          TourButtonPress(e, tour, 'Finish');
                      }
                  }
              }
            ]
        })
    }

    tour.addStep('Later', {
        text: 'Ok, we\'ll remind you later. You can access the tour here any time you want.',
        attachTo: '#dashboard-sidebar-help right',
        classes: 'shepherd-theme-default shepherd-element-custom-bottom',
        buttons: [
          {
              text: 'Finish',
              action: function () {
                  SetCookie("DashboardSidebarTour", "true");
                  tour.cancel();
              },
              events: {
                  'keydown': function (e) {
                      e.stopImmediatePropagation();
                      if (e.keyCode == 13) {
                          SetCookie("DashboardSidebarTour", "true");
                          tour.cancel();
                      }
                  }
              }
          }
        ]

    }).addStep('Never', {
        text: 'Ok, we won\'t ask you again. If you change your mind, you can access the tour here any time you want.',
        attachTo: '#dashboard-sidebar-help right',
        classes: 'shepherd-theme-default shepherd-element-custom-bottom',
        buttons: [
          {
              text: 'Finish',
              action: function () {
                  SetCookie("DashboardSidebarTour", "true");
                  DenyTour();
              },
              events: {
                  'keydown': function (e) {
                      e.stopImmediatePropagation();
                      if (e.keyCode == 13) {
                          SetCookie("DashboardSidebarTour", "true");
                          DenyTour();
                      }
                  }
              }
          }
        ]

    }).addStep('Finish', {
        text: 'All finished! Go here any time to view this tour again.',
        attachTo: '#dashboard-sidebar-help right',
        classes: 'shepherd-theme-default shepherd-element-custom-bottom',
        buttons: [
          {
              text: 'Finish',
              action: function () {
                  SetCookie("DashboardSidebarTour", "true");
                  FinishTour();
              },
              events: {
                  'keydown': function (e) {
                      e.stopImmediatePropagation();
                      if (e.keyCode == 13) {
                          SetCookie("DashboardSidebarTour", "true");
                          FinishTour();
                      }
                  }
              }
          }
        ]
    });

    // start tour
    tour.start();
    TourADA();
}

function TourADA() {
    $(".shepherd-content").attr("tabindex", "0");
    $(".shepherd-button").each(function () {
        var $this = $(this);
        $this.attr("title", $this.text());
        $this.attr("tabindex", "0");
    });
}

function TourADAFocus() {
    TourADA();
    $(".shepherd-content").focus();
}

function TourButtonPress(e, tour, next) {
    e.stopImmediatePropagation();
    if (e.keyCode == 13) {
        if (next == undefined) {
            tour.next();
        } else {
            tour.show(next);
        }
        TourADAFocus();
    }
}

function FinishTour() {
    var data = "";
    var success = 'ActiveTourCancel();';
    var failure = 'CallControllerFailure(result[0].errormessage);';
    CallController(homeURL + "/GlobalUserControls/DashBoardSideBar/DashBoardSideBarController.aspx/FinishTour", data, success, failure);
}

function DenyTour() {
    var data = "";
    var success = 'ActiveTourCancel();';
    var failure = 'CallControllerFailure(result[0].errormessage);';
    CallController(homeURL + "/GlobalUserControls/DashBoardSideBar/DashBoardSideBarController.aspx/DenyTour", data, success, failure);
}

function ActiveTourCancel() {
    Shepherd.activeTour.cancel();
}
// END SIDEBAR TOUR SCRIPTS

// BEGIN DOCUMENT READY
$(document).ready(function () {

    if ($('#sw-sidebar').height() > 1500) {
        $('#sw-page').css('min-height', $('#sw-sidebar').height() + 'px');
        $('#sw-inner').css('min-height', $('#sw-sidebar').height() + 'px');
    } else {
        $('#sw-page').css('min-height', $(document).height() + 'px');
        $('#sw-inner').css('min-height', $(document).height() + 'px');
    }

    $('#sw-footer').show();

    // add focus class to textboxes for IE
    $(document).on('focus', 'input', function () {
        $(this).addClass('focus');
    });

    $(document).on('blur', 'input', function () {
        $(this).removeClass('focus');
    });

    // default ajax setup
    $.ajaxSetup({
        cache: false,
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //swalert(XMLHttpRequest.status + ' ' + textStatus + ': ' + errorThrown, 'Ajax Error', 'critical', 'ok');
            swalert("Something went wrong. We're sorry this happened. Please refresh your page and try again.", "Error", "critical", "ok");
        }
    });

    // make :Contains (case-insensitive version of :contains)
    jQuery.expr[':'].Contains = function (a, i, m) {
        return jQuery(a).text().toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
    };

    // HIDE / SHOW DETAILS IN LIST SCREEN
    $(document).on('click', 'span.ui-show-detail', function () {
        var $this = $(this);

        if ($this.hasClass('open')) {
            // do nothing
        } else {
            $this.addClass('open').parent('div.ui-article-header').nextAll('div.ui-article-detail').slideDown(function () {
                // add close button
                $this.append("<span class='ui-article-detail-close'></span>");
            });
        }
    });

    $(document).on('click', 'span.ui-article-detail-close', function () {
        $this = $(this);
        $this.parents('.ui-article-header').nextAll('.ui-article-detail').slideUp(function () {
            $this.parent('.ui-show-detail').removeClass('open');
            // remove close button
            $this.remove();
        });
    });


    // LIST/EXPANDED VIEW ON LIST PAGES
    $(document).on('click', '#show-list-view', function () {
        $(this).addClass('ui-btn-toolbar-primary').removeClass('ui-btn-toolbar');
        $('#show-expanded-view').addClass('ui-btn-toolbar').removeClass('ui-btn-toolbar-primary');
        $('div.ui-article-detail').slideUp(function () {
            // remove close buttons
            $(this).prevAll('.ui-article-header').find('.ui-article-detail-close').remove();
            //$this.children('.ui-article-detail-close').remove();
        });
        $('span.ui-show-detail').removeClass('open');
    });

    $(document).on('click', '#show-expanded-view', function (i) {
        $(this).addClass('ui-btn-toolbar-primary').removeClass('ui-btn-toolbar');
        $('#show-list-view').addClass('ui-btn-toolbar').removeClass('ui-btn-toolbar-primary');
        $('div.ui-article-detail').slideDown('', function () {
            // add close buttons
            $(this).prevAll('.ui-article-header').children('.ui-show-detail').append("<span class='ui-article-detail-close'></span>");
        });
        $('span.ui-show-detail').addClass('open');
    });

    //Important Message Ok, got it tab=0
    $('#onscreenalert-ctrl-gotit').keydown(function (e) {
        e.stopImmediatePropagation();
        if (e.keyCode == 13) {
            $(this).click();
        }
    });

    $('#onscreenalert-message-dialog-icon').keydown(function (e) {
        e.stopImmediatePropagation();
        if (e.keyCode == 13) {
            $(this).click();
        }
    });
    
}); // end document ready

// load scripts after everything else
$(window).on('load',  IncludeDocReadyScripts);
﻿/// <reference path="../../../../Scripts/Slick/slick.min.js" />
/// <reference path="../../../../Scripts/Slick/slick.min.js" />
/// <reference path="../../../../Scripts/Slick/slick.min.js" />
/// <reference path="../../../../Scripts/Slick/slick.min.js" />
// Check for included script
function CheckScript(ModuleName, ScriptSRC, FunctionName) {

    var loadScriptFile = true;

    switch (ModuleName.toLowerCase()) {
        case 'sectionrobot':
            FunctionName = 'CheckSectionRobotScript';
            ScriptSRC = homeURL + '/cms/Tools/SectionRobot/SectionRobot.js';
            break;
        case 'assignments':
            FunctionName = 'CheckAssignmentsScript';
            ScriptSRC = homeURL + '/cms/Module/Assignments/Assignments.js';
            break;
        case 'spacedirectory':
            FunctionName = 'CheckSpaceDirectoryScript';
            ScriptSRC = homeURL + '/cms/Module/SpaceDirectory/SpaceDirectory.js';
            break;
        case 'calendar':
            FunctionName = 'CheckCalendarScript';
            ScriptSRC = homeURL + '/cms/Module/Calendar/Calendar.js';
            break;
        case 'fullcalendar':
            FunctionName = '$.fn.fullCalendar';
            ScriptSRC = staticURL + '/GlobalAssets/Scripts/ThirdParty/jquery.fullcalendar1.6.1.js';
            break;
        case 'links':
            FunctionName = 'CheckLinksScript';
            ScriptSRC = staticURL + '/cms/Module/Links/Links.js';
            break;
        case 'minibase':
            FunctionName = 'CheckMinibaseScript';
            ScriptSRC = homeURL + '/cms/Module/Minibase/Minibase.js';
            break;
        case 'moduleinstance':
            var randNum = Math.floor(Math.random() * (1000 - 10 + 1) + 1000);
            FunctionName = 'CheckModuleInstanceScript';
            ScriptSRC = homeURL + '/cms/Module/ModuleInstance/ModuleInstance.js?rand=' + randNum;
            break;
        case 'photogallery':
            FunctionName = 'CheckPhotoGalleryScript';
            ScriptSRC = staticURL + '/cms/Module/PhotoGallery/PhotoGallery_2520.js';
            break;
        case 'comments':
            FunctionName = 'CheckModerateCommentsScript';
            ScriptSRC = homeURL + '/cms/Tools/ModerateComments/ModerateComments.js';
            break;
        case 'postings':
            FunctionName = 'CheckModeratePostingsScript';
            ScriptSRC = homeURL + '/cms/Tools/ModerateContribution/ModerateContribution.js';
            break;
        case 'myaccount':
            FunctionName = 'CheckMyAccountScript';
            ScriptSRC = homeURL + '/cms/UserControls/MyAccount/MyAccount.js';
            break;
        case 'formsurvey':
            FunctionName = 'CheckFormSurveyScript';
            ScriptSRC = homeURL + '/cms/tools/FormsAndSurveys/Surveys.js';
            break;
        case 'alerts':
            FunctionName = 'CheckAlertsScript';
            ScriptSRC = homeURL + '/cms/tools/Alerts/Alerts.js';
            break;
        case 'onscreenalerts':
            FunctionName = 'CheckOnScreenalertsScript';
            ScriptSRC = homeURL + '/cms/tools/OnScreenAlerts/OnScreenAlerts.js';
            break;
        case 'workspace':
            FunctionName = 'CheckWorkspaceScript';
            ScriptSRC = staticURL + '/cms/Workspace/PageList_2610.js';
            break;
        case 'moduleview':
            FunctionName = 'CheckModuleViewScript';
            ScriptSRC = staticURL + '/GlobalAssets/Scripts/min/ModuleViewRenderer_2460.js';
            break;
        case 'pageeditingmoduleview':
            FunctionName = 'CheckPageEditingModuleViewScript';
            //ScriptSRC = staticURL + '/GlobalAssets/Scripts/min/PageEditingModuleViewRenderer.js';
            ScriptSRC = homeURL + '/cms/UserControls/ModuleView/PageEditingModuleViewRenderer.js';
            break;
        case 'editarea':
            FunctionName = 'CheckEditAreaScript';
            ScriptSRC = homeURL + '/GlobalUserControls/EditArea/edit_area_full.js';
            break;
        case 'rating':
            FunctionName = '$.fn.rating';
            ScriptSRC = staticURL + '/GlobalAssets/Scripts/min/jquery.rating.min.js';
            break;
        case 'metadata':
            FunctionName = '$.fn.metadata';
            ScriptSRC = staticURL + '/GlobalAssets/Scripts/min/jquery.metadata.min.js';
            break;
        case 'pwcalendar':
            FunctionName = 'CheckPWCalendarScript';
            ScriptSRC = homeURL + '/myview/UserControls/Calendar/Calendar.js';
            break;
        case 'importwizard':
            FunctionName = 'CheckImportWizardScript';
            ScriptSRC = homeURL + '/cms/UserControls/ImportDialog/ImportWizard.js';
            break;
        case 'mustache':
            FunctionName = 'CheckMustacheScript';
            ScriptSRC = staticURL + '/GlobalAssets/Scripts/ThirdParty/mustache.js';
            break;
        case 'slick':
            FunctionName = 'CheckSlickScript';
            ScriptSRC = staticURL + '/GlobalAssets/Scripts/ThirdParty/Slick/slick.min.js';
            break;
        case 'galleria':
            FunctionName = 'CheckGalleriaScript';
            ScriptSRC = staticURL + '/GlobalAssets/Scripts/ThirdParty/galleria-custom-129_2520/galleria-1.2.9.min.js';
            break;
        case 'fine-uploader':
            FunctionName = 'CheckFineUploaderScript';
            ScriptSRC = staticURL + '/GlobalAssets/Scripts/ThirdParty/fine-uploader/fine-uploader.min.js';
            break;
        case 'tinymce':
            FunctionName = 'tinyMCE';
            ScriptSRC = homeURL + '/cms/Module/selectsurvey/ClientInclude/tinyMCE/jquery.tinymce.min.js';
            break;
        case 'attachmentview':
            FunctionName = 'CheckAttachmentScript';
            ScriptSRC = homeURL + '/GlobalUserControls/Attachment/AttachmentView.js';
            break;
        default:
            // module name not found
            if (ScriptSRC !== undefined && ScriptSRC !== null && ScriptSRC.length > 0) {
                // script src was specified in parameter
                if (FunctionName === undefined && ModuleName.length > 0) {
                    // default the function name for lookup from the module name
                    FunctionName = 'Check' + ModuleName + 'Script';
                }
            } else {
                // can't load a script file without at least a src and function name to test
                loadScriptFile = false;
            }
            break;
    }

    if (loadScriptFile === true) {
        try {
            if (eval("typeof " + FunctionName + " == 'function'")) {
                // do nothing, it's already included
            } else {
                var script = document.createElement('script');script.type = 'text/javascript';script.src = ScriptSRC;$('head').append(script);;
            }
        } catch (err) {
        }
    }
}
// End Check for included script
</script><!-- End swuc.CheckScript -->


    <!-- Server Load Time (04): 0.2044422 Seconds -->

    

    <!-- off-canvas menu enabled-->
    

    <!-- Ally Alternative Formats Configure START   -->
    
    <!-- Ally Alternative Formats Configure END     -->

</body>
</html>
